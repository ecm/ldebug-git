
%if 0

lDebug U command - Disassembler

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2012 C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif


	usesection lDEBUG_CODE

		; U command - disassemble.
uu:
	mov word [lastcmd], lastuu
	call iseol?
	jne uu1			; if an address was given

lastuu:
	_386_PM_o32	; mov ecx, dword [u_addr]
	mov cx, word [u_addr]
	_386_PM_o32	; mov edx, ecx
	mov dx, cx
	_386_PM_o32	; add ecx, byte 1Fh
	add cx, byte 1Fh
	jnc .no_overflow	; if no overflow -->
	_386_PM_o32	; or ecx, byte -1
	or cx, byte -1		; til end of segment
.no_overflow:
	jmp short uu2

uu1:
	mov cx, 20h		; default length
	mov bx, word [reg_cs]
	call getrangeX		; get address range into bx:(e)dx
	call chkeol		; expect end of line here
	mov word [u_addr+4], bx
	_386_PM_o32		; mov dword [u_addr], edx
	mov word [u_addr], dx

		; (d)word [u_addr] = offset start address
		; word [u_addr+4] = segment start address
		; (e)cx = end address
		; (e)dx = start adddress (same as [u_addr])
uu2:
	_386_PM_o32	; inc ecx
	inc cx
uu3:
	call handle_serial_flags_ctrl_c
	_386_PM_o32	; push ecx
	push cx
	_386_PM_o32	; push edx
	push dx
	and word [disflags], 0
	call disasm		; do it
	_386_PM_o32	; pop ebx
	pop bx
	_386_PM_o32	; pop ecx
	pop cx
	_386_PM_o32	; mov eax, dword [u_addr]
	mov ax, word [u_addr]
	_386_PM_o32	; mov edx, eax
	mov dx, ax
	_386_PM_o32	; sub eax, ecx
	sub ax, cx		; current position - end
	_386_PM_o32	; sub ebx, ecx
	sub bx, cx		; previous position - end
	_386_PM_o32	; cmp eax, ebx
	cmp ax, bx
	jnb uu3			; if we haven't reached the goal
	retn


	usesection lDEBUG_DATA_ENTRY

		; Jump table for OP_IMM, OP_RM, OP_M, OP_R_MOD, OP_MOFFS, OP_R, OP_R_ADD,
		; and OP_AX.

	align 2
disjmp2:
	dw dop_imm,	dop_rm,		dop_m,		dop_r_mod
	dw dop_moffs,	dop_r,		dop_r_add,	dop_ax
.end:

		; jump table for displaying operands
		; must be in the order of OP_M64 .. OP_FAR_REQUIRED
	align 2
optab:
	dw da_internal_error
	dw dop_m64,	dop_mfloat,	dop_mdouble,	dop_m80
	dw dop_mxx,	da_internal_error,dop_farimm,	dop_rel8
	dw dop_rel1632,	dop49,		dop_sti,	dop_cr
	dw dop_dr,	dop_tr,		dop_segreg,	dop_imms8
	dw dop_imm8,	dop_mmx,	dop_mmx_mod,	dop_shosiz
	dw dop_short,	dop_near,	dop_far,	dop_far_required
.string:
	db '1',0,'3',0,'DXCLSTCSDSESFSGSSS'	;simple strings
.end:


;	DISASM - Disassemble.

	align 2
dis_n:		dw 0		; number of bytes in instruction so far
		dw 0
preflags:	db 0		; flags for prefixes found so far (includes OSIZE,ASIZE)
preused:	db 0		; flags for prefixes used so far
%if _PM
presizeflags:	db 0		; O32,A32 flags = (OSIZE,ASIZE) XOR (32-bit cs?)
%else
presizeflags:	equ preflags	; O32,A32 flags are always equal to OSIZE,ASIZE
%endif

PRESEG	equ 1			; segment prefix
PREREP	equ 2			; rep prefixes
PREREPZ	equ 4			; F3h, not F2h
PRELOCK	equ 8			; lock prefix
PREOSIZE equ 10h		; flag for OSIZE prefix
PREASIZE equ 20h		; flag for ASIZE prefix
PRE32D	equ 10h			; flag for 32-bit data
PRE32A	equ 20h			; flag for 32-bit addressing
PREWAIT	equ 40h			; prefix wait (not really a prefix)
GOTREGM	equ 80h			; set if we have the reg/mem part

instru:	db 0			; the main instruction byte
disp8:	db 0
	align 2
index:	dw 0			; index of the instruction (unsqueezed)
	dw SFPGROUP3, SFPGROUP3+1, SFPGROUP3+4
	dw SPARSE_BASE+24h, SPARSE_BASE+26h ; obsolete-instruction values
rmsize:	db 0			; <0 or 0 or >0 means mod r/m is 8 or 16 or 32
segmnt:	db 0			; segment determined by prefix (or otherwise)
addrr:	dw 0			; address in mod r/m byte
savesp2:dw 0			; save the stack pointer here (used in disasm)

disflags:	dw 0		; flags for the disassembler

;--- equates for disflags:

DIS_F_REPT	equ 1		; repeat after pop ss, etc.
DIS_F_SHOW	equ 2		; show memory contents
DIS_I_SHOW	equ 4		; there are memory contents to show
DIS_I_UNUSED	equ 8		; (internal) print " (unused)"
DIS_I_SHOWSIZ	equ 10h		; (internal) always show the operand size
DIS_I_KNOWSIZ	equ 20h		; (internal) we know the operand size of instr.
DIS_I_MOV_SS	equ 40h		; (internal) note for repetition: mov ss
DIS_I_DONTSHOW	equ 80h		; do not show memory contents override
DIS_I_NOSIZ	equ 100h	; do not show size

disflags2:	db 0		; another copy of DIS_I_KNOWSIZ
disrepeatcount:	db 0

	align 2
sizeloc:	dw 0		; address of size words in output line
%if _COND
condmsg:	dw 0		; -> conditionals message to display (if R and no mem)
%endif

		; Jump table for a certain place.
		; The size of this table matches OPTYPES_BASE.
	align 2
disjmp:
	dw disbad		; illegal instruction
	dw da_twobyte		; two-byte instruction
	dw da_insgrp		; instruction group
	dw da_fpuins		; coprocessor instruction
	dw da_fpugrp		; coprocessor instruction group
	dw da_insprf		; instruction prefix (including 66h/67h)

		; Table for 16-bit mod r/m addressing.  8 = BX, 4 = BP, 2 = SI, 1 = DI.
rmtab:		db 8+2, 8+1, 4+2, 4+1, 2, 1, 4, 8

	align 2
		; Tables of register names.
		; rgnam816/rgnam16/segrgnam/xregnam must be consecutive.
rgnam816:	dw "AL","CL","DL","BL","AH","CH","DH","BH"	; 0..7
rgnam16:	dw "AX","CX","DX","BX","SP","BP","SI","DI"	; 8..15 (16-bit), 16..23 (32-bit)
N_REGS16	equ ($-rgnam16)/2
segrgnam:	dw "ES","CS","SS","DS","FS","GS"		; 24..29
xregnam:	dw "ST","MM","CR","DR","TR"			; 30..34
N_ALLREGS	equ $-rgnam816

REG_ST	equ 30
REG_MM	equ 31
REG_CR	equ 32
REG_DR	equ 33
REG_TR	equ 34

	align 2
segrgaddr:	dw reg_es,reg_cs,reg_ss,reg_ds,reg_fs,reg_gs

;	Tables for handling of named prefixes.

prefixlist:	db 26h,2Eh,36h,3Eh,64h,65h	; segment prefixes (in order)
		db 9Bh,0F0h,0F2h,0F3h		; WAIT,LOCK,REPNE,REPE
		db 066h,067h			; OSIZE, ASIZE
N_PREFIX:	equ $ - prefixlist

	align 2
prefixmnem:	dw MN_WAIT,MN_LOCK,MN_REPNE,MN_REPE
		dw MN_O32,MN_A32	; in 16-bit CS, OSIZE is O32 and ASIZE is A32
%if _PM
		dw MN_O16,MN_A16	; in 32-bit CS, OSIZE is O16 and ASIZE is A16
%endif

%if _COND
	align 2
cond_table:
	dw 800h			; OF
	dw 1			; CF
	dw 40h			; ZF
	dw 41h			; CF | ZF
	dw 80h			; SF
	dw 4			; PF
;	dw cond_L_GE	; handled specifically
;	dw cond_LE_G	; handled specifically
%endif


	usesection lDEBUG_CODE

disasm:
	mov byte [disrepeatcount], 0	; number of repeated disassembly lines
.preserve_disrepeatcount:
%if _COND
	and word [condmsg], 0		; initialize conditions message
%endif
.preserve_condmsg_and_disrepeatcount:
	mov word [savesp2], sp
	_386_PM_o32		; xor eax, eax
	xor ax, ax
	_386_PM_o32		; mov dword [dis_n], eax
	mov word [dis_n], ax
	mov byte [disp8], al
	mov word [preflags], ax		; clear preflags and preused
	mov	byte [segmnt], 3	; initially use DS segment
	mov	byte [rmsize], 80h	; don't display any memory
	mov	word [dismach], ax	; no special machine needed, so far
%if _PM
	mov byte [bCSAttr], al
	mov bx, word [u_addr+4]
	call testattrhigh
	jz .16
	mov byte [bCSAttr], 40h
.16:
%endif
	call disgetbyte			; get a byte of the instruction
	cmp	al, 9Bh			; wait instruction (must be the first prefix)
	jne	da2			; if not -->

;	The wait instruction is actually a separate instruction as far as
;	the x86 is concerned, but we treat it as a prefix since there are
;	some mnemonics that incorporate it.  But it has to be treated specially
;	since you can't do, e.g., seg cs wait ... but must do wait seg cs ...
;	instead.  We'll catch it later if the wait instruction is not going to
;	be part of a shared mnemonic.

	or byte [preflags], PREWAIT

;	If we've found a prefix, we return here for the actual instruction
;	(or another prefix).

da1:
	call disgetbyte
da2:
	mov	[instru],al	; save away the instruction
	mov	ah,0

;	Now we have the sequence number of the instruction in AX.  Look it up.

da3:
	mov	bx,ax
	mov	[index],ax	; save the compressed index
	cmp	ax,SPARSE_BASE
	jb	da4		; if it's not from the squeezed part of the table
	mov	bl,[sqztab+bx-SPARSE_BASE]
	mov	bh,0
	add	bx,SPARSE_BASE	; bx = compressed index

da4:
	mov	cl, [optypes+bx]; cx = opcode type
	mov	ch, 0
	shl	bx, 1
	mov	bx, [opinfo+bx]	; bx = other info (usually the mnemonic)
	mov	si, cx
	mov	ax, bx
	mov	cl, 12
	shr	ax, cl
	cmp	al, [dismach]
	jb	da5		; if a higher machine is already required
	mov	[dismach], al	; set machine type
da5:
	and	bh, 0Fh		; remove the machine field
	cmp	si, OPTYPES_BASE
	jae	da13_unp	; if this is an actual instruction
	call [disjmp+si]	; otherwise, do more specific processing
	jmp s	da3		; back for more

;	Two-byte instruction.

da_twobyte:
	call disgetbyte
	mov	[instru],al
	mov	ah,0
	add	ax,SPARSE_BASE
	ret

;	Instruction group.

da_insgrp:
	call getregmem_r	; get the middle 3 bits of the R/M byte
	cbw
	add	ax,bx		; offset
	ret

;	Coprocessor instruction.

da_fpuins:
	or	byte [disflags],DIS_I_SHOWSIZ
	or	byte [dmflags],DM_COPR
	call getregmem
	cmp	al,0c0h
	jb	da_insgrp	;range 00-bfh is same as an instruction group
	mov	cl,3
	shr	al,cl		;C0h --> 18h
	sub	al,18h-8	;18h --> 8
	cbw
	add	ax,bx		;offset
	ret

;	Coprocessor instruction group.

da_fpugrp:
	mov	al,[regmem]
	and	al,7
	cbw
	add	ax,bx
	ret

;	Instruction prefix.  At this point, bl = prefix bits; bh = segment

da_insprf:
	test bl,[preflags]
	jnz	da12		; if there are duplicates
	or [preflags],bl
	test bl,PRESEG
	jz	da11		; if not a segment
	mov	[segmnt],bh	; save the segment
da11:
	pop	ax		; discard return address
	jmp	da1

da12:
	jmp	disbad		; we don't allow duplicate prefixes

	; si = index into table opindex, + OPTYPES_BASE
da13_unp:
	xor ax, ax
	mov al, [si-OPTYPES_BASE+opindex]	; ax = adjustment (from opindex)
	add si, ax		; add in the adjustment
				; (the existing adjustment by OPTYPES_BASE is unaffected)

;	OK.  Here we go.  This is an actual instruction.
;	bx = offset of mnemonic in mnlist
;	si = offset of operand list in oplists
;	First print the op mnemonic.

da13:
%if _PM
	mov al, byte [preflags]
;	and al, PREOSIZE | PREASIZE	; get OSIZE,ASIZE status (= O32,A32 in 16-bit cs)
_386	test byte [bCSAttr], 40h	; in a 32-bit segment?
_386	jz .16				; no -->
_386	xor al, PRE32D | PRE32A		; OSIZE,ASIZE present means O16,A16
.16:
	mov byte [presizeflags], al	; set O32,A32 status
%endif
	push si
	lea	si,[mnlist+bx]	; offset of mnemonic
	cmp	si,mnlist+MN_BSWAP
	jne	da13a		; if not BSWAP
	call dischk32d
	jz	da12		; if no operand-size prefix -->
da13a:
	call showop		; print out the op code (at line_out+28)
	mov word [sizeloc], 0	; clear out this flag
	pop	si		; recover list of operands
	add	si,oplists-OPTYPES_BASE
	cmp	byte [si], OP_END
	je	da21_e		; if we're done

;	Loop over operands.  si-> next operand type.
;	Fortunately the operands appear in the instruction in the same
;	order as they appear in the disassembly output.

da14:
	mov byte [disflags2], 0	; clear out size-related flags
	lodsb			; get the operand type
	cmp	al,OP_SIZE
	jb	da18		; if it's not size dependent
	mov	byte [disflags2],DIS_I_KNOWSIZ	;indicate variable size
	cmp	al,OP_1632_DEFAULT
	jae	da15_default
	cmp	al,OP_8
	jae	da16		; if the size is fixed (8, 16, 32, 64)
	cmp	al,OP_1632
	jae	da15		; if word or dword
		; OP_ALL here. This has a width of 2.
		;  If the low bit is clear, this means
		;  8 bit, else 16/32 bits.
	mov	ah,-1
	test byte [instru],1
	jz	da17		; if byte -->
	jmp da15
da15_default:
	test byte [preflags], PREOSIZE
	jnz da15
	setopt [disflags], DIS_I_NOSIZ
da15:
	or byte [preused],PRE32D; mark this flag as used
	mov	ah,[presizeflags]
	and	ah,PRE32D	;this will be 10h for dword, 00h for word
	jmp s	da17		;done

da16:
	mov	ah,al		;OP_8, OP_16, OP_32 or OP_64 (we know which)
	and	ah,0f0h		;this converts ah to <0 for byte, =0 for word,
	sub	ah,OP_16	;and >0 for dword (byte=F0h, word=00h, dword=10h, qword=20h)

;	Now we know the size (in ah); branch off to do the operand itself.

da17:
	mov bl, al
	and bx, 0Fh		; 8 entries (IMM, RM, M, R_MOD, M_OFFS, R, R_ADD, AX)
	shl bx, 1
	cmp bx, disjmp2.end - disjmp2
	jae @F
	call [disjmp2 + bx]	; print out the operand
	jmp short da20		; done with operand

@@:
da_internal_error:
	mov dx, msg.uu_internal_error
	call putsz
	jmp cmd3


;	Sizeless operands.

da18:
	cbw
	xchg ax, bx		; bx = index
	shl bx, 1
	mov ax, [optab + bx]
	cmp bx, optab.string - optab
	jb	da19		; if it's not a string
	cmp bx, optab.end - optab
	jae @B
	call	dis_stosw_lowercase
	test	ah, ah
	jnz	da20		; if it's two characters
	dec	di
	jmp s	da20		; done with operand

da19:
	call ax			; otherwise, do something else

		; Operand done, check if there's another one.
da20:
	cmp	byte [si], OP_END
da21_e:
	je	da21		;if we're done
	mov	al,','
	stosb
	testopt [asm_options], disasm_commablank
	jz .nospace
	mov al, 32
	stosb
.nospace:
	jmp	da14		;another operand

		; All operands done. Now check and loop for unused prefixes:
		; OPSIZE (66h), ADDRSIZE (67h), WAIT, segment, REP, LOCK
da21:
	mov	al, [preused]	; = flags that are used
	not	al		; = flags that are not used
	and	al, [preflags]	; = flags that are not used but present
	jz	da28		; if all present flags were used -->
	mov	cx, N_WTAB
	mov	bx, wtab1
	mov	dx, 2*N_WTAB-2
	mov	ah, PREWAIT
	test	al, ah
	jnz	da23		; if there's a WAIT prefix hanging -->

	mov	cx, N_LTABO
	mov	bx, ltabo1
	mov	dx, 2*N_LTABO-2
	mov	ah, PREOSIZE
	test	al, ah
	jnz	da23		; if it's a 66h prefix -->

	mov	ah, PREASIZE
	test	al, ah
	jz	da24		; if it isn't a 67h prefix -->

		; check whether A32 applies to an implicit operand
	push	di
	push	ax
	mov	ax, [index]
	test	ah, ah
	jnz	.nota32prfx_nz	; opcode index > FF, not in this list -->
	mov	di, a32prfxtab
	scasb			; xlatb ?
	je	@F		; yes --> (ZR)
	and	al, ~1		; clear the low bit (MOVSW -> MOVSB)
	mov	cx, A32P_LEN - 1
	repne	scasb		; scan table (low bit cleared)
@@:
.nota32prfx_nz:
	pop	ax
	pop	di
	jne	.nota32prfx	; not in the list -->

	or	[preused], ah	; mark it as used
	mov	cl, 4		; (ch = 0 because A32P_LEN < 256)
	call	moveover	; make space for "A32 "
	mov	ax, "A3"
	call	dis_lowercase	; al = "a" if lowercase option specified
	mov	word [line_out+MNEMONOFS], ax
	mov	word [line_out+MNEMONOFS+2], "2 "
da21_j0: equ $
	jmp s	da21

.nota32prfx:
		; now check whether it instead modifies the opcode
	mov	cx, N_LTABA
	mov	bx, ltaba1
	mov	dx, 2*N_LTABA-2

da23:
	or	[preused], ah	; mark this (OSIZE, ASIZE or WAIT) prefix as used
	push	di
	mov	di, bx
	mov	bl, ah
	mov	ax, [index]
	repne	scasw
	jne	disbad2		; if not found in the list -->
	add	di, dx		; replace the mnemonic with the 32-bit name
	mov	si, [di]
	add	si, mnlist
	call	showop		; copy instruction mnemonic
	pop	di
da21_j1:
	jmp s	da21_j0

disbad2:
	jmp	disbad

da24:
	test	al, PRESEG
	jz	da25		; if not because of a segment prefix -->
	mov	ax, [index]
	test	ah, ah
	jnz	disbad2		; if index > 256, it's none of these -->
	push	di
	mov	cx, SEGP_LEN
	mov	di, segprfxtab
	repne	scasb
	pop	di
	jne	disbad2		; if it's not on the list -->
	mov	cx, 3
	call	moveover
	push	di
	mov	di, line_out+MNEMONOFS
	call	showseg		; show segment register
	mov	al, ':'
	testopt [asm_options], disasm_nasm
	jz	.notnasm
	mov	al, 32
.notnasm:
	stosb
	pop	di
	or	byte [preused], PRESEG	; mark it as used
da21_j2:
	jmp s	da21_j1

da25:
	test	al, PREREP
	jz	da26		; if not a REP prefix
	and	al, PREREP|PREREPZ
	or	[preused], al
	mov	ax, [index]
	test	ah, ah
	jnz	disbad3		; if not in the first 256 bytes
	and	al, ~1		; clear the low bit (MOVSW -> MOVSB)
	push	di
	mov	di, replist
	mov	cx, REP_SAME_LEN; scan those for REP first
	repne	scasb
	mov	si, mnlist+MN_REP
	je	da27		; if one of the REP instructions -->
	mov	cl, REP_DIFF_LEN; (ch = 0)
	repne	scasb
	jne	disbad3		; if not one of the REPE/REPNE instructions
	mov	si, mnlist+MN_REPE
	test	byte [preused], PREREPZ
	jnz	da27		; if REPE
	mov	si, mnlist+MN_REPNE
	jmp s	da27		; it's REPNE

disbad3:
	jmp	disbad

da26:
	test	al, PRELOCK
	jz	disbad3		; if not a lock prefix, either -->
	push	di
	mov	ax, [index]
	mov	di, locktab
	mov	cx, N_LOCK
	repne	scasw
	jne	disbad3		; if not in the approved list -->
	test	byte [preused], PRESEG
	jz	disbad3		; if memory was not accessed -->
	mov	si, mnlist+MN_LOCK
	or	byte [preused], PRELOCK

;	Slip in another mnemonic (REP or LOCK).
;	SI = offset of mnemonic, what should be
;	DI is on the stack.

da27:
	pop	di
	mov	cx, 8
	push	si
	call	moveover
	pop	si
	push	di
	call	showop
	pop	di
	jmp s	da21_j2

;	Done with instruction.  Erase the size indicator, if appropriate.

da28:
	mov	cx, [sizeloc]
	jcxz	da28b		;if there was no size given
	mov	al,[disflags]
	test al,DIS_I_SHOWSIZ
	jnz	da28b		;if we need to show the size
	test al,DIS_I_KNOWSIZ
	jz	da28b		;if the size is not known already
	xchg cx,di
	mov	si,di		;save old di
	mov	al, 32
da28a:
	scasb			;skip size name
	jne	da28a		;if not done yet
					;(The above is the same as repne scasb, but
					;has no effect on cx.)
	testopt [asm_options], disasm_nasm
	jnz .nasm
	add	di, byte 4	;skip 'PTR '
.nasm:
	xchg si,di
	sub	cx,si
	rep	movsb		;move the line

;	Now we're really done.  Print out the bytes on the left.

da28b:
	push di		;print start of disassembly line
	mov	di,line_out
	mov	ax,[u_addr+4]	;print address
	call hexword
	mov	al,':'
	stosb
	_386_PM_o32	; mov eax, dword [u_addr]
	mov ax, word [u_addr]
%if _PM
	test byte [bCSAttr], 40h
	jz .16
	call hexword_high
.16:
%endif
	call hexword
	mov	al, 32
	stosb
	mov	bx, [dis_n]
da28c:
	mov si,line_out+MNEMONOFS - 1
	sub si, di
	shr si, 1
	cmp bx, si
	jbe da29		; if it's a short instruction which fits in one line
	sub bx, si
	push bx
	mov bx, si
	push di
	call disshowbytes
	call putsline_crlf
	pop cx
	pop bx
	mov di, line_out
	sub cx, di
	mov al, 32
	rep stosb
	jmp short da28c
da29:
	call disshowbytes
da30:
	mov	al, 32		; pad to op code
	mov	cx,line_out+MNEMONOFS
	sub	cx,di
	jc  da30_1
	rep	stosb
da30_1:
	pop	di
	test byte [disflags],DIS_I_UNUSED
	jz	da32		; if we don't print ` (unused)'
	mov	si,unused
	cmp	byte [di-1], 32
	jne	da31		; if there's already a space here
	inc	si
da31:
	call showstring

;	Print info on minimal processor needed.

da32:
	push di
	mov	di,index+2
	call showmach	;show the machine type, if needed
	pop	di
	jcxz da32f		;if no message

;	Print a message on the far right.

da32_tabto:
	mov	ax, line_out+79
	sub	ax, cx
	push	cx
	call	tab_to		; tab out to the location
	pop	cx
	rep	movsb		; copy the string
da32z_j1:
	jmp	da32z		; done

%if _COND
		; Try dumping a condition status.
da32_cond:
 %if _COND_RDUMP_ONLY
	test al, DIS_F_SHOW	; (! DIS_F_SHOW|DIS_I_SHOW is negated here)
	jnz da32z_j1		; not showing conditionals message -->
 %endif
	mov si, word [condmsg]
	test si, si		; stored a message here ?
	jz da32z_j1		; no -->
	push di
	mov di, si
	mov cx, -1
	xor ax, ax
	repne scasb
	neg cx
	dec cx
	dec cx			; get string length
	pop di
	jmp short da32_tabto
%else
da32_cond: equ da32z_j1
%endif

;	Dump referenced memory location.

da32f:
	mov	al,[disflags]
	xor	al, DIS_F_SHOW | DIS_I_SHOW
	test	al, DIS_F_SHOW | DIS_I_SHOW | DIS_I_DONTSHOW
		; (NZ if either _SHOW is clear, or _DONTSHOW is set)
	jnz	da32_cond	; if there is no memory location to show -->
_no386	cmp byte [segmnt], 3
_no386	ja da32_cond		; if FS or GS on non-386 --> (invalid)
	cmp byte [segmnt], 5
	ja da32_cond		; if invalid segment -->
	mov	ax,line_out+79-10
	cmp	byte [rmsize],0
	jl	da32h		; if byte
	jz	da32g		; if word
	sub	ax, byte 4
da32g:
	dec	ax
	dec	ax
da32h:
	call tab_to
	call showseg_uppercase_ax
				; ax = segment register name
	call dis_lowercase_refmem_w
	stosw
	mov	al,':'
	stosb
	mov	ax,[addrr]
	call hexword		; show offset
	mov	al,'='
	stosb
	mov al, [segmnt]	; segment number
	cbw
	shl ax, 1
	xchg ax, bx		; mov bx, ax
	mov bx, [segrgaddr + bx]; get address of value
	push es
	mov es, [bx]
	mov bx, [addrr]
	mov al, [es:bx]		; avoid a "mov ax,[-1]"
	cmp byte [rmsize], 0
	jl .displaybyte		; if byte
	mov ah, [es:bx + 1]
	jz .displayword		; if word
	mov dl, [es:bx + 2]	; avoid a "mov dx,[-1]"
	mov dh, [es:bx + 3]
.displaydword:
	pop es
	xchg ax, dx
	call hexword
	xchg ax, dx
	db __TEST_IMM8		; (skip pop)
.displayword:
	pop es
	call hexword
	jmp short .displayed	; done
.displaybyte:
	pop es
	call hexbyte		; display byte
.displayed:

da32z:
	call	trimputs	; done with operand list

da_repeat:
	mov	al, [disflags]
	test	al, DIS_F_REPT
	jz	@FF		; if not repeating -->
	test	al, DIS_I_UNUSED
	jnz	@F		; if " (unused)" was displayed -->
	test	al, DIS_I_MOV_SS
	mov	ax, [index]
	jz	.not_mov_to_ss	; not mov to ss -->

		; DIS_I_MOV_SS is set, check for wo[index] == 8Eh;
		;  as we only want to match move *to* ss, not from (8Ch).
	cmp	ax, 8Eh		; move to seg reg?
	je	@F		; yes, it is mov to ss -->

.not_mov_to_ss:
	test ah, ah
	jnz .not_single_byte_opcode

	cmp	al, 17h
	je	@F		; pop ss -->
	cmp	al, 0E6h	; out ?
	je	@F
	cmp	al, 0EEh
	je	@F
	cmp	al, 06Eh
	je	@F
	cmp	al, 06Fh
	je	@F		; -->
	cmp	al, 0E4h	; in ?
	je	@F
	cmp	al, 0ECh
	je	@F
	cmp	al, 06Ch
	je	@F
	cmp	al, 06Dh
	je	@F		; -->
	cmp	al, 0F4h	; hlt ?
	je	@F
	cmp	al, 0FBh
	jne	@FFF		; not sti -->
@@:
	and	word [disflags], DIS_F_REPT|DIS_F_SHOW
	inc	byte [disrepeatcount]
	cmp	byte [disrepeatcount], 16
	jb	disasm.preserve_disrepeatcount

	mov dx, msg.uu_too_many_repeat
	call putsz
@@:
	retn

@@:
.not_single_byte_opcode:

	mov	bh, byte [disp8]
	cmp	ax, 0EBh	; unconditional short jump ?
	je	@BB		; yes, return -->
	_386_PM_o32	; and dword [dis_n], byte 0
	and	word [dis_n], byte 0
	call	disgetbyte
	mov	bl, 2		; displacement to skip a jmp short
	cmp	al, 0EBh	; jmp short ?
	je	@F		; yes -->
	mov	bl, 3		; displacement to skip a 16-bit jmp near
	cmp	al, 0E9h	; jmp near ?
	jne	@BB		; no, return -->
%if _PM
	test	byte [bCSAttr], 40h	; 32-bit code segment ?
	jz	@F		; no, 16-bit, use displacement 3 -->
	mov	bl, 5		; displacement to skip a 32-bit jmp near
%endif
@@:
	cmp	bh, bl		; right displacement ?
	jne	@BBB		; no -->

	and	word [disflags], DIS_F_REPT|DIS_F_SHOW
	xor	word [condmsg], \
	    (msg.condnotjump + DATASECTIONFIXUP) ^ (msg.condjump + DATASECTIONFIXUP)
	jmp	disasm.preserve_condmsg_and_disrepeatcount


;	Here are the routines for printing out the operands themselves.
;	Immediate data (OP_IMM)

dop_imm:
	cmp	ah, 0
	jl	dop03		; if just a byte -->
	pushf
	test	byte [disflags], DIS_I_SHOWSIZ
	jz	.nosize		; if we don't need to show the size -->
	call	showsize
.nosize:
	call	disgetword
	popf
	je	hexword		; if just a word
	jmp	disp32.ax

dop03:
	call	disgetbyte	; print immediate byte
	jmp	hexbyte


;	Memory offset reference (OP_MOFFS)

dop_moffs:
	mov	al, 5
	test	byte [presizeflags], PRE32A
	jnz	.32		; if 32-bit addressing -->
	inc	ax
.32:
	mov	[regmem], al
	jmp s	dop05


;	MOD R/M (OP_RM)

dop_rm:
	call getregmem
	cmp	al,0c0h
	jae	dop33		; if pure register reference -->

dop05:			; <--- used by OP_M, OP_M64, OP_M80, OP_MOFFS
	testopt [disflags], DIS_I_NOSIZ
	jnz @F
	call showsize		; print out size
	call showptr		; append "PTR " (if not NASM syntax)
@@:
dop06:			; <--- used by OP_MXX, OP_MFLOAT, OP_MDOUBLE
	or	byte [preused],PRESEG	; needed even if there's no segment override
					; because handling of LOCK prefix relies on it
	mov	al, '['
	call	stosb_nasm

	test byte [preflags],PRESEG
	jz	dop07			;if no segment override
	call showseg		;print segment name
	mov	al,':'
	stosb
dop07:
	mov	al,[regmem]
	and	al,0c7h
	or	byte [preused],PREASIZE
	test	byte [presizeflags],PRE32A
	jnz	dop18		;if 32-bit addressing
	or	byte [disflags],DIS_I_SHOW	;we'd like to show this address
	mov	word [addrr],0	;zero out the address initially
	xchg	ax,bx		;mov bx,ax
	call	store_opensqubracket
	cmp	bl,6
	je	dop16		;if [xxxx]
	and	bx,7
	mov	bl,[rmtab+bx]
	test	bl,8
	jnz	dop09		;if BX
	test	bl,4
	jz	dop11		;if not BP
	mov	ax,'BP'
	mov	cx,[reg_ebp]
	test byte [preflags],PRESEG
	jnz	dop10		;if segment override
	dec	byte [segmnt]	;default is now SS
	jmp s	dop10

dop09:
	mov	ax,'BX'		;BX
	mov	cx,[reg_ebx]

dop10:
	mov	[addrr],cx	;print it out, etc.
	call	dis_stosw_lowercase
	test	bl,2+1
	jz	dop13		;if done
	mov	al,'+'
	stosb
dop11:
	mov	ax,'SI'
	mov	cx,[reg_esi]
	test	bl,1
	jz	dop12		;if SI
	mov	al,'D'		;DI
	mov	cx,[reg_edi]

dop12:
	add	[addrr], cx	; print it out, etc.
	call	dis_stosw_lowercase
dop13:
	test	byte [regmem], 0C0h
	jz s	dop17		; if no displacement -->
	test	byte [regmem], 80h
	jnz	dop15		; if word displacement -->
	call	disgetbyte
	cbw
	add	[addrr], ax
	cmp	al, 0
	mov	ah, '+'
	jge	dop14		; if not negative -->
	mov	ah, '-'
	neg	al
dop14:
	mov	[di], ah
	inc	di
	call	hexbyte		; print the byte displacement
	jmp s	dop17		; done -->

dop15:
	mov	al, '+'
	stosb
dop16:
	call	disgetword
	add	[addrr], ax
	call	hexword		; print word displacement

dop17:
	mov	al, ']'
	stosb
	retn

;	32-bit MOD REG R/M addressing.

dop18:
	cmp	al, 5
	je s	dop19		; if just a disp32 address -->
	push	ax
	and	al, 7
	cmp	al, 4
	jne	dop20		; if no SIB -->
	call	disgetbyte	; get and save it
	mov	[sibbyte], al
dop20:
	pop	ax
	test	al, 80h
	jnz	dop22		; if disp32 -->
	test	al, 40h
	jz	dop23		; if no disp8 -->
	call	disgetbyte
	cmp	al, 0
	jge	dop21		; if not negative -->
	neg	al
	mov	byte [di], '-'
	inc	di
dop21:
	call	hexbyte
	jmp s	dop22a		; done -->

dop22:
	call	disp32		; print disp32

dop22a:
	call	store_plus

dop23:
	mov	al,[regmem]
	and	al,7
	cmp	al,4
	jne	dop28		;if no SIB
	mov	al,[sibbyte]
%if 1               ;bugfix: make 'u' correctly handle [ESP],[ESP+x]
	cmp al,24h
	jnz noesp
	mov al,4
	jmp short dop28
noesp:
%endif
	and	al,7
	cmp	al,5
	jne	dop24		; if not [EBP] -->
	test	byte [regmem], 0C0h
	jnz	dop24		; if MOD != 0 -->
	call	disp32		; show 32-bit displacement instead of [EBP]
	jmp s	dop25

dop19:
	call	store_opensqubracket
	call	disp32		; display 32-bit offset
dop17_j1:
	jmp s	dop17

dop24:
	call store_opensqubracket_e
	call showreg16		; show 16-bit register name (number in AL)
	mov	al, ']'
	call	stosb_notnasm
dop25:
	call	store_plus

	mov	al,[sibbyte]
	shr	al,1
	shr	al,1
	shr	al,1
	and	al,7
	cmp	al,4
	je	disbad1		;if illegal
	call store_opensqubracket_e
	call showreg16
	mov	ah,[sibbyte]
	test ah,0c0h
	jz	dop27		;if SS = 0
	mov	al,'*'
	stosb
	mov	al,'2'
	test ah,80h
	jz	dop26		;if *2
	mov	al,'4'
	test ah,40h
	jz	dop26		;if *4
	mov	al,'8'
dop26:
	stosb
dop27:
dop17_j2:
	jmp s dop17_j1

;	32-bit addressing without SIB

dop28:
	call store_opensqubracket_e
	call showreg16
	jmp short dop27

		; Store '[' if not NASM syntax,
		;  then (regardless of syntax) store 'E'
		; INP:	di-> buffer
		; OUT:	di-> behind "[E" or 'E'
		; CHG:	-
		;
		; The 'E' is lowercased if that option is selected.
store_opensqubracket_e:
	push ax
	call store_opensqubracket
	mov al, 'E'
	call dis_lowercase
	stosb
	pop ax
	retn

		; Store '[' if not NASM syntax
		; INP:	di-> buffer
		; OUT:	di-> behind '[' if not NASM syntax
		; CHG:	al
store_opensqubracket:
	mov	al, '['

		; Store al if not NASM syntax
		; INP:	di-> buffer
		; OUT:	di-> behind stored byte if not NASM syntax
		; CHG:	-
stosb_notnasm:
	testopt [asm_options], disasm_nasm
	jnz	.ret
	stosb
.ret:	retn

		; Store '+' if NASM syntax
		; INP:	di-> buffer
		; OUT:	di-> behind '+' if NASM syntax
		; CHG:	al
store_plus:
	mov	al, '+'

		; Store al if NASM syntax
		; INP:	di-> buffer
		; OUT:	di-> behind stored byte if NASM syntax
		; CHG:	-
stosb_nasm:
	testopt [asm_options], disasm_nasm
	jz	.ret
	stosb
.ret:	retn


;	Memory-only reference (OP_M)

dop_m:
	call	getregmem
	cmp	al, 0C0h
	jb	dop05		; if it's what we expect -->

		; it's a register reference
disbad1:jmp	disbad		; this is not supposed to happen -->

;	Register reference from MOD R/M part (OP_R_MOD)

dop_r_mod:
	call getregmem
	cmp	al,0c0h
	jb	disbad1		;if it's a memory reference
	jmp s	dop33

;	Pure register reference (OP_R)

dop_r:
	call getregmem_r

dop33:				; <--- used by OP_RM, OP_R_MOD and OP_R_ADD
	and	al,7			;entry point for regs from MOD R/M, and others
	mov	cl,[disflags2]
	or	[disflags],cl	;if it was variable size operand, the size
						;should now be marked as known.
	cmp	ah,0
	jl	dop35			;if byte register
	jz	dop34			;if word register
dop33a:
	cmp ah, 20h		; qword register (MMX) ?
	je dop35_1		; -->
	push ax
	mov al, 'E'
	call dis_lowercase
	stosb
	pop ax
	;mov	byte [di],'E'	;enter here from OP_ECX
	;inc	di
dop34:
	add	al,8
dop35:
	cbw
	shl	ax,1
	xchg ax,bx			;mov bx,ax
	mov	ax,[rgnam816+bx];get the register name
	jmp	dis_stosw_lowercase

dop35_1:
	push ax
	mov ax, "MM"
	call dis_stosw_lowercase
	pop ax
	add al, '0'
	stosb
	retn

;	Register number embedded in the instruction (OP_R_ADD)

dop_r_add:
	mov	al,[instru]
	jmp s	dop33

;	AL or AX or EAX (OP_AX)

dop_ax:
	mov	al,0
	jmp s	dop33

		; QWORD mem (OP_M64)
		; This operand type is used by CMPXCHG8B, FILD and FISTP.
dop_m64:
	;mov ax, 'Q'		; print "QWORD"
	mov ah, 20h		; size QWORD
	jmp s dop40

		; FLOAT (=REAL4) mem (OP_MFLOAT)
dop_mfloat:
	mov	ax, "FL"
	call	dis_stosw_lowercase
	mov	ax, "OA"
	call	dis_stosw_lowercase
	mov	ax, "T "
	jmp short dop38c

;	DOUBLE (=REAL8) mem (OP_MDOUBLE).

dop_mdouble:
	mov	ax, "DO"
	call	dis_stosw_lowercase
	mov	ax, "UB"
	call	dis_stosw_lowercase
	mov	al, 'L'
	call	dis_lowercase
	stosb
	mov	ax, "E "
 dop38c:
	call	dis_stosw_lowercase
	call	showptr
	jmp s	dop42a

;	TBYTE (=REAL10) mem (OP_M80).

dop_m80:
	mov	ax,0FF00h+'T'	;print 'T' + "BYTE"
	call dis_lowercase
	stosb
dop40:
	call getregmem
	cmp	al,0c0h
	jae	disbad5		; if it's a register reference
	or	byte [disflags], DIS_I_DONTSHOW
				; don't show this
	jmp	dop05

%if 0
	; Far memory (OP_FARMEM).
	; This is either a FAR16 (DWORD) or FAR32 (FWORD) pointer.
dop_farmem:
	call	dischk32d
	jz	dop41a		; if not dword far
	call	showdword
dop41a:
	mov	ax, "FA"	; store "FAR "
	call	dis_stosw_lowercase
	mov	ax, "R "
	call	dis_stosw_lowercase
%endif

;	mem (OP_MXX).

dop_mxx:
	or	byte [disflags], DIS_I_DONTSHOW
				; don't show this
dop42a:
	call	getregmem
	cmp	al,0c0h
	jb	dop06		; mem ref, don't show size -->
disbad5:
	jmp	disbad

	; Far immediate (OP_FARP). Either FAR16 or FAR32.
dop_farimm:
	call disgetword
	push ax
	call dischk32d
	jz	dop44		; if not 32-bit address
	call disgetword
	push ax
dop44:
	call disgetword
	call hexword
	mov	al,':'
	stosb
	call dischk32d
	jz	dop45		;if not 32-bit address
	pop	ax
	call hexword
dop45:
	pop	ax
	call hexword
	ret


%if _COND
		; INP:	[presizeflags] & PRE32A, d[reg_ecx]
		; OUT:	dx:ax = (e)cx
cond_get_ecx:
	mov ax, word [reg_ecx]
	test byte [presizeflags], PRE32A	; A32 ?
	mov dx, word [reg_ecx+2]
	jnz .ecx
	xor dx, dx
.ecx:
	retn

		; INP:	ax = 0..15 condition code, else invalid
		; OUT:	w[condmsg] set as appropriate
cond_handle:
	cmp ax, 15
	ja .return
	mov cx, word [reg_efl]	; get flags
	mov bx, ax
	and bl, ~1		; make even
	and al, 1		; 1 if negated condition
	cmp bl, 12		; L/GE or LE/G?
	jae .specific		; yes -->

	test cx, [cond_table+bx]; flag(s) set ?
	jmp short .jump_ZF	; NZ if (normal) condition true -->

.specific:
	cmp bl, 14
	jb .L_GE

		; Handle LE/NG and G/NLE conditions.
		; The former says ZF | (OF ^ SF).
.LE_G:
	test cl, 40h		; ZF | ..
	jnz .jump_true

		; Handle L/NGE and GE/NL conditions.
		; The former says OF ^ SF.
.L_GE:
	and cx, 880h		; OF ^ SF
	jz .jump_false		; both clear -->
	xor cx, 880h
.jump_ZF:
	jz .jump_false		; both set --> (or ZR: (normal) condition false)
.jump_true:
	xor al, 1		; (negating ^ raw truth) = cooked truth
.jump_false:
	test al, al		; true ?
	jnz .msg_jumping	; yes -->

.msg_notjumping:
	mov word [condmsg], msg.condnotjump
.return:
	retn

.msg_jumping:
	mov word [condmsg], msg.condjump
	retn
%endif


;	8-bit relative jump (OP_REL8)

dop_rel8:
%if _COND
	mov ax, word [index]
	cmp ax, 0E3h
	ja .cond_done		; no conditional jump -->
	jb .cond_noncx		; not jcxz, check for other -->

	call cond_get_ecx
	or ax, dx
	jz .cond_msg_jumping
.cond_msg_notjumping:
	call cond_handle.msg_notjumping
	jmp short .cond_done

.cond_msg_jumping:
	call cond_handle.msg_jumping
	jmp short .cond_done

.cond_noncx:
	cmp al, 0E0h
	jb .cond_nonloop	; not loop, check for other -->

	push ax
	call cond_get_ecx
	dec ax			; = 0 if cx is 1
	or ax, dx		; = 0 if cx is 1 and ecx is cx
	pop ax
	jz .cond_msg_notjumping	; if (e)cx is 1 -->
	cmp al, 0E2h
	je .cond_msg_jumping	; loop without additional condition -->
	xor al, 0E0h^75h	; E0h (loopnz) to 75h (jnz),
				;  E1h (loopz) to 74h (jz)

.cond_nonloop:
	sub al, 70h		; (ah = 0)
	call cond_handle	; call common code (checks for ax < 16)
.cond_done:
%endif
	call disgetbyte
	cbw
	mov byte [disp8], al
	jmp s	dop48

;	16/32-bit relative jump (OP_REL1632)

dop_rel1632:
%if _COND
	mov ax, word [index]
	sub ax, SPARSE_BASE+80h
	call cond_handle
%endif
	call disgetword
	call dischk32d
	jz dop48_near		; if not 32-bit offset
	push ax
%if _PM
	test byte [bCSAttr],40h	;for 32-bit code segments
	jnz dop47_1				;no need to display "DWORD "
%endif
	call showdword
dop47_1:
	pop	dx
	call disgetword

	cmp word [index], 00E8h
	je .not_show_keyword	; no need to distinguish NEAR call -->
		; ax:dx between FFFFh:FF80h (-128) .. 0000h:007Fh (127):
		; == show "NEAR" keyword
		;
		; Note:	This is not entirely correct. If a jump short is
		;	 used, the actual opcode is shorter, thus the
		;	 exact distance that can be reached by the jump short
		;	 differs from what the jump near can reach with
		;	 a rel16/32 displacement between -128..127.
	cmp ax, -1
	je .checkminus
	test ax, ax
	jnz .not_show_keyword
.checkplus:
	cmp dx, byte 127
	jg .not_show_keyword
	cmp dx, 0
	jl .not_show_keyword
	jmp .show_keyword

.checkminus:
	cmp dx, byte -128
	jl .not_show_keyword
	cmp dx, 0
	jge .not_show_keyword

.show_keyword:
	testopt [asm_options], disasm_show_near
	jnz .not_show_keyword
	call dop_show_near
.not_show_keyword:

	mov	bx,[u_addr+0]
	add	bx,[dis_n]
	adc	ax,[dis_n + 2]
	add	dx,bx
	adc	ax,[u_addr+2]
	call hexword
	xchg ax,dx
	jmp s	hexwordj1	; call hexword and return

dop48_near:
	cmp word [index], 00E8h
	je @F			; no need to distinguish NEAR call -->
		; ax between FF80h (-128) .. 007Fh (127):
		; == show "NEAR" keyword
		;
		; Note:	This is not entirely correct. If a jump short is
		;	 used, the actual opcode is shorter, thus the
		;	 exact distance that can be reached by the jump short
		;	 differs from what the jump near can reach with
		;	 a rel16/32 displacement between -128..127.
	cmp ax, byte -128
	jl @F
	cmp ax, byte 127
	jg @F
	testopt [asm_options], disasm_show_near
	jnz @F
	call dop_show_near
@@:
dop48:
_386_PM	movsx eax, ax
	_386_PM_o32	; add eax, dword [u_addr]
	add ax, word [u_addr]
	_386_PM_o32	; add eax, dword [dis_n]
	add ax, word [dis_n]
%if _PM
	test byte [bCSAttr], 40h; 32-bit code segment ?
	jz .16			; no -->
	call hexword_high	; yes, display (possibly non-zero) high word
.16:
%endif
hexwordj1:
	jmp hexword		; call hexword and return


;	Check for ST(1) (OP_1CHK).

dop49:
	pop	ax		;discard return address
	mov	al,[regmem]
	and	al,7
	cmp	al,1
	je	dop50		;if it's ST(1)
	jmp	da14		;another operand (but no comma)

dop50:
	jmp	da21		;end of list

;	ST(I) (OP_STI).

dop_sti:
	mov al, byte [regmem]
	and al, 7
	xchg ax, bx		;mov bx,ax
	mov ax, 'ST'
	call dis_stosw_lowercase; store ST(bl)
	mov al, '('
	stosb
	mov ax, '0)'
	or al, bl
	stosw
	retn

;	CRx (OP_CR).

dop_cr:
	mov	bx,'CR'
	call getregmem_r
	cmp	al,4
	ja	disbad4		;if too large
	jne	dop52a
	mov	byte [dismach],5	;CR4 is new to the 586
dop52a:
	cmp	word [index],SPARSE_BASE+22h
	jne	dop55		;if not MOV CRx,xx
	cmp	al,1
	jne	dop55		;if not CR1

disbad4:jmp	disbad		;can't MOV CR1,xx

;	DRx (OP_DR).

dop_dr:
	call getregmem_r
	mov	bx,'DR'
	mov	cx,-1		;no max or illegal value
	jmp s	dop55

;	TRx (OP_TR).

dop_tr:
	call getregmem_r
	cmp	al,3
	jb	disbad		;if too small
	cmp	al,6
	jae	dop54a		;if TR6-7
	mov	byte [dismach],4	;TR3-5 are new to the 486
dop54a:
	mov	bx,'TR'

dop55:
	xchg ax, bx
	call dis_stosw_lowercase; store XX
	xchg ax, bx
	or al, '0'
	stosb
	retn

;	Segment register (OP_SEGREG).

dop_segreg:
	call getregmem_r
	cmp	al,6
	jae	disbad		; if not a segment register -->
	cmp	al,2
	jne	@F		; if not SS -->
	or	byte [disflags], DIS_I_MOV_SS	; note this
@@:
	cmp	al,4
	jb dop57a		;if not FS or GS
	mov	byte [dismach],3;(no new 486-686 instructions involve seg regs)
dop57a:
	add	al,16
	jmp	dop35		;go print it out

;	Sign-extended immediate byte (OP_IMMS8). "push xx"

dop_imms8:
	call disgetbyte
	cmp	al,0
	xchg ax,bx		;mov bl,al
	mov	al,'+'
	jge	dop58a		;if >= 0
	neg	bl
	mov	al,'-'
dop58a:
	stosb
	xchg ax,bx		;mov al,bl
	jmp s	dop59a		;call hexbyte and return

;	Immediate byte (OP_IMM8).

dop_imm8:
	call disgetbyte
dop59a:
	jmp	hexbyte		;call hexbyte and return


	; Show MMx reg (OP_MMX; previously was "Show ECX if 32-bit LOOPxx").
dop_mmx:
	mov bx, "MM"
	call getregmem_r
	jmp short dop55

	; MMX register (in ModR/M part)
dop_mmx_mod:
	mov bx, "MM"
	call getregmem
	cmp al, 0C0h
	jb disbad		; needs to be encoded as register -->
	and al, 7
	jmp short dop55

	; Set flag to always show size (OP_SHOSIZ).
dop_shosiz:
	or	byte [disflags],DIS_I_SHOWSIZ
dop60a:
	pop	ax		; discard return address
	jmp	da14		; next...

dop_short:
	testopt [asm_options], disasm_show_short
	jz dop60a
	mov ax, "SH"
	call dis_stosw_lowercase
	mov ax, "OR"
	call dis_stosw_lowercase
	mov ax, "T "
	call dis_stosw_lowercase
dop60a_1:
	jmp dop60a

dop_near:
	testopt [asm_options], disasm_show_near
	jz dop60a_1
	call dop_show_near
dop60a_2:
	jmp dop60a_1

dop_far:
	testopt [asm_options], disasm_show_far
	jz dop60a_2
dop_far_required:
	mov	ax, "FA"	; store "FAR "
	call	dis_stosw_lowercase
	mov	ax, "R "
	call	dis_stosw_lowercase
	jmp	dop60a_2

dop_show_near:
	push ax
	mov ax, "NE"
	call dis_stosw_lowercase
	mov ax, "AR"
	call dis_stosw_lowercase
	mov al, " "
	stosb
	pop ax
	retn


disbad:
	mov	sp,[savesp2]	;pop junk off stack
	mov	ax, da13
	push	ax
	_386_PM_o32	; xor eax, eax
	xor	ax, ax
	_386_PM_o32	; mov dword [dis_n], eax
	mov	word [dis_n], ax
	mov	word [preflags], ax	; clear preflags and preused
%if _COND
	mov	word [condmsg], ax	; initialize conditions message
%endif
	mov	byte [rmsize], 80h	; don't display any memory
	mov	word [dismach], ax	; forget about the machine type
	and	byte [disflags],~DIS_I_SHOW	;and flags
	call disgetbyte
	mov	di,prefixlist
	mov	cx,N_PREFIX
	repne scasb
	je	.namedprefix	;if it's a named prefix
	_386_PM_o32	; dec dword [dis_n]
	dec	word [dis_n]
	mov	bx,MN_DB	;offset of 'DB' mnemonic
	mov	si,OPLIST_26+OPTYPES_BASE	;this says OP_IMM8
	retn

.namedprefix:
	or	byte [disflags],DIS_I_UNUSED	;print special flag
	mov	bx,N_PREFIX - 1
	sub	bx,cx
	shl	bx,1
	cmp	bx, byte 6 *2
	jb	.segprefix	; if SEG directive -->
%if _PM
	cmp	bx, byte 10 *2
	jb	.non16prefix	; if not OSIZE or ASIZE -->
	test byte [bCSAttr], 40h; 32-bit code segment ?
	jz .non16prefix		; no, O32 or A32 -->
	add	bx, byte 4	; yes, change to O16 or A16
.non16prefix:
%endif
	mov	bx,[prefixmnem+bx-6*2]
	mov	si,OPTYPES_BASE	; no operand
	retn

.segprefix:
	lea	si,[bx+OPLIST_40+OPTYPES_BASE]	; this is OP_ES
;	lea	si,[bx+fake_oplist-oplists+OPTYPES_BASE]
;						; -> fake OPLIST for segments
	mov	bx,MN_SEG
	retn

%if 0
usesection ASMTABLE1, 1
fake_oplist:
	db OP_ES, 0
	db OP_CS, 0
	db OP_SS, 0
	db OP_DS, 0
	db OP_FS, 0
	db OP_GS, 0
__SECT__
%endif

;	GETREGMEM_R - Get the reg part of the reg/mem part of the instruction
;	Uses	CL

getregmem_r:
	call	getregmem
	mov	cl,3
	shr	al,cl
	and	al,7
	ret

;	GETREGMEM - Get the reg/mem part of the instruction

getregmem:
	test byte [preused],GOTREGM
	jnz	grm1		;if we have it already
	or	byte [preused],GOTREGM
	call disgetbyte	;get the byte
	mov	[regmem],al	;save it away

grm1:	mov	al,[regmem]
	ret

dis_lowercase_w:
	xchg al, ah
	call dis_lowercase
	xchg al, ah
dis_lowercase:
	cmp al, 'A'
	jb .not
	cmp al, 'Z'
	ja .not
	testopt [asm_options], disasm_lowercase
	jz .not
	or al, 20h
.not:
	retn


dis_lowercase_refmem_w:
	xchg al, ah
	call dis_lowercase_refmem
	xchg al, ah
dis_lowercase_refmem:
	cmp al, 'A'
	jb .not
	cmp al, 'Z'
	ja .not
	testopt [asm_options], disasm_lowercase_refmem
	jz .not
	or al, 20h
.not:
	retn


		; Show the opcode mnemonic
		;
		; INP:	si-> Opcode mnemonic string of an mnlist entry.
		;	w[si-2] & 0Fh = Length of that string.
		; OUT:	di-> next available byte in output line
		;	     (>= line_out + 32 due to padding)
		; CHG:	ax, cx, si
showop:
	mov di, line_out+MNEMONOFS
	push si

	mov cx, [si-2]
	and cx, 0Fh
.loop:
	lodsb
	call dis_lowercase
	stosb
	loop .loop

	pop ax				; ax-> mnemonic
	cmp ax, mnlist_a_suffix_allowed	; non-suffixed mnemonic ?
	jb .nosuffix			; yes -->
	cmp ax, mnlist_o_suffix_allowed	; optional address size suffix ?
	jb .a_suffix_allowed		; yes -->
	cmp ax, mnlist_o_suffix_required; optional operand size suffix ?
	 mov ah, PREOSIZE		; (OSIZE: check OSIZE/O32)
	ja .suffix_decide		; no, it's required -->
	db __TEST_IMM16			; (skip mov)
.a_suffix_allowed:
	 mov ah, PREASIZE		; optional ASIZE: check ASIZE/A32

.suffix_decide_optional:	; check whether the suffix is necessary
	test byte [preflags], ah	; check if such a prefix occured (ZR if not)
	jz .suffix_invisible		; no, is default form --> (hide suffix)

.suffix_decide:			; suffix will be displayed, now only decide which
	mov al, 'W'
	test byte [presizeflags], ah	; 32-bit form ?
	jz .got_suffix			; no -->
	mov al, 'D'
.got_suffix:
	call dis_lowercase
	stosb				; store suffix

.suffix_invisible:		; notional suffix either displayed or left hidden,
	or byte [preused], ah		; in any case, mark opcode prefix as used

.nosuffix:

		; Store blanks to pad to 8 characters, but at least one
	mov al, 32
.pad:
	stosb
	cmp di, line_out+MNEMONOFS+8
	jb .pad

	retn


		; INP:	byte [segmnt] = number of segment register
		;	[segrgnam] = uppercase segment register names
		; CHG:	bx, ax
		; OUT:	ax = uppercase segment register name
showseg_uppercase_ax:
	mov	al,[segmnt]	;segment number
	cbw
	shl	ax,1
	xchg	ax,bx		;mov bx,ax
	mov	ax,[segrgnam+bx] ;get register name
	retn

;	SHOWSEG - Show the segment descriptor in SEGMNT
;	Entry	DI	Where to put it
;	Exit	DI	Updated
;	Uses	AX, BX

showseg:
	call showseg_uppercase_ax
dis_stosw_lowercase:
	call	dis_lowercase_w
	stosw
	retn


		; Write a size specifier to the buffer
		;  and set some flags
		; INP:	ah = r/m size value,
		;		F0h byte (less than zero)
		;		00h word (equal to zero)
		;		10h dword (greater than zero, but != 20h)
		;		20h qword (greater than zero, == 20h)
		;	di-> buffer
		; OUT:	di-> behind size specifier in buffer
		;	by[rmsize] set
		;	wo[sizeloc]-> size specifier in buffer
		; CHG:	ax
		;
		; Size specifiers are BYTE, WORD, DWORD, and QWORD. One
		; blank is appended to the size specifier.
		;
		; Size specifiers are lowercased if that option is selected.
showsize:
	mov	[rmsize], ah	; save r/m size
	mov	[sizeloc], di	; save where we're putting this
	mov	al, 'Q'
	cmp	ah, 20h		; QWORD ?
	je	.qword		; yes -->
	cmp	ah, 0
	jge	.notbyte	; if word or dword -->
	mov	ax, "TE"
	push	ax
	mov	ax, "BY"
	jmp s	.common

.notbyte:
	je	.word		; if word
.dword:
	mov	al, 'D'
.qword:
	call	dis_lowercase
	stosb
.word:
	mov	ax, "RD"
	push	ax
	mov	ax, "WO"
.common:
	call	dis_stosw_lowercase
	pop	ax
	call	dis_stosw_lowercase
	mov	al, 32
	stosb
 showptr.ret:
	retn

		; Write "PTR " to a buffer if NASM syntax is not selected.
		; INP:	di-> buffer
		; OUT:	di-> behind written string "PTR " (or unchanged)
		; CHG:	ax
		;
		; The string is lowercased if that option is selected.
showptr:
	testopt [asm_options], disasm_nasm
	jnz	.ret
	mov	ax, "PT"
	call	dis_stosw_lowercase
	mov	ax, "R "
	jmp s	dis_stosw_lowercase

		; Write "DWORD " to a buffer
		; INP:	di-> buffer
		; OUT:	di-> behind written string "DWORD "
		; CHG:	ax
		;
		; The string is lowercased if that option is selected.
showdword: equ showsize.dword

;	DISP32 - Print 32-bit displacement for addressing modes.
;	Entry	None
;	Exit	None
;	Uses	AX

disp32:
	call disgetword
.ax:
	push ax
	call disgetword
	call hexword
	pop ax
	jmp hexword

;	SHOWREG16 - Show 16-bit register name.
;	Entry	AL	register number (0-7)
;	Exit	None
;	Uses	AX

showreg16:
	cbw
	shl	ax,1
	xchg ax,bx
	push ax
	mov	ax,[rgnam16+bx]
	call	dis_stosw_lowercase
	pop	ax
	xchg ax,bx
	ret


		; DISCHK32D - Check for O32 (32-bit operand size).
dischk32d:
	or byte [preused], PREOSIZE
	test byte [presizeflags], PRE32D
	retn


;	SHOWMACH - Return string "[needs math coprocessor]", etc.
;	Entry   di -> second item of table of 6 words
;	Exit	si	Address of string
;		cx	Length of string, or 0 if not needed
;	Uses	al, di

showmach:
	mov	si,needsmsg	; candidate message
	test byte [dmflags],DM_COPR
	jz	sm1		; if not a coprocessor instruction
	mov	byte [si+9],'7'	; change message text
	mov	al,[mach_87]
	cmp	byte [has_87],0
	jnz	sm2		; if it has a coprocessor
	mov	al,[machine]
	cmp	al,[dismach]
	jb	sm3		; if we display the message
	mov	si,needsmath	; print this message instead
	mov	cx,needsmath_L
	retn

sm1:
	mov	byte [si+9],'6'	; reset message text
	mov	al,[machine]
sm2:
	cmp	al,[dismach]
	jae	sm4		; if no message (so far)
sm3:
	mov	al,[dismach]
	add	al,'0'
	mov	[si+7],al
	mov	cx,needsmsg_L	; length of the message
	retn

		; Check for obsolete instruction.
sm4:
	mov	si, obsolete	; candidate message
	mov	ax, word [di-2]	; get info on this instruction
	mov	cx, 5
	repne scasw
	jne	sm6		; if no matches
	mov	di, obsmach + 5 - 1
	sub	di, cx
	xor	cx, cx		; clear CX: no message
	mov	al, byte [mach_87]
	cmp	al, byte [di]
	jle	sm5		; if this machine is OK
	mov	cx, obsolete_L
sm5:
	retn

sm6: equ sm5


;	DISGETBYTE - Get byte for disassembler.
;	Entry	None
;	Exit	AL	Next byte in instruction stream
;	Uses	None

disgetbyte:
	push ds
	_386_PM_o32		; push esi
	push si				; save ds, (e)si
	_386_PM_o32		; mov esi, dword [u_addr]
	mov si, word [u_addr]
	_386_PM_o32		; add esi, dword [dis_n]
	add si, word [dis_n]		; index to the right byte
	mov ds, word [u_addr+4]
	_386_PM_a32
	lodsb 				; get the byte
	_386_PM_o32		; pop esi
	pop si
	pop ds				; restore regs
	_386_PM_o32		; inc dword [dis_n]
	inc word [dis_n]		; indicate that we've gotten this byte
	retn


;	DISGETWORD - Get word for disassembler.
;	Entry	None
;	Exit	AX	Next word
;	Uses	None

disgetword:
	push ds
	_386_PM_o32		; push esi
	push si				; save ds, (e)si
	_386_PM_o32		; mov esi, dword [u_addr]
	mov si, word [u_addr]
	_386_PM_o32		; add esi, dword [dis_n]
	add si, word [dis_n]		; index to the right byte
	mov ds, word [u_addr+4]
	_386_PM_a32
	lodsw 				; get the word
	_386_PM_o32		; pop esi
	pop si
	pop ds				; restore regs
	_386_PM_o32		; add dword [dis_n], byte 2
	add word [dis_n], byte 2	; indicate that we've gotten this word
	retn


;	DISSHOWBYTES - Show bytes for the disassembler.
;	Entry	BX	Number of bytes (must be > 0)
;		di-> output line
;	Exit		u_addr updated
;	Uses	BX, (E)SI.

disshowbytes:
	_386_PM_o32		; mov esi, dword [u_addr]
	mov si, word [u_addr]
	mov ds, word [u_addr+4]
.loop:
	_386_PM_a32
	lodsb
	call hexbyte
	dec bx
	jnz .loop
	push ss
	pop ds
	_386_PM_o32		; mov dword [u_addr], esi
	mov word [u_addr], si
	retn

		; MOVEOVER - Move the line to the right.
		; Entry	DI	Last address + 1 of line so far
		; Exit	CX	Number of bytes to move
		;	DI	Updated
		; Uses	SI
moveover:
	cmp word [sizeloc], byte 0
	je mo1			; if sizeloc not saved
	add word [sizeloc], cx

mo1:
	mov si, di
	add di, cx
	mov cx, di
	sub cx, line_out+MNEMONOFS
	push di
	std			; _AMD_ERRATUM_109_WORKAROUND as below
	dec si
	dec di


	numdef AMD_ERRATUM_109_WORKAROUND, 1
		; Refer to comment in init.asm init_movp.

%if _AMD_ERRATUM_109_WORKAROUND
	jcxz @FF
	cmp cx, 20
	ja @FF
@@:
	movsb
	loop @B
@@:
%endif
	rep movsb
	pop di
	cld
	retn
