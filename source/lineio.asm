
%if 0

lDebug line input and output

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2012 C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif


	usesection lDEBUG_CODE

		; Check for given string (cap-insensitive)
		;
		; INP:	si-> input string to check (either cap),
		;	 terminated by CR (13), semicolon, space, tab, comma
		;	dx-> ASCIZ string to check (all-caps)
		; OUT:	Iff string matches,
		;	 ZR
		;	 si-> at separator that terminates the keyword
		;	else,
		;	 NZ
		;	 si = input si
		; STT:	ds = es = ss
		; CHG:	dx, ax
isstring?:
	push si
	xchg dx, di
.loop:
	lodsb
	call uppercase
	scasb
	jne .mismatch
	test al, al
	jne .loop
	jmp .matched_zr

.mismatch:
	call iseol?
	je .checkend
	cmp al, 32
	je .checkend
	cmp al, 9
	je .checkend
	cmp al, ','
	je .checkend
	cmp al, '='
	je .checkend
.ret_nz:
		; NZ
	pop si
.ret:
	xchg dx, di
	retn

.checkend:
	cmp byte [es:di - 1], 0
	jne .ret_nz
.matched_zr:	; ZR
	pop ax			; (discard)
	lea si, [si - 1]	; -> separator
	jmp .ret


iseol?:
	cmp al, 13
	je .ret
	cmp al, ';'
	je .ret
	cmp al, 0
.ret:
	retn

		; Check for end of line
		;
		; INP:	al = first character
		;	ds:si-> next character
		; OUT:	ZR
		;	al = 13 or al = ';' or al = 0
		;	(does not return if anything on line beside blanks)
chkeol:
	call skipwh0
	call iseol?
	je iseol?.ret		; if EOL -->

errorj8:
	jmp error

;	SKIPCOMMA - Skip white space, then an optional comma, and more white
;		space.
;	SKIPCOMM0 - Same as above, but we already have the character in AL.
		; STK:	3 word
skipcomma:
	lodsb
skipcomm0:
	call skipwh0
	cmp al, ','
	jne .return		; if no comma
	push si
	call skipwhite
	cmp al, 13
	jne .noteol		; if not end of line
	pop si
	mov al, ','
	retn
.noteol:
	add sp, byte 2		; pop si into nowhere
.return:
	retn


skipequals:
	lodsb
skipequ0:
	call skipwh0
	cmp al, '='
	jne .return
	call skipwhite
.return:
	retn


		; Skip alphabetic characters, and then white space
		;
		; INP:	ds:si-> first character
		; OUT:	al = first non-blank character behind alphabetic characters
		;	ds:si-> character behind the first non-blank behind alpha.
skipalpha:
.:
	lodsb
	and al, TOUPPER
	sub al, 'A'
	cmp al, 'Z'-'A'
	jbe .
	dec si

		; Skip blanks and tabs
		;
		; INP:	ds:si-> first character
		; OUT:	al = first non-blank character
		;	ds:si-> character behind the first non-blank
		; CHG:	-
		; STK:	1 word
skipwhite:
	lodsb

		; Same as above, but first character in al
		;
		; INP:	al = first character
		;	ds:si-> next character
		; OUT:	al = first non-blank character
		;	ds:si-> character behind the first non-blank
		; CHG:	-
		; STK:	1 word
skipwh0:
	cmp al, 32
	je skipwhite
	cmp al, 9
	je skipwhite
	retn

		; Compare character with separators
		;
		; INP:	al = character
		; OUT:	ZR if al is blank, tab, comma, semicolon or equal sign
		;	NZ else
ifsep:
	call iseol?
	je .return
	cmp al, 32
	je .return
	cmp al, 9
	je .return
	cmp al, ','
	je .return
	cmp al, '='
.return:
	retn

		; SHOWSTRING - Print ASCIZ string.
showstring.next:
	stosb
showstring:
	lodsb
	test al, al
	jnz .next
	retn


		; Dump byte as decimal number string
		;
		; INP:	al = byte
		;	di-> where to store
		; OUT:	-
		; CHG:	di-> behind variable-length string
decbyte:
	push ax
	push cx
	mov cx, 100
	call .div
	mov cl, 10
	call .div
	add al, '0'
	stosb
	pop cx
	pop ax
	retn

.div:
	xor ah, ah
	div cl
	or ch, al
	jz .leadingzero
	add al, '0'
	stosb
.leadingzero:
	xchg al, ah
	retn



		; dump high word of eax - assumes 386
hexword_high:
cpu 386
	rol eax, 16
	call hexword
	rol eax, 16
cpu 8086
	retn


		; hexdword - dump dword (in eax) to hex ASCII - assumes 386
		; HEXWORD - Print hex word (in AX).
		; HEXBYTE - Print hex byte (in AL).
		; HEXNYB - Print hex digit.
		; Uses	none.
%if 0	; currently disabled because only one call made to here (ID command)
hexdword:
	call hexword_high
%endif
hexword:
	xchg al, ah
	call hexbyte
	xchg al, ah

hexbyte:
	push cx
	mov cl, 4
	rol al, cl
	call hexnyb
	rol al, cl
	pop cx

hexnyb:
	push ax
	and al, 0Fh
	add al, 90h
	daa
	adc al, 40h
	daa			; these four instructions change to ASCII hex
	stosb
	pop ax
	retn

;	TAB_TO - Space fill until reaching the column indicated by AX.
;	(Print a new line if necessary.)

tab_to:
	push ax
	sub ax, di
	ja tabto1		; if there's room on this line
	call trimputs
	mov di, line_out
tabto1:
	pop cx
	sub cx, di
	mov al, 32
	rep stosb		; space fill to the right end
	retn

		; Trim excess blanks, append linebreak and display line_out.
		;
		; INP:	di-> behind last character to display, or blank
		;
		; Note:	May overflow if line_out only contains blanks. The byte at
		;	trim_overflow is used to avoid overflows.
trimputs:
	dec di
	cmp byte [di], 32
	je trimputs
	inc di

		; Append linebreak and display line_out
		;
		; INP:	di-> behind last character to display
		; STT:	all segment registers same
		; CHG:	ax, bx, cx, dx
putsline_crlf:
	mov ax, 10<<8| 13
	stosw

		; Display line_out
		;
		; INP:	di-> behind last character to display
		; STT:	all segment registers same
		; CHG:	ax, bx, cx, dx
putsline:
	mov cx, di
	mov dx, line_out
	sub cx, dx

		; Display message
		;
		; INP:	dx-> message to display
		;	cx = length of message
		; STT:	all segment registers same
		; CHG:	ax, bx, cx, dx
puts:
	testopt [internalflags], tt_silence
	jnz puts_silence

			; The following code contains most of the paging support.
			; Based on the number of LF characters in the string it
			; displays only parts of the string, then interrupts it by
			; the "[more]" prompt which waits for any key before
			; proceeding. This is ensured to work proper in InDOS mode.
			;
			; Paging is deactivated if the command's output mustn't be
			; paged (clears pagedcommand, which is set by cmd3). It is
			; also not used when we output to a file.
	testopt [options], nonpagingdevice
	jnz .display			; deactivated by user -->
	call InDos			; InDOS mode ?
	jnz .dontcheckredirection	; yes, then we display with Int10 anyway -->
%if _INPUT_FILE_HANDLES
	testopt [internalflags2], dif2_input_file
	jnz .display
%endif
	testopt [options], enable_serial ; I/O done using serial port ?
	jnz .dontcheckredirection	; yes, is paged -->
	testopt [internalflags], outputfile
	jnz .display			; output redirected to file. never page -->
	testopt [options], nondospaging
	jnz .dontcheckredirection
	testopt [internalflags], inputfile
	jnz .display			; input redirected from a file. never page -->
.dontcheckredirection:
	push di
	push cx				; used as variable: remaining (not yet displayed) line length
	mov di, dx			; es:di-> string, cx = length
.looplf:
	test cx, cx
	jz .display_pop			; end of string (or ended in LF) -->
				; Important: We only ever jump back to .looplf when cx
				; zero means it's okay to ignore the waiting prompt as
				; flagged below. This is (A) at the start of a string,
				; where cx is the whole string's length, (B) after
				; determining that prompting is not yet necessary, in
				; which case the flag was checked earlier already, (C)
				; after the flag has been set and a substring was already
				; displayed (so cx is again the whole substring's length)
				; or (D) just after the prompt was displayed, in which
				; case the flag cannot be set.
				; In case A and C, when the (sub)string is empty (ie. cx
				; is zero) it's crucial to ignore the waiting prompt as
				; this is the exact behaviour we want: If nothing is
				; displayed anymore before the getline code prompts
				; anyway, do not display our prompt.
	testopt [internalflags], promptwaiting	; do we have a prompt to display ?
	jnz .promptnow			; yes, display it before the string -->

	testopt [options], enable_serial ; serial ?
	jz @F				; no -->
	xor ax, ax
	or al, byte [serial_rows]	; ax = number of rows if serial
	jz .display_pop			; if zero, do not page -->
	dec ax
	jmp @FF
@@:
	push es
	mov ax, 40h			; 0040h is a bimodal segment/selector
	mov es, ax
	mov al, byte [ es:84h ]		; rows on screen
	pop es
@@:
	cmp byte [ linecounter ], al
	jb .notyet			; not yet reached -->
	testopt [internalflags], pagedcommand	; active ?
	jnz .prompt			; yes, prompt -->
	dec byte [ linecounter ]	; keep count, but don't prompt til next LF
	jmp short .notyet

.prompt:
	 pop ax				; ax = length of string, cx = length of string remaining
	sub ax, cx			; ax = length of string til LF
	xchg ax, cx			; cx = til LF incl., ax = behind LF
	 push ax			; new count
				; cx = length til LF
				; ds:dx-> start of part til LF
	call .display			; display part of message which fits on screen
	 pop cx
	 push cx			; update cx from variable
	mov dx, di			; dx-> start of next part
	setopt [internalflags], promptwaiting	; mark as prompting necessary
	jmp short .looplf		; now check whether anything follows at all
				; This is the magic to suppress unnecessary prompts as
				; were displayed previously. Now, we'll set this flag
				; which effectively displays the prompt before (!) any
				; other output is done. Previously, the prompt would be
				; displayed right here. The only case where behaviour
				; changed is when no more output occurs until the flag
				; is reset elsewhere - ie. if getline prompts anyway.

.promptnow:
	push dx
	push cx
	mov byte [ linecounter ], 0	; prompting, so reset the line counter
	clropt [internalflags], promptwaiting
	mov dx, msg.more
	mov cx, msg.more_size
	call .display			; print string (avoiding a recursion)

				; This option is a hack for the sole use of
				; demo scripts that only want the user to press
				; a key for paging.
	testopt [options], nondospaging
	jz .getc
	call getc.rawnext		; get a character from BIOS
	jmp short .dispover
.getc:
	call getc			; get a character
.dispover:
	cmp al, 3			; is it Ctrl+C ?
	je .ctrlc			; yes, handle that -->
	mov dx, msg.more_over
	mov cx, msg.more_over_size
	call .display			; overwrite the prompt (avoiding a recursion)
	pop cx
	pop dx

.notyet:
	mov al, 10
	repne scasb			; search LF
	jne .display_pop		; none -->

	inc byte [ linecounter ]	; record how many LFs will be displayed
	jmp .looplf			; search for next LF -->

.display_pop:
	pop cx
	pop di
.display:
			; Non-paged output code follows.
	testopt [options], enable_serial
	jnz .notdos
	call InDos
	jnz .notdos
	mov bx, 1			; standard output
	mov ah, 40h			; write to file
	doscall
	retn

.ctrlc:
	mov dx, msg.ctrlc
	mov cx, msg.ctrlc_size
	call .display			; visually acknowledge Ctrl+C and start a new line
	jmp cmd3			; abort currently running command -->
		; If handled by DOS, Ctrl+C causes our process to be terminated.
		; Because we are self-owned, we re-enter our code at debug22 then.
		; debug22 only does some re-initialization of registers before
		; entering cmd3. Therefore, instead of aborting we can directly jump
		; to cmd3 here. This has the additional benefit of not requiring DOS
		; at all, so that no workarounds for InDOS mode and boot loader
		; operation are necessary.

		; No command should fail spectacularly when being aborted this way,
		; because in fact every command calling puts can already be aborted by
		; DOS's Ctrl+C checking if DOS is used. This check is really only an
		; _additional_ way the commands can be aborted.

		; Note that a more complete way to support command abortion would be
		; to hook Int1B, and to keep a flag of whether Ctrl+C or Ctrl+Break
		; were requested, and to additionally check before or after every I/O
		; operation whether Ctrl+C was pressed using non-destructive reads.
		; In short, exactly what DOS does.

.notdos:
	push si
	testopt [internalflags], usecharcounter
	jnz .dontresetcharcounter
	mov byte [ charcounter ], 1
				; This assumes we always start at the beginning of a line.
				; Therefore any call to puts must display at the beginning
				; of a line or tab parsing will not work. Only calls to puts
				; not containing tab characters may display partial lines.
				; (Calls to puts with partial lines and tab characters have
				; to set the flag usecharcounter in internalflags.)
.dontresetcharcounter:
	jcxz .return
	mov si, dx
.loop:
	lodsb
	cmp al, 9
	jne .nottab			; is no tab -->
	mov al, byte [ charcounter ]
	and al, 7			; at 8 character boundary ?
	mov al, 32			; (always replaced by blank)
	jz .nottab			; yes, don't use hack -->
	inc cx
	dec si				; find tab again next lodsb
.nottab:
	cmp al, 13
	jne .notcr
	mov byte [ charcounter ], 0	; increased to one before displaying
.notcr:
%if 0				; currently we never receive BS here
	cmp al, 8
	jne .notbs
	mov ah, 0Fh
	int 10h				; get page
	mov bl, al			; save number of characters per column
	push cx
	mov ah, 03h
	int 10h				; get cursor position dx
	pop cx
	dec byte [ charcounter ]	; assume not at start of line
	mov al, 8			; changed by Int10
	or dl, dl
	jnz .dontcount			; not first column, so display normal -->
	mov byte [ charcounter ], 1	; assume at start of screen
	or dh, dh
	jz .next			; at start of screen, don't display -->
	dec dh				; previous line
	mov dl, bl
	mov byte [ charcounter ], dl	; really at end of line (one-based counter)
	dec dl				; last column
	mov ah, 02h
	int 10h				; set new cursor position
	jmp short .next
.notbs:
%endif
	cmp al, 10
	je .dontcount			; must not count line feeds!
	inc byte [ charcounter ]
.dontcount:
	testopt [options], enable_serial
	jz @F

	call serial_send_char

	jmp .next
@@:
	mov bx, 0007
	mov ah, 0Eh
	int 10h
.next:
	loop .loop
.return:
	pop si
	retn


		; INP:	dx-> message to display
		;	cx = length of message
		; STT:	all segment registers same
		; CHG:	ax, bx, cx, dx
puts_silence:
	push si
	push di

.try_again:
	mov si, dx			; ds:si -> message
	mov es, word [auxbuff_segorsel]
	mov di, word [auxbuff_behind_last_silent]
					; es:di -> next buffer (if it fits)
	mov ax, _AUXBUFFSIZE - 1
	sub ax, di			; number of bytes left free
					;  (+ 1 byte terminator)
	jc .delete
	cmp ax, cx			; fits ?
	jae .simple			; yes -->

.delete:
	call silence_delete_one_string
	jmp .try_again

.simple:
	rep movsb			; copy over
	mov word [auxbuff_behind_last_silent], di
					; update pointer
	 push ss
	 pop es
	pop di
	pop si
	retn


		; INP:	es => auxbuff
		;	ds = ss
		;	[auxbuff_behind_last_silent] -> behind last silent
		;	[auxbuff_behind_while_condition]
		; OUT:	[auxbuff_behind_last_silent] updated
		;	auxbuff updated (deleted one of the dump strings,
		;	 moved forwards in the buffer the remainder)
		; CHG:	ax, di, si
silence_delete_one_string:
	push cx
	mov cx, [auxbuff_behind_last_silent]
					; -> next buffer position
	mov di, word [auxbuff_behind_while_condition]
	sub cx, di
	mov al, 0
	repne scasb
	jne .error			; error, no NUL found in data
		; es:di -> behind first NUL

	mov si, di			; es:si -> next message
	mov di, word [auxbuff_behind_while_condition]
	mov cx, word [auxbuff_behind_last_silent]
	push es
	pop ds				; ds:si -> next message
	sub cx, si			; remaining buffer
	rep movsb			; move to start of silent buffer
	push ss
	pop ds
	mov word [auxbuff_behind_last_silent], di
	pop cx
	retn

.error:
	push ss
	pop ds
	push ss
	pop es
	clropt [internalflags], tt_silence
	mov dx, msg.silent_error
	call putsz
	jmp cmd3


		; After having used puts_silence, this dumps all data
		;  remaining in the silent buffer in auxbuff.
		; If word [tt_silent_mode_number] is set, only that many
		;  data strings (zero-terminated) are dumped, from the end
		;  of the buffer.
		;
		; CHG:	ax, bx, cx, dx, si, di, es
		; STT:	ds = ss = debugger data selector
		;	sets es to ss
silence_dump:
	testopt [internalflags], tt_silent_mode	; is in use ?
	jnz @F			; yes -->
	retn			; no. simple

@@:
	clropt [internalflags], tt_silent_mode | tt_silence

	mov dx, word [tt_silent_mode_number]
	test dx, dx
	jz .no_number_given

	mov es, word [auxbuff_segorsel]
	mov di, word [auxbuff_behind_while_condition]
	mov cx, word [auxbuff_behind_last_silent]
	sub cx, di
	jz .no_number_given

	xor bx, bx		; counter of zeros
@@:
	mov al, 0
	jcxz @F			; no more data -->
	repne scasb		; another zero ?
	jne @F			; no, done -->
	inc bx			; count zeros
	jmp @B			; search for next -->

@@:
	sub bx, dx		; number of dumps - requested number
				;  = excess number of dumps
	jbe .no_number_given

	mov cx, bx		; use excess number as loop counter
@@:
	call silence_delete_one_string
				; delete one string
	loop @B			; loop for however many to delete -->

.no_number_given:
	mov di, word [auxbuff_behind_while_condition]
				; es:di -> silent buffer
.loop_line:
	mov es, word [auxbuff_segorsel]
	mov cx, word [auxbuff_behind_last_silent]
	sub cx, di		; any more data ?
	jz .return		; no, return -->
	mov al, 0
	dec cx			; (in case of branching for next conditional)
	scasb			; starts with a NUL byte ?
	je @F			; yes, skipped -->
	inc cx			; (restore cx to original value)
	dec di			; no, decrement
@@:
	mov si, di		; es:si -> start of string

	cmp cx, 256		; cx > 256 ?
	jbe @F			; no -->
	mov cx, 256		; limit to 256 bytes per string
				; (line_out is 264 bytes)
@@:
	jcxz .return		; (if single byte that was NUL remaining -->)
	mov bx, cx		; search string length
	mov al, 0
	repne scasb		; scan for NUL bytes
	jne @F			; (if none found: cx = 0)
				; (if any found: cx = remaining length)
	inc cx			; cx = remaining length + 1 (do not output NUL)
@@:
	sub bx, cx		; search length - remaining = found length
	mov cx, bx		; how much to show
	 push es
	 pop ds			; ds => auxbuff (ds:si -> start of string)
	 push ss
	 pop es			; es => line_out
	mov di, line_out	; es:di -> line_out
	 push cx
	rep movsb		; copy over to line_out
	 pop cx			; cx = message length

	push ss
	pop ds			; reset seg regs to ss
	mov dx, line_out	; dx -> message, cx = length
	push si
	call puts		; print out
	pop di			; -> next silent message
	jmp .loop_line

.return:
	push ss
	pop es
	push word [auxbuff_behind_while_condition]
	pop word [auxbuff_behind_last_silent]
	retn


		; Display ASCIZ message
		;
		; INP:	dx-> ASCIZ message to display
		; CHG:	-
		; STT:	ds = es = ss
putsz:
	push ax
	push bx
	push cx
	push dx
	push di
	mov di, dx			; es:di-> string
	xor al, al
	mov cx, -1
	repne scasb			; search zero
	neg cx
	dec cx
	dec cx				; cx = length of message
	pop di
	call puts
	pop dx
	pop cx
	pop bx
	pop ax
	retn

		; Display character
		;
		; INP:	al = character to display
		; CHG:	-
		; STT:	ds = es = ss
putc:
	push bx
	push cx
	push dx
	push ax
	mov cx, 1			; one character
	mov dx, sp			; ds:dx-> ax on stack
	call puts
	pop ax
	pop dx
	pop cx
	pop bx
	retn


		; Get character/key
		;
		; OUT:	al = character (if zero, look for ah)
		;	ah = scan code or zero
		; CHG:	ax
		; STT:	ds = ss = debugger segment/selector
		;
		; Idles system when in InDOS mode. When not in InDOS mode, Int21.08
		; is used which is assumed to idle the system itself.
getc:
	testopt [options], enable_serial
	jnz @F
	call InDos
	jz getc_dos
@@:
.rawnext:
	call near word [getline_timer_func]

	testopt [options], enable_serial
	jnz @F
	testopt [options], biosidles
				; idling disabled?
	jnz .rawkey		; yes, just request a key -->

@@:
	call getc_if_any	; got a key ?
	jnz .return		; yes -->
.idle:				; common idling for BIOS keyboard and serial
	call idle
	jmp .rawnext		; check again -->

.rawkey:
	xor ax, ax
	int 16h			; get the key and scancode
.return:
	retn


getc_dos:		; DOS character devices handle one-byte characters. Therefore
			; non-ASCII keys cannot be returned with scancode in the high
			; byte of the same call. A non-ASCII key will be split into
			; two characters by CON: one NUL byte followed by the scancode.
%if _INPUT_FILE_HANDLES
.:
	testopt [internalflags2], dif2_input_file
	jz .file_not
	push dx
	push cx
	push bx
	push di
	xor ax, ax		; initialise ah to zero
	push ax
	mov dx, sp		; ds:dx -> al byte on stack
	mov ah, 3Fh
	call yy_get_handle
	mov cx, 1
	doscall			; (depends on ds = ss)
	test ax, ax
	jnz .file_got

	call yy_close_file
	pop ax
	pop di
	pop bx
	pop cx
	pop dx
	jmp .


.file_got:
	pop ax			; ah = 0, al = character read
	pop di
	pop bx
	pop cx
	pop dx
	retn

.file_not:
%endif
	mov ah, 8
	doscall			; wait for a key
	mov ah, 0		; assume it is ASCII
	or al, al
	jne .return		; ASCII, return with ah zero -->
	mov ah, 8
	doscall			; scancode of non-ASCII key to al
	xchg al, ah		; to ah
	mov al, 0		; return zero for non-ASCII key
.return:
	retn


%if _INPUT_FILE_HANDLES
		; INP:	[input_file_handles], dif2_input_file
		; OUT:	most recent file closed, flag cleared if no longer file
		; CHG:	di, bx, ax
yy_close_file:
	mov di, word [input_file_handles.active]
	shl di, 1
	mov bx, -1
	xchg bx, word [input_file_handles + di]
	mov ah, 3Eh
	doscall
	shr di, 1
	dec di
	jns .next
	clropt [internalflags2], dif2_input_file
	setopt [internalflags2], dif2_closed_input_file
	jmp .done
.next:
	mov word [input_file_handles.active], di
.done:
	retn


		; INP:	-
		; OUT:	di = active handle offset
		;	bx = active handle
		; CHG:	-
yy_get_handle:
	mov di, word [input_file_handles.active]
	shl di, 1
	mov bx, word [input_file_handles + di]
	retn
%endif


		; INP:	-
		; OUT:	NZ if received any,
		;	 al = character
		;	 ah = scan code or zero
		;	ZR if none received
		; CHG:	ax
		; STT:	ds = ss = debugger segment/selector
getc_if_any:
	testopt [options], enable_serial
	jz @F			; do BIOS keyboard or DOS getc -->

	call serial_receive_char ; do serial getc (check rx buffer)
	jz .return		; no data, go and idle -->
	mov ah, 0
	retn

@@:
	call InDos
	jnz .bios

%if _INPUT_FILE_HANDLES
	testopt [internalflags2], dif2_input_file
	jz .file_not

	push di
	push bx
	push cx
	push dx
	xor ax, ax
	push ax

	mov dx, sp		; ds:dx -> al byte on stack
	mov ah, 3Fh
	call yy_get_handle
	mov cx, 1		; buffer length = 1
	doscall			; DOS read file (depends on ds = ss)
	test ax, ax
	pop ax
	pop dx
	pop cx
	pop bx
	pop di
	retn			; ZR if no character read

.file_not:
%endif
	push dx
	mov ah, 06h
	mov dl, -1
	doscall
	jz .return_dx		; none available
	call getc_dos
	pop dx
	jmp .return_NZ
.return_dx:
	pop dx
	retn

.bios:
	mov ah, 01h
	int 16h			; key available ?
	jz .return
	xor ax, ax
	int 16h
.return_NZ:
	push ax
	or al, 1		; (NZ)
	pop ax
.return:
	retn


		; INP:	-
		; OUT:	-
		; CHG:	ax
		; STT:	ds = ss = debugger segment/selector
		;
		; Idle system, using 2F.1680 (in given mode), or 2F.1680
		; (calling down to 86 Mode), or sti \ hlt.
idle:
%if _BOOTLDR
	testopt [internalflags], nodosloaded
	jnz .hlt		; can't call 2F -->
%endif
	mov ax, 1680h
	int 2Fh			; release timeslice in multitasker
	test al, al
	jz .return		; done idling -->
%if _PM
	call ispm
	jnz .hlt

	push bx
	push cx
	push es
	_386_PM_o32	; push edi
	push di
_386	xor edi, edi		; clear EDIH
	xor cx, cx		; (copy no words from stack)

[cpu 286]
	push cx			; ss
	push cx			; sp (0:0 = host should allocate a stack)
	sub sp, byte 12		; cs:ip (ignored), segments (uninitialized)
	pushf
	push cx			; EAXH (uninitialized)
	push 1680h		; AX
	sub sp, byte 12		; ecx, edx, ebx (uninitialized)
	push cx
	push cx			; reserved (zero)
	sub sp, byte 12		; ebp, esi, edi (uninitialized)
	push ss
	pop es
	mov di, sp		; es:(e)di -> 86 Mode call structure
	mov ax, 0300h
	mov bx, 2Fh		; bl = interrupt, bh = reserved (zero)
	int 31h			; call real mode 2F.1680
__CPU__

	add sp, byte 28		; discard RM call structure
	pop ax			; get AX
	add sp, byte 20		; discard RM call structure

	_386_PM_o32	; pop edi
	pop di
	pop es
	pop cx
	pop bx

	test al, al
	jz .return		; done idling -->
%endif
.hlt:
	testopt [options], nohlt
	jnz .return
%if _PM
 %if (protectedmode|dpminohlt)&~0FF00h
  %error Option bits re-ordered, adjust code here
 %endif
	mov al, byte [internalflags+1]
	and al, (protectedmode|dpminohlt)>>8
	xor al, (protectedmode|dpminohlt)>>8
	jz .return		; DPMI host throws GPF when we execute hlt -->
%endif
	sti
	hlt			; else idle by hlt
	nop
.return:
	retn


;	GETLINE - Print a prompt (address in DX, length in CX) and read a line
;	of input.
;	GETLINE0 - Same as above, but use the output line (so far), plus two
;	spaces and a colon, as a prompt.
;	GETLINE00 - Same as above, but use the output line (so far) as a prompt.
;	Entry	CX	Length of prompt (getline only)
;		DX	Address of prompt string (getline only)
;
;		DI	Address + 1 of last character in prompt (getline0 and
;			getline00 only)
;
;	Exit	AL	First nonwhite character in input line
;		SI	Address of the next character after that
;	Uses	AH,BX,CX,DX,DI

getline0:
	mov ax, 32<<8|32	; add two spaces and a colon
	stosw
	mov al, ':'
	stosb
getline00:
	mov dx, line_out
	mov cx, di
	sub cx, dx

getline:	; note: this entry is no longer used
	mov word [promptlen], cx	; save length of prompt
	mov byte [linecounter], 0	; reset counter
	clropt [internalflags], promptwaiting

	call InDos
	jnz getline_nofile	; InDOS, not reading from a file -->
%if _INPUT_FILE_HANDLES
	testopt [internalflags2], dif2_input_file
	jnz @F
%endif
	testopt [options], enable_serial
	jnz getline_nofile
	cmp byte [notatty], 0	; check this weird flag
	je getline_nofile	; not reading from a file -->

@@:
	setopt [internalflags2], dif2_did_getline_file
numdef NEWFULLHANDLING, 1

		; This part reads the input line from a file (in the case of
		; `debug < file').  It is necessary to do this by hand because DOS
		; function 0Ah does not handle EOF correctly otherwise.  This is
		; especially important for DEBUG because it traps Control-C.
	mov word [lastcmd], dmycmd	; disable auto-repeat while reading from a file

%if _NEWFULLHANDLING
	mov di, line_in+3	; read max
%else
	mov di, line_in+2
%endif
	mov si, word [bufnext]
	cmp si, word [bufend]
	jb .char_buffered	; if there's a character already
	call fillbuf
%if _INPUT_FILE_HANDLES
	jnc @F
		; EOF reached. if not input file, quit. else, close input file.
	testopt [internalflags2], dif2_input_file
	jz qq			; if EOF, quit -->

		; si = di
	push di
	call yy_close_file	; close file
	pop di
	mov ax, 13 | 10 << 8	; pretend we read a CR LF sequence
	stosw
	mov word [bufend], di
	dec di
	dec di
%else
	jc qq			; if EOF, quit -->
%endif

@@:
.char_buffered:
%if _NEWFULLHANDLING
	dec di
%endif

		; Discard an LF if the last character read was CR.
	cmp byte [notatty], 13	; last parsed character was CR ?
	jne .no_lf_skip		; no, nothing more to do -->
	cmp byte [si], 10	; first read character is LF ?
	jne .no_lf_skip		; no -->
	inc si			; skip the LF
	inc byte [notatty]	; avoid repeating this
.no_lf_skip:
	call puts		; display prompt (having checked it wasn't EOF)

		; si-> next character in buffer
		; w[bufend]-> behind last valid character of buffer
gl1:
	mov cx, word [bufend]
	sub cx, si		; cx = number of valid characters in buffer
	jz gl3			; if none -->
gl2:
	lodsb
	cmp al, 13
	je gl4
	cmp al, 10
	je gl4			; if EOL -->
	stosb
	loop gl2		; if more valid characters -->

		; The buffer is empty. Fill it again.
gl3:
%if _NEWFULLHANDLING
	inc di
%endif
	call fillbuf
%if _NEWFULLHANDLING
	dec di
%endif
	jnc gl1			; if we have more characters -->
	mov al, 10
%ifn _NEWFULLHANDLING	; should now always have at least one byte free
	cmp di, line_in+LINE_IN_LEN
	jb gl4
	dec si
	dec di
%endif
gl4:
	mov word [bufnext], si
	mov byte [notatty], al	; store 10 or 13 (depending on the kind of EOL)

%if _INPUT_FILE_HANDLES
	testopt [internalflags2], dif2_closed_input_file
	jz @F
	clropt [internalflags2], dif2_closed_input_file
	testopt [internalflags], inputfile | notstdinput
	jnz @F
	mov byte [notatty], 0	; it _is_ a tty
@@:
%endif

	mov al, 13
	stosb			; terminate line for our usage
	mov cx, di
	mov dx, line_in + 2
	sub cx, dx
	call puts		; print out the received line
	dec cx
	mov byte [line_in+1], cl
	jmp short getline_eol	; done

getline_nofile:
	call yy_reset_buf
	call puts		; display prompt
	mov dx, line_in
	testopt [options], enable_serial
	jnz rawinput
	call InDos
	jnz rawinput
	mov ah, 0Ah		; buffered keyboard input
	doscall
getline_eol:
	mov al, 13
	call putc		; fix ZDOS Int21.0A display bug
	mov al, 10
	call putc
	mov si, line_in+2
	jmp skipwhite

rawinput:
	push di
	push ds
	pop es
	mov di, line_in+2
.next:
	call handle_serial_flags_ctrl_c
	call getc.rawnext
	test al, al			; waste?
	je .next
	cmp al, 0E0h
	je .next			; waste -->
	cmp al, 08h
	je .del
	cmp al, 7Fh
	je .del				; backspace -->
	cmp al, 13
	je .done
	cmp di, line_in+LINE_IN_LEN-1	; buffer filled?
	jb .store			; no, store -->
	mov al, 7
	db __TEST_IMM8			; skip to .put; beep!
.store:
	stosb				; store character
.put:
	call putc
	jmp short .next

.done:
	stosb				; store the CR (there always is room)
	xchg ax, di			; -> behind CR
	sub al, ((-section.DATASTACK.vstart+100h+ldebug_data_entry_size \
				+asmtable1_size+asmtable2_size) \
		 +line_in+3) & 0FFh	; length of string, excluding CR
		; (This instruction disregards the unnecessary higher byte.)
	pop di				; restore di
	mov byte [line_in+1], al	; store the length byte
	jmp short getline_eol

.del:
	cmp di, line_in+2
	jz .next
	dec di
	call fullbsout
	jmp short .next

fullbsout:
	mov al, 8
	call putc
	mov al, 32
	call putc
	mov al, 8
	jmp putc

		; Fill input buffer from file.
		;
		; INP:	di-> first available byte in input buffer
		; OUT:	CY if DOS returned an error or EOF occured
		;	NC if no error
		;	si = di
		; CHG:	-
fillbuf:
	call handle_serial_flags_ctrl_c
	push ax
	push bx
	push cx
	push dx
	mov si, di		; we know this already
	mov ah, 3Fh		; read from file
	xor bx, bx		; bx = handle (0 is STDIN)
%if _INPUT_FILE_HANDLES
	testopt [internalflags2], dif2_input_file
	jz @F			; if not input file -->
	push di
	call yy_get_handle	; bx = handle
	pop di
@@:
%endif
	mov cx, line_in+LINE_IN_LEN
	mov dx, di
	sub cx, di
	jbe .ret_cy		; if no more room -->
	doscall
	jc .ret_cy		; if error -->
	test ax, ax
	jz .ret_cy		; if EOF -->
	add dx, ax		; -> behind last valid byte
	db __TEST_IMM8		; (NC)
.ret_cy:
	stc
	mov word [bufend], dx	; -> behind last valid byte
	pop dx
	pop cx
	pop bx
	pop ax
	retn


%ifn _INPUT_FILE_HANDLES
yy equ error
%else
yy:
	call InDos
	jz @F
	mov dx, msg.yy_no_dos
	jmp .disp_error_1

@@:
	dec si
	mov bx, si		; -> start of name
	mov di, si		; -> start of name
	lodsb			; load character
	call iseol?
	jne @F
	mov dx, msg.yy_requires_filename
.disp_error_1:
	call putsz
	jmp cmd3

@@:
.unquoted_loop:
	cmp al, 32		; blank or EOL outside quoted part ?
	je .blank
	cmp al, 9
	je .blank
	call iseol?		; (includes semicolon in lDebug)
	je .blank		; yes -->
	cmp al, '"'		; starting quote mark ?
	je .quoted		; yes -->
	stosb			; store character
.unquote:
	lodsb			; load character
	jmp .unquoted_loop	; continue in not-quoted loop -->

.quoted_loop:
	cmp al, 13		; EOL inside quoted part ?
	je .quoted_eol
	cmp al, 0
	je .quoted_eol		; if yes, error -->
	cmp al, '"'		; ending quote mark ?
	je .unquote		; yes -->
	stosb			; store character
.quoted:
	lodsb			; load character
	jmp .quoted_loop	; continue in quoted loop -->

.empty:
	mov dx, msg.yy_filename_empty
	jmp .disp_error_1

.quoted_eol:
	mov dx, msg.yy_filename_missing_unquote
	jmp .disp_error_1

.blank:
	; mov byte [si - 1], 0	; terminate (shouldn't be needed)
	mov byte [di], 0	; terminate
	cmp bx, di		; empty ?
	je .empty		; yes -->
				; done
	call chkeol

	testopt [internalflags2], dif2_input_file
	jz @F
	cmp word [input_file_handles.active], _INPUT_FILE_HANDLES - 1
	jb @F

	mov dx, msg.yy_too_many_handles
	jmp .disp_error_1

@@:
	mov di, bx

		; INP:	ds:di -> filename
		; OUT:	File opened,
		;	 bx = file handle
		; STT:	ds = es = ss = debugger data selector/segment
yy_open_file:
	call .setup_opencreate			; ds:si -> pathname
	mov ax, 716Ch				; LFN open-create
	push di
	xor di, di				; alias hint
	stc
	doscall
	pop di
	jnc .got		; LFN call succeeded -->

		; Early case for no-LFN-interface available.
	; cmp ax, 1
	; je .try_sfn
	cmp ax, 7100h
	je .try_sfn

		; Only now, we check whether the used drive supports LFNs.
		; If it does, then we treat the error received as an
		; actual error and cancel here. If not, the SFN function
		; is called next as a fallback.
		;
		; We cannot rely on specific error returns like the
		; expected 7100h CY (or 7100h CF-unchanged) or the similar
		; 0001h CY (Invalid function) because no one agrees on what
		; error code to use.
		;
		; dosemu returns 0003h (Path not found) on FATFS and
		; redirected-non-dosemu drives. But may be changed so as to
		; return 0059h (Function not supported on network).
		; MSWindows 98SE returns 0002h (File not found) on
		; DOS-redirected drives.
		; DOSLFN with Fallback mode enabled supports the call (albeit
		; limited to SFNs).
		;
		; To suss out what the error means, check LFN availability.
		;
		; Refer to https://github.com/stsp/dosemu2/issues/770
	push ds
	push es
	push di
	push ax
	lframe
	lvar 34, fstype_buffer
	lvar 4, pathname_buffer
	lenter

	push ss
	pop ds
	mov dx, sp		; ds:dx -> ?pathname_buffer
	push ss
	pop es
	mov di, sp		; es:di -> ?pathname_buffer

	lodsw			; load first two bytes of pathname
	cmp ah, ':'		; starts with drive specifier ?
	je @F			; yes -->

	mov ah, 19h
	doscall			; get current default drive
	add al, 'A'		; A: = 0, convert to drive letter
	mov ah, ':'		; drive specifier
@@:
	stosw
	mov ax, '\'		; backslash and zero terminator
	stosw			; es:di -> ?fstype_buffer

	xor ax, ax
	mov cx, 34 >> 1
	push di
	rep stosw		; initialise ?fstype_buffer to all zeros
	pop di			; -> ?fstype_buffer

	mov cx, 32		; size of ?fstype_buffer
	xor bx, bx		; harden, initialise this
	mov ax, 71A0h		; get volume information
	stc
	doscall			; (depends on ds = es = ss)

	jc @F			; if call not supported -->
				; bx = FS flags
	test bh, 0100_0000b	; LFN interface available ?
	stc			; if no
	jz @F			; no -->

	clc			; is available
@@:

	lleave
	pop ax			; (restore error code)
	pop di
	pop es
	pop ds
	jnc .error		; if LFN interface is available, actual error
				; if LFN interface is not available, try SFN

.try_sfn:
	call .setup_opencreate
	mov ax, 6C00h				; Open-create
	stc
	doscall
	jnc .got

	cmp ax, 1
	je .try_old_open
	cmp ax, 6C00h
	jne .error

.try_old_open:
	mov al, bl				; access and sharing modes
	mov ah, 3Dh				; Open
	mov dx, si				; -> filename
	stc
	doscall
	jnc .got

.error:
	mov dx, msg.yy_error_file_open
	jmp yy.disp_error_1

.setup_opencreate:
	mov si, di				; -> filename
	mov bx, 0110_0000_0010_0000b		; Auto-commit, no int 24h
						; DENY WRITE, Read-only
	xor cx, cx				; create attribute
	mov dx, 0000_0000_0000_0001b		; no create / open, no truncate
	retn

.got:
		; ax = file handle
	call yy_reset_buf

	testopt [internalflags2], dif2_input_file
	jnz @F
	setopt [internalflags2], dif2_input_file
	mov word [input_file_handles + 0], ax
	retn

@@:
	inc word [input_file_handles.active]
	mov bx, word [input_file_handles.active]
	shl bx, 1
	mov word [input_file_handles + bx], ax
	retn
%endif


		; INP:	word [bufnext], word [bufend]
		; CHG:	-
		;
		; Note:	When reading from a file, we buffer some of the input
		;	 in line_in. When switching to a non-file, or starting
		;	 to read from another file, we have to reset the seek
		;	 position of the (prior) file to avoid losing the data.
		;	This cropped up during yy development, but actually
		;	 affects serial and InDOS input, too. Therefore,
		;	 this function is not below the conditional for yy.
yy_reset_buf:
	push cx
	mov cx, line_in + 2
	xchg cx, word [bufnext]
	neg cx
	add cx, word [bufend]	; cx = how much remaining in buffer
	mov word [bufend], line_in + 2

	testopt [internalflags2], dif2_did_getline_file
	jz .not_used

	push ax
	push dx
	push bx
	mov dx, cx
	xor cx, cx
	neg dx			; dx = minus how much remaining
	jz @F
	dec cx			; sign extension into cx:dx
@@:

	xor bx, bx		; bx = handle (0 is STDIN)
%if _INPUT_FILE_HANDLES
	testopt [internalflags2], dif2_input_file
	jz @F			; if not input file -->
	push di
	call yy_get_handle	; bx = handle
	pop di
@@:
%endif
	mov ax, 4201h		; lseek, from current file position
	doscall			; call DOS
	pop bx
	pop dx
	pop ax

	clropt [internalflags2], dif2_did_getline_file

.not_used:
	pop cx
	retn


%include "serialp.asm"
