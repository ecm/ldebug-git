
%if 0

lDebug R commands - Register access

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2012 C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif


	usesection lDEBUG_CODE

		; R command - dump and manipulate registers.
rr:
	call iseol?
	je dumpregs		; if no parameters -->

%if !_ONLYNON386 || (_OPTIONS || _VARIABLES) || _MMXSUPP || _RN
	push ax
	push si
	call skipwhite
	call iseol?		; line ends after single character ?
	pop si
	pop ax
	jne short rr1		; no, not other kinds of dumps -->

	call uppercase
	cmp al, 'F'		; only valid input to a later check
	je rr2.writeprompt	; so go there -->
				; (note that byte [si-1] must != '.')
%if _OPTIONS || _VARIABLES
	cmp al, 'V'
	je dumpvars
%endif
%if _MMXSUPP
	cmp al, 'M'
	jne .notrm
	cmp byte [has_mmx], 0
	je .notrm
	jmp dumpregsMMX
.notrm:
%endif
_386	cmp al, 'X'
_386	je short rrx
%if _RN
	cmp al, 'N'
	jne .notrn
	cmp byte [has_87], 0
	je .notrn
	jmp dumpregsFPU
.notrn:
%endif
	jmp error		; all other single characters are invalid
%endif

%ifn _ONLYNON386
rrx:
	lodsb
	call chkeol
	xoropt [options], dispregs32
	mov dx, msg.regs386
	call putsz
	mov dx, msg.regs386_on
	testopt [options], dispregs32
	jnz .on
	mov dx, msg.regs386_off
.on:
	jmp putsz
%endif

rr1:
	mov bp, si		; (bp-1)-> potential name
	call isvariable?
	jc rr2
	mov al, cl
	mov cl, 0
	dec bp			; -> name
	dec si			; (to reload al)
	 push ax		; h = variable's field type, l = its size
	xchg cl, ch		; cx = variable's name's length

	call skipcomma
	cmp al, '.'		; special ?
	je short .writeprompt	; yes -->
	call iseol?
	jne short .noprompt
.writeprompt:
	 push si		; -> behind dot if any
	mov si, bp		; -> name
	mov di, line_out
.writename:
	lodsb
	call uppercase
	stosb
	loop .writename
	 pop si			; -> behind dot if any
	mov al, 32
	stosb
	 pop cx 		; h = variable's field type, l = its size
	xchg bx, dx
	mov ax, word [bx]
	xchg bx, dx
	 push cx
	cmp cl, 4
	jb .pnohigh
	call hexword		; display high word
	jmp @F
.pnohigh:
	cmp cl, 3
	jb @F
	call hexbyte
@@:
	mov ax, word [bx]
	cmp cl, 2
	jb .pnobyte
	xchg al, ah
	call hexbyte		; display high byte
	xchg al, ah
.pnobyte:
	call hexbyte		; display low byte
	call dot_prompt
	 pop cx			; h = variable's field type, l = its size
	je .return
	db __TEST_IMM8		; (skip pop)
.noprompt:
	 pop cx			; h = variable's field type, l = its size
	test ch, ch
	jnz .readonly
	mov byte [rrtype], cl
	 push bx
	 push dx
	push cx
	push ax
	push si
	call isoperator?
	jne .nooperator
	mov bx, cx
	add bx, bx
	push ax
	call near [operatordispatchers+bx]
	pop ax
	test bx, bx
	jnz .gotoperator
.nooperator:
	mov bx, OPERATOR_RIGHTOP; set default computation function
	db __TEST_IMM8
.gotoperator:
	lodsb

	call isassignmentoperator?
	jnc .assign_op_done	; found an assignment operator -->
	cmp bx, OPERATOR_RIGHTOP; dummy (no operator specified) ?
	je .assign_op_done	; yes, assignment operator not required -->
	pop cx
	pop ax			; restore al, si in front of operator
	push ax
	push cx
	xchg si, cx
	push cx
	call isunaryoperator?	; is this a valid unary operator too ?
	pop cx
	je .nooperator		; yes -->
	xchg si, cx
errorj9: equ $
	jmp error		; error where the assignment operator needs to be
.assign_op_done:
	pop cx
	pop cx
	pop cx
	 push bx
	call skipcomm0

	call getexpression	; bx:dx = expression result
	call chkeol
	pop cx			; operator computation function
	pop bp			; bp-> high word
	pop di			; di-> low word
	push word [bp]
	push word [di]
	pop word [hhvar]
	pop word [hhvar+2]	; save variable's current value (as left operand)
	mov byte [hhtype], ah	; type info
	cmp byte [rrtype], 4
	jae .cleardword
	cmp byte [rrtype], 2
	ja .clearthreebyte
	je .clearword
.clearbyte:
	mov byte [hhvar + 1], 0	; clear second byte
.clearword:
	mov byte [hhvar + 2], 0	; clear third byte
.clearthreebyte:
	mov byte [hhvar + 3], 0	; clear fourth byte
.cleardword:
	xchg cx, bx
	add bx, bx
	mov bx, word [bx + operatorfunctions]
	xchg cx, bx		; cx = operator function
	call cx			; compute

		; This should check whether the computed number fits.
	cmp byte [rrtype], 2
	jb .setbyte
	je .setword
	cmp byte [rrtype], 4
	jb .setthreebyte
.setdword:
	mov byte [bp + 1], bh	; set fourth byte
.setthreebyte:
	mov byte [bp], bl	; set third byte
.setword:
	mov byte [di + 1], dh	; set second byte
.setbyte:
	mov byte [di], dl	; set first byte
.return:
rr1b:
	retn

rr1.readonly:
	mov dx, msg.readonly
	jmp putsz

		; Change flag register with mnemonics - F
rr2:
	call uppercase
	cmp al, 'F'
	jne rr3			; if not 'F' -->
	push ax
	mov al, byte [si]
	call isseparator?	; valid separator ?
	pop ax
	jne rr3			; no -->
.ef:
	call skipcomma
	cmp al, '.'		; special ?
	je .writeprompt		; yes -->
	call iseol?		; end of line ?
	jne .noprompt		; no -->
.writeprompt:
	push si
	call dmpflags
	pop si			; -> behind dot if any
	call dot_prompt
	je short rr1b		; if no change
.noprompt:

	call isassignmentoperator?
	push si
	jnc .noteol		; at least one value is required -->
.check_loop:
	call skipcomm0
	call iseol?
	je .really		; return if done
.noteol:
	call uppercase
	xchg al, ah
	lodsb
	call uppercase
	xchg al, ah		; ax = mnemonic
	mov di, flagson
	mov cx, 16
	repne scasw
	jne short .errordec	; if no match
	lodsb
	call isseparator?
	je .check_loop
.errordec2:
	dec si
.errordec:
errordec: equ $
	dec si			; back up one before flagging an error
	jmp error

.really:
	pop si
	dec si
	lodsb
.loop:
	call iseol?
	je rr1b			; return if done

	call uppercase
	xchg al, ah
	lodsb
	call uppercase
	xchg al, ah		; ax = mnemonic

	mov di, flagson
	mov cx, 16
	repne scasw
	jne short .errordec	; if no match
	cmp di, flagsoff
	ja .clear		; if we're clearing
	mov ax, word [di-(flagson-flagbits)-2]
	or word [reg_efl], ax	; set the bit
	jmp short .common
.clear:
	mov ax, word [di-(flagsoff-flagbits)-2]
	not ax
	and word [reg_efl], ax	; clear the bit
.common:
	lodsb
	call isseparator?
	jne short .errordec2
	call skipcomm0
	jmp short .loop

		; Change flag register with mnemonics - EF
rr3:
	xchg al, ah
	lodsb
	call uppercase
	xchg al, ah		; ax = next two characters
_386	cmp ax, "EF"
_386	jne rr4			; if not "EF" -->
_386	push ax
_386	mov al, byte [si]
_386	call isseparator?	; valid separator ?
_386	pop ax
_386	je rr2.ef

		; Change a single flag with mnemonic
rr4:
	mov di, flagnames
	mov cx, 8
	repne scasw
	jne short rr2.errordec
	mov dx, ax
	lodsb
	call isseparator?
	jne short rr2.errordec2
	push word [di-(flagnames-flagbits)-2]
	call skipcomm0
	cmp al, '.'		; special ?
	je .writeprompt		; yes -->
	call iseol?
	jne .noprompt
.writeprompt:
	mov di, line_out
	mov ax, dx
	stosw
	pop ax
	push ax
	test word [reg_efl], ax	; is it off ?
	mov ax, " 0"		; assume so
	jz .off			; it is off -->
	inc ah			; is on, set to '1'
.off:
	stosw
	call dot_prompt
	je .ret_pop		; if no change -->
.noprompt:
	call iseol?		; end of line ?
	je .ret_pop		; yes, no change requested -->
	push cx
	push ax
	push si
	call isoperator?
	jne .nooperator
	mov bx, cx
	add bx, bx
	push ax
	call near [operatordispatchers+bx]
	pop ax
	test bx, bx
	jz .nooperator
	cmp bl, OPERATOR_BOOL_AND
	ja .nooperator
	cmp bl, OPERATOR_BOOL_OR
	jae .gotoperator
	add bl, OPERATOR_BOOL_OR - OPERATOR_BIT_OR
	cmp bl, OPERATOR_BOOL_OR
	jae .gotoperator
.nooperator:
	mov bx, OPERATOR_RIGHTOP; set default computation function
	db __TEST_IMM8
.gotoperator:
	lodsb

	call isassignmentoperator?
	jnc .assign_op_done	; found an assignment operator -->
	cmp bx, OPERATOR_RIGHTOP; dummy (no operator specified) ?
%if 1	; since | ^ & are never unary operators
	jne error
%else
	je .assign_op_done	; yes, assignment operator not required -->
	pop cx
	pop ax			; restore al, si in front of operator
	push ax
	push cx
	xchg si, cx
	push cx
	call isunaryoperator?	; is this a valid unary operator too ?
	pop cx
	je .nooperator		; yes -->
	xchg si, cx
	jmp error		; error where the assignment operator needs to be
%endif
.assign_op_done:
	pop cx
	pop cx
	pop cx
	 push bx
	call getexpression
	call chkeol
	call toboolean
	 pop cx			; operator index
	xor ax, ax
	mov byte [hhtype], al
	mov word [hhvar+2], ax
	 pop si
	 push si		; flag
	test word [reg_efl], si
	jz .notset
	inc ax
.notset:
	mov word [hhvar], ax
	xchg cx, bx
	add bx, bx
	mov bx, word [bx+operatorfunctions]
	xchg cx, bx		; cx = operator function
	call cx			; compute
	pop ax
	test dx, dx
	jz .clear
	or word [reg_efl], ax	; set the bit
	retn
.clear:
	not ax
	and word [reg_efl], ax	; clear the bit
	retn

.ret_pop:
	pop ax
	retn


		; INP:	di-> behind prompt to display (in line_out)
		;	Iff byte[si-1] == '.',
		;	 only display without actual prompting
		;	 si-> line to check for EOL
		; OUT:	NZ iff actually prompted and got non-empty line,
		;	 al = first character
		;	 si-> next character
		;	 dx, bx preserved
		; CHG:	ax, cx, si, di, dx, bx
dot_prompt:
	cmp byte [si-1], '.'	; syntax for display without prompt ?
	je .onlydisplay		; yes -->
	push bx
	push dx
	call getline0
	pop dx
	pop bx
	call iseol?		; no change requested ?
	je .ret			; yes --> (ZR)
	cmp al, '.'		; other syntax for no change ?
	jne .ret		; no --> (NZ)
.chkeol1:
	lodsb
	jmp chkeol		; (ZR)

.onlydisplay:
	call .chkeol1
	call putsline_crlf
	cmp al, al		; ZR
.ret:
	retn


		; INP:	al = first character
		;	si-> remaining string
		; OUT:	CY if no assignment operator was found
		;	NC if an assignment operator was found,
		;	 al = first character behind it (skipcomma called)
		;	 si-> remaining string behind character al
isassignmentoperator?:
	cmp al, ':'
	jne .checksingleequality
	lodsb
	cmp al, '='
	je .skip		; long form assignment operator -->
		; A single colon. Report "no assignment operator" here.
	dec si
	mov al, ':'		; restore si, al
.return_cy:
	stc
	retn

.checksingleequality:
	cmp al, '='
	jne .return_cy		; no assignment operator -->
.skip:
	call skipcomma
	clc
	retn


		; DUMPREGS - Dump registers.
		;
		; 16 bit: 8 regs, line break, first 4 segment regs, IP, flags
		; 32 bit: 6 regs, line break, 2 regs, flags, line break, 6 segment regs, EIP
dumpregs:
	mov si, reg16names
	mov di, line_out
	mov cx, 8			; display all 8 standard regs (16-bit)
	testopt [options], dispregs32
	jz .firstrow16
	mov cl, 6			; room for 6 standard regs (32-bit) only
.firstrow16:
	pushf
	push di
	call dmpr1			; display first row
	call trimputs
	pop di				; (reset di)
	popf				; (reset ZF)
	jnz .secondrow32
	mov cl, 4			; display 4 segment regs
	call dmpr1
	add si, byte 2*2		; skip FS+GS
	inc cx				; (= 1)
	call dmpr1			; display IP
	call dmpflags			; display flags in 16-bit display
	jmp short .lastrowdone
.secondrow32:
	push di
	mov cl, 2			; display rest of 32-bit standard regs
	call dmpr1
	push si
	call dmpflags			; display flags in 32-bit display
	call putsline_crlf
	pop si
	pop di				; (reset di)
	mov cl, 6			; display all segment registers
	call dmpr1
	inc cx				; (= 1)
	call dmpr1			; display EIP
.lastrowdone:
	call trimputs

		; Set U address to CS:(E)IP.
	mov si, reg_eip
	mov di, u_addr
	push si
	push di
	movsw
	movsw
	mov ax, word [reg_cs]
	stosw
	mov ax, DIS_F_REPT | DIS_F_SHOW
	testopt [options], rr_disasm_no_rept
	jz @F
	and al, ~ DIS_F_REPT
@@:
	testopt [options], rr_disasm_no_show
	jz @F
	and al, ~ DIS_F_SHOW
@@:
	mov word [disflags], ax
	call disasm

		; Reset U offset to (E)IP.
	pop di
	pop si
	movsw
_386_PM	movsw
	retn


		; Function to display multiple register entries.
		;
		; INP:	[options]&dispregs32 = whether to display 32-bit registers,
		;				except segment registers which are always 16-bit
		;	si-> 2-byte register name in table
		;	cx = number of registers to display
		; OUT:	si-> register name in table after the last one displayed
		;	cx = 0
		; CHG:	bx, ax
dmpr1:
.:
	lea bx, [si-(reg16names+DATASECTIONFIXUP)]
	add bx, bx			; index * 4
	cmp byte [si+1], 'S'		; segment register ?
	je .no_e			; always 16-bit --> (ZR)
	testopt [options], dispregs32	; display 32-bit register ?
	jz .no_e			; no --> (ZR)
	mov al, 'E'
	stosb				; store E for Exx register name
.no_e:
	movsw				; store register name, increase pointer
	mov al, '='
	stosb				; store equality sign
	jz .no_high			; (ZF left from before)
	mov ax, word [regs + bx + 2]
	call hexword			; store high word (only if 32-bit register)
.no_high:
	mov ax, word [regs + bx]
	call hexword			; store low word
	mov al, 32
	stosb				; store space
	loop .
	retn
			; Note:	This code doesn't use 386+ registers to display our internal
			;	variables for these. Currently, setting the RX bit of options
			;	will display the 32-bit variables even on non-386 machines.
			;	Changing this code to require EAX would require changes to our
			;	check too.
			;	32-bit code probably wouldn't be much shorter than the current
			;	implementation as well.

%if _RN
		; The layout for FSAVE/FRSTOR depends on mode and 16-/32-bit.

%if 0
	struc FPENV16
.cw:	resw 1	; 00h
.sw:	resw 1	; 02h
.tw:	resw 1	; 04h
.fip:	resw 1	; 06h IP offset
.opc:		; 08h RM: opcode (0-10), IP 16-19 in high bits
.fcs:	resw 1	; 08h PM: IP selector
.fop:	resw 1	; 0Ah operand pointer offset
.foph:		; 0Ch RM: operand pointer 16-19 in high bits
.fos:	resw 1	; 0Ch PM: operand pointer selector
	endstruc; 0Eh

	struc FPENV32
.cw:	resd 1	; 00h
.sw:	resd 1	; 04h
.tw:	resd 1	; 08h
.fip:	resd 1	; 0Ch ip offset (RM: bits 0-15 only)
.fopcr:		; 10h (dword) RM: opcode (0-10), ip (12-27)
.fcs:	resw 1	; 10h PM: ip selector
.fopcp:	resw 1	; 12h PM: opcode (bits 0-10)
.foo:	resd 1	; 14h operand pointer offset (RM: bits 0-15 only)
.fooh:		; 18h (dword) RM: operand pointer (12-27)
.fos:	resw 1	; 18h PM: operand pointer selector
	resw 1	; 1Ah PM: not used
	endstruc; 1Ch
%endif


	usesection lDEBUG_DATA_ENTRY

		; dumpregsFPU - Dump Floating Point Registers
fregnames:
	db "CW", "SW", "TW"
	db "OPC=", "IP=", "DP="
msg.empty:	db "empty"
	endarea msg.empty
msg.nan:	db "NaN"
	endarea msg.nan


	usesection lDEBUG_CODE

dumpregsFPU:
	mov es, word [auxbuff_segorsel]
			; => auxbuff
	xor di, di	; -> auxbuff
	mov cx, 128
	xor ax, ax
	rep stosw	; initialise auxbuff
%if _AUXBUFFSIZE < (128 * 2)
 %error auxbuff not large enough for dumpregsFPU
%endif
	mov di, line_out
	mov si, fregnames
	xor bx, bx	; es:bx -> auxbuff
	_386_o32
	fnsave [es:bx]

		; display CW, SW and TW
	push ss
	pop es		; es:di -> line_out
	mov cx, 3
.nextfpr:
	 push ss
	 pop ds		; ds:si -> fregnames entry
	movsw
	mov al, '='
	stosb
	xchg si, bx
	 mov ds, word [auxbuff_segorsel]
			; ds:si -> auxbuff entry
	_386_o32	; lodsd
	lodsw
	xchg si, bx
	push ax
	call hexword
	mov al, 32
	stosb
	loop .nextfpr

		; display OPC
		; in 16-bit PM, there's no OPC
		; in 32-bit PM, there's one, but the location differs from RM
	push bx
%if _PM
	call ispm
	jz .notpm_opc
	add bx, byte 2		; location of OPC in PM differs from RM
_no386	add si, byte 4		; no OPC in 16-bit PM
_no386	jmp short .no_opc
.notpm_opc:
%endif
	 push ss
	 pop ds			; ds:si -> fregnames entry
	movsw
	movsw
	xchg si, bx
	 mov ds, word [auxbuff_segorsel]
				; ds:si -> auxbuff entry
	_386_o32	; lodsd
	lodsw			; skip word/dword
	lodsw
	xchg si, bx
	and ax, 07FFh		; bits 0-10 only
	call hexword
	mov al, 32
	stosb
.no_opc:
	pop bx

		; display IP and DP
	mov cl, 2
.nextfp:
	push cx
	 push ss
	 pop ds			; ds:si -> fregnames entry
	movsw
	movsb
	xchg si, bx
	 mov ds, word [auxbuff_segorsel]
				; ds:si -> auxbuff entry
	_386_o32	; lodsd
	lodsw
	_386_o32	; mov edx, eax
	mov dx, ax
	_386_o32	; lodsd
	lodsw
	xchg si, bx
	 push ss
	 pop ds			; ds:si -> fregnames entry
%if _PM
	call ispm
	jz .notpm_ipdp
	call hexword
	mov al, ':'
	stosb
	jmp short .fppm
.notpm_ipdp:
%endif
	mov cl, 12
	_386_o32	; shr eax, cl
	shr ax, cl
_386	call hexword
_386	jmp short .fppm
	call hexnyb
.fppm:
	_386_PM_o32	; mov eax, edx
	mov ax, dx
_386_PM	call ispm
_386_PM	jz .notpm_fppm
_386_PM	call hexword_high
.notpm_fppm:
	call hexword
	mov al, 32
	stosb
	pop cx
	loop .nextfp

	xchg si, bx
	 push ss
	 pop ds			; ds = es = ss
	call trimputs

		; display ST0..7
	pop bp			; TW
	pop ax			; SW
	pop dx			; CW (discarded here)

	mov cl, 10
	shr ax, cl		; move TOP to bits 1..3
	and al, 1110b		; separate TOP
	mov cl, al
	ror bp, cl		; adjust TW

	mov cl, '0'
.nextst:
	mov di, line_out
	push cx
	mov ax, "ST"
	stosw
	mov al, cl
	mov ah, '='
	stosw
	push di
	test al, 1
	mov al, 32
	mov cx, 22
	rep stosb
	jz .oddst
	mov ax, 10<<8|13
	stosw
.oddst:
	mov al, 0
	stosb			; make it an ASCIZ string
	pop di

	mov ax, bp
	ror bp, 1
	ror bp, 1
	and al, 3		; 00b = valid, 01b = zero, 10b = NaN, 11b = empty
	jz .isvalid
	push si
	 push ss
	 pop ds			; ds = es = ss
	mov si, msg.empty
	mov cl, msg.empty_size
	cmp al, 3
	je .gotst
	mov si, msg.nan
	mov cl, msg.nan_size
	cmp al, 2
	je .gotst
	mov al, '0'
	stosb
	xor cx, cx
.gotst:
	rep movsb
	pop si
	jmp short .regoutdone

.isvalid:
	 mov ds, word [auxbuff_segorsel]
				; ds:si -> auxbuff entry
	testopt [ss:options], hexrn
	jnz .hex
	push di			; -> buffer (first parameter; in es = ss)
	push ds
	push si			; -> auxbuff entry (second parameter)
	call FloatToStr
	jmp short .regoutdone

.hex:
	mov ax, word [si+8]
	call hexword
	mov al, '.'
	stosb
	mov ax, word [si+6]
	call hexword
	mov ax, word [si+4]
	call hexword
	mov ax, word [si+2]
	call hexword
	mov ax, word [si+0]
	call hexword

.regoutdone:
	mov dx, line_out
	 push ss
	 pop ds			; ds = es = ss
	call putsz
	pop cx

	add si, byte 10		; -> next ST
	inc cl
	cmp cl, '8'
	jne .nextst
	 mov es, word [auxbuff_segorsel]
				; es => auxbuff
	_386_o32
	frstor [es:0]
	retn
%endif


		; DMPFLAGS - Dump flags output.
dmpflags:
	mov si, flagbits
	mov cx, 8
.loop:	lodsw
	test ax, word [reg_efl]
	mov ax, word [si+(flagsoff-flagbits)-2]
	jz .off			; if not set
	mov ax, word [si+(flagson-flagbits)-2]
.off:	stosw
	mov al, 32
	stosb
	loop .loop
	dec di			; -> last (unnecessary) blank
	retn


%if _OPTIONS || _VARIABLES
dumpvars:
%if _VARIABLES
	mov si, vregs
%endif
	xor bx, bx
.loop:
	mov di, line_out
	xor dx, dx
%if _VARIABLES
	mov cx, 4
	call .dump		; display four variables
	inc bx			; (would be one off here)
	push si
%else
	add bx, byte 4		; (no motivation to optimize that)
%endif
%if _OPTIONS
 %if _VARIABLES
	mov ax, 32<<8|32
	stosw			; more blanks inbetween
 %endif
	cmp bl, 16
	je .3
	cmp bl, 8
	ja .2
	je .1

		; First line, display DCO and DCS
.0:
	mov ax, "CO"
	mov si, options
	call .dump_option
	mov ax, "CS"
	jmp short .next

		; Second line, DAO and DAS
.1:
	mov ax, "AO"
	mov si, asm_options
	call .dump_option
	mov ax, "AS"
	jmp short .next

		; Third line, DIF and DPI
.2:
	mov ax, "IF"
	mov si, internalflags
	call .dump_option
	mov ax, "PI"
	mov si, psp22
	inc dx
	inc dx
	jmp short .next

		; Fourth line, DPR, DPS (if _PM) and DPP
.3:
	inc dx
	mov ax, "PR"
	mov si, pspdbg
	call .dump_option
 %if _PM
	xor ax, ax
	call ispm
	jnz .3_rm
	push ds
	db __TEST_IMM8		; (skip push)
.3_rm:
	push ax
	mov ax, "PS"
	mov si, sp
	call .dump_options
	pop ax
 %else
	mov ax, 32<<8|32
	stosw
	stosw
 %endif
	mov ax, "PP"
	mov si, parent

.next:
	call .dump_options
%endif
	push bx
	call putsline_crlf	; display line
	pop bx			; (retain counter)
%if _VARIABLES
	pop si			; (retain pointer to next variable)
%endif
	cmp bl, 16		; was end ?
	jne .loop		; no, loop -->
	retn			; done

		; INP:	ax = 2-byte option name ('N' will precede this)
		;	d[si] = value
		; OUT:	si-> behind value
		;	cx = 0
		; CHG:	ax
.dump_options:
%if _VARIABLES
.dump_option:
	mov word [di], " D"
	scasw
%else
	mov byte [di], ' '
	inc di
.dump_option:
	mov byte [di], 'D'
	inc di
%endif
	stosw
%if _VARIABLES		; falls through otherwise, always count 1
	mov cx, 1
	jmp short .dump_one
%endif

%if 0
PM && OPTIONS && VARIABLES
V0=00000000 V1=00000000 V2=00000000 V3=00000000   DCO=00000000 DCS=00000000
V4=00000000 V5=00000000 V6=00000000 V7=00000000   DAO=00000000 DAS=00000000
V8=00000000 V9=00000000 VA=00000000 VB=00000000   DIF=0000840D DPI=0616:01DE
VC=00000000 VD=00000000 VE=00000000 VF=00000000   DPR=0984 DPS=0000 DPP=0616

!PM && OPTIONS && VARIABLES
V0=00000000 V1=00000000 V2=00000000 V3=00000000   DCO=00000000 DCS=00000000
V4=00000000 V5=00000000 V6=00000000 V7=00000000   DAO=00000000 DAS=00000000
V8=00000000 V9=00000000 VA=00000000 VB=00000000   DIF=0000840D DPI=0616:01DE
VC=00000000 VD=00000000 VE=00000000 VF=00000000   DPR=0984     DPP=0616

!OPTIONS && VARIABLES
V0=00000000 V1=00000000 V2=00000000 V3=00000000
V4=00000000 V5=00000000 V6=00000000 V7=00000000
V8=00000000 V9=00000000 VA=00000000 VB=00000000
VC=00000000 VD=00000000 VE=00000000 VF=00000000

!PM && OPTIONS && !VARIABLES
DCO=00000000 DCS=00000000
DAO=00000000 DAS=00000000
DIF=0000840D DPI=0616:01DE
DPR=0984     DPP=0616

PM && OPTIONS && !VARIABLES
DCO=00000000 DCS=00000000
DAO=00000000 DAS=00000000
DIF=0000840D DPI=0616:01DE
DPR=0984 DPS=0000 DPP=0616

!OPTIONS && !VARIABLES
%endif
%if 0
DCO Debugger Common Options
DCS Debugger Common Startup options
DIF Debugger Internal Flags
DPR Debugger Process (Real-mode segment)
DPS Debugger Process Selector, or zero
DPP Debugger Parent Process
DPI Debugger Parent Interrupt 22h
DAO Debugger Assembler/disassembler Options
DAS Debugger Assembler/disassembler Startup options
%endif

%if _VARIABLES
.dump_loop:
	inc bx
	mov al, 32
	stosb
.dump:
	mov al, 'V'
	stosb
	mov al, bl
	call hexnyb
%endif
.dump_one:
	mov al, '='
	stosb
	lodsw
	cmp dl, 1
	je .dumpw
	push ax
	lodsw
	pushf
	call hexword
	popf
	jb .nocolon
	mov al, ':'
	stosb
.nocolon:
	pop ax
.dumpw:
	call hexword
%if _VARIABLES
	loop .dump_loop
%endif
	retn
%endif

%if _MMXSUPP
cpu 586
dumpregsMMX:
	mov ds, word [auxbuff_segorsel]	; => auxbuff
	o32
	fnsave [0]
	mov si, 7*4
	mov cl, '0'
	mov di, line_out
.nextreg:
	mov ds, word [auxbuff_segorsel]	; => auxbuff
	mov ax, "MM"
	stosw
	mov al, cl
	mov ah, '='
	stosw
	push cx
	mov dl, 8
.nextbyte:
	lodsb
	call hexbyte
	mov al, 32
	test dl, 1
	jz .oddbyte
	mov al, '-'
.oddbyte:
	stosb
	dec dl
	jnz .nextbyte
	dec di
	mov ax, 32<<8|32
	stosw
	add si, byte 2
	pop cx
	test cl, 1
	jz .oddreg
	push cx
	 push ss
	 pop ds				; ds = es = ss
	call trimputs
	pop cx
	mov di, line_out
.oddreg:
	inc cl
	cmp cl, '8'
	jne .nextreg
	mov ds, word [auxbuff_segorsel]	; => auxbuff
	o32
	fldenv [0]
	retn
cpu 8086
%endif
