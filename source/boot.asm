
%if 0

lDebug "boot" commands - boot loading

Copyright (C) 2008-2017 C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif


	usesection lDEBUG_DATA_ENTRY

	align 16
load_partition_table:	times 16 * 4 db 0
.end:

	align 16
		; Data passed to us from loader (in case we booted)
loaddata_loadedfrom:
	times (-LOADDATA + bsBPB + ebpbNew + BPBN_size) db 0

	align 16
		; data used to access storage
load_data:
	times (-LOADDATA2 + bsBPB + ebpbNew + BPBN_size) db 0
	align 16
load_current_settings:
	istruc LOADSETTINGS
at lsKernelName,	load_kernelname_default:	dw 0
at lsAddName,		load_addname_default:		dw 0
at lsMinPara,		load_minpara:			dw 0
at lsMaxPara,		load_maxpara:			dw 0
at lsOptions,		load_options:			dw 0
at lsSegment,		load_loadseg:			dw 0
at lsEntry,		load_entrypoint:		dd 0
at lsBPB,		load_bpb:			dd 0
at lsName
	;	iend
	%pop		; (pop off the istruc context)

	align 4
load_bpb_dest:		dd 0
load_sectors:		equ load_data - LOADDATA2 + bsBPB + bpbCHSSectors
load_heads:		equ load_data - LOADDATA2 + bsBPB + bpbCHSHeads
load_sectorsize:	equ load_data - LOADDATA2 + bsBPB + bpbBytesPerSector
load_sectorsizepara:	equ load_data - LOADDATA2 + ldParaPerSector
load_sectorseg:		equ load_data - LOADDATA2 + ldSectorSeg
load_partition_sector:	equ loaddata_loadedfrom - LOADDATA + bsBPB + bpbHiddenSectors
load_partition_cycle:	dw 0
load_lba:		equ load_data - LOADDATA2 + ldHasLBA
load_unit:		equ load_data - LOADDATA2 + bsBPB + ebpbNew + bpbnBootUnit
load_partition:		db 0
load_current_partition:	db 0
load_found_partition:	db 0
load_check_dir_attr:	db 0
load_sector_alt:	db 0

	align 4
load_kernel_name:	times 11 + 1 db 0
	align 2
;	load_kernelname_default:dw 0
		; ASCIZ filename for if load_kernelname_input terminates in '/'
load_kernelname_input:	dw 0
		; input (ASCIZ '/'-terminated pathnames + optional filename)
load_kernelname_next:	dw 0
		; next element in ASCIZ load_kernelname_input,
		;  empty string if pathname terminated in '/'
;	load_addname_default:	dw 0
load_addname_input:	dw 0
load_addname_next:	dw 0

	align 2
boot_new_memsizekib:	dw 0
boot_old_memsizekib:	dw 0
boot_ebdaflag:	db 0		; EBDA flag: non-zero if present

	align 2
load_readwrite_sector:	dd 0
load_readwrite_buffer:	dw 0
load_readwrite_count:	dw 0
load_readwrite_function:dw 0


	usesection lDEBUG_CODE

bootcmd:
%if _PM
	call ispm
	jnz .rm
	mov dx, nopmsupp
	jmp putsz
.rm:
%endif
d4	call d4message
d4	asciz "In bootcmd",13,10

	mov bp, load_data - LOADDATA2
	xor ax, ax
	mov [bp + bsBPB + bpbHiddenSectors + 0], ax
	mov [bp + bsBPB + bpbHiddenSectors + 2], ax
	mov byte [load_sector_alt], al

	call skipcomma
	dec si
	mov dx, msg.list
	call isstring?
	je .list

 %if _DOSEMU
	testopt [internalflags], runningdosemu
	jz .not_dosemu
	mov dx, msg.quit
	call isstring?
	jne .not_dosemu
	xor bx, bx
	mov ax, -1
	int 0E6h			; dosemu quit
@@:
	sti
	hlt
	jmp @B
.not_dosemu:
 %endif

	mov dx, msg.read
	call isstring?
	je boot_read

	mov dx, msg.write
	call isstring?
	je boot_write

	mov dx, msg.protocol
	call isstring?
	jne .notproto

	call skipequals
	dec si
	mov dx, msg.sector
	call isstring?
	je .proto_sector

	mov dx, msg.sector_alt
	call isstring?
	je .proto_sector_alt

	mov bx, loadsettings
.proto_settings_next:
	lea dx, [bx + lsName]
	call isstring?
	je .proto_settings
	add bx, LOADSETTINGS_size
	cmp word [bx], 0
	jne .proto_settings_next

	jmp error

.proto_sector_alt:
	mov byte [load_sector_alt], 1
.proto_sector:
	call skipcomma
	db __TEST_IMM8
.notproto:
	lodsb
	call parseloadunit
	jnc .load

	db __TEST_IMM8		; skip dec
.errordec:
	dec si
.error:
	db __TEST_IMM8		; skip pop
.errorpop:
	pop si
	jmp error


.proto_settings:
	push si
	mov si, bx
	mov di, load_current_settings
	mov cx, lsName >> 1
	rep movsw
%if lsName & 1
	movsb
%endif
	push word [load_kernelname_default - (load_current_settings + lsName) + di]
	pop word [load_kernelname_input - (load_current_settings + lsName) + di]
	push word [load_addname_default - (load_current_settings + lsName) + di]
	pop word [load_addname_input - (load_current_settings + lsName) + di]

	pop si

d4	call d4message
d4	asciz "In bootcmd.proto_settings",13,10

.proto_next:
	call skipcomma
	dec si
	mov dx, msg.segment
	call isstring?
	je .proto_segment
	mov dx, msg.entry
	call isstring?
	je .proto_entry
	mov dx, msg.bpb
	call isstring?
	je .proto_bpb
	mov dx, msg.minpara
	call isstring?
	je .proto_minpara
	mov dx, msg.maxpara
	call isstring?
	je .proto_maxpara

	mov bx, loadoptiontable
.proto_lot_next:
	mov cx, [bx]		; flag
	mov dx, [bx + 2]	; -> ASCIZ message
	test cx, cx
	jz .proto_done
	call isstring?
	je .proto_lot
	add bx, 4
	jmp .proto_lot_next

.proto_lot:
	call skipequals
	mov dx, cx
	not dx
	and word [load_options], dx
	call getexpression
	call toboolean
	test dx, dx
	jz @F
	or word [load_options], cx
@@:
	dec si
	jmp .proto_next

.proto_segment:
	call skipequals
	call getword		; dx = word
	cmp dx, 50h
	jb @F
	mov word [load_loadseg], dx
	dec si
	jmp .proto_next

@@:
	mov dx, msg.boot_segment_too_low
	jmp .fail


.proto_entry:
	call skipequals
	xor bx, bx
	call getaddr		; bx:(e)dx = addr
_386	test edx, 0_FFFF_0000h
_386	jnz .error
	mov word [load_entrypoint], dx
	mov word [load_entrypoint + 2], bx
	dec si
	jmp .proto_next

.proto_bpb:
	call skipequals
	xor bx, bx
	call getaddr		; bx:(e)dx = addr
_386	test edx, 0_FFFF_0000h
_386	jnz .error
	mov word [load_bpb], dx
	mov word [load_bpb + 2], bx
	dec si
	jmp .proto_next

.proto_minpara:
	call skipequals
	call getword		; dx = word
	mov word [load_minpara], dx
	dec si
	jmp .proto_next

.proto_maxpara:
	call skipequals
	call getword		; dx = word
	mov word [load_maxpara], dx
	dec si
	jmp .proto_next

.proto_done:
	mov bx, loadoptiontable.incompatible
@@:
	mov cx, [bx]
	or cx, [bx + 2]
	jz .proto_compatible
	mov dx, word [load_options]
	and dx, cx
	cmp dx, cx
	je .proto_incompatible
	add bx, 4
	jmp @B

.proto_incompatible:
	 push ss
	 pop es
	mov dx, msg.bootfail
	call putsz
	mov dx, msg.boot_cannot_set_both
	call putsz
	 mov ax, word [bx]
	 call .proto_incompatible_get_label
	 call putsz
	mov dx, msg.boot_and
	call putsz
	 mov ax, word [bx + 2]
	 call .proto_incompatible_get_label
	 call putsz
	mov dx, msg.boot_dot_crlf
.putsz_cmd3:
	call putsz
	jmp cmd3

.proto_incompatible_get_label:
	push bx
	mov bx, loadoptiontable
@@:
	mov cx, [bx]
	jcxz .proto_internal_error
	cmp ax, cx
	mov dx, word [bx + 2]
	je @F
	add bx, 4
	jmp @B

@@:
	pop bx
	retn

.proto_internal_error:
	mov dx, msg.boot_internal_error
	jmp .putsz_cmd3


.proto_compatible:
	lodsb
	call parseloadunit
	jc .error
	jz .fn_done		; no filename given, use defaults -->
		; al was = '/' or '\' or first pathname's first character
		; si-> next char
	mov bx, load_kernelname_input
	call .pathname_parse_super

	push si
	push ax
	call skipwh0
	call iseol?
	pop ax
	pop si
	je .fn_done

	mov bx, load_addname_input

	call skipwh0
	cmp al, '/'
	je @F
	cmp al, '\'
	jne .proto_not_double_slash
@@:
	cmp byte [si], '/'
	je @F
	cmp byte [si], '\'
	jne .proto_not_double_slash
@@:
	dec si
	mov word [bx], si
	mov word [si], "//"
	inc si
	inc si
	push si
	call skipwhite
	call chkeol
	pop si
	mov byte [si], 0
	jmp .fn_done

.proto_not_double_slash:
	call .pathname_parse_super

	call skipwh0		; check that no trailing garbage
	call chkeol		;  lives here
	jmp .fn_done


.pathname_parse_super:
d4	call d4message
d4	asciz "In bootcmd.pathname_parse_super",13,10

	call skipwh0
	cmp al, '/'
	je @F
	cmp al, '\'
	jne @FF
@@:
	dec si
	mov word [bx], si
	inc si
	jmp .pathname_check

@@:
	dec si
	mov word [bx], si

.pathname_parse:
.pathname_next:
d4	call d4message
d4	asciz "In bootcmd.pathname_parse",13,10
	call boot_parse_fn
		; al = separator char
		; si -> next char after that (if any)
	cmp al, '/'		; path separator?
	je .pathname_check
	cmp al, '\'
	jne .pathname_none	; no, this was the filename -->
.pathname_check:
d4	call d4message
d4	asciz "In bootcmd.pathname_parse_check",13,10
	mov byte [si - 1], '/'	; normalise path separator
	lodsb
	cmp al, 32		; space ?
	je .pathname_gotfirst
	cmp al, 9
	je .pathname_gotfirst	; yes, allow for second name -->
	dec si
	call iseol?		; EOL ?
	jne .pathname_next	; no, next pathname element -->
	mov byte [si], 0	; terminate after trailing path sep
	retn

.pathname_gotfirst:
.pathname_none:
	mov byte [si - 1], 0	; terminate after filename
	retn


		; DPR:word [load_kernelname_input] -> ASCIZ pathname.
		; if it ends in '/', append DPR:word [load_kernelname_default]
		; DPR:word [load_addname_input] -> ASCIZ pathname.
		; if it ends in '/', append DPR:word [load_addname_default]
		; if it's empty (and no trailing '/' in front of the zero)
		;  then no additional name is given.
.fn_done:
d4	call d4message
d4	asciz "In bootcmd.fn_done",13,10

	testopt [internalflags], load_is_ldp
	jnz .load_kernel_from_ldp

	mov bl, [load_partition]
	cmp byte [load_unit], 80h
	jb .p_f_is_diskette
	test bl, bl		; partition specified ?
	jz .error		; no, error -->

	call query_geometry

	mov cx, load_freedos_from_partition
	call scan_partitions
	mov dx, msg.boot_partition_not_found
	jmp .fail


.p_f_is_diskette:
	test bl, bl		; partition specified ?
	jnz .error		; yes, error -->

	call query_geometry

	xor ax, ax
	xor dx, dx
@@:
	mov bx, word [auxbuff_segorsel]	; bx => auxbuff
	 push dx
	 push ax
	call read_ae_512_bytes

	mov dx, msg.bootfail_sig
	cmp word [es:510], 0AA55h
	jne .fail
	 pop ax
	 pop dx

	jmp load_freedos_common


.load_kernel_from_ldp:
	call query_geometry
	mov ax, word [load_partition_sector]
	mov dx, word [load_partition_sector + 2]
	jmp @B


.load:
	jnz bootcmd.error

	testopt [internalflags], load_is_ldp
	jnz .load_sector_from_ldp

	cmp byte [load_partition], 0
	je .load_boot

	call query_geometry

d4	call d4message
d4	asciz "In bootcmd.load (before call to scan_partitions)",13,10

	mov cx, load_from_partition
	call scan_partitions
	mov dx, msg.boot_partition_not_found
	jmp .fail


.load_sector_from_ldp:
	call query_geometry
	mov ax, word [load_partition_sector]
	mov dx, word [load_partition_sector + 2]
	jmp @F

.load_boot:
	call query_geometry

	xor ax, ax
	xor dx, dx
@@:
	mov bx, 7C0h
d4	call d4dumpregs
d4	call d4message
d4	asciz 13,10,"In bootcmd.load_boot (before call to read_sector)",13,10
	 push dx
	 push ax
	call read_ae_512_bytes
d4	call d4message
d4	asciz "In bootcmd.load_boot (after call to read_sector)",13,10
	xor dx, dx
	mov es, dx

	mov al, byte [load_unit]	; al = boot unit
	mov bx, 7C00h

	mov dx, msg.bootfail_sig
	cmp word [es:7C00h + 510], 0AA55h
	jne .fail

	mov dx, msg.bootfail_code
	cmp word [es:bx], 0
	je .fail

	push ax
	mov cx, 510 / 2
	mov di, 600h			; MBR location
	xor ax, ax
	rep stosw			; initialise (sector and all entries)
	mov ax, 0AA55h
	stosw				; initialise boot sector signature
	mov word [es:600h], 019CDh	; initialise boot sector code
	mov di, 600h + 510 - 4*16	; -> first partition table entry
	pop ax
	 pop word [es:di + piStart]
	 pop word [es:di + piStart + 2]	; = boot sector LBA
	mov byte [es:di + 0], 80h	; "bootable" flag set
	mov byte [es:di + 4], 0FFh	; dummy value for FS type (nonzero)
	mov byte [es:di + 12], 1	; dummy value for length (nonzero)

load_partition_common: equ $
	and word [reg_efl], ~(400h|200h|100h)	; UP, DI, TF=0
	mov word [reg_esi], di
	mov word [reg_ebp], di
	mov word [reg_ds], cx		; ds:si -> 0:600h + offset to first entry
	mov byte [reg_edx], al		; dl = boot unit
	mov word [reg_eip], bx
	mov word [reg_eip + 2], cx
	mov word [reg_cs], cx		; cs:eip = 0:7C00h
	 cmp byte [load_sector_alt], 0
	 je @F
	 mov word [reg_eip], cx
	 mov word [reg_cs], 7C0h	; cs:eip = 07C0h:0
@@:
	mov word [reg_esp], bx
	mov word [reg_esp + 2], cx
	mov word [reg_ss], cx		; ss:esp = 0:7C00h
	setopt [internalflags2], dif2_boot_loaded_kernel
	retn


.fail_read:
	 push ss
	 pop es
	mov di, msg.bootfail_read_errorcode
	mov al, ah
	call hexbyte
	mov dx, msg.bootfail_read

.fail:
	 push ss
	 pop es
	push dx
	mov dx, msg.bootfail
	call putsz
	pop dx
	call putsz
	jmp cmd3


bootcmd.list:
	call skipcomma

	call parseloadunit
	jc bootcmd.error
	jnz bootcmd.error

	call query_geometry

	testopt [internalflags], load_is_ldp
	jnz .list_ldp

	cmp byte [load_partition], 0
	je .listall

	mov byte [load_found_partition], 0
	mov cx, list_single_partition
	call scan_partitions
	cmp byte [load_found_partition], 0
	jne @F
	mov dx, msg.boot_partition_not_found
	jmp bootcmd.fail
@@:
	retn

.listall:
	mov cx, list_any_partition
	jmp scan_partitions

.list_ldp:
	mov byte [load_found_partition], 0
	mov cx, list_partition_if_ldp
	call scan_partitions
	cmp byte [load_found_partition], 0
	jne @F
	mov dx, msg.boot_partition_not_found
	jmp bootcmd.fail
@@:
	retn


list_partition_if_ldp:
d4	call d4message
d4	asciz "In list_partition_if_ldp",13,10

	mov ax, word [bp + di - 8]
	mov dx, word [bp + di - 6]	; root
	add ax, word [es:si + 8]
	adc dx, word [es:si + 8 + 2]	; add partition offset
	cmp word [load_partition_sector], ax
	jne @F
	cmp word [load_partition_sector + 2], dx
	je list_single_partition.gotit
@@:
	retn

list_single_partition:
d4	call d4message
d4	asciz "In list_single_partition",13,10

	mov al, byte [load_current_partition]
	cmp al, byte [load_partition]
	je .gotit
	retn

.gotit:
	inc byte [load_found_partition]

		; INP:	es:si -> partition table entry,
		;	 si = load_partition_table .. load_partition_table+48,
		;	 es = ss
		;	bp + di -> above part table metadata,
		;	 dwo [bp + di - 4] = root (outermost extended position)
		;	 dwo [bp + di - 8] = base (current table position)
		; CHG:	ax, bx, (cx), dx
list_any_partition:
	 push es
	 push cx
	 push si
	 push di

	mov di, line_out	; reset di
	mov al, "u"
	stosb
	mov al, byte [load_unit]
	call hexbyte
	mov al, '.'
	stosb
	mov al, byte [load_current_partition]
	call decbyte
	cmp al, 10
	mov al, 32
	jae @F
	stosb
@@:
	stosb

	mov al, byte [load_unit]
	mov bx, "fd"
	cmp al, 80h
	jb @F
	mov bl, "h"
@@:
	and al, ~80h
	add al, 'a'
	cmp al, 'z'
	jbe @F
	mov al, 32
	mov cx, 3 + 2
	rep stosb
	jmp @FF

@@:
	xchg ax, bx
	stosw
	xchg ax, bx
	stosb
	mov al, byte [load_current_partition]
	call decbyte
	cmp al, 10
	mov al, 32
	jae @F
	stosb
@@:
	stosb

	mov al, byte [si + 4]
	call hexbyte

	mov al, 32
	stosb

	mov cx, di		; (preserve di in line_out)
	 pop di			; get di of scan_partitions
	 push di
	mov ax, word [bp + di - 8]
	mov dx, word [bp + di - 6]	; root
	mov di, cx		; (preserve di in line_out)
	add ax, word [si + 8]
	adc dx, word [si + 8 + 2]	; add partition offset
	xchg ax, dx
	call hexword
	xchg ax, dx
	call hexword

	 push ax
	mov ax, " ("
	stosw
	 pop ax
	  push cx
	  push bx
	mov cx, [load_sectorsize]
	mov bx, 4+4
	call disp_dxax_times_cx_width_bx_size
	 push ax
	mov ax, ") "
	stosw
	 pop ax

	push dx
	push ax

	mov ax, word [si + 12]
	mov dx, word [si + 12 + 2]
	xchg ax, dx
	call hexword
	xchg ax, dx
	call hexword

	 push ax
	mov ax, " ("
	stosw
	 pop ax
	call disp_dxax_times_cx_width_bx_size
	mov al, ")"
	stosb

	pop ax
	pop dx

	  pop bx
	  pop cx

	cmp byte [si + piType], ptLinux
	jne .notlinux

	mov bx, word [auxbuff_segorsel]	; bx => auxbuff
	call read_ae_1536_bytes

	cmp word [es:1024 + 56], 0xEF53	; s_magic == EXT2_SUPER_MAGIC ?
	jne .nolabel

	cmp word [es:1024 + 76 + 2], 0
	jne .nolabel
	cmp word [es:1024 + 76], 1	; s_rev_level == EXT2_DYNAMIC_REV ?
	jne .nolabel

	push es
	pop ds
	mov si, 1024 + 120
	mov cx, 16
	push ss
	pop es

	mov al, 32
	stosb
@@:
	lodsb
	test al, al
	jz @F
	stosb
	loop @B
@@:

	push ss
	pop ds

	pop cx
	pop si
	push si				; get si of scan_partitions
	push cx

.notlinux:
	mov bl, byte [si + piType]
	cmp bl, ptFAT12
	je .isfat
	cmp bl, ptFAT16_16BIT_CHS
	je .isfat
	cmp bl, ptFAT16_CHS
	je .isfat
	cmp bl, ptFAT32_CHS
	je .isfat
	cmp bl, ptFAT32
	je .isfat
	cmp bl, ptFAT16
	jne .notfat
.isfat:

	mov bx, word [auxbuff_segorsel]	; bx => auxbuff
	call read_ae_512_bytes

	cmp word [es:510], 0AA55h
	jne .nolabel
	cmp word [es:bsBPB + bpbBytesPerSector], 0
	je .nolabel
	mov si, bsBPB + bpbNew + bpbnVolumeLabel
	cmp word [es:bsBPB + bpbSectorsPerFAT], 0
	jne @F
	mov si, bsBPB + ebpbNew + bpbnVolumeLabel
@@:
	cmp byte [es:si - bpbnVolumeLabel + bpbnExtBPBSignature], 29h
	jne .nolabel
	mov cx, 11

	push es
	pop ds
	push ss
	pop es

	mov al, 32
	stosb
@@:
	lodsb
	test al, al
	jz @F
	stosb
	loop @B
@@:

	push ss
	pop ds
.notfat:
.nolabel:
	push ss
	pop es
	call putsline_crlf

	 pop di
	 pop si
	 pop cx
	 pop es
	retn


		; INP:	al = first character
		;	si -> next
		; OUT:	CY if no load unit (al not "H" nor "F")
		;	 note: this is barely used!
		;	NC else,
		;	 byte [load_unit] set
		;	 byte [load_partition] set (zero if none specified)
		;	 ZR if no filename specified (at end of input)
		;	 NZ if presumably a filename specified,
		;	  al = first character (slash or whatever non-blank)
		;	  si -> next
		; CHG:	bx, cx, dx, ax, si
parseloadunit:
	clropt [internalflags], load_is_ldp
	call uppercase
	cmp al, 'H'
	je .load_hd
	cmp al, 'F'
	je .load_fd
	cmp al, 'L'
	je .load_ld
	cmp al, 'U'
	je .load_u
.retc:
	stc
	retn

.load_ld:
	lodsb
	call uppercase
	cmp al, 'D'
	jne bootcmd.errordec

d4	call d4message
d4	asciz "In parseloadunit.load_ld",13,10

	mov dl, byte [loaddata_loadedfrom - LOADDATA \
			+ bsBPB + ebpbNew + bpbnBootUnit]
	lodsb
	call uppercase
	cmp al, 'P'
	jne @F

d4	call d4message
d4	asciz "In parseloadunit.load_ld with ldp",13,10

	mov byte [load_unit], dl
	mov byte [load_partition], -1
	setopt [internalflags], load_is_ldp

	call skipwhite
	call iseol?
	jne .fn
	jmp .ret_nc

.load_u:
	lodsb
	cmp al, '('
	jne .u_not_expr
	lodsb
	call getexpression
	call skipwh0
	cmp al, ')'
	lodsb
	jne bootcmd.errordec
	jmp .u_check_dot

.u_not_expr:
	call boot_get_hexadecimal_literal
.u_check_dot:
	cmp al, '.'
	jne bootcmd.error
	lodsb
	test bx, bx
	jnz bootcmd.error
	cmp dx, 256
	jae bootcmd.error
	jmp .got_unit

.load_fd:
	mov dl, 0

d4	call d4message
d4	asciz "In parseloadunit.load_fd",13,10

	db __TEST_IMM16		; skip mov
.load_hd:
	mov dl, 80h
d4	call d4message
d4	asciz "In parseloadunit.load_fd or .load_hd",13,10

	lodsb
	call uppercase
	cmp al, 'D'
	jne bootcmd.errordec
	lodsb
	call uppercase
	sub al, 'A'
	cmp al, 'Z' - 'A'
	ja bootcmd.error
	or dl, al			; hdX: 80h + number, fdX: 0 + number
	lodsb
@@:

.got_unit:
	mov cx, dx
	mov byte [load_unit], cl
	mov byte [load_partition], 0
	cmp al, '/'			; slash ?
	je .fn
	cmp al, '\'
	je .fn				; got a filename -->
	cmp al, 32			; or blank ?
	je @F
	cmp al, 9
	jne .checkeol			; check for EOL -- but no filename
		;  (hdd1name is invalid -- must be hdd1/name or hdd1 name)
@@:					; was blank
	call skipwh0			; skip blanks
	call iseol?			; EOL ?
	jne .fn				; no, is filename -->
					; will jump after this
.checkeol:
	call iseol?			; EOL ?
	je .ret_nc			; yes, no filename -->

d4	call d4message
d4	asciz "In parseloadunit (after no EOL found)",13,10
	push cx
	cmp dl, 80h
	jb bootcmd.error		; diskettes aren't partitioned
	cmp al, '('
	jne .not_expr
	lodsb				; skip opening paren

d4	call d4message
d4	asciz "In parseloadunit (before call to getexpression)",13,10

	call getexpression

d4	call d4message
d4	asciz "In parseloadunit (after call to getexpression)",13,10
	call skipwh0
	cmp al, ')'
	lodsb
	je .got_expr
	jmp bootcmd.errordec

.not_expr:
d4	call d4message
d4	asciz "In parseloadunit (before call to boot_get_decimal_literal)",13,10
	call boot_get_decimal_literal
.got_expr:			; bx:dx = load partition number
d4	call d4message
d4	asciz "In parseloadunit.got_expr",13,10
	pop cx			; cl = load unit
	test bx, bx
	jnz bootcmd.error
	cmp dx, 255
	ja bootcmd.error
	test dx, dx
	jz bootcmd.error
	call skipwh0
	mov byte [load_partition], dl
	mov byte [load_unit], cl
	call iseol?
	jne .fn
.ret_nc:
	xor bx, bx		; NC, ZR
	retn

.fn:
	call skipwh0
	or bx, 1		; NC, NZ
	retn


	align 4

boot_read:
	mov word [load_readwrite_function], read_sector
	jmp boot_readwrite

boot_write:
	mov word [load_readwrite_function], write_sector

boot_readwrite:

d4	call d4message
d4	asciz "In boot_readwrite",13,10

	call skipequals
	call parseloadunit
	jc .error
	jz .error

%if 0
	call skipwh0
	mov bx, word [reg_ds]	; default segment
	call getaddr		; get buffer address into bx:(e)dx

_386	test edx, 0FFFF_0000h
_386	jnz .error

		; (variable must be a dword!)
	mov word [load_readwrite_buffer], dx
	mov word [load_readwrite_buffer + 2], bx
%else
	call getword
		; (variable is a word)
	mov word [load_readwrite_buffer], dx
%endif

	call skipwh0
	call iseol?
	jne @F

d4	call d4message
d4	asciz "In boot_readwrite no sector given no count given",13,10

	and word [load_readwrite_sector], 0
	and word [load_readwrite_sector + 2], 0
	mov dx, 1
	jmp @FF

@@:
	call getexpression	; bx:dx = value

	mov word [load_readwrite_sector], dx
	mov word [load_readwrite_sector + 2], bx

	call skipwh0
	mov dx, 1
	call iseol?
	je @F
	call getword
	call chkeol
@@:
	mov word [load_readwrite_count], dx

	call query_geometry

	testopt [internalflags], load_is_ldp
	jnz .ldp

	cmp byte [load_partition], 0
	je .whole_unit

	mov byte [load_found_partition], 0
	mov cx, .single_partition
	call scan_partitions
;	cmp byte [load_found_partition], 0
;	jne @F
	mov dx, msg.boot_partition_not_found
	jmp bootcmd.fail
;@@:
;	retn


.single_partition:
		; INP:	es:si -> partition table entry,
		;	 si = load_partition_table .. load_partition_table+48,
		;	 es = ss
		;	bp + di -> above part table metadata,
		;	 dwo [bp + di - 4] = root (outermost extended position)
		;	 dwo [bp + di - 8] = base (current table position)
		;; CHG:	ax, bx, (cx), dx
		; CHG:	all

d4	call d4message
d4	asciz "In boot_readwrite.single_partition",13,10

	mov al, byte [load_current_partition]
	cmp al, byte [load_partition]
	je .gotit
	retn

.gotit:
d4	call d4message
d4	asciz "In boot_readwrite.gotit",13,10

;	inc byte [load_found_partition]

	mov ax, [bp + di - 8]
	mov dx, [bp + di - 6]		; base (current table position)

	add ax, [es:si + 8]
	adc dx, [es:si + 8 + 2]		; add offset to logical partition

	mov sp, bp
	pop bp				; restore bp (scan_partitions)
	pop bx				; discard ret address (scan_partitions)
	jmp .gotbase_dxax


.ldp:
	mov ax, word [load_partition_sector]
	mov dx, word [load_partition_sector + 2]

.gotbase_dxax:
	mov word [bp + bsBPB + bpbHiddenSectors + 0], ax
	mov word [bp + bsBPB + bpbHiddenSectors + 2], dx

.whole_unit:
	mov ax, word [load_readwrite_sector]
	mov dx, word [load_readwrite_sector + 2]
	mov cx, word [load_readwrite_count]
	mov bx, word [load_readwrite_buffer]

	jcxz @FF
@@:
	call near word [load_readwrite_function]
	loop @B
@@:
	retn

.error:
	jmp error


		; INP:	ds:si-> first letter of name
		;	es:load_kernel_name-> 12-byte buffer (for fn + 0)
		; CHG:	ax, cx, di
		; OUT:	al = first character after name (EOL, blank, or slash)
		;	si -> next character
boot_parse_fn:
	mov al, 32
	mov di, load_kernel_name
	mov cx, 11
	rep stosb		; initialise to empty

	mov di, load_kernel_name
	mov cx, 9
.loop_name:
	lodsb
	call uppercase
	call iseol?
	je .loop_name_done
	cmp al, 32
	je .loop_name_done
	cmp al, 9
	je .loop_name_done
	cmp al, '/'
	je .loop_name_done
	cmp al, '\'
	je .loop_name_done
	cmp al, '.'
	je .loop_name_ext
	stosb
	loop .loop_name
.invalid:
	mov dx, msg.boot_invalid_filename
	jmp bootcmd.fail

.loop_name_ext:
	cmp cx, 9
	je .invalid
	mov cx, 4
	mov di, load_kernel_name + 8
.loop_ext:
	lodsb
	call uppercase
	call iseol?
	je .loop_ext_done
	cmp al, 32
	je .loop_ext_done
	cmp al, 9
	je .loop_ext_done
	cmp al, '/'
	je .loop_ext_done
	cmp al, '\'
	je .loop_ext_done
	cmp al, '.'
	je .invalid
	stosb
	loop .loop_ext
	jmp .invalid

.loop_ext_done:
	cmp cx, 4
	je .invalid
.loop_name_done:
	cmp cx, 9
	je .invalid
	mov byte [load_kernel_name + 11], 0
	cmp byte [load_kernel_name], 0E5h
	jne @F
	mov byte [load_kernel_name], 05h
@@:
	retn


		; INP:	es:si -> partition table entry,
		;	 si = load_partition_table .. load_partition_table+48,
		;	 es = ss
		;	bp + di -> above part table metadata,
		;	 dwo [bp + di - 4] = root (outermost extended position)
		;	 dwo [bp + di - 8] = base (current table position)
		; CHG:	ax, bx, (cx), dx
load_from_partition:
d4	call d4message
d4	asciz "In load_from_partition",13,10

	mov al, byte [load_current_partition]
	cmp al, byte [load_partition]
	je .gotit
	retn

.gotit:
d4	call d4message
d4	asciz "In load_from_partition.gotit",13,10

	mov ax, [bp + di - 8]
	mov dx, [bp + di - 6]		; base (current table position)

	push dx
	push ax
	push es
	mov bx, 60h
	call read_ae_512_bytes		; load partition table to 0:600h
	pop es
	pop ax
	pop dx

	add ax, [es:si + 8]
	adc dx, [es:si + 8 + 2]		; add offset to logical partition

	mov word [es:si + 8], ax
	mov word [es:si + 8 + 2], dx	; store in partition table entry

	xor cx, cx
	mov es, cx			; es = 0
	lea si, [si - (load_partition_table + DATASECTIONFIXUP) + 600h + (510 - 64)]
					; si = 600h + 510-64 .. 600h + 510-16
	mov word [es:si + 8], ax
	mov word [es:si + 8 + 2], dx	; store in partition table entry

					; dx:ax = absolute sector number
	mov bx, 7C0h			; bx:0 = 7C0h:0 -> boot sector area
	call read_ae_512_bytes		; load partition boot sector to 0:7C00h

	mov sp, bp
	pop bp				; restore bp (scan_partitions)
	pop ax				; discard ret address (scan_partitions)

	mov dx, msg.bootfail_sig
	cmp word [es:510], 0AA55h
	jne bootcmd.fail

	xor cx, cx
	mov dx, msg.bootfail_code
	cmp word [es:0], cx
	je bootcmd.fail

	mov es, cx			; cx = 0, es = 0
	mov di, si			; di -> partition table entry (seg 0)
	or byte [es:di + 0], 80h	; set bootable flag
	mov al, byte [load_unit]	; al = unit
	mov bx, 7C00h			; bx = 7C00h
	jmp load_partition_common


		; INP:	es:si -> partition table entry,
		;	 si = load_partition_table .. load_partition_table+48,
		;	 es = ss
		;	bp + di -> above part table metadata,
		;	 dwo [bp + di - 4] = root (outermost extended position)
		;	 dwo [bp + di - 8] = base (current table position)
		; CHG:	ax, bx, (cx), dx
load_freedos_from_partition:
d4	call d4message
d4	asciz "In load_freedos_from_partition",13,10

	mov al, byte [load_current_partition]
	cmp al, byte [load_partition]
	je .gotit
	retn

.gotit:
d4	call d4message
d4	asciz "In load_freedos_from_partition.gotit",13,10

	mov ax, [bp + di - 8]
	mov dx, [bp + di - 6]		; base (current table position)

	add ax, [es:si + 8]
	adc dx, [es:si + 8 + 2]		; add offset to logical partition

	mov word [es:si + 8], ax
	mov word [es:si + 8 + 2], dx	; store in partition table entry

	mov sp, bp
	pop bp				; restore bp (scan_partitions)
	pop bx				; discard ret address (scan_partitions)

					; dx:ax = absolute sector number
	mov bx, word [auxbuff_segorsel]	; bx => auxbuff
	push ax
	push dx
	call read_ae_512_bytes		; load partition boot sector

	mov dx, msg.bootfail_sig
	cmp word [es:510], 0AA55h
	jne bootcmd.fail

	xor cx, cx
;	mov dx, msg.bootfail_code
;	cmp word [es:0], cx
;	je bootcmd.fail

	pop dx
	pop ax

		; dx:ax = boot sector
		; byte [load_unit] = unit
		; es:0-> read sector
load_freedos_common:
	mov word [es:bsBPB + bpbHiddenSectors], ax
	mov word [es:bsBPB + bpbHiddenSectors + 2], dx

	mov bx, [bp + bsBPB + bpbBytesPerSector]
	cmp bx, [es:bsBPB + bpbBytesPerSector]
	mov dx, msg.bootfail_secsizediffer
	jne bootcmd.fail

		; preserve some variables from our pseudo BPB
	xor ax, ax
	push word [bp + bsBPB + bpbCHSSectors]
	pop word [es:bsBPB + bpbCHSSectors]
	push word [bp + bsBPB + bpbCHSHeads]
	pop word [es:bsBPB + bpbCHSHeads]	; preserve geometry

	mov bx, word [bp + ldParaPerSector]
	shr bx, 1
	mov word [bp + ldEntriesPerSector], bx

	cmp word [es:bsBPB + bpbSectorsPerFAT], ax
	mov bl, byte [bp + bsBPB + ebpbNew + bpbnBootUnit]
	je .is_fat32
	mov byte [es:bsBPB + bpbNew + bpbnBootUnit], bl
	jmp short .was_fat1612
.is_fat32:
	mov byte [es:bsBPB + ebpbNew + bpbnBootUnit], bl
.was_fat1612:

	 push es
	 push ds
	push es
	pop ds
	xor si, si				; -> BPB from boot partition
	push ss
	pop es
	mov di, load_data - LOADDATA2		; -> our copy of a BPB
	mov cx, (bsBPB + ebpbNew + BPBN_size)
	rep movsb				; get the BPB

	 pop ds

	cmp word [bp + bsBPB + bpbSectorsPerFAT], ax
	je @F					; is FAT32 -->
	mov si, load_data - LOADDATA2 + bsBPB + bpbNew
	mov di, load_data - LOADDATA2 + bsBPB + ebpbNew
	mov cx, BPBN_size
	rep movsb				; clone the FAT16 / FAT12 BPBN
						; to where the FAT32 BPBN lives
@@:
	 pop es

	call bootgetmemorysize
	mov word [bp + ldMemoryTop], dx
	sub dx, (20 * 1024) >> 4
		; leave 20 KiB free at the top, to
		; allow loading with the lDOS protocol (needs BPB and FAT seg
		; to live below its destination buffers for these)
	jnc @F
.outofmem:
	jmp query_geometry.out_of_memory_error
@@:

	sub dx, 8192 >> 4
	jc .outofmem
	mov word [bp + lsvFATSeg], dx
	mov ax, -1
	mov word [bp + lsvFATSector], ax
	mov word [bp + lsvFATSector + 2], ax

	push word [load_loadseg]
	pop word [bp + lsvLoadSeg]

	cmp word [load_bpb + 2], -1
	je .auto_bpb

	mov ax, [load_bpb]
	shr ax, 1
	shr ax, 1
	shr ax, 1
	shr ax, 1		; round down: start of BPB
	add ax, [load_bpb + 2]	; start of BPB
	sub ax, (4096 - LOADDATA2 + 15) >> 4	; start of stack area
	push ax
	jc .bpb_too_low

	cmp ax, word [bp + lsvLoadSeg]
	ja .loads_below_bpb

	cmp ax, 50h
	jb .bpb_too_low

	mov ax, [load_bpb]
	add ax, 512 + 15
	shr ax, 1
	shr ax, 1
	shr ax, 1
	shr ax, 1
	add ax, [load_bpb + 2]	; end of BPB / pseudo-boot-sector

	cmp ax, word [bp + lsvLoadSeg]
	jbe .loads_above_bpb

	mov dx, msg.boot_bpb_load_overlap
.fail:
	jmp bootcmd.fail

.bpb_too_low:
	mov dx, msg.boot_bpb_too_low
	jmp .fail


.loads_above_bpb:
	mov ax, dx		; word [bp + ldLoadTop] = word [bp + lsvFATSeg]

.loads_below_bpb:
	mov word [bp + ldLoadTop], ax
	push word [load_bpb + 2]
	pop word [load_bpb_dest + 2]
	push word [load_bpb]
	pop word [load_bpb_dest]
	jmp .got_bpb


		; auto-BPB: allocate BPB at top and load below that
.auto_bpb:
	sub dx, (512 - LOADDATA2 + 15 + 4096) >> 4
	jc .outofmem

	push dx
	mov ax, dx
			; eg dx = 800h
			; want (((800h<<4) + 4096 - LOADDATA2) - 7C00h) >> 4
			; which is 143h
			; which :7C00h = 903h:0
			; dx + ( 4096 - LOADDATA2 - 7C00h) / 16
			; dx - (-4096 + LOADDATA2 + 7C00h) / 16
	mov bx, (-4096 +LOADSTACKVARS)
	add bx, word [load_bpb]
	mov cl, 4
	shr bx, cl
	sub ax, bx
	; sub ax, (-4096 +LOADSTACKVARS + 7C00h) / 16
	jc .outofmem
	; mov word [load_bpb_dest], 7C00h
	push word [load_bpb]
	pop word [load_bpb_dest]
	mov word [load_bpb_dest + 2], ax
	mov word [bp + ldLoadTop], dx

.got_bpb:
	pop ax		; -> stack area
	push es
	mov es, ax
	xor di, di
	xor ax, ax
	mov cx, ((512 - LOADDATA2 + 15 + 4096) & ~15) >> 1
	rep stosw
	pop es


; (boot.asm code starts here)

	xor ax, ax
; calculate some values that we need:
; adjusted sectors per cluster (store in a word,
;  and decode EDR-DOS's special value 0 meaning 256)
	mov al, [bp + bsBPB + bpbSectorsPerCluster]
	dec al
	inc ax
	mov [bp + ldClusterSize], ax

	mov ax, [bp + ldEntriesPerSector]

; number of sectors used for root directory (store in CX)
	xor dx, dx
	mov bx, ax
	dec ax				; rounding up
	add ax, [bp + bsBPB + bpbNumRootDirEnts]	; (0 iff FAT32)
	adc dx, dx			; account for overflow (dx was zero)
	div bx				; get number of root sectors
	xchg ax, cx			; cx = number of root secs


; (iniload.asm code starts here)

	push cx				; number of root secs
	xor ax, ax
; first sector of root directory
	mov al, [bp + bsBPB + bpbNumFATs]	; ! ah = 0, hence ax = number of FATs
	mov cx, word [bp + bsBPB + bpbSectorsPerFAT]
	xor di, di			; di:cx = sectors per FAT
					;  iff FAT12, FAT16
	test cx, cx			; is FAT32 ?
	jnz @F				; no -->
	mov cx, word [bp + bsBPB + ebpbSectorsPerFATLarge]
	mov di, word [bp + bsBPB + ebpbSectorsPerFATLarge + 2]	; for FAT32
@@:
	push ax
	mul cx
		; ax = low word SpF*nF
		; dx = high word
	xchg bx, ax
	xchg cx, dx
		; cx:bx = first mul
	pop ax
	mul di
		; ax = high word adjust
		; dx = third word
	test dx, dx
	jz @F
error_badchain: equ $
	mov dx, msg.boot_badchain
	jmp bootcmd.fail

@@:
	xchg dx, ax
		; dx = high word adjust
	add dx, cx
		; dx:bx = result
	xchg ax, bx
		; dx:ax = result
	jc error_badchain

	add ax, [bp + bsBPB + bpbReservedSectors]
	adc dx, byte 0
	jc error_badchain

	pop cx				; number of root sectors
	xor di, di

; first sector of disk data area:
	add cx, ax
	adc di, dx
	jc error_badchain
	mov [bp + lsvDataStart], cx
	mov [bp + lsvDataStart + 2], di

	mov [bp + ldRootSector], ax
	mov [bp + ldRootSector + 2], dx

; total sectors
	xor dx, dx
	mov ax, [bp + bsBPB + bpbTotalSectors]
	test ax, ax
	jnz @F
	mov dx, [bp + bsBPB + bpbTotalSectorsLarge + 2]
	mov ax, [bp + bsBPB + bpbTotalSectorsLarge]

		; fall through and let it overwrite the field with the
		; already current contents. saves a jump.
@@:
	mov [bp + bsBPB + bpbTotalSectorsLarge + 2], dx
	mov [bp + bsBPB + bpbTotalSectorsLarge], ax

	; dx:ax = total sectors

	mov bx, [bp + bsBPB + bpbSectorsPerFAT]
	mov byte [bp + ldFATType], 32
	test bx, bx
	jz .gotfattype

	xor cx, cx

	mov word [bp + bsBPB + ebpbSectorsPerFATLarge], bx
	mov word [bp + bsBPB + ebpbSectorsPerFATLarge + 2], cx
	mov word [bp + bsBPB + ebpbFSFlags], cx
	; FSVersion, RootCluster, FSINFOSector, BackupSector, Reserved:
	;  uninitialised here (initialised by loaded_all later)

	; dx:ax = total amount of sectors
	sub ax, word [bp + lsvDataStart]
	sbb dx, word [bp + lsvDataStart + 2]

	; dx:ax = total amount of data sectors
	mov bx, ax
	xchg ax, dx
	xor dx, dx
	div word [bp + ldClusterSize]
	xchg bx, ax
	div word [bp + ldClusterSize]
	; bx:ax = quotient, dx = remainder
	; bx:ax = number of clusters
	test bx, bx
	jz @F
.badclusters:
	mov dx, msg.boot_badclusters
	jmp bootcmd.fail

@@:
	cmp ax, 0FFF7h - 2
	ja .badclusters
	mov byte [bp + ldFATType], 16
	cmp ax, 0FF7h - 2
	ja .gotfattype

	mov byte [bp + ldFATType], 12


; (boot.asm code continues here)

; Load the entire FAT into memory. This is easily feasible for FAT12,
;  as the FAT can only contain at most 4096 entries.
; (The exact condition should be "at most 4087 entries", or with a
;  specific FF7h semantic, "at most 4088 entries"; the more reliable
;  and portable alternative would be "at most 4080 entries".)
; Thus, no more than 6 KiB need to be read, even though the FAT size
;  as indicated by word[sectors_per_fat] could be much higher. The
;  first loop condition below is to correctly handle the latter case.
; (Sector size is assumed to be a power of two between 32 and 8192
;  bytes, inclusive. An 8 KiB buffer is necessary if the sector size
;  is 4 or 8 KiB, because reading the FAT can or will write to 8 KiB
;  of memory instead of only the relevant 6 KiB. This is always true
;  if the sector size is 8 KiB, and with 4 KiB sector size it is true
;  iff word[sectors_per_fat] is higher than one.)
		mov di, 6 << 10		; maximum size of FAT12 to load
		mov cx, [bp + bsBPB + bpbSectorsPerFAT]
					; maximum size of this FS's FAT
		xor dx, dx
		mov ax, [bp + bsBPB + bpbReservedSectors]; = first FAT sector
		mov bx, [bp + lsvFATSeg]
@@:
		call read_sector	; read next FAT sector
		sub di, [bp + bsBPB + bpbBytesPerSector]
					; di = bytes still left to read
		jbe @F			; if none -->
					; (jbe means jump if CF || ZF)
		loop @B			; if any FAT sector still remains -->
@@:					; one of the limits reached; FAT read

.gotfattype:

	mov byte [load_check_dir_attr], 0
	mov si, word [load_kernelname_input]
	cmp byte [si], '/'
	jne @F
	inc si
@@:
	cmp byte [si], 0
	jne @F
	mov si, word [load_kernelname_default]
@@:
	 push ss
	 pop es
	call boot_parse_fn	; get next pathname
	cmp al, '/'
	jne @F
	mov byte [load_check_dir_attr], ATTR_DIRECTORY
	mov word [load_kernelname_next], si
@@:

	mov di, -1
	mov si, di
	mov [bp + lsvFATSector], di
	mov [bp + lsvFATSector + 2], si

	xor ax, ax
	xor dx, dx

scan_dir_kernelname_loop:
	mov bx, 500h
	push dx
	push ax
	call scan_dir
	pop ax
	pop dx

	cmp byte [load_check_dir_attr], ATTR_DIRECTORY
	jne got_kernelentry

	push si
	push di
	mov byte [load_check_dir_attr], 0
	mov si, word [load_kernelname_next]
	cmp byte [si], 0
	jne @F
	mov si, word [load_kernelname_default]
@@:
	push es
	 push ss
	 pop es
	call boot_parse_fn	; get next pathname
	pop es
	cmp al, '/'
	jne @F
	mov byte [load_check_dir_attr], ATTR_DIRECTORY
	mov word [load_kernelname_next], si
@@:
	pop di
	pop si

	xor dx, dx
	mov ax, [es:bx + deClusterLow]
				; = first cluster (not FAT32)
	cmp byte [bp + ldFATType], 32
	jne @F
	mov dx, [es:bx + deClusterHigh]
				; dx:ax = first cluster (FAT32)
@@:

	jmp scan_dir_kernelname_loop


got_kernelentry:
	push si
	push di
	push dx
	push ax

	mov byte [load_check_dir_attr], 0
	mov si, word [load_addname_input]
	cmp byte [si], '/'
	jne @F
	xor dx, dx
	pop ax
	pop ax			; discard last-used directory cluster
	push dx
	push dx			; search from root directory
	inc si
	cmp byte [si], '/'
	je got_no_addentry
@@:
	cmp byte [si], 0
	jne @F
	mov si, word [load_addname_default]
	cmp byte [si], 0
	je got_no_addentry
@@:
	 push ss
	 pop es
	call boot_parse_fn	; get next pathname
	cmp al, '/'
	jne @F
	mov byte [load_check_dir_attr], ATTR_DIRECTORY
	mov word [load_kernelname_next], si
@@:
	pop ax
	pop dx
	pop di
	pop si

scan_dir_addname_loop:
	mov bx, 520h	;  0:bx -> space for second directory entry
	call scan_dir

	cmp byte [load_check_dir_attr], ATTR_DIRECTORY
	jne got_addentry

	push si
	push di
	push dx
	push ax
	mov byte [load_check_dir_attr], 0
	mov si, word [load_addname_next]
	cmp byte [si], 0
	jne @F
	mov si, word [load_addname_default]
	cmp byte [si], 0
	je got_no_addentry
@@:
	push es
	 push ss
	 pop es
	call boot_parse_fn	; get next pathname
	pop es
	cmp al, '/'
	jne @F
	mov byte [load_check_dir_attr], ATTR_DIRECTORY
	mov word [load_addname_next], si
@@:
	pop ax
	pop dx
	pop di
	pop si

	xor dx, dx
	mov ax, [es:bx + deClusterLow]
				; = first cluster (not FAT32)
	cmp byte [bp + ldFATType], 32
	jne @F
	mov dx, [es:bx + deClusterHigh]
				; dx:ax = first cluster (FAT32)
@@:

	jmp scan_dir_addname_loop


helper_shift_down_and_clamp:
		mov cx, 4
@@:
		shr dl, 1
		rcr ax, 1
		rcr bx, 1
		loop @B

		or al, dl
		mov dx, bx		; size in paragraphs
		test ax, ax		; > 0FFFFh ?
		jz @F			; no, take actual size -->
		mov dx, 0FFFFh		; clamp to 0FFFFh
@@:
		retn

got_no_addentry:
	mov cx, 16
	xor ax, ax
	mov es, ax
	mov di, 520h	; es:di -> space for second directory entry
	rep stosw	; store zeros

	pop ax
	pop dx
	pop di
	pop si

got_addentry:
	xor ax, ax
	mov es, ax


; (boot32.asm code starts here)

	mov ax, word [bp + ldLoadTop]
	sub ax, word [bp + ldParaPerSector]
	jc load_freedos_common.outofmem
	mov [bp + ldLastAvailableSector], ax

		mov bx, [es:500h + deSize]
		mov ax, [es:500h + deSize + 2] ; ax:bx = file size
		mov dl, [es:500h + 12]	; dl = FAT+ size bits
		mov dh, dl
		and dx, 0E007h		; obtain bits 7-5 and 2-0
		shr dh, 1
		shr dh, 1
		or dl, dh		; dl:ax:bx = file size
		push dx
		push ax
		push bx

		call helper_shift_down_and_clamp
					; round down to next paragraph boundary
		cmp word [load_minpara], dx
		ja error_filetoosmall

		pop bx
		pop ax
		pop dx
		mov cx, [bp + bsBPB + bpbBytesPerSector]
		dec cx			; BpS - 1
		add bx, cx
		adc ax, 0
		adc dl, 0		; round up to next sector
		not cx			; ~ (BpS - 1)
		and bx, cx		; mask to limit to rounded-up sector
		call helper_shift_down_and_clamp

			; dl:ax:bx = size in paragraphs
		mov ax, word [load_maxpara]
		cmp dx, ax
		jbe @F
		mov dx, ax
@@:
		mov word [bp + ldParasLeft], dx
		mov word [bp + ldParasDone], 0

; get starting cluster of file
		xor dx, dx
		mov ax, [es:500h + deClusterLow]
					; = first cluster (not FAT32)

		cmp byte [bp + ldFATType], 32
		jne @F
		mov dx, [es:500h + deClusterHigh]
					; dx:ax = first cluster (FAT32)
@@:

		mov word [bp + lsvFirstCluster], ax
		mov word [bp + lsvFirstCluster + 2], dx

		call check_clust
		jc error_badchain

next_load_cluster:
		call clust_to_first_sector
			; dx:ax = first sector of cluster
			; cx:bx = cluster value
		push cx
		push bx			; preserve cluster number for later

		mov cx, [bp + ldClusterSize]

		mov bx, [bp + lsvLoadSeg]
; xxx - this will always load an entire cluster (e.g. 64 sectors),
; even if the file is shorter than this
@@:
		cmp bx, [bp + ldLastAvailableSector]
		jbe @F
		mov dx, msg.boot_file_too_big_error
		jmp bootcmd.fail

@@:
		push es		; (must preserve ADR_FATBUF reference)
		call read_sector
		pop es
		mov [bp + lsvLoadSeg], bx	; => after last read data

		push ax
		mov ax, [bp + ldParaPerSector]
		add word [bp + ldParasDone], ax
		sub word [bp + ldParasLeft], ax
		pop ax
		jbe @F		; read enough -->

		loop @BB
		pop bx
		pop cx

		call clust_next
		jnc next_load_cluster
		inc ax
		inc ax
		test al, 8	; set in 0FFF_FFF8h--0FFF_FFFFh,
				;  clear in 0, 1, and 0FFF_FFF7h
		jz error_badchain
		db __TEST_IMM16
@@:
		pop bx
		pop cx

		mov ax, word [load_minpara]
		cmp ax, word [bp + ldParasDone]
		jbe @F
error_filetoosmall:
		mov dx, msg.boot_file_too_small_error
		jmp bootcmd.fail
@@:

; turn off floppy motor
		mov dx,3F2h
		mov al,0
		out dx,al

; Set-up registers for and jump to loaded file
; Already: ss:bp-> boot sector containing BPB

		mov dl, [bp + bsBPB + ebpbNew + bpbnBootUnit]
;		test word [load_options], LOAD_SET_DL_UNIT
;		jz @F
	; (always set dl)
		mov byte [reg_edx], dl
@@:

;		test word [load_options], LOAD_SET_BL_UNIT
;		jz @F
	; (always set bl -- overwritten later if LOAD_SET_AXBX_DATASTART)
		mov byte [reg_ebx], dl
@@:

		mov ch, byte [bp + bsBPB + bpbMediaID]
		mov byte [reg_ecx + 1], ch

		test word [load_options], LOAD_DATASTART_HIDDEN
		jz @F
		mov bx, [bp + bsBPB + bpbHiddenSectors]
		mov ax, [bp + bsBPB + bpbHiddenSectors + 2]
		add word [bp + lsvDataStart], bx
		adc word [bp + lsvDataStart + 2], ax
@@:

		test word [load_options], LOAD_SET_AXBX_DATASTART
		jz @F
		mov bx, word [bp + lsvDataStart]
		mov ax, word [bp + lsvDataStart + 2]
		mov word [reg_ebx], bx
		mov word [reg_eax], ax
@@:

		test word [load_options], LOAD_SET_AXBX_ROOT_HIDDEN
		jz @F
		mov bx, word [bp + ldRootSector]
		mov ax, word [bp + ldRootSector + 2]
		add bx, word [bp + bsBPB + bpbHiddenSectors]
		adc ax, word [bp + bsBPB + bpbHiddenSectors + 2]
		mov word [reg_ebx], bx
		mov word [reg_eax], ax
@@:

		test word [load_options], LOAD_SET_SIDI_CLUSTER
		jz @F
		mov dx, word [bp + lsvFirstCluster + 2]
		mov ax, word [bp + lsvFirstCluster]
		mov word [reg_esi], dx
		mov word [reg_edi], ax
@@:

; (boot.asm code ends here)


	les di, [load_bpb_dest]
	push di
	sub di, -LOADSTACKVARS
	mov si, load_data - LOADDATA2 + LOADSTACKVARS
	mov cx, -LOADSTACKVARS + bsBPB + bpbNew
	rep movsb		; move common BPB part

	cmp word [bp + bsBPB + bpbSectorsPerFAT], cx
	mov cx, ebpbNew - bpbNew + BPBN_size	; move FAT32 EBPB part + BPBN
	je @F
	add si, ebpbNew - bpbNew; -> BPBN
	mov cx, BPBN_size	; move only BPBN
@@:
	rep movsb

	mov ax, di
	pop di
	sub ax, di
	dec ax
	dec ax
	xchg al, ah
	mov al, 0EBh
	mov word [es:di], ax
	mov byte [es:di + 2], 90h

		test word [load_options], LOAD_LBA_SET_TYPE
		jz @F
	test byte [bp + ldHasLBA], 1
	jz @F

	mov byte [es:di + 2], 0Eh	; (LBA-enabled) FAT16 FS partition type
	cmp byte [bp + ldFATType], 32
	jb @F
	mov byte [es:di + 2], 0Ch	; (LBA-enabled) FAT32 FS partition type
@@:

	mov word [es:di + 510], 0AA55h
	 push ds
	xor cx, cx
	mov ds, cx
	mov si, 500h
	 push di
	add di, 512 - 2 - 2 - 14 - 12 - 12
			; 2: AA55 sig, 2: null word, 14: MS-DOS 7 protocol
			;  message table pointer lives here,
			;  12: add name, 12: kernel name
	mov cl, 11
	rep movsb	; put kernel filename into the pseudo boot sector
	inc di
	mov si, 520h
	mov cl, 11
	rep movsb	; put additional filename (if any)
	 pop di
	 pop ds

	and word [reg_efl], ~(400h|200h|100h)	; UP, DI, TF=0

	mov word [reg_eip + 2], cx
	push word [load_entrypoint]
	pop word [reg_eip]
	mov ax, word [load_entrypoint + 2]
	add ax, word [load_loadseg]
	mov word [reg_cs], ax

	mov ax, word [load_bpb_dest + 2]
	mov word [reg_ss], ax
;	test word [load_options], LOAD_SET_DSBP_BPB
;	jz @F
		; (always set ds -- overwritten later if LOAD_SET_DSSI_DPT)
	mov word [reg_ds], ax
@@:
	mov bx, word [load_bpb_dest]
	mov word [reg_ebp + 2], cx
	mov word [reg_ebp], bx
	mov ax, bx
	sub bx, -LOADSTACKVARS	; (subtracts --10h)
	mov word [reg_esp + 2], cx
	mov word [reg_esp], bx

		test word [load_options], LOAD_MESSAGE_TABLE
		jz @F
	mov cx, (bsBPB + ebpbNew + BPBN_size + 2 + 15) & ~15
	add ax, cx
	mov word [es:di + 1EEh], ax
		; this pointer points to the MS-DOS 7 message table.
		;
		; note that in actual MS-DOS 7 boot sectors, this value is
		; eg 17Fh, which is incorrectly used with the boot sector's
		; ss to load the table into the initial loader.
		;
		; refer to comments in msg.asm about msdos7_message_table.
	mov si, msdos7_message_table
	add di, cx
	mov cx, msdos7_message_table.size
	rep movsb
@@:

		xor ax, ax
		mov es, ax
		mov di, 1Eh * 4
		mov dx, word [es:di + 2]
		mov bx, word [es:di]

		test word [load_options], LOAD_SET_DSSI_DPT
		jz @F
		mov word [reg_ds], dx
		mov word [reg_esi], bx
@@:
		test word [load_options], LOAD_PUSH_DPT
		jz @F

		 push es
		 push di
		sub word [reg_esp], 4 * 2	; push four words
		mov es, [reg_ss]
		mov di, [reg_esp]	; es:di -> stack area for the pointers
		 pop ax		; di (1Eh * 4)
		stosw
		 pop ax		; es (0)
		stosw
		mov ax, bx
		stosw		; si (Int1E offset)
		mov ax, dx
		stosw		; ds (Int1E segment)
@@:
	setopt [internalflags2], dif2_boot_loaded_kernel
	retn


		; INP:	0:bx -> where to place directory entry
		;	si:di = loaded FAT sector (0 = first FAT sector)
		;	dx:ax = directory cluster to scan, 0 for root dir
		;	byte [bp + ldFATType] = size of FAT entry in bits
		; OUT:	es = 0
		;	es:bx -> directory entry (bx unchanged)
		;	si:di = loaded FAT sector
		; CHG:	dx, ax, si, di, cx
scan_dir:
	test ax, ax
	jnz fat32_scan_root.dir_clust_dxax
	test dx, dx
	jnz fat32_scan_root.dir_clust_dxax

		; got to scan root directory. use FAT12/FAT16 walker if so,
		;  else use FAT32 walker

	cmp byte [bp + ldFATType], 16
	ja fat32_scan_root

	push si
	push di
	push bx

	mov si, word [bp + bsBPB + bpbNumRootDirEnts]


ADR_DIRBUF equ 600h

; (boot.asm code starts here)

fat16_scan_root:
	test si, si
	jz error_filenotfound

	mov ax, [bp + ldRootSector]
	mov dx, [bp + ldRootSector + 2]

; Scan root directory for file. We don't bother to check for deleted
;  entries (E5h) or entries that mark the end of the directory (00h).
		; number of root entries in si here
fat16_next_sect:
	mov bx, ADR_DIRBUF>>4
	call read_sector

	mov cx, [bp + ldEntriesPerSector] ; entries per sector as loop counter
	xor di, di		; es:di-> first entry in this sector
fat16_next_ent:
	mov bl, byte [es:di + deAttrib]
	test bl, ATTR_VOLLABEL
	jnz @F			; skip volume labels (and LFNs) --> (NZ)
	and bl, ATTR_DIRECTORY	; isolate directory bit
	cmp bl, byte [load_check_dir_attr]	; is it what we're searching?
	jne @F			; no -->
	push si
	push di
	push cx
	mov si, load_kernel_name	; ds:si-> name to match
	mov cx, 11		; length of padded 8.3 FAT filename
	repe cmpsb		; check entry
	pop cx
	pop di
	pop si
@@:
	lea di, [di + DIRENTRY_size]	; bytes/dirent
	je fat16_found_it	; found entry -->

	dec si			; count down entire root's entries
	loopnz fat16_next_ent	; count down sector's entries (jumps iff si >0 && cx >0)
	jnz fat16_next_sect	; (jumps iff si >0 && cx ==0)
				; ends up here iff si ==0
				;  ie all root entries checked unsuccessfully
error_filenotfound:
	mov dx, msg.boot_file_not_found
	jmp bootcmd.fail

fat16_found_it:
	  pop bx
	mov cx, 32
	sub di, cx
	push ds
	 push es
	 pop ds
	mov si, di		; ds:si -> entry in directory buffer
	mov di, bx
	xor ax, ax
	mov es, ax		; es:di -> destination for entry
	rep movsb
	pop ds
	  pop di
	  pop si
	retn


; (boot32.asm code starts here)

fat32_scan_root:
	mov ax, [bp + bsBPB + ebpbRootCluster]
	mov dx, [bp + bsBPB + ebpbRootCluster + 2]

.dir_clust_dxax:
	push bx

	call check_clust
	jc error_filenotfound

fat32_next_root_clust:
	call clust_to_first_sector
	push cx
	push bx
	mov cx, [bp + ldClusterSize]
fat32_next_root_sect:
	push cx
	mov cx, [bp + ldEntriesPerSector]

; Scan root directory for file. We don't bother to check for deleted
;  entries (E5h) or entries that mark the end of the directory (00h).
	mov bx, ADR_DIRBUF >> 4
	call read_sector

	push di
	xor di, di		; es:di-> first entry in this sector
fat32_next_ent:
	mov bl, byte [es:di + deAttrib]
	test bl, ATTR_VOLLABEL
	jnz @F			; skip volume labels (and LFNs) --> (NZ)
	and bl, ATTR_DIRECTORY	; isolate directory bit
	cmp bl, byte [load_check_dir_attr]	; is it what we're searching?
	jne @F			; no -->
	push si
	push di
	push cx
	mov si, load_kernel_name	; ds:si-> name to match
	mov cx, 11		; length of padded 8.3 FAT filename
	repe cmpsb		; check entry
	pop cx
	pop di
	pop si
@@:
	lea di, [di + DIRENTRY_size]	; bytes/dirent
	je fat32_found_it	; found entry -->

	loop fat32_next_ent	; count down sector's entries (jumps iff cx >0)
	pop di
	pop cx
	loop fat32_next_root_sect
	pop bx
	pop cx
	call clust_next
	jnc fat32_next_root_clust
	jmp error_filenotfound

fat32_found_it:
	pop dx			; value for di
	add sp, 6		; discard sector-in-cluster counter and cluster
	  pop bx
	mov cx, 32
	sub di, cx
	push ds
	 push es
	 pop ds
	push si
	mov si, di		; ds:si -> entry in directory buffer
	mov di, bx
	xor ax, ax
	mov es, ax		; es:di -> destination for entry
	rep movsb
	pop si
	pop ds
	  mov di, dx		; restore si:di = loaded FAT sector
	retn


; (iniload.asm code continues here)

		; INP:	dx:ax = cluster - 2 (0-based cluster)
		; OUT:	cx:bx = input dx:ax
		;	dx:ax = first sector of that cluster
		; CHG:	-
clust_to_first_sector:
	push dx
	push ax
	 push dx
	mul word [bp + ldClusterSize]
	xchg bx, ax
	xchg cx, dx
	 pop ax
	mul word [bp + ldClusterSize]
	test dx, dx
	jnz .error_badchain
	xchg dx, ax
	add dx, cx
	jc .error_badchain
	xchg ax, bx

	add ax, [bp + lsvDataStart]
	adc dx, [bp + lsvDataStart + 2]
	jc .error_badchain
				; dx:ax = first sector in cluster
	pop bx
	pop cx			; cx:bx = cluster
	retn

.error_badchain:
	jmp error_badchain


		; INP:	cx:bx = cluster (0-based)
		;	si:di = loaded FAT sector, -1 if none
		; OUT:	CY if no next cluster
		;	NC if next cluster found,
		;	 dx:ax = next cluster value (0-based)
		;	si:di = loaded FAT sector
		; CHG:	cx, bx
clust_next:
	mov ax, bx
	mov dx, cx
	add ax, 2
	adc dx, 0

	push es
	cmp byte [bp + ldFATType], 16
	je .fat16
	ja .fat32

.fat12:
; FAT12 entries are 12 bits, bytes are 8 bits. Ratio is 3 / 2,
;  so multiply cluster number by 3 first, then divide by 2.
					; ax = cluster number (up to 12 bits set)
		mov dx, ax
		shl ax, 1		; = 2n (up to 13 bits set)
		add ax, dx		; = 2n+n = 3n (up to 14 bits set)
		shr ax, 1		; ax = byte offset into FAT (0..6129)
					; CF = whether to use high 12 bits
		sbb cx, cx		; = -1 iff CY, else 0

; Use the calculated byte offset as an offset into the FAT
;  buffer, which holds all of the FAT's relevant data.
		mov es, [bp + lsvFATSeg]
		mov bx, ax		; -> 16-bit word in FAT to load

; get 16 bits from FAT
		mov ax, [es:bx]

		and cl, 4	; = 4 iff CY after shift, else 0
		shr ax, cl	; shift down iff odd entry, else unchanged
		and ax, 0FFFh	; insure it's only 12 bits
		xor dx, dx
	jmp short .gotvalue

.fat32:
		; * 4 = byte offset into FAT (0--4000_0000h)
	add ax, ax
	adc dx, dx
.fat16:
		; * 2 = byte offset into FAT (0--2_0000h)
	add ax, ax
	adc dx, dx

	 push ax
	xchg ax, dx
	xor dx, dx		; dx:ax = high word
	div word [bp + bsBPB + bpbBytesPerSector]
	mov bx, ax
	 pop ax			; dx = remainder, ax = low word
	div word [bp + bsBPB + bpbBytesPerSector]
	xchg dx, bx		; dx:ax = result, bx = remainder
				; dx:ax = sector offset into FAT (0--200_0000h)
				; bx = byte offset into FAT sector (0--8190)
	cmp dx, si
	jne @F		; read sector
	cmp ax, di
	je @FF		; sector is already buffered
@@:
	mov si, dx
	mov di, ax
	mov word [bp + lsvFATSector + 2], dx
	mov word [bp + lsvFATSector + 0], ax

	push bx
	add ax, [bp + bsBPB + bpbReservedSectors]
	adc dx, 0
	mov bx, [bp + lsvFATSeg]
	call read_sector
	pop bx
@@:
	mov es, [bp + lsvFATSeg]
	xor dx, dx
	mov ax, [es:bx]

	cmp byte [bp + ldFATType], 16
	je @F
	mov dx, [es:bx + 2]
@@:
.gotvalue:
	pop es

		; INP:	dx:ax = cluster value, 2-based
		; OUT:	dx:ax -= 2 (makes it 0-based)
		;	CY iff invalid cluster
check_clust:
	and dx, 0FFFh
	sub ax, 2
	sbb dx, 0

	cmp byte [bp + ldFATType], 16
	ja .fat32
	je .fat16

.fat12:
	cmp ax, 0FF7h - 2
	jmp short .common

.fat32:
	cmp dx, 0FFFh
	jb @F		; CY here means valid ...-

.fat16:
	cmp ax, 0FFF7h - 2
@@:			;  -... or if NC first, CY here also
.common:
	cmc		; NC if valid
	retn


scan_partitions:
	and word [load_partition_cycle], 0
d4	call d4message
d4	asciz "In scan_partitions",13,10

	push bp
	mov bp, sp
	xor di, di
	push di		; [bp+di-2]
	push di		; [bp+di-4]
	push di		; [bp+di-6]
	push di		; [bp+di-8]

	xor ax, ax
	xor dx, dx
d4	call d4message
d4	asciz "In scan_partitions (before first call to read_partition_table)",13,10
	call read_partition_table
d4	call d4message
d4	asciz "In scan_partitions (after first call to read_partition_table)",13,10
	mov si, load_partition_table
	mov byte [load_current_partition], dl	; = 0
.loop_primary_parts:
	inc byte [load_current_partition]
	cmp byte [es:si + 4], 0
	je .loop_primary_skip
	call cx			; es:si -> partition table entry
				; byte [load_current_partition] = which
.loop_primary_skip:
	add si, 16
	cmp si, load_partition_table.end
	jb .loop_primary_parts

scan_logical:
.:
d4	call d4message
d4	asciz "In scan_logical.",13,10
	mov si, load_partition_table
.loop:
	inc word [load_partition_cycle]
	jz .got_partition_cycle

	mov al, [es:si + 4]

	xor bx, bx
	test al, al
	jz .next
	cmp al, 0Fh		; extended partition (LBA aware) ?
	je .push		; yes -->
	and al, ~80h		; extended partition Linux (85h) ?
	cmp al, 05h		;  or extended partition DOS (05h) ?
	je .push		; yes -->

	cmp word [bp+di-2], bx
	jne .logical
	cmp word [bp+di-4], bx
	je .next
.logical:
	inc byte [load_current_partition]
	jz .error_too_many_partitions
	call cx			; CHG: ax, bx, (cx), dx,
				; preserve: (cx), si, di, bp
.next:
	add si, 16		; -> next partition table entry
	cmp si, load_partition_table.end	; was last?
	jb .loop		; no, loop -->
	test di, di		; still some on stack?
	jnz .pop		; yes, pop
	mov sp, bp		; restore sp
	pop bp
	retn			; and bye

.push:
d4	call d4message
d4	asciz "In scan_logical.push",13,10

	push si
.push_check_empty_next:
	add si, 16		; -> next
	cmp si, load_partition_table.end	; at end?
	jae .replace		; yes, no other partitions found, replace -->
	cmp byte [es:si + 4], 0	; is this a partition?
	je .push_check_empty_next	; no, check next -->
				; found a partition after this, do push
				; (possibly logical or another extended)
.push_check_is_not_empty:
d4	call d4message
d4	asciz "In scan_logical.push_check_is_not_empty",13,10
	pop si			; restore -> partition table entry
	push si			; stored at word [bp+di-10]
	sub di, 10
	push word [bp+di+10-2]
	push word [bp+di+10-4]	; copy root

	mov ax, word [es:si + 8]
	mov dx, word [es:si + 8 + 2]	; get extended partition offset
	add ax, word [bp+di-4]
	adc dx, word [bp+di-2]	; add in root to get absolute sector number

	push dx
	push ax			; new base

.replace_common:
	cmp word [bp+di-2], bx	; have a (nonzero) root?
	jne .have_root
	cmp word [bp+di-4], bx
	jne .have_root		; yes -->

	mov word [bp+di-2], dx
	mov word [bp+di-4], ax	; set root
.have_root:

	call read_partition_table
	jmp .

.pop:
d4	call d4message
d4	asciz "In scan_logical.pop",13,10

	add di, 10
	add sp, 8
	pop si

	mov ax, word [bp+di-8]
	mov dx, word [bp+di-6]
	call read_partition_table
	jmp .next

.replace:
d4	call d4message
d4	asciz "In scan_logical.replace",13,10

	pop si				; (discard)
	mov ax, word [es:si + 8]
	mov dx, word [es:si + 8 + 2]	; get extended partition offset
	add ax, word [bp+di - 4]
	adc dx, word [bp+di - 2]	; add in root
	mov word [bp+di - 8], ax
	mov word [bp+di - 6], dx	; set base

	jmp .replace_common

.got_partition_cycle:
	mov dx, msg.boot_partition_cycle_error
	jmp bootcmd.fail

.error_too_many_partitions:
	mov dx, msg.boot_too_many_partitions_error
	jmp bootcmd.fail


		; INP:	-
		; OUT:	es = ss
		;	64 bytes [es:load_partition_table] = partition table
		;	does not return if error
read_partition_table:
	mov bx, word [auxbuff_segorsel]	; bx => auxbuff
	call read_ae_512_bytes
	cmp word [es:510], 0AA55h
	jne @F
	 push ds
	 push si
	 push di
	 push cx
	push es
	pop ds
	mov si, 510 - 64
	push ss
	pop es
	mov di, load_partition_table
	mov cx, (16 * 4) >> 1
	rep movsw
	 pop cx
	 pop di
	 pop si
	 pop ds
	retn

@@:
	mov dx, msg.bootfail_sig_parttable
	jmp bootcmd.fail


		; INP:	al = first character
		;	si -> next
		; OUT:	doesn't return if error
		;	bx:dx = number read
		;	al = character after the number
		;	si -> next
		; CHG:	cx, ax, di
boot_get_decimal_literal:
	mov dx, 10		; set base: decimal
%if 1
	mov cx, '9' | (('A'-10-1 + 10) << 8)
%else
	mov cl, dl
	add cl, '0'-1
	cmp cl, '9'
	jbe .lit_basebelow11
	mov cl, '9'
.lit_basebelow11:		; cl = highest decimal digit for base ('1'..'9')
	mov ch, dl
	add ch, 'A'-10-1	; ch = highest letter for base ('A'-x..'Z')
%endif
	jmp @F


boot_get_hexadecimal_literal:
	mov dx, 16		; set base: hexadecimal
%if 1
	mov cx, '9' | (('A'-10-1 + 16) << 8)
%else
	mov cl, dl
	add cl, '0'-1
	cmp cl, '9'
	jbe .lit_basebelow11
	mov cl, '9'
.lit_basebelow11:		; cl = highest decimal digit for base ('1'..'9')
	mov ch, dl
	add ch, 'A'-10-1	; ch = highest letter for base ('A'-x..'Z')
%endif

@@:
	mov ah, 0
	xor bx, bx
	mov di, dx		; di = base

	call getexpression.lit_isdigit?	; first character must be a digit
	jc .err2
	xor dx, dx		; initialize value
.lit_loopdigit:
	cmp al, '_'
	je .lit_skip
	call getexpression.lit_isdigit?	; was last character ?
	jc .lit_end		; yes -->
	call uppercase
	sub al, '0'
	cmp al, 9		; was decimal digit ?
	jbe .lit_decimaldigit	; yes -->
	sub al, 'A'-('9'+1)	; else adjust for hexadecimal digit
.lit_decimaldigit:
	push ax
	mov ax, dx
	push bx
	mul di			; multiply low word with base
	mov bx, dx
	mov dx, ax
	pop ax
	push dx
	mul di			; multiply high word with base
	test dx, dx
	pop dx
	jnz .err2		; overflow -->
	add bx, ax		; add them
	pop ax
	jc .err2		; overflow -->
	add dl, al		; add in the new digit
	adc dh, 0
	adc bx, byte 0
.lit_skip:
	lodsb
	jmp short .lit_loopdigit

.lit_end:
	call isseparator?	; after the number, there must be a separator
	jne .err2		; none here -->
	retn

.err2:
	jmp error


query_geometry:
	mov dl, [load_unit]
;	test dl, dl		; floppy?
;	jns @F			; don't attempt query, might fail -->
	; Note that while the original PC BIOS doesn't support this function
	;  (for its diskettes), it does properly return the error code 01h.
	; https://sites.google.com/site/pcdosretro/ibmpcbios (IBM PC version 1)
	mov ah, 08h
	xor cx, cx		; initialise cl to 0
	mov [load_heads], cx
	mov [load_sectors], cx
	stc			; initialise to CY
	call .int13_retry	; query drive geometry
	jc .try_bootsector	; apparently failed -->
	mov dl, dh
	mov dh, 0		; dx = maximum head number
	inc dx			; dx = number of heads (H is 0-based)
	mov ax, cx		; ax & 3Fh = maximum sector number
	and ax, 3Fh		; get sectors (number of sectors, S is 1-based)
	jnz .got_sectors_heads	; valid (S is 1-based), use these -->
				; zero = invalid
.try_bootsector:
	mov ax, word [auxbuff_segorsel]	; ax => auxbuff
	dec ax				; ax => auxbuff - 16
	mov es, ax
	mov bx, 16
	mov ax, 0201h			; read sector, 1 sector
	mov cx, 1			; sector 1 (1-based!), cylinder 0 (0-based)
	mov dh, 0			; head 0 (0-based)
	mov dl, [load_unit]
	stc
	call .int13_retry
	jc .access_error

		; note: the smallest supported sector size, 32 bytes,
		;  does contain these entries (offset 18h and 1Ah in sector)
		;  within the first BPB sector.
	mov ax, word [es:bx + bsBPB + bpbCHSSectors]
	mov dx, word [es:bx + bsBPB + bpbCHSHeads]

.got_sectors_heads:
	mov word [load_sectors], ax
	mov word [load_heads], dx

	test ax, ax
	jz .invalid_sectors
	cmp ax, 63
	ja .invalid_sectors
	test dx, dx
	jz .invalid_heads
	cmp dx, 100h
	ja .invalid_heads

	mov ax, word [auxbuff_segorsel]	; ax => auxbuff
	dec ax				; ax => auxbuff - 16
	mov es, ax
	xor ax, ax
	mov bx, 16

%if _AUXBUFFSIZE < 8192+2
 %error Expecting to use auxbuff as sector size detection buffer
%endif

d5	call d5dumpregs
d5	call d5message
d5	asciz 13,10,"In query_geometry 0",13,10

	mov di, bx
	mov cx, (8192 + 2) >> 1
					; es:bx -> auxbuff, es:di = same
	rep stosw			; fill buffer, di -> behind (auxbuff+8192+2)
	mov ax, 0201h			; read sector, 1 sector
	inc cx				; sector 1 (1-based!), cylinder 0 (0-based)
	mov dh, 0			; head 0 (0-based)
	mov dl, [load_unit]
	stc
	call .int13_retry
	jc .access_error

	std				; _AMD_ERRATUM_109_WORKAROUND does not apply
	mov word [es:bx - 2], 5E5Eh	; may overwrite last 2 bytes at line_out_end
	scasw				; -> auxbuff+8192 (at last word to sca)
d5	call d5dumpregs
d5	call d5message
d5	asciz 13,10,"In query_geometry 1",13,10
	mov cx, (8192 + 2) >> 1
	xor ax, ax
	repe scasw
	add di, 4			; di -> first differing byte (from top)
	cld
	push di

	mov di, bx
	mov cx, (8192 + 2) >> 1
	dec ax				; = FFFFh
	rep stosw

	mov ax, 0201h
	inc cx
	mov dh, 0
	mov dl, [load_unit]
	stc
	call .int13_retry
	jc .access_error

	std				; _AMD_ERRATUM_109_WORKAROUND does not apply
	scasw				; di -> auxbuff+8192 (last word to sca)
d5	call d5dumpregs
d5	call d5message
d5	asciz 13,10,"In query_geometry 2",13,10
	pop dx
	mov ax, -1
	mov cx, (8192 + 2) >> 1
	repe scasw
%if 0
AAAB
   ^
	sca B, match
  ^
	sca B, mismatch
 ^
	stop
%endif
	add di, 4			; di -> first differing byte (from top)
	cld

%if 0
0000000000000
AAAAAAAA00000
	^
FFFFFFFFFFFFF
AAAAAAAA00FFF
	  ^
%endif
	cmp dx, di			; choose the higher one
	jae @F
	mov dx, di
@@:
	sub dx, bx			; dx = sector size

d5	call d5dumpregs
d5	call d5message
d5	asciz 13,10,"In query_geometry 3",13,10

	cmp dx, 8192 + 2
	jae .sector_too_large
	mov ax, 32
	cmp dx, ax
	jb .sector_too_small
@@:
	cmp dx, ax
	je .got_match
	cmp ax, 8192
	jae .sector_not_power
	shl ax, 1
	jmp @B

.got_match:
	mov word [load_sectorsize], ax
	mov cl, 4
	shr ax, cl
	mov word [load_sectorsizepara], ax

	mov byte [load_lba], 0
	mov ah, 41h
	mov dl, [load_unit]
	mov bx, 55AAh
	stc
	int 13h		; 13.41.bx=55AA extensions installation check
	jc .no_lba
	cmp bx, 0AA55h
	jne .no_lba
	test cl, 1	; support bitmap bit 0
	jz .no_lba

	inc byte [load_lba]
.no_lba:

	mov ax, word [auxbuff_segorsel]	; ax => auxbuff
	mov dx, ax
	add dx, (8192 - 16) >> 4
	mov bx, ax
	mov cx, dx
	and bx, 0F000h
	and cx, 0F000h
	cmp cx, bx
	jne @F
	mov word [load_sectorseg], ax
	retn

@@:
	mov dx, msg.boot_auxbuff_crossing
	jmp .error_common_j


.int13_retry:
	pushf
	push ax
	int 13h		; first try
	jnc @F		; NC, success on first attempt -->

; reset drive
	xor ax, ax
	int 13h
	jc @F		; CY, reset failed, error in ah -->

; try read again
	pop ax		; restore function number
	popf
	int 13h		; retry, CF error status, ah error number
	retn

@@:			; NC or CY, stack has function number
	inc sp
	inc sp
	inc sp
	inc sp		; discard two words on stack, preserve CF
	retn


.out_of_memory_error:
	mov dx, msg.boot_out_of_memory_error
	jmp .error_common_j
.access_error:
	mov dx, msg.boot_access_error
	jmp .error_common_j
.sector_too_large:
	mov dx, msg.boot_sector_too_large
	jmp .error_common_j
.sector_too_small:
	mov dx, msg.boot_sector_too_small
	jmp .error_common_j
.sector_not_power:
	mov dx, msg.boot_sector_not_power
	jmp .error_common_j
.invalid_sectors:
	mov dx, msg.boot_invalid_sectors
	jmp .error_common_j
.invalid_heads:
	mov dx, msg.boot_invalid_heads
.error_common_j:
	jmp .error_common

.error_common: equ bootcmd.fail


		; INP:	dx:ax = first sector
		;	bx:0 -> buffer
		; OUT:	dx:ax = sector number after last read
		;	es = input bx
		;	bx:0 -> buffer after last written
		; CHG:	-
		; STT:	ds = ss
read_ae_1536_bytes:
	push cx
	push bx
	mov cx, 1536
.loop:
	call read_sector
	sub cx, word [load_data - LOADDATA2 + bsBPB + bpbBytesPerSector]
	ja .loop
	pop es
	pop cx
	retn

		; INP:	dx:ax = first sector
		;	bx:0 -> buffer
		; OUT:	dx:ax = sector number after last read
		;	es = input bx
		;	bx:0 -> buffer after last written
		; CHG:	-
		; STT:	ds = ss
read_ae_512_bytes:
	push cx
	push bx
	mov cx, 512
.loop:
	call read_sector
	sub cx, word [load_data - LOADDATA2 + bsBPB + bpbBytesPerSector]
	ja .loop
	pop es
	pop cx
	retn


		; Write a sector using Int13.03 or Int13.43
		;
		; Protocol as for read_sector
write_sector:
	db __TEST_IMM8		; (skip stc, NC)

		; Read a sector using Int13.02 or Int13.42
		;
		; INP:	dx:ax = sector number (within partition)
		;	bx:0-> buffer
		;	(_LBA) ds = ss
		;	dword[load_data - LOADDATA2 + bsBPB + bpbHiddenSectors]
		;	 = base sector number (dx:ax is added to this to get
		;	    the absolute sector number in the selected unit.)
		; OUT:	If unable to read,
		;	 ! jumps to error instead of returning
		;	If sector has been read,
		;	 dx:ax = next sector number (has been incremented)
		;	 bx:0-> next buffer (bx = es+word[load_sectorsizepara])
		;	 es = input bx
		; CHG:	-
		;
		; Note:	If error 09h (data boundary error) is returned,
		;	 the read is done into the load_sectorseg buffer,
		;	 then copied into the user buffer.
read_sector:
	stc

read_sector_CY_or_write_sector_NC:
	lframe near
	lenter
	lvar word, is_read_bit0
	 pushf

.err: equ bootcmd.fail_read
d5	call d5dumpregs
d5	call d5message
d5	asciz 13,10,"In read_sector",13,10

	push dx
	push cx
	push ax
	push si

	push bx

; DX:AX==LBA sector number
; add partition start (= number of hidden sectors)
		add ax,[load_data - LOADDATA2 + bsBPB + bpbHiddenSectors + 0]
		adc dx,[load_data - LOADDATA2 + bsBPB + bpbHiddenSectors + 2]

	xor cx, cx
	push cx
	push cx
	push dx
	push ax		; qword sector number (lpSector)
	push bx
	push cx		; bx:0 -> buffer (lpBuffer)
	inc cx
	push cx		; word number of sectors to read (lpCount)
	mov cl, 10h
	push cx		; word size of disk address packet (lpSize)
	mov si, sp	; ds:si -> disk address packet (on stack)

	test byte [load_data - LOADDATA2 + ldHasLBA], 1
	jz .no_lba

d5	call d5message
d5	asciz "In read_sector.lba",13,10

	mov dl, byte [load_unit]
	call .set_ah_function_42_or_43
	int 13h		; 13.42 extensions read
	jnc .lba_done

	xor ax, ax
	int 13h
	jc .lba_error

		; have to reset the LBAPACKET's lpCount, as the handler may
		;  set it to "the number of blocks successfully transferred".
		; (in any case, the high byte is still zero.)
	mov byte [si + lpCount], 1

	call .set_ah_function_42_or_43
	int 13h
	jnc .lba_done

	cmp ah, 9	; data boundary error?
	jne .lba_error

.lba_sectorseg:
d4	call d4dumpregs
d4	call d4message
d4	asciz 13,10,"In read_sector.lba_sectorseg",13,10

	test byte [bp + ?is_read_bit0], 1
	jnz .lba_sectorseg_read

.lba_sectorseg_write:
	push ds
	push si
	push es
	push di
	mov cx, word [load_sectorsize]
	mov es, word [load_sectorseg]	; => sectorseg
	; lds si, [si + lpBuffer + 0]
	mov ds, word [si + lpBuffer + 2]; => user buffer
	xor si, si
	xor di, di
	rep movsb			; copy data into sectorseg
	pop di
	pop es
	pop si
	pop ds

	 push word [load_sectorseg]
	 pop word [si + lpBuffer + 2]	; => sectorseg
	; and word [si + lpBuffer + 0], byte 0

	mov byte [si + lpCount], 1
	mov ah, 43h
	int 13h
	jnc @F

	xor ax, ax
	int 13h
	jc .lba_error

	mov byte [si + lpCount], 1
	mov ah, 43h
	int 13h
	jc .lba_error
@@:
	jmp .lba_done


.lba_sectorseg_read:
		; the offset part of the pointer is already zero!
	; push word [si + lpBuffer + 0]
	push word [si + lpBuffer + 2]	; user buffer
	 push word [load_sectorseg]
	 pop word [si + lpBuffer + 2]
	; and word [si + lpBuffer + 0], byte 0

	mov byte [si + lpCount], 1
	call .set_ah_function_42_or_43
	int 13h
	jnc .lba_sectorseg_done

	xor ax, ax
	int 13h
	jc .lba_error

	mov byte [si + lpCount], 1
	call .set_ah_function_42_or_43
	int 13h
	jc .lba_error
.lba_sectorseg_done:

	xor si, si
	mov ds, word [load_sectorseg]
	pop es
	; pop cx
	 push di
	; mov di, cx
	xor di, di
	mov cx, word [load_sectorsize]
	rep movsb
	 pop di

	push ss
	pop ds
.lba_done:
	add sp, 10h
	pop bx
	jmp .chs_done

.lba_error: equ .err

.no_lba:
	add sp, 8
	pop ax
	pop dx
	pop cx
	pop cx

; DX:AX=LBA sector number
; divide by number of sectors per track to get sector number
; Use 32:16 DIV instead of 64:32 DIV for 8088 compatability
; Use two-step 32:16 divide to avoid overflow
			mov cx,ax
			mov ax,dx
			xor dx,dx
			div word [load_sectors]
			xchg cx,ax
			div word [load_sectors]
			xchg cx,dx

; DX:AX=quotient, CX=remainder=sector (S) - 1
; divide quotient by number of heads
			mov bx, ax
			xchg ax, dx
			xor dx, dx
			div word [load_heads]
			xchg bx, ax
			div word [load_heads]

; bx:ax=quotient=cylinder (C), dx=remainder=head (H)
; move variables into registers for INT 13h AH=02h
			mov dh, dl	; dh = head
			inc cx		; cl5:0 = sector
			xchg ch, al	; ch = cylinder 7:0, al = 0
			shr ax, 1
			shr ax, 1	; al7:6 = cylinder 9:8
	; bx has bits set iff it's > 0, indicating a cylinder >= 65536.
			 or bl, bh	; collect set bits from bh
			or cl, al	; cl7:6 = cylinder 9:8
	; ah has bits set iff it was >= 4, indicating a cylinder >= 1024.
			 or bl, ah	; collect set bits from ah
			mov dl, [load_unit]
					; dl = drive
			mov ah, 04h	; error number: sector not found
			 jnz .err	; error if cylinder >= 1024 -->
					; ! bx = 0 (for 13.02 call)

; we call INT 13h AH=02h once for each sector. Multi-sector reads
; may fail if we cross a track or 64K boundary
			pop es

			call .set_ax_function_0201_or_0301
			int 13h		; read one sector
			jnc .done
; reset drive
			xor ax, ax
			int 13h
			jc .err

; try read again
			call .set_ax_function_0201_or_0301
			int 13h
	jnc .done
	cmp ah, 9	; data boundary error?
	jne .err

.chs_sectorseg:
d4	call d4dumpregs
d4	call d4message
d4	asciz 13,10,"In read_sector.chs_sectorseg",13,10

	test byte [bp + ?is_read_bit0], 1
	jnz .chs_sectorseg_read

.chs_sectorseg_write:
	push es

	push ds
	push di
	push cx
	mov cx, word [load_sectorsize]
	push es
	mov es, word [load_sectorseg]	; => sectorseg
	pop ds				; => user buffer
	xor si, si
	xor di, di
	rep movsb			; copy data into sectorseg
	pop cx
	pop di
	pop ds

	mov ax, 0301h
	int 13h
	jnc @F

	xor ax, ax
	int 13h
	jc .err

	mov ax, 0301h
	int 13h
	jc .err
@@:
	pop bx
	jmp .chs_done


.chs_sectorseg_read:

	push es		; user buffer
	 mov es, word [load_sectorseg]

	call .set_ax_function_0201_or_0301
	int 13h
	jnc .chs_sectorseg_done

	xor ax, ax
	int 13h
	jc .err

	call .set_ax_function_0201_or_0301
	int 13h
	jc .err
.chs_sectorseg_done:

	xor si, si
	mov ds, word [load_sectorseg]
	pop es
	 push di
	xor di, di
	mov cx, word [load_sectorsize]
	rep movsb
	 pop di

	push ss
	pop ds
.done:
; increment segment
	mov bx, es

.chs_done:
	mov es, bx
	add bx, word [load_sectorsizepara]

	pop si
	pop ax
	pop cx
	pop dx
; increment LBA sector number
	inc ax
	jne @F
	inc dx
@@:
	lleave code
	retn

.set_ah_function_42_or_43:
	mov ah, 42h
	test byte [bp + ?is_read_bit0], 1
	jnz @F
	mov ah, 43h
@@:
	retn

.set_ax_function_0201_or_0301:
	mov al, 1
.set_ah_function_02_or_03:
	mov ah, 02h
	test byte [bp + ?is_read_bit0], 1
	jnz @F
	mov ah, 03h
@@:
	retn

	lleave ctx

