
%if 0

lDebug D commands - Dump data

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2012 C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif


	usesection lDEBUG_CODE

		; D command - hex/ASCII dump.
ddd:
%if _INT || _PM || _MCB || _DSTRINGS
	call uppercase
%endif
	xchg al, ah
	mov al, byte [si - 2]
	call uppercase
	cmp al, 'D'
	xchg al, ah
	jne .not_d_suffix
%if _DSTRINGS
	cmp al, 'Z'		; DZ command ?
	je dz			; yes -->
	cmp al, '$'		; D$ command ?
	je dcpm			; yes -->
	cmp al, '#'		; D# command ?
	je dcounted		; yes -->
	cmp al, 'W'
	jne .notstring
	push ax
	lodsb
	cmp al, '#'		; DW# command ?
	pop ax
	je dwcounted		; yes -->
	dec si
.notstring:
%endif
%if _INT
	cmp al, 'I'		; DI command ?
	jne .notdi
%if 1
	push ax
	lodsb
	dec si
	and al, TOUPPER
	cmp al, 'P'		; distinguish 'di ...' and 'd ip'
	pop ax
	je .notdi
%endif
	jmp gateout		; yes -->
.notdi:
%endif
%if _PM
	cmp al, 'L'		; DL command ?
	jne .notdl
	jmp descout		; yes -->
.notdl:
	cmp al, 'X'		; DX command ?
_386	je extmem		; yes -->
.notdx:
%endif
%if _MCB
	cmp al, 'M'		; DM command ?
	jne .notdm
	jmp mcbout		; yes -->
.notdm:
%endif
.not_d_suffix:
	call skipwh0
	call iseol?
	jne dd1			; if an address was given
lastddd:
	_386_PM_o32	; mov edx, dword [d_addr]
	mov dx, word [d_addr]	; compute range of 80h or until end of segment
	_386_PM_o32	; mov esi, edx
	mov si, dx
	mov bx, [d_addr+4]
_386_PM	call testattrhigh
_386_PM	jnz .32
	add dx, byte 7Fh
	jnc dd2
	or dx, byte -1
	jmp short dd2

%if _PM
cpu 386
.32:
	add edx, byte 7Fh
	jnc dd2			; if no overflow
	or edx, byte -1
	jmp short dd2
cpu 8086
%endif
dd1:
	mov cx, 80h		; default length
	mov bx, word [reg_ds]
	call getrangeX		; get address range into bx:(e)dx
	call chkeol		; expect end of line here

	mov word [d_addr+4], bx	; save segment (offset is saved later)
	_386_PM_o32	; mov esi, edx
	mov si, dx		; bx:(e)si = start
	_386_PM_o32	; mov edx, ecx
	mov dx, cx		; bx:(e)dx = last
%if _PM && 0
	jmp short dd2_1
%endif

		; Parsing is done.  Print first line.
dd2:
	call handle_serial_flags_ctrl_c
%if _PM
	call ispm
	jnz dd2_1
cpu 286
	verr bx			; readable ?
cpu 8086
	jz dd2_1
%if 1
	mov dx, .errmsg
	jmp putsz
.errmsg:asciz "Segment is not readable.",13,10
%else
	mov bx, word [reg_ds]
	mov word [d_addr+4], bx
%endif
dd2_1:
%endif
	mov word [lastcmd], lastddd
	mov ax, word [d_addr+4]
	call hexword
	mov al, ':'
	stosb
	_386_PM_o32	; mov eax, esi
	mov ax, si
	and al, 0F0h
%if _PM
	call testattrhigh	; 32-bit segment ?
	jz .16			; no --> (don't display zero high word)
	call hexword_high	; yes, display high word of address
	jmp short .common

		; Insure that the high word is zero.
.16:
;_386	test esi, ~0FFFFh
;_386	jnz .error
_386	test edx, ~0FFFFh
_386	jz .common
;.error:
_386	mov dx, msg.ofs32
_386	jmp putsz
.common:
%endif
	call hexword
	push ax
	mov ax, 32<<8|32
	stosw
	pop ax
	lea bx, [di+3*16]
	call prephack		; set up for faking int vectors 23 and 24

		; blank the start of the line if offset isn't paragraph aligned
dd3:
	cmp ax, si		; skip to position in line
	je dd4			; if we're there yet
	push ax
	mov ax, 32<<8|32
	stosw
	stosb
	mov byte [es:bx], al
	inc bx
	pop ax
	inc ax
	jmp short dd3

		; Begin main loop over lines of output.
dd4:
	_386_PM_o32	; mov ecx, esi
	mov cx, si
	or cl, 0Fh
	_386_PM_o32	; cmp ecx, edx
	cmp cx, dx		; compare with end address
	jb dd5			; if we write to the end of the line
	;_386_PM_o32	; mov ecx, edx
	mov cx, dx
dd5:
	;_386_PM_o32	; sub ecx, esi
	sub cx, si
	;_386_PM_o32	; inc ecx
	inc cx			; cx = number of bytes to print this line
				;      up to 16.. no 32-bit register required
	call dohack		; substitute interrupt vectors
	mov ds, word [d_addr+4]
dd6:
	_386_PM_a32
	lodsb
	call dd_store		; stores number and space at es:di->, char at es:bx->
	loop dd6		; (16-bit. cx >= 16)

dd9:
	test si, 0Fh		; space out till end of line
	jz dd10
	mov ax, 32<<8|32
	stosw
	stosb
	_386_PM_o32	; inc esi
	inc si
	jmp short dd9

dd10:
	push ss			; restore ds
	pop ds
	mov byte [di-(1+(8*3))], '-'
	call unhack
	mov di, bx
	push dx
	call putsline_crlf
	pop dx
	mov bx, word [d_addr+4]
	mov di, line_out	; reset di,bx for next line
	_386_PM_o32	; dec esi
	dec si
	_386_PM_o32	; cmp esi, edx
	cmp si, dx
	_386_PM_o32	; inc esi
	inc si
	jb dd2			; display next line -->
dd11:
		; This check is necessary to wrap around at FFFFh (64 KiB)
		; for 16-bit segments instead of at FFFFFFFFh (4 GiB).
_386_PM	call testattrhigh	; 32-bit segment ?
_386_PM	jz .16			; no -->
	_386_PM_o32	; inc edx
.16:
	inc dx			; set up the address for the next 'D' command.
	_386_PM_o32	; mov dword [d_addr], edx
	mov word [d_addr], dx
	retn


		; Store a character into the buffer. Characters that can't
		; be displayed are replaced by a dot.
		;
		; INP:	al = character
		;	es:bx-> buffer for displayed characters
		;	es:di-> buffer for hexadecimal number
		; OUT:	es:bx-> behind displayed character
		;	es:di-> behind hexadecimal number and space
		; CHG:	ax
		; STT:	ds unknown
dd_store:
	mov ah, al
	cmp al, 32		; below blank ?
	jb .ctrl		; control char -->
	cmp al, 127		; DEL ?
	je .ctrl		; yes, control char -->
	jb .noctrl		; below, not a control char -->
	testopt [ss:options], cpdepchars	; allow CP-dependant characters ?
	jnz .noctrl		; yes -->
.ctrl:
	mov ah, '.'
.noctrl:
	mov byte [es:bx], ah
	inc bx
	push cx
	call hexbyte
	pop cx
	mov al, 32
	stosb
	retn


%if _PM
		; DL command
descout:
	call skipwhite
	call getword	; get word into DX
	mov bx, dx
	call skipcomm0
	mov dx, 1
	cmp al, 13
	je .onlyone
	call uppercase
	cmp al, 'L'
	jne .notlength
	call skipcomma
.notlength:
	call getword
	call chkeol
.onlyone:
	inc dx		; (note js at nextdesc changed to jz)
	mov si, dx	; save count
	call ispm
	je nextdesc
	mov dx, nodesc
	jmp putsz
desc_done:
	retn
cpu 286
nextdesc:
	dec si
	jz desc_done
	mov di, descr
	mov ax, bx
	call hexword
	mov di, descbase
	push di
	mov ax, "??"
	stosw
	stosw
	stosw
	stosw
	add di, byte (desclim-(descbase+8))
	stosw
	stosw
	stosw
	stosw
	add di, byte (descattr-(desclim+8))
	stosw
	stosw
	pop di
;	lar ax, bx
;	jnz skipdesc	; tell that this descriptor is invalid
	mov ax, 6
	int 31h
	jc desc_o1
	mov ax, cx
	call hexword
	mov ax, dx
	call hexword
desc_o1:
	mov di, desclim
	_no386_jmps use16desc
cpu 386
	lsl eax, ebx
	jnz desc_out
	push ax
	shr eax, 16
	call hexword
	pop ax
	call hexword
	lar eax, ebx
	shr eax, 8
desc_o2:
	mov di, descattr
	call hexword
desc_out:
	mov dx, descr
	call putsz
	add bx, byte 8
	jmp short nextdesc
cpu 286
use16desc:
	lsl ax, bx
	jnz desc_out
	call hexword
	mov ax, 32<<8|32
	stosw
	stosw
	lar ax, bx
	shr ax, 8
	jmp short desc_o2
cpu 8086
%endif

%if _DSTRINGS
		; D$ command
dcpm:
	mov byte [dstringtype], 36
	mov word [dstringaddr], dcpm_addr
	jmp short dstring

		; DW# command
dwcounted:
	mov byte [dstringtype], 0FEh
	mov word [dstringaddr], dwcount_addr
	jmp short dstring

		; D# command
dcounted:
	mov byte [dstringtype], 0FFh
	mov word [dstringaddr], dcount_addr
	jmp short dstring

		; DZ command
dz:
	mov byte [dstringtype], 0
	mov word [dstringaddr], dz_addr

		; common code for all string commands
dstring:
	call skipwhite
	cmp al, ';'
	je .last
	cmp al, 13
	jne .getaddr		; if an address was given
.last:
	mov bx, word [dstringaddr]
	_386_PM_o32	; mov edx, dword [bx]
	mov dx, word [bx]
	jmp short .haveaddr	; edx = offset, [bx+4] = segment
.getaddr:
	mov bx, word [reg_ds]
	call getaddrX		; get address into bx:(e)dx
	call chkeol		; expect end of line here
	push bx
	mov bx, word [dstringaddr]
	pop word [bx+4]		; save segment (offset behind string is saved later)
.haveaddr:
	mov word [lastcmd], dstring.last
	call prephack
	_386_PM_o32	; mov esi, edx
	mov si, dx
	setopt [internalflags], usecharcounter
	mov byte [ charcounter ], 1	; initialize
	call dohack
	mov ds, word [bx+4]	; ds:(e)si-> string
	cmp byte [ss:dstringtype], 0FEh
	jb .terminated		; terminated string -->
	lahf
	_386_PM_a32
	lodsb			; load first byte
	xor cx, cx
	mov cl, al		; low byte of count
	sahf
	jne .counted		; only byte count -->
	_386_PM_a32
	lodsb			; load second byte
	mov ch, al		; high byte of count
.counted:
	jcxz .done		; length zero -->
.loop:
	_386_PM_a32
	lodsb			; get character
	call .char		; display
	loop .loop		; until done -->
	jmp short .done

.char:
	push ss
	pop ds
	push ax
	call unhack		; restore state
	pop ax
	push si
	push cx
	call putc		; display
	pop cx
	pop si
	call handle_serial_flags_ctrl_c
	call dohack
	mov bx, word [dstringaddr]
	mov ds, word [bx+4]	; go back to special state
	retn

.terminated:
	_386_PM_a32
	lodsb			; load character
	cmp al, byte [ss:dstringtype]
	je .done		; it's the terminator -->
	call .char		; display
	jmp short .terminated	; and get next -->

.done:
	push ss
	pop ds			; restore ds
	_386_PM_o32	; mov dword [bx], esi
	mov word [bx], si
	call unhack
	mov al, 13
	call putc
	mov al, 10
	call putc
	retn
%endif

%if _INT
		; DI command
gateout:
	call skipwhite
	call getbyte	; get byte into DL
	xor dh, dh
	mov bx, dx
	call skipcomm0
	mov dx, 1
	cmp al, 13
	je .onlyone
	cmp al, ';'
	je .onlyone
	call uppercase
	cmp al, 'L'
	jne .notlength
	call skipcomma
	call getword	; get byte into DL
	or dx, dx
	jz .err
	cmp dx, 100h
	je .checkrange
	push ax
	and ah, 1Fh
	cmp ah, 8
	pop ax
	ja .err
.checkrange:
	push dx
	add dx, bx
	cmp dx, 100h
	pop dx
	jna .rangeok
.err:
	jmp error

.last:
	xor bx, bx
	mov bl, byte [lastint]
	mov dx, 1
	inc bl
	jnz .onlyone
	mov word [lastcmd], dmycmd
	retn

.notlength:
	call getbyte
	xor dh, dh
	sub dl, bl
	inc dx
.rangeok:
	call chkeol
.onlyone:
	mov word [lastcmd], .last
	call prephack
	mov si, dx	; save count
.next:
	mov byte [lastint], bl
	call dohack
	mov di, line_out
	mov ax, "in"
	stosw
	mov ax, "t "
	stosw
	mov al, bl
	call hexbyte
	mov al, 32
	stosb
%if _PM
	call ispm
	jnz .rm
cpu 286
	mov ax, 0204h
	cmp bl, 20h
	adc bh, 1	; if below, bh = 2
.loopexception:
	int 31h
	jc .failed
	mov ax, cx
	call hexword
	mov al, ':'
	stosb
	_386_PM_o32	; mov eax, edx
	mov ax, dx
	cmp byte [dpmi32], 0
	jz .gate16
	call hexword_high
.gate16:
	call hexword
	mov al, 32
	stosb
	mov ax, 0202h
	dec bh
	jnz .loopexception
	call unhack
	push bx
	call putsline_crlf
	pop bx
	inc bx
	dec si
	jnz .next
	retn
.rm:
%endif
	mov cl, 2
	push bx
	push ds
	shl bx, cl
	xor ax, ax
	mov ds, ax
	mov ax, word [bx+2]
	mov dx, word [bx+0]
	pop ds
	pop bx
	call hexword
	mov al, ':'
	stosb
	mov ax, dx
%if _PM
	mov bh, 1
	jmp short .gate16

.failed:
	call unhack
	mov dx, gatewrong
	jmp putsz
%else
	call hexword
	call unhack
	push bx
	call putsline_crlf
	pop bx
	inc bx
	dec si
	jnz .next
	retn
%endif
%endif

%if _MCB
		; DM command
mcbout:
	call skipwhite
	mov dx, word [wMCB]
	cmp al, 13
	je .lolmcb
	cmp al, ';'
	je .lolmcb
	call getword
	call chkeol
.lolmcb:
	mov si, dx
	mov di, line_out
	mov ax, "PS"
	stosw
	mov ax, "P:"
	stosw
	mov al, 32
	stosb
	mov ax, word [pspdbe]
	call hexword
	call putsline_crlf	; destroys cx,dx,bx
	mov cl, 'M'
.next:
	cmp si, byte -1
	je .invmcb
	cmp si, byte 50h
	jae .valmcb
.invmcb:
	mov dx, msg.invmcbadr
	jmp putsz
.valmcb:
	mov di, line_out
	push ds
%if _PM
	call setds2si
%else
	mov ds, si
%endif
	mov ch, byte [0000]
	mov bx, word [0001]
	mov dx, word [0003]

	mov ax, si
	call hexword		; segment address of MCB
	mov al, 32
	stosb
	mov al, ch
	call hexbyte		; 'M' or 'Z'
	mov al, 32
	stosb
	mov ax, bx
	call hexword		; MCB owner
	mov al, 32
	stosb
	mov ax, dx
	call hexword		; MCB size in paragraphs

	mov al, 32
	stosb
	mov ax, dx		; ax = size in paragraphs
	push bx
	push ax
	push dx
	push cx
	xor dx, dx		; dx:ax = size in paragraphs
	mov cx, 16		; cx = 16, multiplier (get size in bytes)
	mov bx, 4+4		; bx = 4+4, width

	call disp_dxax_times_cx_width_bx_size
	pop cx
	pop dx
	pop ax
	pop bx

	test bx, bx
	jz .freemcb		; free MCBs have no name -->
	mov al, 32
	stosb
	push si
	push cx
	push dx
	mov si, 8
	mov cx, 2
	cmp bx, si		; is it a "system" MCB? (owner 0008h or 0007h)
	ja @F
	cmp byte [si], "S"	; "S", "SD", "SC" ?
	je .nextmcbchar		; yes, limit name to two characters -->
	jmp .nextmcbchar_cx_si	; no, assume full name given
@@:
	dec bx			; => owner block's MCB
%if _PM
	call setds2bx
%else
	mov ds, bx
%endif
.nextmcbchar_cx_si:
	mov cx, si		; = 8
.nextmcbchar:			; copy name of owner MCB
	lodsb
	stosb
	or al, al
	loopnz .nextmcbchar	; was not NUL and more bytes left ?
	test al, al
	jnz @F
	dec di
@@:

	cmp word [1], 8
	jne .not_s_mcb
	cmp word [8], "S"	; S MCB ?
	jne .not_s_mcb

	mov ax, " t"
	stosw
	mov ax, "yp"
	stosw
	mov ax, "e "
	stosw

	xor ax, ax
	mov al, [10]
	call hexbyte

	push ds
	 push ss
	 pop ds
	mov si, smcbtypes
.s_mcb_loop:
	cmp word [si], -1
	je .s_mcb_unknown
	cmp word [si], ax
	je .s_mcb_known
	add si, 4
	jmp .s_mcb_loop

.s_mcb_known:
	mov si, word [si + 2]
	jmp .s_mcb_common

.s_mcb_unknown:
	mov si, smcbmsg_unknown
.s_mcb_common:
	mov al, 32
@@:
	stosb
	lodsb
	test al, al
	jnz @B

	pop ds

.not_s_mcb:
	pop dx
	pop cx
	pop si
.freemcb:

	pop ds
	cmp ch, 'M'
	je .disp
	cmp ch, 'Z'
	je .disp
.ret:
	retn

.disp:
	mov cl, ch
	push dx
	push cx
	call putsline_crlf	; destroys cx,dx,bx
	pop cx
	pop dx
	add si, dx
	jc .ret			; over FFFFh, must be end of chain --> (hmm)
	inc si
	jz .ret
	jmp .next

%if _PM
setds2si:
	mov bx, si
setds2bx:
	call ispm
	jnz sd2s_ex
	mov dx, bx
	call setrmsegm
sd2s_ex:
	mov ds, bx
	retn
%endif	; _PM
%endif	; _MCB

;--- DX command. Display extended memory

%if _PM
cpu 386
extmem:
	mov dx, word [x_addr+0]
	mov bx, word [x_addr+2]
	call skipwhite
	cmp al, ';'
	je extmem_1
	cmp al, 13
	jz extmem_1
	call getdword		; get linear address into bx:dx
	call chkeol		; expect end of line here
extmem_1:
	mov word [lastcmd], extmem
	push bx
	push dx
	pop ebp

	mov di, stack		; create a GDT for Int15.87
	xor ax, ax
	mov cx, 8
	rep stosw
	mov ax, 007Fh
	stosw
	mov ax, dx
	stosw
	mov al, bl
	stosb
	mov ax, 0093h
	stosw
	mov al, bh
	stosb
	mov ax, 007Fh
	stosw
	mov ax, line_in+128
	mov bx, word [pspdbg]
	movzx ebx, bx
	shl ebx, 4
	movzx eax, ax
	add eax, ebx		; eax = flat address of line_in+128
	stosw
	shr eax, 16
	stosb
	mov bl, ah
	mov ax, 0093h
	stosw
	mov al, bl
	stosb
	mov cx, 8
	xor ax, ax
	rep stosw

	call ispm
	mov si, stack
	mov cx, 0040h
	mov ah, 87h
	jnz extmem_rm
	push word [pspdbg]
	push 15h
	call intcall
	jmp short i15ok
extmem_rm:
	int 15h
i15ok:
	jc extmem_exit
	mov si, line_in+128
	mov ch, 8h
nexti15l:
	call handle_serial_flags_ctrl_c
	mov di, line_out
	mov eax, ebp
	shr eax, 16
	call hexword
	mov ax, bp
	call hexword
	mov ax, 32<<8|32
	stosw
	mov bx, line_out+10+3*16
	mov cl, 10h
nexti15b:
	lodsb
	call dd_store
	dec cl
	jnz nexti15b
	mov byte [di-(8*3+1)], '-'	; display a '-' after 8 bytes
	add di, 16
	push cx
	call putsline_crlf
	pop cx
	add ebp, byte 10h
	dec ch
	jnz nexti15l
	mov dword [x_addr], ebp
extmem_exit:
	retn
cpu 8086
%endif

		; INP:	dx:ax = numerator
		;	cx = multiplier
		;	bx = field width
		;	es:di -> buffer where to store
		; STT:	UP
		; OUT:	written to buffer, es:di -> behind written string
disp_dxax_times_cx_width_bx_size:
	lframe near
	lvar 4 + 4 + 2, buffer
	lvar 6, dividend
	lenter
	 lvar word, width
	 push bx
	push si
	push ds
	push cx
	push ax
	push dx
	push es
	push di

	 push ss	; push cs
	 pop ds
	 push ss
	 pop es

	push dx
	mul cx
	xchg ax, di
	xchg dx, si		; si:di = first mul

	pop ax
	mul cx
	add ax, si
	adc dx, 0		; dx:ax = second mul + adj, dx:ax:di = mul

	mov cx, di		; dx:ax:cx = mul

	mov word [bp + ?dividend], cx
	mov word [bp + ?dividend + 2], ax
	mov word [bp + ?dividend + 4], dx

	mov si, msg.prefixes
.loop:
	cmp word [bp + ?dividend + 4], 0
	jnz .divide
	cmp word [bp + ?dividend + 2], 0
	jnz .divide
	cmp word [bp + ?dividend], 2048
	jbe .end
.divide:
	inc si
	mov cx, 1024		; 1000 here if SI units
	testopt [options], use_si_units	; SI units ?
	jz @F			; no -->
	mov cx, 1000		; yes, use 1000
@@:

	xor dx, dx
	mov di, 6
.loop_divide:
	mov ax, [bp + ?dividend - 2 + di]
	div cx
	mov word [bp + ?dividend - 2 + di], ax
	dec di
	dec di
	jnz .loop_divide
				; dx = last remainder
	jmp .loop

.end:
	lea di, [bp + ?buffer + 4 + 4 + 1]
	std			; _AMD_ERRATUM_109_WORKAROUND does not apply
	mov al, "B"
	stosb
	mov al, [si]
	cmp al, 32
	je @FF

	testopt [options], use_si_units | use_jedec_units ; SI / JEDEC units ?
	jnz @F			; yes -->
	and al, ~20h		; uppercase, don't do this if SI units
	push ax
	mov al, "i"
	stosb			; don't store this if SI units
	pop ax
@@:
	stosb
@@:
	mov al, 32
	stosb

	mov ax, word [bp + ?dividend]
	mov cx, 10
.loop_write:
	xor dx, dx
	div cx
	xchg ax, dx
				; ax = remainder (next digit)
				; dx = result of div
	add al, '0'
	stosb
	xchg ax, dx		; ax = result of div
	test ax, ax		; any more ?
	jnz .loop_write		; loop -->

	cld

	lea bx, [bp + ?buffer + 4 + 4 + 1]
	sub bx, di
	mov si, di

	pop di
	pop es

	mov cx, [bp + ?width]
	sub cx, bx
	jbe .none_blank
.loop_blank:
	mov al, 32
	stosb
	loop .loop_blank
.none_blank:

	mov cx, bx
.loop_disp:
	inc si
	mov al, [ss:si]
	stosb
	loop .loop_disp

	pop dx
	pop ax
	pop cx
	pop ds
	pop si
	pop bx
	lleave
	lret

