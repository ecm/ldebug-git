
%if 0

lDebug messages

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2012 C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif


	usesection lDEBUG_DATA_ENTRY

msg:
.help:
	db _PROGNAME,_VERSION," help screen",13,10
	db "assemble",9,	"A [address]",13,10
%if 0
	db "set option bits",9,	"BS value",13,10
	db "unset opt bits",9,	"BU value",13,10
	db "reset opt bits",9,	"BR value",13,10
%endif
%if _BREAKPOINTS
	db "set breakpoint",9,	"B[P] index|NEW address",13,10	; "[X/R/W/RW]" " [cond]"
	db " clear",9,9,	"BC index|ALL",13,10
	db " disable",9,	"BD index|ALL",13,10
	db " enable",9,9,	"BE index|ALL",13,10
	db " toggle",9,9,	"BT index|ALL",13,10
	db " list",9,9,		"BL [index|ALL]",13,10
%endif
%if _DEBUG
;	db "assume InDOS",9,	"BD [0|1]",13,10
	db "break upwards",9,	"BU",13,10
%endif
	db "compare",9,9,	"C range address",13,10
	db "dump",9,9,		"D [range]",13,10
%if _INT
	db "dump interrupts",9,	"DI interrupt [count]",13,10
%endif
%if _PM
	db "dump LDT",9,	"DL selector [count]",13,10
%endif
%if _MCB
	db "dump MCB chain",9,	"DM",13,10
	;db "dump S/SD MCBs",9,	"DS",13,10
%endif
%if _DSTRINGS
	db "display strings",9,	"DZ/D$/D[W]# [address]",13,10
%endif
%if _PM
	db "dump ext memory",9,	"DX physical_address",13,10
%endif
	db "enter",9,9,		"E address [list]",13,10
	db "fill",9,9,		"F range list",13,10
	db "go",9,9,		"G [=address] [breakpts]",13,10
	db "hex add/sub",9,	"H value1 value2",13,10
	db "input",9,9,		"I[W|D] port",13,10
	db "load program",9,	"L [address]",13,10
	db "load sectors",9,	"L address drive sector count",13,10
	db "move",9,9,		"M range address",13,10
	db "80x86/x87 mode",9,	"M [0..6|C|NC|C2|?]",13,10
	db "set name",9,	"N [[drive:][path]progname.ext [parameters]]",13,10
	db "output",9,9,	"O[W|D] port value",13,10
	db "proceed",9,9,	"P [=address] [count [WHILE cond] [SILENT [count]]]",13,10
	db "quit",9,9,		"Q",13,10
	db "register",9,	"R [register [value]]",13,10
%if _MMXSUPP
	db "MMX register",9,	"RM",13,10
%endif
%if _RN
	db "FPU register",9,	"RN",13,10
%endif
	db "toggle 386 regs",9,	"RX",13,10
	db "search",9,9,	"S range list",13,10
	db "trace",9,9,		"T [=address] [count [WHILE cond] [SILENT [count]]]",13,10
	db "trace (exc str)",9
	db			"TP [=address] [count [WHILE cond] [SILENT [count]]]",13,10
	db "trace mode",9,	"TM [0|1]",13,10
%if _TSR
	db "enter TSR mode",9,  "TSR",13,10
%endif
	db "unassemble",9,	"U [range]",13,10
	db "write program",9,	"W [address]",13,10
	db "write sectors",9,	"W address drive sector count",13,10
%if _EMS
	db "expanded mem",9,	"XA/XD/XM/XR/XS, X? for help",13,10
%endif
	db 13,10
	db "Additional help topics:",13,10
%if _EXTHELP
	db " Registers",9,	"?R",13,10
	db " Flags",9,9,	"?F",13,10
 %if _COND
	db " Conditionals",9,	"?C",13,10
 %endif
 %if _EXPRESSIONS
	db " Expressions",9,	"?E",13,10
 %endif
 %if _VARIABLES || _OPTIONS || _PSPVARIABLES
	db " Variables",9,	"?V",13,10
 %endif
	db " Run keywords",9,	"?RUN",13,10
 %if _OPTIONS
	db " Options",9,	"?O",13,10
 %endif
 %if _BOOTLDR
	db " Boot loading",9,	"?BOOT",13,10
 %endif
%endif
	db " lDebug build",9,	"?BUILD",13,10
	db " lDebug build",9,	"?B",13,10
%if _EXTHELP
	db " lDebug license",9,	"?L",13,10
%endif
%if _PM
	db 13,10
	db "Prompts: '-' = real or V86 mode; '#' = protected mode",13,10
%endif
	asciz

.run:
	asciz "RUN"
.help_run:
	db "T (trace), TP (trace except proceed past string operations), and P (proceed)",13,10
	db "can be followed by a number of repetitions and then the keyword WHILE,",13,10
	db "which must be followed by a conditional expression.",13,10
	db 13,10
	db "The selected run command is repeated as many times as specified by the",13,10
	db "number, or until the WHILE condition evaluates no longer to true.",13,10
	db 13,10
	db "After the number of repetitions or (if present) after the WHILE condition",13,10
	db "the keyword SILENT may follow. If that is the case, all register dumps",13,10
	db "done during the run are buffered by the debugger and the run remains",13,10
	db "silent. After the run, the last dumps are replayed from the buffer",13,10
	db "and displayed. At most as many dumps as fit into the buffer are",13,10
	db "displayed. (The buffer is currently up to 8 KiB sized.)",13,10
	db 13,10
	db "If a number follows behind the SILENT keyword, only at most that many",13,10
	db "dumps are displayed from the buffer. The dumps that are displayed",13,10
	db "are always those last written into the buffer, thus last occurred.",13,10
	asciz

	align 2
.build_array:
	dw .build_nameversion
	dw .build_lmacros
	dw .build_inicomp
	dw .build_ldosboot
.build_short_amount: equ ($ - .build_array) / 2
	dw .build_long
.build_long_amount: equ ($ - .build_array) / 2

.string_build:
	asciz "BUILD"

.build_nameversion:
	db _PROGNAME,_VERSION,13,10
%ifnidn _REVISIONID,""
	db "Source Control Revision ID: ",_REVISIONID,13,10
%endif
	asciz
	_fill 128, 0, .build_nameversion
.build_lmacros:
	fill 64, 0, asciz _REVISIONID_LMACROS
.build_inicomp:
	fill 64, 0, asciz _REVISIONID_INICOMP
.build_ldosboot:
	fill 64, 0, asciz _REVISIONID_LDOSBOOT

.build_long:
%if _EXTHELP
	db 13,10
 %if _PM
	db "DPMI-capable",13,10
 %endif
 %if _PM && _NOEXTENDER
	db " DPMI host without extender",13,10
 %endif
 %if _PM && _WIN9XSUPP
	db " No Windows 4 DPMI hook",13,10
 %endif
 %if _PM && _DOSEMU
	db " No DOSEMU DPMI hook",13,10
 %endif
 %if _PM && _EXCCSIP
	db " Display exception address",13,10
 %endif
 %if _PM && _DISPHOOK
	db " Display hooking DPMI entry",13,10
 %endif
 %if _DEBUG
	db "Debuggable",13,10
 %endif
 %if _INT
	db "DI command",13,10
 %endif
 %if _MCB
	db "DM command",13,10
 %endif
 %if _DSTRINGS
	db "D string commands",13,10
 %endif
 %if _SDUMP
	db "S match dumps line of following data",13,10
 %endif
 %if _RN
	db "RN command",13,10
 %endif
 %if _USESDA
	db "Access SDA current PSP field",13,10
 %endif
 %if _VDD
	db "Load NTVDM VDD for sector access",13,10
 %endif
 %if _EMS
	db "X commands for EMS access",13,10
 %endif
 %if _MMXSUPP
	db "RM command and reading MMX registers as variables",13,10
 %endif
 %if _EXPRESSIONS
	db "Expression evaluator",13,10
 %endif
 %if _INDIRECTION
	db " Indirection in expressions",13,10
 %endif
 %if _VARIABLES
	db "Variables with user-defined purpose",13,10
 %endif
 %if _OPTIONS
	db "Debugger option and status variables",13,10
 %endif
 %if _PSPVARIABLES
	db "PSP variables",13,10
 %endif
 %if _COND
	db "Conditional jump notice in register dump",13,10
 %endif
 %if _TSR
	db "TSR mode (Process detachment)",13,10
 %endif
 %if _DEVICE
	db "Loadable device driver",13,10
 %endif
 %if _BOOTLDR
	db "Boot loader",13,10
 %endif
 %if _BREAKPOINTS
	db "Permanent breakpoints",13,10
 %endif
%push
	db "Intercepted"
%if _PM
	db " 86M"
%endif
	db " interrupts:"
 %define %$pref " "
%macro dispint 2.nolist
 %if %1
	db %$pref, %2
  %define %$pref ", "
 %endif
%endmacro
	dispint 1, "00"
	dispint _CATCHINT01, "01"
	dispint _CATCHINT03, "03"
	dispint _CATCHINT06, "06"
	dispint _CATCHINT18, "18"
	dispint _CATCHINT19, "19"
	dispint _PM, "2F.1687 (internal)"
 %ifidn %$pref," "
	db " none"
 %endif
	db 13,10
 %if _PM
	db "Intercepted DPMI exceptions:"
  %define %$pref " "
	dispint 1, "00"
	dispint 1, "01"
	dispint 1, "03"
	dispint _CATCHEXC06, "06"
	dispint _CATCHEXC0C, "0C"
	dispint 1, "0D"
	dispint 1, "0E"
  %ifidn %$pref," "
	db " none"
  %endif
	db 13,10
 %endif
%unmacro dispint 2.nolist
%pop
 %if _EXTHELP
	db "Extended built-in help pages",13,10
 %endif
 %if _ONLYNON386
	db "Only supports non-386 operation",13,10
 %endif
 %if _ONLY386
	db "Only supports 386+ operation",13,10
 %endif
%else
 %if _BOOTLDR
	asciz
	; This message is used by mak.sh to detect that we
	;  are building with boot load support.
	db 13,10,"Boot loader",13,10
 %endif
%endif
	asciz

%if _EXTHELP
.license:
	db "lDebug - libre 86-DOS debugger",13,10
	db 13,10
	db "Copyright (C) 1995-2003 Paul Vojta",13,10
	db "Copyright (C) 2008-2012 C. Masloch",13,10
	db 13,10
	db "Usage of the works is permitted provided that this",13,10
	db "instrument is retained with the works, so that any entity",13,10
	db "that uses the works is notified of this instrument.",13,10
	db 13,10
	db "DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.",13,10
	db 13,10
	db 13,10
	db "This is the license and copyright information that applies to lDebug; but note",13,10
	db "that there have been substantial contributions to the code base that are not",13,10
	db "copyrighted (public domain).",13,10
	asciz

.reghelp:
	db "Available 16-bit registers:",9,9,"Available 32-bit registers: (386+)",13,10
	db "AX",9,"Accumulator",9,9,9,"EAX",13,10
	db "BX",9,"Base register",9,9,9,"EBX",13,10
	db "CX",9,"Counter",9,9,9,9,"ECX",13,10
	db "DX",9,"Data register",9,9,9,"EDX",13,10
	db "SP",9,"Stack pointer",9,9,9,"ESP",13,10
	db "BP",9,"Base pointer",9,9,9,"EBP",13,10
	db "SI",9,"Source index",9,9,9,"ESI",13,10
	db "DI",9,"Destination index",9,9,"EDI",13,10
	db "DS",9,"Data segment",13,10
	db "ES",9,"Extra segment",13,10
	db "SS",9,"Stack segment",13,10
	db "CS",9,"Code segment",13,10
	db "FS",9,"Extra segment 2 (386+)",13,10
	db "GS",9,"Extra segment 3 (386+)",13,10
	db "IP",9,"Instruction pointer",9,9,"EIP",13,10
	db "FL",9,"Flags",9,9,9,9,"EFL",13,10
	db 13,10
 %if _MMXSUPP
	db "Available 64-bit Matrix Math Extension (MMX) registers: (if supported)",13,10
	db "MMx",9,"MM(x)",9,"MMX register x, where x is 0 to 7",13,10
	db 13,10
 %endif
	db "Enter ?F to display the recognized flags.",13,10
	asciz

.flaghelp:
	db "Recognized flags:",13,10
	db "Value",9,"Name",9,9,9,	"  Set",9,9,9,		"  Clear",13,10
	db "0800  OF  Overflow Flag",9,9,"OV  Overflow",9,9,	"NV  No overflow",13,10
	db "0400  DF  Direction Flag",9,"DN  Down",9,9,		"UP  Up",13,10
	db "0200  IF  Interrupt Flag",9,"EI  Enable interrupts",9,"DI  Disable interrupts",13,10
	db "0080  SF  Sign Flag",9,9,	"NG  Negative",9,9,	"PL  Plus",13,10
	db "0040  ZF  Zero Flag",9,9,	"ZR  Zero",9,9,		"NZ  Not zero",13,10
	db "0010  AF  Auxiliary Flag",9,"AC  Auxiliary carry",9,"NA  No auxiliary carry",13,10
	db "0004  PF  Parity Flag",9,9,	"PE  Parity even",9,9,	"PO  Parity odd",13,10
	db "0001  CF  Carry Flag",9,9,	"CY  Carry",9,9,	"NC  No carry",13,10
	db 13,10
	db "The short names of the flag states are displayed when dumping registers",13,10
	db "and can be entered to modify the symbolic F register with R. The short",13,10
	db "names of the flags can be modified by R.",13,10
	asciz

 %if _COND
.condhelp:
	db "In the register dump displayed by the R, T, P and G commands, conditional",13,10
	db "jumps are displayed with a notice that shows whether the instruction will",13,10
	db "cause a jump depending on its condition and the current register and flag",13,10
	db 'contents. This notice shows either "jumping" or "not jumping" as appropriate.',13,10
	db 13,10
	db "The conditional jumps use these conditions: (second column negates)",13,10
	db " jo",9,9,"jno",9,9,"OF",13,10
	db " jc jb jnae",9,"jnc jnb jae",9,"CF",13,10
	db " jz je",9,9,"jnz jne",9,9,"ZF",13,10
	db " jbe jna",9,"jnbe ja",9,9,"ZF||CF",13,10
	db " js",9,9,"jns",9,9,"SF",13,10
	db " jp jpe",9,9,"jnp jpo",9,9,"PF",13,10
	db " jl jnge",9,"jnl jge",9,9,"OF^^SF",13,10
	db " jle jng",9,"jnle jg",9,9,"OF^^SF || ZF",13,10
	db " j(e)cxz",9,9,9,"(e)cx==0",13,10
	db " loop",9,9,9,9,"(e)cx!=1",13,10
	db " loopz loope",9,9,9,"(e)cx!=1 && ZF",13,10
	db " loopnz loopne",9,9,9,"(e)cx!=1 && !ZF",13,10
	db 13,10
	db "Enter ?F to display a description of the flag names.",13,10
	asciz
 %endif

 %if _EXPRESSIONS
.expressionhelp:
	db "Recognized operators in expressions:",13,10
	db "|",9,	"bitwise OR",9,9,		"||",9,	"boolean OR",13,10
	db "^",9,	"bitwise XOR",9,9,		"^^",9,	"boolean XOR",13,10
	db "&",9,	"bitwise AND",9,9,		"&&",9,	"boolean AND",13,10
	db ">>",9,	"bit-shift right",9,9,		">",9,"test if above",13,10
	db ">>>",9,	"signed bit-shift right",9,	"<",9,"test if below",13,10
	db "<<",9,	"bit-shift left",9,9,		">=",9,"test if above-or-equal",13,10
	db "><",9,	"bit-mirror",9,9,		"<=",9,"test if below-or-equal",13,10
	db "+",9,	"addition",9,9,			"==",9,"test if equal",13,10
	db "-",9,	"subtraction",9,9,		"!=",9,"test if not equal",13,10
	db "*",9,	"multiplication",9,9,		"=>",9,"same as >=",13,10
	db "/",9,	"division",9,9,			"=<",9,"same as <=",13,10
	db "%",9,	"modulo (A-(A/B*B))",9,		"<>",9,"same as !=",13,10
	db "**",9,	"power",13,10
	db 13,10
	db "Implicit operater precedence is handled in the listed order, with increasing",13,10
	db "precedence: (Brackets specify explicit precedence of an expression.)",13,10
	db " boolean operators OR, XOR, AND (each has a different precedence)",13,10
	db " comparison operators",13,10
	db " bitwise operators OR, XOR, AND (each has a different precedence)",13,10
	db " shift and bit-mirror operators",13,10
	db " addition and subtraction operators",13,10
	db " multiplication, division and modulo operators",13,10
	db " power operator",13,10
	db 13,10
	db "Recognized unary operators: (modifying the next number)",13,10
	db "+",9,	"positive (does nothing)",13,10
	db "-",9,	"negative",13,10
	db "~",9,	"bitwise NOT",13,10
	db "!",9,	"boolean NOT",13,10
	db "?",9,	"absolute value",13,10
	db "!!",9,	"convert to boolean",13,10
	db 13,10
	db "Note that the power operator does not affect unary operator handling.",13,10
	db 'For instance, "- 2 ** 2" is parsed as "(-2) ** 2" and evaluates to 4.',13,10
	db 13,10
	db "Although a negative unary and signed bit-shift right operator are provided",13,10
	db "the expression evaluator is intrinsically unsigned. Particularly the division,",13,10
	db "multiplication, modulo and all comparison operators operate unsigned. Due to",13,10
	db 'this, the expression "-1 < 0" evaluates to zero.',13,10
	db 13,10
	db "Recognized terms in an expression:",13,10
	db " 32-bit immediates",13,10
	db " 8-bit registers",13,10
	db " 16-bit registers including segment registers (except FS, GS)",13,10
	db " 32-bit compound registers made of two 16-bit registers (eg DXAX)",13,10
	db " 32-bit registers and FS, GS only if running on a 386+",13,10
  %if _MMXSUPP
	db " 64-bit MMX registers only if running on a CPU with MMX (r/o for now)",13,10
	db "  MM0L, MM(0)L accesses the low 32 bits of the register",13,10
	db "  MM0H, MM(0)H accesses the high 32 bits of the register",13,10
	db "  MM0Z, MM(0)Z reads the low 32 bits; writes the full register (zero-extend)",13,10
	db "  MM0S, MM(0)S reads the low 32 bits; writes the full register (sign-extend)",13,10
	db "  MM0, MM(0) is an alias for the MM0Z syntax",13,10
  %endif
  %if _VARIABLES
	db " 32-bit variables V0..VF",13,10
  %endif
  %if _OPTIONS || _PSPVARIABLES
	db " 32-bit special variable"
   %if _OPTIONS
	db "s DCO, DCS, DAO, DAS, DIF, DPI"
    %if _PSPVARIABLES
	db ","
    %endif
   %endif
   %if _PSPVARIABLES
	db " PPI"
   %endif
	db 13,10
	db " 16-bit special variables"
   %if _OPTIONS
	db " DPR, DPP"
    %if _PM
	db ", DPS"
    %endif
    %if _PSPVARIABLES
	db ","
    %endif
   %endif
   %if _PSPVARIABLES
	db " PSP, PPR"
   %endif
	db 13,10
  %endif
  %if _INDIRECTION
	db " byte/word/dword memory content (eg byte [seg:ofs], where both the optional",13,10
	db "  segment as well as the offset are expressions too)",13,10
  %endif
	db "The expression evaluator case-insensitively checks for names of variables",13,10
	db "and registers"
  %if _INDIRECTION
	db		" as well as size specifiers"
  %endif
	db					   '.',13,10
	db 13,10
	db "Enter ?R to display the recognized register names.",13,10
  %if _VARIABLES || _OPTIONS || _PSPVARIABLES
	db "Enter ?V to display the recognized variables.",13,10
  %endif
	asciz
 %endif

 %if _OPTIONS
.ophelp:
	db "Available options: (read/write DCO, read DCS)",13,10
	db _4digitshex(dispregs32),	" RX: 32-bit register display",13,10
	db _4digitshex(traceints),	" TM: trace into interrupts",13,10
	db _4digitshex(cpdepchars),	" allow dumping of CP-dependant characters",13,10
	db _4digitshex(fakeindos),	" always assume InDOS flag non-zero, to debug DOS or TSRs",13,10
	db _4digitshex(nonpagingdevice)," disallow paged output to StdOut",13,10
	db _4digitshex(pagingdevice),	" allow paged output to non-StdOut",13,10
	db _4digitshex(hexrn),		" display raw hexadecimal content of FPU registers",13,10
	db _4digitshex(nondospaging),	" when prompting during paging, do not use DOS for input",13,10
	db _4digitshex(nohlt),		" do not execute HLT instruction to idle",13,10
	db _4digitshex(biosidles),	" do not idle, the keyboard BIOS idles itself",13,10
	db _4digitshex(use_si_units),	" in disp_*_size use SI units (kB = 1000, etc)."
					db " overrides ",_4digitshex(use_jedec_units),"!",13,10
	db _4digitshex(use_jedec_units)," in disp_*_size use JEDEC units (kB = 1024)",13,10
	db _4digitshex(enable_serial)," enable serial I/O (port ",_4digitshex(_UART_BASE),"h interrupt ",_2digitshex(_INTNUM),"h)",13,10
	db _4digitshex(int8_disable_serial)," disable serial I/O when breaking after 5 seconds Ctrl pressed",13,10
	db 13,10
	db "Internal flags: (read DIF)",13,10
	db _6digitshex(oldpacket),	" Int25/Int26 packet method available",13,10
	db _6digitshex(newpacket),	" Int21.7305 packet method available",13,10
  %if _VDD
	db _6digitshex(ntpacket),	" VDD registered and usable",13,10
  %endif
	db _6digitshex(pagedcommand),	" internal flag for paged output",13,10
	db _6digitshex(notstdinput),	" DEBUG's input isn't StdIn",13,10
	db _6digitshex(inputfile),	" DEBUG's input is a file",13,10
	db _6digitshex(notstdoutput),	" DEBUG's output isn't StdOut",13,10
	db _6digitshex(outputfile),	" DEBUG's output is a file",13,10
  %if _PM
	db _6digitshex(hooked2F),	" Int2F.1687 hooked",13,10
	db _6digitshex(nohook2F),	" Int2F.1687 won't be hooked",13,10
	db _6digitshex(dpminohlt),	" do not execute HLT to idle in PM",13,10
	db _6digitshex(protectedmode),	" in protected mode",13,10
  %endif
	db _6digitshex(debuggeeA20),	" state of debuggee's A20",13,10
	db _6digitshex(debuggerA20),	" state of debugger's A20 (not implemented: same as previous)",13,10
  %if _BOOTLDR
	db _6digitshex(nodosloaded),	" debugger booted independent of a DOS",13,10
  %endif
	db _6digitshex(has386),		" CPU is at least a 386 (32-bit CPU)",13,10
	db _6digitshex(usecharcounter),	" internal flag for tab output processing",13,10
 %if _VDD
	db _6digitshex(runningnt),	" running inside NTVDM",13,10
 %endif
 %if _PM
	db _6digitshex(canswitchmode),	" DPMI raw mode switch usable to set breakpoints",13,10
	db _6digitshex(modeswitched),	" internal flag for mode switching",13,10
 %endif
	db _6digitshex(promptwaiting),	" internal flag for paged output",13,10
 %if _PM
	db _6digitshex(switchbuffer),	" internal flag for mode switching",13,10
 %endif
 %if _TSR
	db _6digitshex(tsrmode),	" in TSR mode (detached debugger process)",13,10
 %endif
 %if _DOSEMU
	db _8digitshex(runningdosemu),	" running inside dosemu",13,10
 %endif
 %if _BOOTLDR
	db _8digitshex(load_is_ldp)
	db " boot load: partition specified as ",'"ldp"',13,10
 %endif
	db _8digitshex(tt_while)
	db " T/TP/P: while condition specified",13,10
	db _8digitshex(tt_p)
	db " TP: P specified (proceed past string ops)",13,10
	db _8digitshex(tt_silent_mode)
	db " T/TP/P: silent mode (SILENT specified)",13,10
	db _8digitshex(tt_silence)
	db " T/TP/P: silent mode is active, writing to silent buffer",13,10
	db 13,10
	db "Available assembler/disassembler options: (read/write DAO, read DAS)",13,10
	db _2digitshex(disasm_lowercase), " Disassembler: lowercase output",13,10
	db _2digitshex(disasm_commablank)," Disassembler: output blank behind comma",13,10
	db _2digitshex(disasm_nasm),      " Disassembler: output addresses in NASM syntax",13,10
	db _2digitshex(disasm_lowercase_refmem)
	db				  " Disassembler: lowercase referenced memory location segreg",13,10
	db _2digitshex(disasm_show_short)," Disassembler: always show SHORT keyword",13,10
	db _2digitshex(disasm_show_near), " Disassembler: always show NEAR keyword",13,10
	db _2digitshex(disasm_show_far),  " Disassembler: always show FAR keyword",13,10
	asciz
 %endif

 %if _VARIABLES || _OPTIONS || _PSPVARIABLES
.varhelp:
	db "Available "
  %if _PSPVARIABLES && !(_VARIABLES || _OPTIONS)
	db "read-only "
  %endif
	db "lDebug variables:",13,10
  %if _VARIABLES
	db "V0..VF",9,"User-specified usage",13,10
  %endif
  %if _OPTIONS
	db "DCO",9,"Debugger Common Options",13,10
	db "DAO",9,"Debugger Assembler/disassembler Options",13,10
  %endif
  %if _OPTIONS || _PSPVARIABLES && (_OPTIONS || _VARIABLES)
	db " The following variables cannot be written:",13,10
  %endif
  %if _PSPVARIABLES
	db "PSP",9,"Debuggee Process"
  %if _PM
	db " (as real mode segment)"
  %endif
	db 13,10
	db "PPR",9,"Debuggee's Parent Process",13,10
	db "PPI",9,"Debuggee's Parent Process Interrupt 22h",13,10
  %endif
  %if _OPTIONS
	db "DIF",9,"Debugger Internal Flags",13,10
	db "DCS",9,"Debugger Common Startup options",13,10
	db "DAS",9,"Debugger Assembler/disassembler Startup options",13,10
	db "DPR",9,"Debugger Process"
   %if _PM
	db " (as Real mode segment)",13,10
	db "DPS",9,"Debugger Process Selector (zero in real mode)"
   %endif
	db 13,10
	db "DPP",9,"Debugger's Parent Process"
  %if _TSR
	db " (zero in TSR mode)"
  %endif
	db 13,10
	db "DPI",9,"Debugger's Parent process Interrupt 22h"
  %if _TSR
	db " (zero in TSR mode)"
  %endif
	db 13,10
	db 13,10
	db "Enter ?O to display the options and internal flags.",13,10
  %endif
	asciz
 %endif
 %if _BOOTLDR
.boothelp:
	db "Boot loading commands:",13,10
	db "BOOT LIST HDA",9,"[note: writes to memory @ 600h and 7C00h]",13,10
	db "BOOT READ|WRITE HDA[num]|FDA segment [sector] [count]",13,10
  %if _DOSEMU
	db "BOOT QUIT",9,"[exits dosemu]",13,10
  %endif
	db "BOOT [PROTOCOL=SECTOR] HDA[num]|FDA",13,10
	db "BOOT PROTOCOL=proto [opt] HDAnum|FDA [filename1] [filename2]",13,10
	db 13,10
	db "Available protocols:",13,10
	db " LDOS",9,9,	"LDOS.COM or L[D]DEBUG.COM at 70h, 0:400h",13,10
	db " FREEDOS",9,"KERNEL.SYS or METAKERN.SYS at 60h, 0:0",13,10
	db " EDRDOS",9,9,"DRBIO.SYS at 70h, 0:0",13,10
	db " MSDOS6",9,9,	"IO.SYS + MSDOS.SYS at 70h, 0:0",13,10
	db " MSDOS7",9,9,	"IO.SYS at 70h, 0:200h",13,10
	db " IBMDOS",9,9,	"IBMBIO.COM + IBMDOS.COM at 70h, 0:0",13,10
	db " NTLDR",9,9,	"NTLDR at 2000h, 0:0",13,10
	db " RXDOS.0",9,"RXDOSBIO.SYS + RXDOS.SYS at 70h, 0:0",13,10
	db " RXDOS.1",9,"RXBIO.SYS + RXDOS.SYS at 70h, 0:0",13,10
	db " RXDOS.2",9,"RXDOS.COM at 70h, 0:400h",13,10
	db " SECTOR",9,9,"(default) load partition boot sector or MBR",13,10
	db " SECTORALT",9,"as SECTOR, but entry at 07C0h:0",13,10
	db 13,10
	db "Available options:",13,10
	db " MINPARA=num",9,9,	"load at least that many paragraphs",13,10
	db " MAXPARA=num",9,9,	"load at most that many paragraphs",13,10
	db " SEGMENT=num",9,9,	"change segment at that the kernel loads",13,10
	db " ENTRY=[num:]num",9,"change entrypoint (CS (relative) : IP)",13,10
	db " BPB=[num:]num",9,9, \
		"change BPB load address (segment -1 = auto-BPB)",13,10
	db "Boolean options: [opt=bool]",13,10
	db " SET_DL_UNIT",9,9,"set dl to load unit",13,10
	db " SET_BL_UNIT",9,9,"set bl to load unit",13,10
	db " SET_SIDI_CLUSTER",9,"set si:di to first cluster",13,10
	db " SET_DSSI_DPT",9,9,"set ds:si to DPT address",13,10
	db " PUSH_DPT",9,9,"push DPT address and DPT entry address",13,10
	db " DATASTART_HIDDEN",9,"add hidden sectors to datastart var",13,10
	db " SET_AXBX_DATASTART",9,"set ax:bx to datastart var",13,10
	db " SET_DSBP_BPB",9,9,"set ds:bp to BPB address",13,10
	db " LBA_SET_TYPE",9,9,"set LBA partition type in BPB",13,10
	db " MESSAGE_TABLE",9,9, \
		"provide message table pointed to at 1EEh",13,10
	db " SET_AXBX_ROOT_HIDDEN",9, \
		"set ax:bx to root start with hidden sectors",13,10
	asciz
 %endif
%endif

.readonly:	asciz "This lDebug variable cannot be written to. See ?V.",13,10
.more:		db "[more]"
 .more_size equ $-.more
.more_over:	db 13,"      ",13		; to overwrite previous prompt
 .more_over_size equ $-.more_over
.ctrlc:		db "^C",13,10
 .ctrlc_size equ $-.ctrlc
 		asciz
.regs386:	asciz "386 registers are "
.regs386_off:db "not "
.regs386_on:	asciz "displayed",13,10

%if _EMS
.xhelp:
	db "Expanded memory (EMS) commands:",13,10
	db "  Allocate",9,	"XA count",13,10
	db "  Deallocate",9,	"XD handle",13,10
	db "  Map memory",9,	"XM logical-page physical-page handle",13,10
	db "  Reallocate",9,	"XR handle count",13,10
	db "  Show status",9,	"XS",13,10
	asciz
%endif

%if _MCB
.invmcbadr:	asciz "End of chain: invalid MCB address.",13,10
%endif

%if _TSR
.pspnotfound:	asciz "Cannot go resident, child PSP not found.",13,10
.psphooked:	asciz "Cannot go resident, child PSP parent return address hooked.",13,10
.nowtsr1:	asciz "Patched PSP at "
.nowtsr2:	asciz ", now resident.",13,10
.alreadytsr:	asciz "Already resident.",13,10
%endif
%if _PM && (_TSR || _BOOTLDR)
.cannotpmquit:	asciz "Cannot quit, still in protected mode.",13,10
%endif
%if _BOOTLDR
.nobootsupp:	asciz "Command not supported in boot loaded mode.",13,10
.bootfail:	asciz "Boot failure: "
.bootfail_read:	db "Reading sector failed (error "
.bootfail_read_errorcode:	asciz "__h).",13,10
.bootfail_sig:	asciz "Boot sector signature missing (is not AA55h).",13,10
.bootfail_sig_parttable:	ascii "Partition table signature missing"
				asciz " (is not AA55h).",13,10
.bootfail_code:	asciz "Boot sector code invalid (is 0000h).",13,10
.bootfail_secsizediffer:
		asciz "BPB BpS differs from actual sector size.",13,10
.boot_out_of_memory_error:	asciz "Out of memory.", 13,10
.boot_too_many_partitions_error:asciz "Too many partitions (or a loop).",13,10
.boot_partition_cycle_error:	asciz "Partition table cycle detected.",13,10
.boot_partition_not_found:	asciz "Partition not found.",13,10
.boot_access_error:	asciz "Read error.", 13,10
.boot_sector_too_large:	asciz "Sector size too small (< 32 bytes).", 13,10
.boot_sector_too_small:	asciz "Sector size too large (> 8192 bytes).", 13,10
.boot_sector_not_power:	asciz "Sector size not a power of two.", 13,10
.boot_invalid_sectors:	asciz "Invalid geometry sectors.", 13,10
.boot_invalid_heads:	asciz "Invalid geometry heads.", 13,10
.boot_file_not_found:	asciz "File not found.",13,10
.boot_file_too_big_error:	asciz "File too big.",13,10
.boot_file_too_small_error:	asciz "File too small.",13,10
.boot_badclusters:	asciz "Bad amount of clusters.",13,10
.boot_badchain:		asciz "Bad cluster chain.",13,10
.boot_invalid_filename:	asciz "Invalid filename.",13,10
.boot_cannot_set_both:	asciz "Cannot set both "
.boot_and:		asciz " and "
.boot_dot_crlf:		asciz ".",13,10
.boot_internal_error:	asciz "! Internal error !",13,10
.boot_bpb_load_overlap:	asciz "BPB and load area overlap.",13,10
.boot_segment_too_low:	asciz "Segment too low.",13,10
.boot_bpb_too_low:	asciz "BPB too low.",13,10
.boot_auxbuff_crossing:	db "! Internal error !, "
			asciz "auxbuff crosses 64 KiB boundary.",13,10
.read:		asciz "READ"
.write:		asciz "WRITE"
.boot:		asciz "BOOT"
.quit:		asciz "QUIT"
.protocol:	asciz "PROTOCOL"
.segment:	asciz "SEGMENT"
.entry:		asciz "ENTRY"
.bpb:		asciz "BPB"
.minpara:	asciz "MINPARA"
.maxpara:	asciz "MAXPARA"
.sector:	asciz "SECTOR"
.sector_alt:	asciz "SECTORALT"
.freedos_kernel_name:	asciz "KERNEL.SYS"
.edrdos_kernel_name:	asciz "DRBIO.SYS"
.ldos_kernel_name:	asciz "LDOS.COM"
.msdos7_kernel_name:
.msdos6_kernel_name:	asciz "IO.SYS"
.msdos6_add_name:	asciz "MSDOS.SYS"
.ibmdos_kernel_name:	asciz "IBMBIO.COM"
.ibmdos_add_name:	asciz "IBMDOS.COM"
.ntldr_kernel_name:	asciz "NTLDR"
.bootmgr_kernel_name:	asciz "BOOTMGR"
.rxdos.0_kernel_name:	asciz "RXDOSBIO.SYS"
.rxdos.1_kernel_name:	asciz "RXBIO.SYS"
.rxdos.0_add_name:
.rxdos.1_add_name:	asciz "RXDOS.SYS"
.rxdos.2_kernel_name:	asciz "RXDOS.COM"
.addname_empty:		asciz
.cannotbootquit_memsizes:	asciz "Cannot quit, memory size changed.",13,10
%endif
.while:		asciz "WHILE"
.silent:	asciz "SILENT"
.silent_error:	asciz "! Internal error during silent buffer handling !",13,10
.while_not_true:asciz "While condition not true, returning.",13,10
.while_terminated_before:	asciz "While condition ",'"'
.while_terminated_after:	asciz '"'," no longer true.",13,10
.no_progress:	asciz "No serial comm progress after 5 seconds, giving up. (Keyboard enabled.)",13,10
.serial_request_keep:	asciz 13,10,_PROGNAME," connected to serial port. Enter KEEP to confirm.",13,10
.serial_no_keep_timer:	asciz "No KEEP keyword confirmation after timeout, giving up. (Keyboard enabled.)",13,10
.serial_no_keep_enter:	asciz "No KEEP keyword confirmation, enabling keyboard.",13,10
.keep:		asciz "KEEP"
.prefixes:	asciz " kMGT"
.ll_unterm:	ascizline "Process loading aborted: Attached process didn't terminate!"
.qq_unterm:	ascizline "Cannot quit, attached process didn't terminate!"

%if _INPUT_FILE_HANDLES
.yy_no_dos:		asciz "Y command requires DOS to be available.",13,10
.yy_requires_filename:	asciz "Y command requires a filename.",13,10
.yy_filename_missing_unquote:
			asciz "Y command filename missing ending quote.",13,10
.yy_filename_empty:	asciz "Y command filename is empty.",13,10
.yy_too_many_handles:	asciz "Y command has too many open files.",13,10
.yy_error_file_open:	asciz "Y command failed to open file.",13,10
%endif

%if _BREAKPOINTS
.all:		asciz "ALL"
.new:		asciz "NEW"
.bb_no_new:	asciz "No unused breakpoint left!",13,10
.bb_hit:	db "Hit permanent breakpoint "
.bb_hit.index:	db "__"
.bb_hit.edit:	asciz 13,10
		; db ", c"
		db "ounter="
.bb_hit.counter:asciz "____",13,10

.bb_pass:		db "Passed permanent breakpoint "
.bb_pass.index:		db "__, counter="
.bb_pass.counter:	asciz "____",13,10
.bp:		asciz "BP "
.bpenabled:	asciz " +"
.bpdisabled:	asciz " -"
.bpunused:	asciz " Unused"
.bpaddress:	asciz " Lin="
.bpcontent:	asciz " ("
.bpcounter:	asciz ") Counter="
%if 0
BP 00 Unused
BP 00 + Lin=12345678 (CC) Counter=8000
1234567890123456789012345678901234567890
%endif
.bpnone:	asciz "No breakpoints set currently.",13,10
%endif
.cant_bp_the:			asciz "The "
.cant_bp_type_proceed:		asciz "proceed breakpoint"
.cant_bp_type_permanent:	db    "permanent breakpoint "
.cant_bp_type_permanent.index:	asciz "__"
.cant_bp_type_gg:		asciz " G breakpoint"
.cant_bp_linear:		db    " (linear "
.cant_bp_linear.address1:	db    "----_"
.cant_bp_linear.address2:	asciz "----) "
.cant_bp_write:			asciz "cannot be written. "
.cant_bp_restore:		db    "cannot be restored to "
.cant_bp_restore.value:		asciz "__. "
.cant_bp_reason0:		asciz "No error. (Internal error, report!)",13,10
.cant_bp_reason1:		asciz "It is read-only.",13,10
.cant_bp_reason2:		asciz "It is unreachable.",13,10
.cant_bp_reason3:		db    "It has been overwritten with "
.cant_bp_reason3.value:		asciz "__.",13,10
.cant_bp_reasonu:		asciz "Unknown error. (Internal error, report!)",13,10

.list_bp_first:	asciz "   "
.list_bp:	db " G breakpoint, linear "
.list_bp.address1:
		db "----_"
.list_bp.address2:
		db "----, content "
.list_bp.value:
		asciz "__"
.list_bp_not_cseip: equ crlf
%if _PM
.list_bp_cseip_32:
		asciz " (is at CS:EIP)",13,10
%endif
.list_bp_csip_16:
		asciz " (is at CS:IP)",13,10
.list_bp_none:
		asciz "The G breakpoint list is empty.",13,10

.empty_message:	asciz
.list:		asciz "LIST"
.again:		asciz "AGAIN"
.uu_too_many_repeat:	asciz "Reached limit of repeating disassembly.",13,10
.uu_internal_error:	asciz "Internal error in disassembler!",13,10
.aa_internal_error:	asciz "Internal error in assembler!",13,10
.stack_overflow:	db "Stack overflow occurred, IP="
.stack_overflow.caller:	asciz "____h, due to "
.stack_overflow.indirection:	asciz "expression indirection.",13,10
.stack_overflow.parens:		asciz "expression parentheses.",13,10
.stack_overflow.precedence:	asciz "expression precedence.",13,10

%if 0
.optiontable:	dw dispregs32, .r32off, .r32on
		dw traceints, .traceoff, .traceon
		dw cpdepchars, .cpoff, .cpon
		dw fakeindos, .dosoff, .doson
		dw nonpagingdevice, .nonpageoff, .nonpageon
		dw pagingdevice, .pageoff, .pageon
		dw hexrn, .readrnoff, .readrnon
		dw 0

.r32off:	asciz "Dump 16-bit register set"
.r32on:		asciz "Dump 32-bit register set"
.traceoff:	asciz "Interrupts are traced"
.traceon:	asciz "Interrupts are processed"
.cpoff:		asciz "Extended ASCII characters replaced"
.cpon:		asciz "Extended ASCII characters displayed"
.dosoff:	asciz "InDOS is checked"
.doson:		asciz "InDOS assumed on"
		;asciz "InDOS assumed off"
.nonpageoff:	asciz
.nonpageon:	asciz "Paging disabled"
.pageoff:	asciz
.pageon:	asciz "Paging enabled"
.readrnoff:	asciz "Readable RN enabled"
.readrnon:	asciz "Readable RN disabled"
%endif

.warnprefix:	asciz "Warning: Prefixes in excess of 14, using trace flag.",13,10

%if _DEBUG
.bu:		asciz "Breaking to next instance.",13,10
%else
.notbu:		asciz "Already in topmost instance. (This is no debugging build of lDebug.)",13,10
%endif

%if _PM
.ofs32:		asciz "Cannot access 16-bit segment with 32-bit offset.",13,10
%endif


%define smcb_messages ..@notype,""

	%imacro smcbtype 2.nolist
		dw %2, %%label
%defstr %%str	%1
%xdefine smcb_messages smcb_messages,%%label,%%str
	%endmacro

	%imacro smcbmsg 2-*.nolist
%if %0 & 1
 %error Expected even number of parameters
%endif
%rotate 2
%rep (%0 - 2) / 2
%1:	asciz %2
%rotate 2
%endrep
	%endmacro

	align 4
smcbtypes:
smcbtype S_OTHER,	00h
smcbtype S_DOSENTRY,	01h
smcbtype S_DOSCODE,	02h
smcbtype S_DOSDATA,	03h
smcbtype S_IRQSCODE,	04h
smcbtype S_IRQSDATA,	05h
smcbtype S_CDS,		06h
smcbtype S_LFNCDS,	07h
smcbtype S_DPB,		08h
smcbtype S_UPB,		09h
smcbtype S_SFT,		0Ah
smcbtype S_FCBSFT,	0Bh
smcbtype S_CCB,		0Ch
smcbtype S_IRT,		0Dh
smcbtype S_SECTOR,	0Eh
smcbtype S_NLS,		0Fh
smcbtype S_EBDA,	10h
smcbtype S_INITCONFIG,	19h
smcbtype S_INITFATSEG,	1Ah
smcbtype S_INITSECTORSEG,	1Bh
smcbtype S_INITSTACKBPB,1Ch
smcbtype S_INITPSP,	1Dh
smcbtype S_ENVIRONMENT,	1Eh
smcbtype S_INITIALIZE,	1Fh
smcbtype S_DEVICE,	20h					; Device
smcbtype S_DEVICEMEMORY,21h					; Allocated by device
smcbtype S_EXCLDUMA,	30h					; Excluded UMA
smcbtype S_EXCLDUMASUB,	31h					; Excluded UMA with sub-chain of used MCBs
smcbtype S_EXCLDLH,	32h					; Excluded by LH
smcbtype S_EXCLDDOS,	33h
	dw -1, -1

smcbmsg smcb_messages

smcbmsg_unknown:	asciz "unknown"

%undef smcb_messages
%unimacro smcbtype 2.nolist
%unimacro smcbmsg 2-*.nolist

errcarat:	db "^ Error"
crlf:		asciz 13,10


%if _BOOTLDR
%define lot_list
%define lot_comma
%macro lot_entry 2.nolist
LOAD_%2 equ %1
	dw LOAD_%2, .%2
%defstr %%string %2
%xdefine lot_list lot_list lot_comma .%2:, db %%string, db 0
%define lot_comma ,
%endmacro

%macro lot_messages 0-*.nolist
%rep (%0 / 3)
%1
	%2
	%3
%rotate 3
%endrep
%endmacro

	align 4
loadoptiontable:
	lot_entry    1, SET_DL_UNIT
	lot_entry    2, SET_BL_UNIT
	lot_entry    4, SET_SIDI_CLUSTER
	lot_entry  10h, SET_DSSI_DPT
	lot_entry  20h, PUSH_DPT
	lot_entry  40h, DATASTART_HIDDEN
	lot_entry  80h, SET_AXBX_DATASTART
	lot_entry 100h, SET_DSBP_BPB
	lot_entry 200h, LBA_SET_TYPE
	lot_entry 400h, MESSAGE_TABLE
	lot_entry 800h, SET_AXBX_ROOT_HIDDEN
	dw 0, 0

.incompatible:
	dw LOAD_SET_BL_UNIT, LOAD_SET_AXBX_DATASTART
	dw LOAD_SET_BL_UNIT, LOAD_SET_AXBX_ROOT_HIDDEN
	dw LOAD_SET_AXBX_DATASTART, LOAD_SET_AXBX_ROOT_HIDDEN
	dw LOAD_SET_SIDI_CLUSTER, LOAD_SET_DSSI_DPT
	dw LOAD_SET_DSBP_BPB, LOAD_SET_DSSI_DPT
	dw 0, 0

lot_messages lot_list

%unmacro lot_entry 2.nolist
%unmacro lot_messages 0-*.nolist


msdos7_message_table:
		; the first four bytes give displacements to the various
		;  messages. an ASCIZ message indicates that this was the
		;  last message. a message terminated by 0FFh indicates
		;  that the last message (displacement at table + 3) is
		;  to follow after this message.
		; the maximum allowed displacement is 7Fh. the minimum
		;  allowed displacement is 1, to avoid a zero displacement.
		; only the last message is terminated by a zero byte,
		;  as that zero byte indicates the end of the message table.
		;  (the entire table is treated as one ASCIZ string.)
		; MS-DOS 7.10 from MSW 98 SE seems to have at least 167h (359)
		;  bytes allocated to its buffer for these.
		;
		; this message table was discussed in a dosemu2 repo at
		;  https://github.com/stsp/dosemu2/issues/681
.	db .msg_invalid_system - ($ + 1)
	db .msg_io_error - ($ + 1)
	db .msg_invalid_system - ($ + 1)
	db .msg_press_any_key - ($ + 1)

.msg_invalid_system:
	db 13,10,"Invalid system", -1

.msg_io_error:
	db 13,10,"I/O error", -1

.msg_press_any_key:
	db 13,10,"Change disk and press any key",13,10,0
.end:
.size: equ .end - .

%if .size > 150h
 %error Message table too large!
%endif



	align 4
loadsettings:
	istruc LOADSETTINGS
at lsKernelName,	dw msg.ldos_kernel_name
at lsAddName,		dw msg.addname_empty
at lsMinPara,		dw 60h
at lsMaxPara,		dw -1
at lsOptions,		dw LOAD_SET_DL_UNIT | LOAD_SET_BL_UNIT
at lsSegment,		dw 70h
at lsEntry,		dd 400h
at lsBPB,		dw 7C00h, -1
at lsName,		asciz "LDOS"
	iend
	istruc LOADSETTINGS
at lsKernelName,	dw msg.freedos_kernel_name
at lsAddName,		dw msg.addname_empty
at lsMinPara,		dw 20h
at lsMaxPara,		dw -1
at lsOptions,		dw LOAD_SET_DL_UNIT | LOAD_SET_BL_UNIT
at lsSegment,		dw 60h
at lsEntry,		dd 0
at lsBPB,		dw 7C00h, -1
at lsName,		asciz "FREEDOS"
	iend
	istruc LOADSETTINGS
at lsKernelName,	dw msg.edrdos_kernel_name
at lsAddName,		dw msg.addname_empty
at lsMinPara,		dw 20h
at lsMaxPara,		dw -1
at lsOptions,		dw LOAD_SET_DL_UNIT | LOAD_SET_BL_UNIT \
			 | LOAD_SET_DSBP_BPB
at lsSegment,		dw 70h
at lsEntry,		dd 0
at lsBPB,		dw 7C00h, -1
at lsName,		asciz "EDRDOS"
	iend
	istruc LOADSETTINGS
at lsKernelName,	dw msg.msdos6_kernel_name
at lsAddName,		dw msg.msdos6_add_name
at lsMinPara,		dw 20h
at lsMaxPara,		dw 60h
at lsOptions,		dw LOAD_SET_DL_UNIT | LOAD_SET_AXBX_DATASTART \
			 | LOAD_DATASTART_HIDDEN | LOAD_SET_DSSI_DPT \
			 | LOAD_PUSH_DPT
at lsSegment,		dw 70h
at lsEntry,		dd 0
at lsBPB,		dw 7C00h, 0
at lsName,		asciz "MSDOS6"
	iend
	istruc LOADSETTINGS
at lsKernelName,	dw msg.rxdos.0_kernel_name
at lsAddName,		dw msg.rxdos.0_add_name
at lsMinPara,		dw 20h
at lsMaxPara,		dw 60h
at lsOptions,		dw LOAD_SET_DL_UNIT | LOAD_SET_AXBX_ROOT_HIDDEN \
			 | LOAD_SET_DSSI_DPT | LOAD_PUSH_DPT
at lsSegment,		dw 70h
at lsEntry,		dd 0
at lsBPB,		dw 7C00h, 0
at lsName,		asciz "RXDOS.0"
	iend
	istruc LOADSETTINGS
at lsKernelName,	dw msg.rxdos.1_kernel_name
at lsAddName,		dw msg.rxdos.1_add_name
at lsMinPara,		dw 20h
at lsMaxPara,		dw 60h
at lsOptions,		dw LOAD_SET_DL_UNIT | LOAD_SET_AXBX_ROOT_HIDDEN \
			 | LOAD_SET_DSSI_DPT | LOAD_PUSH_DPT
at lsSegment,		dw 70h
at lsEntry,		dd 0
at lsBPB,		dw 7C00h, 0
at lsName,		asciz "RXDOS.1"
	iend
	istruc LOADSETTINGS
at lsKernelName,	dw msg.rxdos.2_kernel_name
at lsAddName,		dw msg.addname_empty
at lsMinPara,		dw 60h
at lsMaxPara,		dw -1
at lsOptions,		dw LOAD_SET_DL_UNIT | LOAD_SET_BL_UNIT
at lsSegment,		dw 70h
at lsEntry,		dd 400h
at lsBPB,		dw 7C00h, -1
at lsName,		asciz "RXDOS.2"
	iend
	istruc LOADSETTINGS
at lsKernelName,	dw msg.ibmdos_kernel_name
at lsAddName,		dw msg.ibmdos_add_name
at lsMinPara,		dw 20h
at lsMaxPara,		dw 80h
at lsOptions,		dw LOAD_SET_DL_UNIT | LOAD_SET_AXBX_DATASTART \
			 | LOAD_DATASTART_HIDDEN | LOAD_SET_DSSI_DPT \
			 | LOAD_PUSH_DPT
at lsSegment,		dw 70h
at lsEntry,		dd 0
at lsBPB,		dw 7C00h, 0
at lsName,		asciz "IBMDOS"
	iend
	istruc LOADSETTINGS
at lsKernelName,	dw msg.msdos7_kernel_name
at lsAddName,		dw msg.addname_empty
at lsMinPara,		dw 20h
at lsMaxPara,		dw 80h
at lsOptions,		dw LOAD_SET_DL_UNIT | LOAD_SET_SIDI_CLUSTER \
			 | LOAD_DATASTART_HIDDEN | LOAD_PUSH_DPT \
			 | LOAD_LBA_SET_TYPE | LOAD_MESSAGE_TABLE
at lsSegment,		dw 70h
at lsEntry,		dd 200h
at lsBPB,		dw 7C00h, -1
at lsName,		asciz "MSDOS7"
	iend
	istruc LOADSETTINGS
at lsKernelName,	dw msg.ntldr_kernel_name
at lsAddName,		dw msg.addname_empty
at lsMinPara,		dw 20h
at lsMaxPara,		dw -1
at lsOptions,		dw LOAD_SET_DL_UNIT \
			 | LOAD_DATASTART_HIDDEN
at lsSegment,		dw 2000h
at lsEntry,		dd 0
at lsBPB,		dw 7C00h, 0
at lsName,		asciz "NTLDR"
	iend
	istruc LOADSETTINGS
at lsKernelName,	dw msg.bootmgr_kernel_name
at lsAddName,		dw msg.addname_empty
at lsMinPara,		dw 20h
at lsMaxPara,		dw -1
at lsOptions,		dw LOAD_SET_DL_UNIT \
			 | LOAD_DATASTART_HIDDEN
at lsSegment,		dw 2000h
at lsEntry,		dd 0
at lsBPB,		dw 7C00h, 0
at lsName,		asciz "BOOTMGR"
	iend
			dw 0
%endif


dskerrs:	db dskerr0-dskerrs,dskerr1-dskerrs
		db dskerr2-dskerrs,dskerr3-dskerrs
		db dskerr4-dskerrs,dskerr9-dskerrs
		db dskerr6-dskerrs,dskerr7-dskerrs
		db dskerr8-dskerrs,dskerr9-dskerrs
		db dskerra-dskerrs,dskerrb-dskerrs
		db dskerrc-dskerrs
dskerr0:	asciz "Write protect error"
dskerr1:	asciz "Unknown unit error"
dskerr2:	asciz "Drive not ready"
dskerr3:	asciz "Unknown command"
dskerr4:	asciz "Data error (CRC)"
dskerr6:	asciz "Seek error"
dskerr7:	asciz "Unknown media type"
dskerr8:	asciz "Sector not found"
dskerr9:	asciz "Unknown error"
dskerra:	asciz "Write fault"
dskerrb:	asciz "Read fault"
dskerrc:	asciz "General failure"
reading:	asciz " read"
writing:	asciz " writ"
drive:		db "ing drive "
driveno:	asciz "_"
msg8088:	asciz "8086/88"
msgx86:		asciz "x86"
no_copr:	asciz " without coprocessor"
has_copr:	asciz " with coprocessor"
has_287:	asciz " with 287"
tmodes:		db "trace mode is "
tmodev:		asciz "_ - interrupts are "
tmode1:		asciz "traced"
tmode0:		asciz "processed"
unused:		asciz " (unused)"
needsmsg:	db "[needs x86]"
needsmsg_L:	equ $-needsmsg
needsmath:	db "[needs math coprocessor]"
needsmath_L:	equ $-needsmath
obsolete:	db "[obsolete]"
obsolete_L:	equ $-obsolete
int0msg:	asciz "Divide error",13,10
int1msg:	asciz "Unexpected single-step interrupt",13,10
int3msg:	asciz "Unexpected breakpoint interrupt",13,10
%if _CATCHINT06
int6msg:	asciz "Invalid opcode",13,10
%endif
%if _CATCHINT08
int8msg:	asciz "Detected Control pressed 5 seconds",13,10
int8_kbd_msg:	asciz "Detected Control pressed 5 seconds (Keyboard enabled)",13,10
runint_ctrlc_msg:
		asciz "Detected double Control-C via serial",13,10
%endif
%if _CATCHINT18
int18msg:	asciz "Diskless boot hook called",13,10
%endif
%if _CATCHINT19
int19msg:	asciz "Boot load called",13,10
%endif
%if _PM
 %if _CATCHEXC06
exc6msg:	asciz "Invalid opcode fault",13,10
 %endif
 %if _CATCHEXC0C
excCmsg:	asciz "Stack fault",13,10
 %endif
excDmsg:	asciz "General protection fault",13,10
 %if _EXCCSIP
excloc:		db "CS:IP="
exccsip:	asciz "    :    ",13,10
 %endif
excEmsg:	asciz "Page fault",13,10
nodosext:	asciz "Command not supported in protected mode without a DOS extender",13,10
nopmsupp:	asciz "Command not supported in protected mode",13,10
 %if _DISPHOOK
dpmihook:	db "DPMI entry hooked, new entry="
dpmihookcs:	asciz "____:",_4digitshex(mydpmientry+DATASECTIONFIXUP),13,10
msg.dpmi_no_hook:	asciz "DPMI entry cannot be hooked!",13,10
 %endif
nodesc:		asciz "resource not accessible in real mode",13,10
;descwrong:	asciz "descriptor not accessible",13,10
gatewrong:	asciz "gate not accessible",13,10
msg.msdos:	asciz "MS-DOS"
descr:		db "____ base="
descbase:	db "________ limit="
desclim:	db "________ attr="
descattr:	db "____",13,10
		asciz
%endif	; _PM
ph_msg:		asciz "Error in sequence of calls to hack.",13,10

progtrm:	db 13,10,"Program terminated normally ("
progexit:	asciz "____)",13,10
nowhexe:	asciz "EXE and HEX files cannot be written",13,10
nownull:	asciz "Cannot write: no file name given",13,10
wwmsg1:		asciz "Writing "
wwmsg2:		asciz " bytes",13,10
diskful:	asciz "Disk full",13,10
openerr:	db "Error "
openerr1:	asciz "____ opening file",13,10
doserr2:	asciz "File not found",13,10
doserr3:	asciz "Path not found",13,10
doserr5:	asciz "Access denied",13,10
doserr8:	asciz "Insufficient memory",13,10

%if _EMS
;emmname:	db "EMMXXXX0"
emsnot:		asciz "EMS not installed",13,10
emserr1:	asciz "EMS internal error",13,10
emserr3:	asciz "Handle not found",13,10
emserr5:	asciz "No free handles",13,10
emserr7:	asciz "Total pages exceeded",13,10
emserr8:	asciz "Free pages exceeded",13,10
emserr9:	asciz "Parameter error",13,10
emserra:	asciz "Logical page out of range",13,10
emserrb:	asciz "Physical page out of range",13,10
emserrs:	dw emserr1,emserr1,0,emserr3,0,emserr5,0,emserr7
		dw emserr8,emserr9,emserra,emserrb
emserrx:	asciz "EMS error "
xaans:		db "Handle created = "
xaans1:		asciz "____",13,10
xdans:		db "Handle "
xdans1:		asciz "____ deallocated",13,10
xrans:		asciz "Handle reallocated",13,10
xmans:		db "Logical page "
xmans1:		db "__ mapped to physical page "
xmans2:		asciz "__",13,10
xsstr1:		db "Handle "
xsstr1a:	db "____ has "
xsstr1b:	asciz "____ pages allocated",13,10
xsstr2:		db "phys. page "
xsstr2a:	db "__ = segment "
xsstr2b:	asciz "____  "
xsstr3:		db "____ of a total "
xsstr3a:	asciz "____ EMS "
xsstr4:		asciz "es have been allocated",13,10
xsstrpg:	asciz "pag"
xsstrhd:	asciz "handl"
xsnopgs:	asciz "no mappable pages",13,10,13,10
%endif

		align 4
flagbits:	dw 800h,400h,200h, 80h,040h,010h,004h,001h
flagson:	dw "OV","DN","EI","NG","ZR","AC","PE","CY"
flagsoff:	dw "NV","UP","DI","PL","NZ","NA","PO","NC"
flagnames:	dw "OF","DF","IF","SF","ZF","AF","PF","CF"

%if _COND
msg.condnotjump:db "not "
msg.condjump:	asciz "jumping"
%endif

msg.matches:	asciz " matches",13,10

		align 4
reg8names:	dw "AL","AH","BL","BH","CL","CH","DL","DH"
; Even entries are xL registers, odd ones the xH ones.
; Order matches that of the first four regs entries.

reg16names:	dw "AX","BX","CX","DX","SP","BP","SI","DI"
		dw "DS","ES","SS","CS","FS","GS","IP","FL"
; 32-bit registers are the first eight and last two entries of
;  reg16names with 'E', which are all non-segment registers.
; Segment registers can be detected by the 'S' as second letter.
; FS and GS are the fourth- and third-to-last entries.
; Order matches that of the sixteen regs entries.


		; Table of recognised default (unsigned) types.
		;
		; If any number of characters match, use the type.
		; If an additional "S" is found in front of a valid
		;  type, the type is set to signed. (Word and byte
		;  types are sign-extended to a dword value.)
		;
		; Each odd entry is an alternative name for the even
		;  entry preceding it.
types:
	countedb "BYTE"		; ("B" is hexadecimal)
	countedb "CHAR"		; ("C" is hexadecimal)
	countedb "WORD"
	countedb "SHORT"
	countedb "DWORD"	; ("D" is hexadecimal)
	countedb "LONG"
.addresses:
	countedb "POINTER"
	countedb "PTR"
	countedb "OFFSET"
	countedb "OFS"
	countedb "SEGMENT"
.end:

maxtypesize equ 7		; size of "SEGMENT" and "POINTER"
