
%if 0

lDebug - Free x86 DOS debugger

Copyright (C) 1995-2003 Paul Vojta
Copyright (C) 2008-2012 C. Masloch

Usage of the works is permitted provided that this
instrument is retained with the works, so that any entity
that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

%endif

[list -]
%if 0

lDebug build notes

Compile MKTABLES:	wcl -ox -3 -d__MSDOS__ mktables.c
or:	gcc -xc MKTABLES.C -o mktables -Wno-write-strings -DOMIT_VOLATILE_VOID
Execute MKTABLES:	mktables
	(This deletes debugtbl.old then creates debugtbl.inc.)
	(A temporary file named debugtbl.tmp is used.)
Assemble DEBUG.COM:	nasm debug.asm -I../macro/ -oDEBUG.COM
Assemble DEBUGX.COM:	nasm debug.asm -I../macro/ -d_PM -oDEBUGX.COM

OpenWatcom 1.9 was used for compiling MKTABLES but it should reasonably well
work with other C compilers. gcc 6.3.0 is now also able to compile MKTABLES.
Assembling lDebug requires NASM (2.12.01 tested).

Note that MKTABLES only needs to be used if either the source files changed or
the MKTABLES program itself has been altered. If the assembler and disassembler
table is not to change, NASM is sufficient to assemble lDebug.


lDebug build options

There are some more options that can be used on the NASM command line to create
alternative lDebug versions. Unless otherwise specified, each option is a flag
and so should be set to either 1 (to enable an option) or 0 (to disable it).
Omitting a numeric value as well as the equals sign enables the option too.

-d_FILENAME=	Set base string for file name. Up to six characters.
-d_PROGNAME=	Set program name string displayed in help screens.
-d_VERSION=	Set version string displayed after _PROGNAME. Include a blank.
-d_REVISIONID=	Set revision ID string. (Disabled if empty or undefined.)
-d_PM=		Set DPMI support.
-d_DEBUG=	Set support for debugging lDebug, for lDebug development. The
		interrupt handlers will be reset to these of the next debugger
		in lDebug code and the BU command breaks to the next debugger.
		(Currently does not work with DPMI support enabled.)
-d_EMS=		Set EMS allocation commands (XA, XD, XM, XR, XS, X?).
-d_INT=		Set command (DI) to display exception and interrupt handlers.
-d_MCB=		Set command (DM) to display DOS Memory Control Blocks.
-d_RN=		Set command (RN) to display numerical co-processor registers.
-d_DSTRINGS=	Set commands (DZ, D$, D#, DW#) to display strings in memory.
-d_SDUMP=	Set that the search command (S) displays memory after matches.
-d_COND=	Set that the register dump (R, T, P, G) displays jump notices.
-d_USESDA=	Set that switching processes uses the DOS Swappable Data Area.
-d_VDD=		Set NTVDM direct disk access support via loading DEBXXVDD.DLL.
-d_EXPRESSIONS=	Set expression evaluator that accepts calculations.
		(Currently is required enabled.)
-d_VARIABLES=	Set sixteen 32-bit variables (V0..VF) that can be set freely.
-d_OPTIONS=	Set 32-bit option variables (DCO, DCS, DIF, DAO, DAS).
-d_BOOTLDR=	Set boot loading support.
-d_BREAKPOINTS=	Set permanent breakpoint support (B commands).
		(Not yet implemented.)
-d_NUM_B_BP=	Set number of permanent breakpoints. Must be numerical.
		(Not yet implemented.)
-d_NUM_G_BP=	Set number of temporary breakpoints. Must be numerical.
-d_MMXSUPP=	Set command (RM) to display MMX registers.
-d_CATCHINT06=	Set that Interrupt 06h (Invalid opcode) is hooked.
-d_CATCHINT08=	Set that Interrupt 08h (IRQ0, timer) is hooked. (Special!)
-d_CATCHINT18=	Set that Interrupt 18h (Diskless boot hook) is hooked.
-d_CATCHINT19=	Set that Interrupt 19h (Boot load) is hooked.
; -d_STACKSIZE=	Set size of lDebug's stack (in byte). Must be numerical.
-d_AUXBUFFSIZE=	Set size of the auxiliary buffer (in byte). Must be numerical.
	(The following only apply if DPMI support is enabled.)
-d_NOEXTENDER=	Set support of DPMI hosts without DOS extender.
-d_WIN9XSUPP=	Set Win9x detection to avoid hooking its DPMI entry.
-d_DOSEMU=	Set dosemu detection to avoid hooking its DPMI entry.
-d_EXCCSIP=	Set to display where exceptions inside lDebug occured.
-d_CATCHEXC06=	Set that exception 06h (Invalid opcode) is hooked.
-d_CATCHEXC0C=	Set that exception 0Ch (Stack fault) is hooked.
-d_DISPHOOK=	Set to display when the DPMI entry is hooked.

Refer to the list in debug.mac for the default values of these options. You
can also adjust the defaults there. Refer to the source code and commentary
for what each option does if these descriptions are not specific enough.


lDebug contributions

lDebug is based on DEBUG/X 1.13 to 1.18 as released by Japheth, whose work
on DEBUG/X has been released as Public Domain.

%endif

%include "debug.mac"

%ifndef _MAP
%elifempty _MAP
%else	; defined non-empty, str or non-str
	[map all _MAP]
%endif

	cpu 8086
	org 100h
	addsection lDEBUG_DATA_ENTRY, align=16 start=100h
data_entry_start:
	addsection ASMTABLE1, align=16 follows=lDEBUG_DATA_ENTRY
	addsection ASMTABLE2, align=16 follows=ASMTABLE1
	addsection lDEBUG_CODE, align=16 follows=ASMTABLE2 vstart=0
code_start:
	addsection DATASTACK, align=16 follows=ASMTABLE2 nobits
	addsection INIT, align=16 follows=lDEBUG_CODE vstart=0


	usesection lDEBUG_DATA_ENTRY

%define DATASECTIONFIXUP -data_entry_start+100h
%define CODESECTIONFIXUP -code_start+0
%define CODESECTIONOFFSET (100h+ldebug_data_entry_size+asmtable1_size+asmtable2_size)
%define INITSECTIONOFFSET (CODESECTIONOFFSET+ldebug_code_size)

%define CODETARGET1 (CODESECTIONOFFSET+datastack_size)
%define CODETARGET2 (CODETARGET1+auxbuff_size)

%define AUXTARGET1 (CODETARGET1+ldebug_code_size)
%define AUXTARGET2 CODETARGET1
%define AUXTARGET3 AUXTARGET1+auxbuff_size

%define NONBOOTINITTARGET (INITSECTIONOFFSET+datastack_size+auxbuff_size*2)
%define NONBOOTINITSTACK_START (NONBOOTINITTARGET+init_size)
NONBOOTINITSTACK_SIZE equ 512	; must be even
%define NONBOOTINITSTACK_END (NONBOOTINITSTACK_START+NONBOOTINITSTACK_SIZE)

BOOTINITSTACK_SIZE equ 512	; must be divisible by 16
%define BOOTDELTA	(fromkib(kib(auxbuff_size*2 + datastack_size + INITSECTIONOFFSET + 16)))


%if _DEVICE
	fill 4,0FFh,jmp initcode_j
	dw 8000h
	dw deviceinit -$$
	dw deviceinit.retf -$$
	fill 8,32,db "DEBUG$$"
%else
	jmp initcode_j
%endif
%if _BOOTLDR
	align 32
	mov ax, cs
	sub ax, 10h
	mov ds, ax
	mov bx, boot_initcode
	jmp @F
%endif
	align 64
initcode_j:
	mov ax, cs
	xor bx, bx
@@:
	add ax, paras(INITSECTIONOFFSET)
	push ax
	push bx
	retf


	align 2
cmdlist:	dw aa,bb,cc,ddd,ee,ff,gg,hh,ii,error,error,ll,mm,nn,oo
		dw pp,qq,rr,sss,tt,uu,error,ww,xx,yy

	align 4
				; options, startoptions and internalflags
				; have to be consecutive
options:	dd DEFAULTOPTIONS ; run-time options
dispregs32	equ	  1	; RX: 32-bit register display (R, T/P/G)
traceints	equ	  2	; TM: trace into interrupts (T)
cpdepchars	equ	  4	; allow dumping of CP-dependant characters (D, DX)
fakeindos	equ	  8	; always assume InDOS flag non-zero (all)
nonpagingdevice	equ	 10h	; disallow paged output with [more] prompt (all exc. P, T)
pagingdevice	equ	 20h	; allow paged output with [more] prompt (all exc. P, T)
				; paged output is by default on if the output device is StdOut, else off
hexrn		equ	 40h	; display raw hexadecimal content of FPU registers (RN)
;novdd		equ	 80h	; don't use a registered NTVDM VDD (L, W)
nondospaging	equ	100h	; paging: don't use DOS for input when waiting for a key
nohlt		equ	200h	; HLT doesn't work, don't use it
biosidles	equ	400h	; don't idle with HLT or Int2F.1680, only call BIOS Int16.00
use_si_units	equ    1000h	; in disp_*_size use SI units (kB = 1000, etc)
use_jedec_units	equ    2000h	; in disp_*_size use JEDEC units (kB = 1024)
enable_serial	equ    4000h	; enable serial I/O (preferred over DOS or BIOS terminal)
int8_disable_serial equ	   8000h	; disable serial I/O when breaking due to intr8
gg_do_not_skip_bp equ	 1_0000h	; gg: do not skip a breakpoint (bb or gg)
gg_no_autorepeat equ	 2_0000h	; gg: do not auto-repeat
tp_do_not_skip_bp equ	 4_0000h	; T/TP/P: do not skip a (bb) breakpoint
gg_bb_hit_no_repeat equ	 8_0000h	; gg: do not auto-repeat after bb hit
tp_bb_hit_no_repeat equ	10_0000h	; T/TP/P: do not auto-repeat after bb hit
gg_unexpected_no_repeat equ 20_0000h	; gg: do not auto-repeat after unexpectedinterrupt
tp_unexpected_no_repeat equ 40_0000h	; T/TP/P
rr_disasm_no_rept:	equ 1000_0000h
rr_disasm_no_show:	equ 2000_0000h
DEFAULTOPTIONS	equ 0

startoptions:	dd DEFAULTOPTIONS ; options as determined during startup; read-only for user

internalflags:	dd attachedterm|pagedcommand|notstdinput|inputfile|notstdoutput|outputfile|(!!_PM*dpminohlt)|debuggeeA20|debuggerA20
				; flags only modified by DEBUG itself
oldpacket	equ	  1	; Int25/Int26 packet method available (L, W)
newpacket	equ	  2	; Int21.7305 packet method available (L, W)
ntpacket	equ	  4	; VDD registered and usable (L, W)
pagedcommand	equ	  8	; allows paging in puts
notstdinput	equ	 10h	; DEBUG's StdIn isn't a device with StdIn bit (is file or other device)
inputfile	equ	 20h	; DEBUG's StdIn is a file, notstdinput also set
notstdoutput	equ	 40h	; DEBUG's StdOut isn't a device with StdOut bit (is file or other device)
outputfile	equ	  80h	; DEBUG's StdOut is a file, notstdoutput also set
hooked2F	equ	 100h	; Int2F hooked
nohook2F	equ	 200h	; don't hook Int2F.1687 (required for Win9x, DosEmu?)
dpminohlt	equ	 400h	; DPMI doesn't like hlt
protectedmode	equ	 800h	; in (DPMI) protected mode
debuggeeA20	equ	1000h	; state of debuggee's A20
debuggerA20	equ	2000h	; state of debugger's A20 (will be on if possible)
nodosloaded	equ	4000h	; No DOS loaded currently (Boot loader mode)
has386		equ	8000h	; CPU is a 386
usecharcounter	equ    1_0000h	; don't reset charcounter between calls to puts
runningnt	equ    2_0000h	; running in NTVDM
canswitchmode	equ    4_0000h	; can switch modes (auxbuff large enough, DPMI mode switch set up)
modeswitched	equ    8_0000h	; switched mode (now in the mode that we weren't entered in)
promptwaiting	equ   10_0000h	; puts: any more output needs to display a prompt first
switchbuffer	equ   20_0000h	; mode switch needs a buffer (auxbuff)
tsrmode		equ   40_0000h	; in TSR mode; DPI and DPP not valid
attachedterm	equ   80_0000h	; the attached process terminated
runningdosemu	equ  100_0000h	; running in dosemu
load_is_ldp	equ  200_0000h	; boot load: partition specified as "ldp"
tt_while:	equ  400_0000h	; tt: while condition specified
tt_p:		equ  800_0000h	; tt: proceed past repeated string instructions
tt_silent_mode:	equ 1000_0000h	; tt: run should be silent (dump at end)
tt_silence:	equ 2000_0000h	; tt: silent writing (write to auxbuff instead)
tt_no_bb:	equ 4000_0000h	; tt: do not use bb breakpoints
tt_no_bb_first:	equ 8000_0000h	; tt: do not use bb breakpoints at first

internalflags2:	dd 0
dif2_gg_is_first:	equ   1
dif2_gg_skip_non_cseip:	equ   2
dif2_gg_skip_cseip:	equ   4
dif2_gg_is_gg:		equ   8
dif2_gg_first_detected:	equ  10h
dif2_gg_again:		equ  20h
dif2_tpg_proceed_bp_set:equ  40h
dif2_tpg_keep_proceed_bp: equ 80h
dif2_tpg_have_bp:	equ 100h
dif2_tpg_adjusted_cseip:equ 200h
dif2_tpg_do_not_adjust:	equ 400h
dif2_bp_failure:	equ 800h
dif2_is_pp:		equ 1000h
%if _INPUT_FILE_HANDLES
dif2_input_file:	equ 10_0000h
dif2_closed_input_file:	equ 20_0000h
%endif
dif2_did_getline_file:	equ 40_0000h
dif2_boot_loaded_kernel:equ 100_0000h

asm_options:	dd DEFAULTASMOPTIONS
disasm_lowercase	equ 1
disasm_commablank	equ 2
disasm_nasm		equ 4
disasm_lowercase_refmem:equ 8
disasm_show_short:	equ 10h
disasm_show_near:	equ 20h
disasm_show_far:	equ 40h
DEFAULTASMOPTIONS equ disasm_lowercase|disasm_commablank|disasm_nasm

asm_startoptions:
		dd DEFAULTASMOPTIONS

gg_first_cseip_linear:	dd 0
gg_next_cseip_linear:	dd 0
tpg_possible_breakpoint:dd 0
gg_deferred_message:	dw msg.empty_message
bb_deferred_message:	dw msg.empty_message
tpg_proceed_bp:		times BPSIZE db 0
%if _DEBUG1
		align 2
test_records_Readmem:		times 6 * 16 db 0
test_records_Writemem:		times 6 * 16 db 0
test_records_getLinear:		times 6 * 16 db 0
test_records_getSegmented:	times 6 * 16 db 0

test_readmem_value:		db 0
%endif
		align 2
code_seg:	dw 0
%if _PM
code_sel:	dw 0
%endif


		align 2
%if _PM
auxbuff_switchbuffer_size:	dw 0
%endif
auxbuff_segorsel:
%if _PM
		dw 0
%endif
auxbuff_segment:
		dw 0
auxbuff_behind_while_condition:
		dw 0		; -> behind while condition stored in auxbuff
				;  (this is also the first silent buffer entry)
auxbuff_behind_last_silent:
		dw 0		; -> behind last silent buffer entry
tt_silent_mode_number:
		dw 0		; if non-zero: maximum amount of dumps
				;  displayed after T/TP/P while silent
%if _INPUT_FILE_HANDLES
		align 2
input_file_handles:
		times _INPUT_FILE_HANDLES dw -1
.active:	dw 0
%endif
charcounter:	db 0		; used by raw output to handle tab
linecounter:	db 0		; used by paging in puts
		align 2
savesp:		dw 0		; saved stack pointer
errret:		dw cmd3		; return here if error
throwret:	dw errhandler	; return here if error - priority, no display
throwsp:	dw stack_end - 2; stack pointer set before jumping to throwret
run_sp:		dw 0		; stack pointer when running
spadjust:	dw 40h 		; adjust sp by this amount for save
pspdbe:		dw 0		; debuggee's PSP (unless DIF&attachedterm)
pspdbg:		dw 0		; debugger's PSP (RM segment)
	align 4
run2324:	dd 0,0		; debuggee's interrupt vectors 23h and 24h (both modes)
%if _PM
		dd 0
dbg2324:	dw i23pm, i24pm
%endif
%if _VDD
hVdd:		dw -1		; NTVDM VDD handle
%endif
	align 4
sav2324:	dd 0,0		; debugger's interrupt vectors 23h and 24h (real-mode only)
hakstat:	db 0		; whether we have hacked the vectors or not
	align 4
psp22:		dd 0		; original terminate address from our PSP
parent:		dw 0		; original parent process from our PSP (must follow psp22)
%if _MCB
wMCB:		dw -1		; start of MCB chain (always segment)
%endif
pInDOS:		dd 0		; far16 address of InDOS flag (bimodal)
%if _PM
InDosSegm:	dw 0		; saved segment value of pInDOS
%endif
%if _USESDA
pSDA:		dd -1		; far16 address of SDA (bimodal)
%if _PM
SDASegm:	dw -1		; saved segment value of pSDA
%endif
%endif
machine:	db 0		; type of processor for assembler and disassembler (1..6)
has_87:		db 0		; if there is a math coprocessor present
mach_87:	db 0		; type of coprocessor present
%if _MMXSUPP
has_mmx:	db 0
%endif
bInDbg:		db 1		; 1=debugger is running
notatty:	db 10		; if standard input is from a file
				; this is also used for a linebreak processing hack
switchar:	db 0		; switch character
swch1:		db ' '		; switch character if it's a slash
	align 2
promptlen:	dw 0		; length of prompt
bufnext:	dw line_in+2	; address of next available character
bufend:		dw line_in+2	; address + 1 of last valid character

a_addr:		dw 0,0,0	; address for next A command
d_addr:		dw 0,0,0	; address for next D command; must follow a_addr
u_addr:		dw 0,0,0	; address for next U command; must follow d_addr
e_addr:		dw 0,0,0	; address for current/next E command
%if _DSTRINGS
dz_addr:	dw 0,0,0	; address for next ASCIZ string
dcpm_addr:	dw 0,0,0	; address for next $-terminated string
dcount_addr:	dw 0,0,0	; address for next byte-counted string
dwcount_addr:	dw 0,0,0	; address for next word-counted string
%endif
%if _PM
x_addr:		dd 0		; (phys) address for next DX command
%endif
%if _DSTRINGS
dstringtype:	db 0		; FFh byte-counted, FEh word-counted, else terminator byte
dstringaddr:	dw dz_addr	; -> address of last string
%endif
%if _INT
lastint:	db 0
%endif
	align 4
sscounter:	dw 0
%if _PM
		dw 0
%endif
rrtype:		db 0
eqflag:		db 0		; flag indicating presence of `=' operand
eqladdr:	dw 0,0,0	; address of `=' operand in G, P and T command
	align 2
run_int:	dw 0		; interrupt type that stopped the running
lastcmd:	dw dmycmd
bInit:		db 0		; 0=ensure a valid opcode is at debuggee's CS:IP
fileext:	db 0		; file extension (0 if no file name)
EXT_OTHER	equ 1
EXT_COM		equ 2
EXT_EXE		equ 4
EXT_HEX		equ 8

	align 4
mmxbuff:	dd 0		; buffer with a (read-only) part of MMX register
				; for access from within expressions
%if _CATCHINT08
intr8_counter:	dw 0
%endif
serial_rows:
		db 24
serial_keep_timeout:
		db 15
%if _USE_TX_FIFO
serial_fifo_size:
		db _BI_TX_FIFO_SIZE
			; size of built-in TX fifo (1 is as if no FIFO)
%endif
serial_flags:
		db 0
sf_init_done:	equ 1
sf_ctrl_c:	equ 2
sf_double_ctrl_c:	equ 4
sf_built_in_fifo:	equ 8

	align 2
getline_timer_count:	dw 0
getline_timer_last:	dw 0
getline_timer_func:	dw dmycmd


inttab:
	db 0
	dw intr0	; table of interrupt initialization stuff
%if _CATCHINT01
	db 1
	dw intr1
%endif
%if _CATCHINT03
	db 3
	dw intr3
%endif
%if _CATCHINT06
	db 6
	dw intr6
%endif
%if _CATCHINT08
	db 8
	dw intr8
%endif
%if _CATCHINT18
	db 18h
	dw intr18
%endif
%if _CATCHINT19
	db 19h
	dw intr19
%endif
	endarea inttab
	inttab_number equ inttab_size / 3

	align 4
intsave:dd -1		; storage for interrupt vectors 00h, 01h, 03h, 06h
%if _CATCHINT01
	dd -1
%endif
%if _CATCHINT03
	dd -1
%endif
%if _CATCHINT06
	dd -1
%endif
intr8_original:
%if _CATCHINT08
	dd -1
%endif
%if _CATCHINT18
	dd -1
%endif
%if _CATCHINT19
	dd -1
%endif

		; Parameter block for EXEC call
execblk:dw 0		;(00) zero: copy the parent's environment
	dw 0,0		;(02) address of command tail to copy
	dw 5Ch,0	;(06) address of first FCB to copy
	dw 6Ch,0	;(10) address of second FCB to copy
	dw 0,0		;(14) initial SS:SP
	dw 0,0		;(18) initial CS:IP


		; Register save area (32 words).
		; must be DWORD aligned, used as stack
	align 4
regs:
reg_eax:	dd 0	;+00 eax
reg_ebx:	dd 0	;+04 ebx
reg_ecx:	dd 0	;+08 ecx
reg_edx:	dd 0	;+12 edx
reg_esp:	dd 0	;+16 esp
reg_ebp:	dd 0	;+20 ebp
reg_esi:	dd 0	;+24 esi
reg_edi:	dd 0	;+28 edi
reg_ds:		dd 0	;+32  ds (high word unused)
reg_es:		dd 0	;+36  es (high word unused)
reg_ss:		dd 0	;+40  ss (high word unused)
reg_cs:		dd 0	;+44  cs (high word unused)
reg_fs:		dd 0	;+48  fs (high word unused)
reg_gs:		dd 0	;+52  gs (high word unused)
reg_eip:	dd 0	;+56 eip
reg_efl:	dd 0	;+60 efl(ags)
%if _VARIABLES
vregs: times 16 dd 0	;+64 internal v0..vf
%endif

; possible byte encoding of lDebug variables for dynamic computations:
; xxxxyyyy
; 10: register
;   xx: size (0 = 1, 1 = 2, 2 = 4)
;     yyyy: 0..15: register as stored in the register save area
;                  as SIL, DIL, BPL, SPL aren't supported these map to xH
;                  xSL, IPL and FLL are invalid, ExS are invalid
; 1011: variable
;     yyyy: which variable. variables are always dword-sized
; 11000000: 32-bit compound, next byte stores: xxxxyyyy first, second 16-bit reg
; 11000001..11111111: available for encoding other compound regs, vars, indirection,
;                     symbols, types etc
; 0xxxxxxx: operators


; Instruction set information needed for the 'p' command.
; ppbytes and ppinfo needs to be consecutive.
ppbytes:db 66h,67h,26h,2Eh,36h,3Eh,64h,65h,0F2h,0F3h	; prefixes
	db 0ACh,0ADh,0AAh,0ABh,0A4h,0A5h	; lods,stos,movs
	db 0A6h,0A7h,0AEh,0AFh			; cmps,scas
	db 6Ch,6Dh,6Eh,6Fh			; ins,outs
PPLEN_ONLY_STRING	equ $-ppbytes
	db 0CCh,0CDh				; int instructions
	db 0E0h,0E1h,0E2h			; loop instructions
	db 0E8h					; call rel16/32
	db 09Ah					; call far seg16:16/32
;	(This last one is done explicitly by the code.)
;	db 0FFh					; FF/2 or FF/3: indirect call

PPLEN	equ	$-ppbytes	; size of the above table

;	Info for the above, respectively.
;	80h = prefix; 82h = operand size prefix; 81h = address size prefix.
;	If the high bit is not set, the next highest bit (40h) indicates that
;	the instruction size depends on whether there is an address size prefix,
;	and the remaining bits tell the number of additional bytes in the
;	instruction.

PP_ADRSIZ	equ 01h
PP_OPSIZ	equ 02h
PP_PREFIX	equ 80h
PP_VARSIZ	equ 40h

ppinfo:	db 82h,81h,80h,80h,80h,80h,80h,80h,80h,80h
	db 0,0,0,0,0,0
	db 0,0,0,0
	db 0,0,0,0
	db 0,1
	db 1,1,1
	db 42h
	db 44h

%if PPLEN != $-ppinfo
 %error "ppinfo table has wrong size"
%endif


;	Equates for instruction operands.
;	First the sizes.

OP_ALL		equ 40h		; byte/word/dword operand (could be 30h but ...)
OP_1632		equ 50h		; word or dword operand
OP_8		equ 60h		; byte operand
OP_16		equ 70h		; word operand
OP_32		equ 80h		; dword operand
OP_64		equ 90h		; qword operand
OP_1632_DEFAULT	equ 0A0h	; word or dword or default opsize

OP_SIZE	equ OP_ALL		; the lowest of these

;	These operand types need to be combined with a size.
;	Bits 0 to 3 give one of these types (maximum 15),
;	 and bits 4 to 7 specify the size. Table entries
;	 for these are identified by detecting that they
;	 are above-or-equal OP_SIZE.
;	asm_jmp1 and bittab in aa.asm contain entries for
;	 each of these. disjmp2 in uu.asm does likewise.

OP_IMM		equ 0		; immediate
OP_RM		equ 1		; reg/mem
OP_M		equ 2		; mem (but not reg)
OP_R_MOD 	equ 3		; register, determined from MOD R/M part
OP_MOFFS 	equ 4		; memory offset; e.g., [1234]
OP_R		equ 5		; reg part of reg/mem byte
OP_R_ADD 	equ 6		; register, determined from instruction byte
OP_AX		equ 7		; al or ax or eax

;	These don't need a size.
;	Because the size needs to be clear to indicate
;	 that one of these is to be used, the maximum
;	 value for these is 63 (as 64 is OP_SIZE).
;	The minimum value for these is 1 because a 0
;	 without size means the end of an op list.
;	asm_jmp1 and bittab in aa.asm contain entries for
;	 each of these, though no entry is used for OP_END.
;	 optab in uu.asm also contains entries for these.

OP_END		equ 0
OP_M64		equ 1		; 0 qword memory (obsolete?)
OP_MFLOAT 	equ 2		; 1 float memory
OP_MDOUBLE 	equ 3		; 2 double-precision floating memory
OP_M80		equ 4		; 3 tbyte memory
OP_MXX		equ 5		; 4 memory (size unknown)
OP_FARMEM 	equ 6		; 5 memory far16/far32 pointer
OP_FARIMM	equ 7		; 6 far16/far32 immediate
OP_REL8		equ 8		; 7 byte address relative to IP
OP_REL1632 	equ 9		; 8 word or dword address relative to IP
OP_1CHK		equ 10		; 9 check for ST(1)
OP_STI		equ 11		;10 ST(I)
OP_CR		equ 12		;11 CRx
OP_DR		equ 13		;12 DRx
OP_TR		equ 14		;13 TRx
OP_SEGREG 	equ 15		;14 segment register
OP_IMMS8 	equ 16		;15 sign extended immediate byte
OP_IMM8		equ 17		;16 immediate byte (other args may be (d)word)
OP_MMX		equ 18		;17 MMx
OP_MMX_MOD	equ 19		;18 MMx, but in ModR/M part
OP_SHOSIZ 	equ 20		;19 set flag to always show the size
OP_SHORT	equ 21		; allow short keyword
OP_NEAR		equ 22		; allow near keyword
OP_FAR		equ 23		; allow far keyword
OP_FAR_REQUIRED	equ 24		; require far keyword

OP_1		equ 25		; 1 (simple "string" ops from here on)
OP_3		equ 26		; 3
OP_DX		equ 27		; DX
OP_CL		equ 28		; CL
OP_ST		equ 29		; ST (top of coprocessor stack)
OP_CS		equ 30		; CS
OP_DS		equ 31		; DS
OP_ES		equ 32		; ES
OP_FS		equ 33		; FS
OP_GS		equ 34		; GS
OP_SS		equ 35		; SS

OP_AMOUNT_TABLE	equ 36 + 16 - 1
		; 36: amount sizeless types
		; 16: OP_SIZE combined types
		; -1: OP_END does not occur in tables

	; Instructions that have an implicit operand subject to a segment prefix.
	; This means a prefixed segment is allowed by the strict assembler, and
	; the disassembler treats a segment prefix as part of the instruction and
	; displays it in front of the instruction's mnemonic.
	; (outs, movs, cmps, lods, xlat).
segprfxtab:
	db 06Eh,06Fh,0A4h,0A5h,0A6h,0A7h,0ACh,0ADh
a32prfxtab:
	db 0D7h				; xlat, last in segprfxtab, first in a32prfxtab
SEGP_LEN equ $-segprfxtab

		; Instructions that can be used with REPE/REPNE.
		; (ins, outs, movs, stos, lods; cmps, scas)
replist:db 06Ch,06Eh,0A4h,0AAh,0ACh	; REP (no difference)
REP_SAME_LEN equ $-replist		; number of indifferent replist entries
	db 0A6h,0AEh			; REPE/REPNE
REP_LEN equ $-replist
REP_DIFF_LEN equ REP_LEN-REP_SAME_LEN	; number of replist entries with difference

A32P_LEN equ $-a32prfxtab

; prfxtab P_LEN REP_LEN REPE_REPNE_LEN

		; All the instructions in replist also have an implicit operand
		; subject to ASIZE (similar to segprfxtab). Additionally, the
		; xlat instruction (0D7h) has such an implicit operand too.
		; maskmovq too.


	%include "asmtabs.asm"


	usesection lDEBUG_DATA_ENTRY

msg_start:
	%include "msg.asm"

msg_end:

	numdef SHOWMSGSIZE, 0
%if _SHOWMSGSIZE
%assign MSGSIZE msg_end - msg_start
%warning msg holds MSGSIZE bytes
%endif


	usesection lDEBUG_DATA_ENTRY

%if _PM
	align 4
dpmientry:	dd 0	; DPMI entry point returned by DPMI host
dpmiwatch:	dd 0
dpmi_rm2pm:	dd 0
dpmi_rmsav:	dd 0
dpmi_pm2rm:	dw 0,0,0
dpmi_pmsav:	dw 0,0,0
	align 2
dssel:		dw 0	; debugger's (16-bit RW) data selector
cssel:		dw 0	; debugger's (16-bit RE) code selector
scratchsel:	dw 0	; scratch selector used for various purposes, limit -1
dpmi32:		db 0	; 32-bit client if true
bCSAttr:	db 0	; current code attribute (D bit)
bAddr32:	db 0	; Address attribute. if 1, hiword(edx) is valid


		; Int2F handler. Starts with an IISP header.
debug2F:
	jmp short actual2F
oldi2F:	dd -1
	db "KB"
	db 0
	jmp short _retf
	times 7 db 0

actual2F:
	pushf
	cmp ax, 1687h
dpmidisable:		; set this byte to __TEST_IMM8 to disable new DPMI entry
	je dpmiquery
	popf
jumpoldi2F:
	jmp far [ cs:oldi2F ]

dpmiquery:
	push cs
	call jumpoldi2F
	test ax, ax
	jnz .nohost

	mov word [ cs:dpmientry+0 ], di
	mov word [ cs:dpmientry+2 ], es
	mov di, mydpmientry
	push cs
	pop es
.nohost:
	iret

mydpmientry:
	mov byte [ cs:dpmi32 ], 0
	test al, 1
	jz .16
	inc byte [ cs:dpmi32 ]
.16:
	call far [ cs:dpmientry ]
	jnc installdpmi
_retf:
	retf

%xdefine __CPU_PREV__ __CPU__
cpu 286
installdpmi:
	pusha
	mov bp, sp		; [bp+16]=ip, [bp+18]=cs
	pushf
	push ds
	push es
	mov bx, cs
	mov ax, 000Ah		; get a data descriptor for DEBUG's segment
	int 31h
	jc .fataldpmierr
	mov ds, ax
	mov word [ cssel ], cs
	mov word [ dssel ], ax

	mov cx, 1		; allocate code_sel selector
	xor ax, ax
	int 31h
	jc .fataldpmierr
	mov word [ code_sel ], ax
	mov bx, ax
	xor cx, cx
	or dx, -1		; cx:dx = 0FFFFh
	mov ax, 0008h
	int 31h			; set limit 64 KiB
	jc .fataldpmierr
	lar cx, word [ cssel ]	; get access rights/type of cs
	shr cx, 8		; proper format for 31.0009
				; high byte zero (16-bit and byte-granular selector)
	mov ax, 0009h
	int 31h			; set descriptor access rights/type
	jc .fataldpmierr

	mov dx, word [ code_seg ]
	mov cx, dx
	shl dx, 4
	shr cx, 12
	mov ax, 0007h
	int 31h			; set selector base to code segment's base

	call entry_to_code_sel, installdpmi_code

.fataldpmierr:
	mov ax, 4CFFh
	int 21h


	usesection lDEBUG_CODE

	code_insure_low_byte_not_0CCh
installdpmi_code:
		; Some code (particularly d4message) may expect us to
		;  run on the debugger's stack, to access the data
		;  segment. Therefore, switch stacks.
	mov dx, ds		; dx = ds = debugger data selector
	mov ax, ss
	mov bx, sp		; ax:bx = stack to restore
	mov ss, dx
	mov sp, [run_sp]	; switch to our stack

	push ax
	push bx			; save original stack, far pointer

	setopt [internalflags], protectedmode

d4	call d4message
d4	asciz "In installdpmi_code",13,10

	mov cx, 2		; alloc 2 descriptors
	xor ax, ax
	int 31h
	jc .fataldpmierr

d4	call d4message
d4	asciz "In installdpmi_code, allocated 2 descriptors",13,10

	mov word [ scratchsel ], ax	; the first is used as scratch descriptor
	mov bx, ax
	xor cx, cx
%if 1
_386	dec cx			; set a limit of FFFFFFFFh if 386
%else
	cmp byte [ dpmi32 ], 0
	je .16
	dec cx			; set a limit of FFFFFFFFh if 32-bit client
.16:
%endif
	or dx, byte -1
	mov ax, 0008h
	int 31h
	mov ax, 0003h
	int 31h			; get selector increment
%if 0
	jnc .03sup
	mov ax, 8
.03sup:
%endif
	add bx, ax		; the second selector is client's CS
	xor cx, cx		; this limit is FFFFh even for 32-bits
	mov ax, 0008h
	int 31h

	pop dx
	pop ax
	mov es, ax
	push ax
	push dx

	mov dx, word [ es:bp+18 ]
				; get client's CS
	call setrmaddr		; set base
	lar cx, word [ cssel ]
	shr cx, 8		; CS remains 16-bit
	mov ax, 0009h
	int 31h
	mov dx, bx
	mov word [ es:bp+18 ], bx

	cld

	mov si, convsegs
	mov cx, NUMSEGS
.loopseg:
d4	call d4message
d4	asciz "In installdpmi_code.loopseg",13,10

	lodsw
	mov di, ax
	mov bx, word [di]
	mov ax, 0002h
	int 31h
	jc .fataldpmierr
	mov word [di], ax
	loop .loopseg

d4	call d4message
d4	asciz "In installdpmi_code after .loopseg",13,10

_386	push edi
_386	xor edi, edi		; clear edih
	clropt [internalflags], canswitchmode|switchbuffer
	xor bp, bp
_386	inc bp
_386	inc bp
.save16:
	mov ax, 0305h		; get raw mode-switch save state addresses
	int 31h
	jc .cannotswitch
	cmp ax, _AUXBUFFSIZE	; fits into auxbuff ?
	ja .cannotswitch	; no -->
	test ax, ax
	jz .nobuffer
	mov word [auxbuff_switchbuffer_size], ax
	setopt [internalflags], switchbuffer
	mov word [dpmi_rmsav+0], cx
	mov word [dpmi_rmsav+2], bx
	_386_o32	; mov dword [dpmi_pmsav], edi
	mov word [dpmi_pmsav], di
	mov word [ds:bp+dpmi_pmsav+2], si
.nobuffer:
_386	xor edi, edi		; clear edih
	mov ax, 0306h		; get raw mode-switch addresses
	int 31h
	jc .cannotswitch
	setopt [internalflags], canswitchmode
	mov word [dpmi_rm2pm+0], cx
	mov word [dpmi_rm2pm+2], bx
	_386_o32	; mov dword [dpmi_pm2rm], edi
	mov word [dpmi_pm2rm], di
	mov word [ds:bp+dpmi_pm2rm+2], si
.cannotswitch:
_386	pop edi

_386	push edx
_386	xor edx, edx		; clear edxh
	mov si, exctab		; hook several exceptions
	mov dx, exc00
.loopexc:
	lodsb
d4	call d4message
d4	asciz "In installdpmi_code.loopexc, ax="
d4	push ax
d4	call disp_stack_hex
d4	call d4message
d4	asciz 13,10

	mov bl, al
	mov cx, word [cssel]
	mov ax, 0203h
	int 31h
	add dx, byte exc01-exc00
	cmp si, endexctab
	jb .loopexc
_386	pop edx

	mov bl, 2Fh		; get int 2Fh real-mode vector
	mov ax, 0200h
	int 31h
	cmp cx, word [pspdbg]	; did we hook it and are the last in chain?
	jne .notours		; no -->
	mov dx, word [oldi2F+0]
	mov cx, word [oldi2F+2]	; then unhook
	mov ax, 0201h
	int 31h
	clropt [internalflags], hooked2F
.notours:
d4	call d4message
d4	asciz "In installdpmi_code end",13,10

	pop bx
	pop ax
	mov ss, ax
	mov sp, bx		; return to user stack

	pop es
	pop ds
	popf
	popa
	retf

.fataldpmierr:
d4	call d4message
d4	asciz "In installdpmi_code.fataldpmierr",13,10
	mov ax, 4CFFh
	int 21h


	usesection lDEBUG_DATA_ENTRY

convsegs:
	dw pInDOS+2
%if _USESDA
	dw pSDA+2
%endif
	dw a_addr+4
	dw d_addr+4
	dw auxbuff_segorsel
NUMSEGS equ ($-convsegs)/2

exctab:
	db 00h
	db 01h
	db 03h
%if _CATCHEXC06
	db 06h
%endif
%if _CATCHEXC0C
	db 0Ch
%endif
	db 0Dh
	db 0Eh
endexctab:


	struc exceptionframe16
		resw 8			; pusha
fr16_ds:	resw 1			; push ds
		resw 2			; 16-bit return address to DPMI host
		resw 1			; error code
fr16_ip:	resw 1
fr16_cs:	resw 1
fr16_fl:	resw 1
fr16_sp:	resw 1
fr16_ss:	resw 1
	endstruc

	struc exceptionframe32
		resd 8			; pushad
		resw 1			; stack alignment
fr32_ds:	resw 1			; push ds
		resd 2			; 32-bit return address to DPMI host
		resd 1			; error code
fr32_eip:	resd 1
fr32_cs:	resd 1
fr32_efl:	resd 1
fr32_esp:	resd 1
fr32_ss:	resd 1
	endstruc

	%macro exc_entry 2-3.nolist
exc %+ %1 %+ :
	push ds
	push %2
%ifempty %3
	jmp short exc
%elifnidn %3,last
 %fatal Invalid third parameter
%endif
	%endmacro

		; Exception handlers.
		; These are the entry into the debugger in protected mode.
		; The address difference between exc00 and exc01 is
		; assumed to be the same for all the entries. (Above, in
		; installdpmi, where the exception handlers are installed.)

	exc_entry 00, int0msg
	exc_entry 01, int1msg
	exc_entry 03, int3msg
%if _CATCHEXC06
	exc_entry 06, exc6msg
%endif
%if _CATCHEXC0C
	exc_entry 0C, excCmsg
%endif
	exc_entry 0D, excDmsg
	exc_entry 0E, excEmsg, last
exc:
	mov ds, word [cs:dssel]
	pop word [run_int]
	times 1 - (($ - $$) & 1) nop	; align in-code parameter
	call entry_to_code_sel, exc_code


	usesection lDEBUG_CODE

	code_insure_low_byte_not_0CCh
exc_code:
	cmp byte [dpmi32], 0
	jz exc16

[cpu 386]
exc32:
	push ax				; stack alignment
	pushad
	mov ebp, esp
	mov eax, dword [ ebp + fr32_eip ]
	mov bx, word [ ebp + fr32_cs ]
	mov ecx, dword [ ebp + fr32_efl ]
	mov edx, dword [ ebp + fr32_esp ]
	mov si, word [ ebp + fr32_ss ]
	mov word [ ebp + fr32_cs ], cs
	mov word [ ebp + fr32_ss ], ds
	cmp byte [ bInDbg ], 0		; did the exception occur inside DEBUG?
	je @F				; no -->

		; inside debugger
	push ds
	pop es
%if _EXCCSIP
	mov di, exccsip
	mov ax, bx
	call hexword
	inc di
	mov ax, word [ ebp + fr32_eip ]
	call hexword
%endif
	mov dword [ ebp + fr32_eip ], debuggerexception
	movzx eax, word [ savesp ]
	mov dword [ ebp + fr32_esp ], eax
	clropt [ ebp + fr32_efl ], 100h	; reset TF
	jmp short @FF

@@:		; inside debuggee
	setopt [internalflags], protectedmode
	mov dword [ ebp + fr32_eip ], intrtn2_code
	clropt [ ebp + fr32_efl ], 300h	; reset IF + TF
	mov dword [ ebp + fr32_esp ], reg_ss
	mov dword [ reg_eip ], eax
	mov word [ reg_cs ], bx
	; mov dword [ reg_efl ], ecx	; (eflh is saved in intrtn2_code)
	mov word [ reg_efl ], cx
	mov dword [ reg_esp ], edx
	mov word [ reg_ss ], si
	push word [ ebp + fr32_ds ]
	pop word [ reg_ds ]

@@:
	popad
	pop ax				; stack alignment
	pop ds
	o32 retf

__CPU__
exc16:
	pusha
	mov bp, sp
	mov ax, word [ bp + fr16_ip ]
	mov bx, word [ bp + fr16_cs ]
	mov cx, word [ bp + fr16_fl ]
	mov dx, word [ bp + fr16_sp ]
	mov si, word [ bp + fr16_ss ]
	mov word [ bp + fr16_cs ], cs
	mov word [ bp + fr16_ss ], ds
	cmp byte [ bInDbg ], 0		; did the exception occur inside DEBUG?
	je isdebuggee16
	push ds
	pop es
%if _EXCCSIP
	mov di, exccsip			; render CS:IP if internal GPF
	mov ax, bx
	call hexword
	inc di
	mov ax, word [ bp + fr16_ip ]
	call hexword
%endif
	mov word [ bp + fr16_ip ], debuggerexception
	mov ax, word [ savesp ]
	mov word [ bp + fr16_sp ], ax
	clropt [ bp + fr16_fl ], 100h	; reset TF
	jmp short isdebugger16
isdebuggee16:
	setopt [internalflags], protectedmode
	mov word [ bp + fr16_ip ], intrtn2_code
	clropt [ bp + fr16_fl ], 300h	; reset IF + TF
	mov word [ bp + fr16_sp ], reg_ss
	mov word [ reg_eip ], ax
	mov word [ reg_cs ], bx
	mov word [ reg_efl ], cx
	mov word [ reg_esp ], dx
	mov word [ reg_ss ], si
	push word [ bp + fr16_ds ]
	pop word [ reg_ds ]
isdebugger16:
	popa
	pop ds
	retf

%xdefine __CPU__ __CPU_PREV__
%undef __CPU_PREV__
__CPU__


		; Test if bx is a 32-bit selector (as opposed to a 16-bit selector or a segment)
		;
		; INP:	bx = selector (PM) or segment (RM)
		; OUT:	NZ = 32-bit
		;	ZR = 16-bit
		;	NC
testattrhigh:
_386	call ispm
_386	jz .pm				; 386 and PM, check selector -->
		; not PM or no 386
	cmp al, al			; ZR, NC
	retn
.pm:
[cpu 386]
	push eax
	lar eax, ebx			; access rights
	test eax, 400000h		; test bit (NC)
	pop eax
	retn
__CPU__


i23pm:
	cmp byte [ cs:dpmi32 ], 0	; always NC
	je .retf16_2
	o32
	retf 4				; retfd 4
.retf16_2:
	retf 2

i24pm:
	mov al, 03h			; fail
	cmp byte [ cs:dpmi32 ], 0
	je .iret16
	o32				; iretd
.iret16:
	iret

cpu 8086
%endif	; _PM



	usesection lDEBUG_CODE
%if _DEBUG4 || _DEBUG5
%define _DEB_ASM_PREFIX
%include "deb.asm"
%endif

	usesection lDEBUG_DATA_ENTRY
		; INP:	word [cs:ip] = near address to jump to in other segment
entry_to_code_seg:
	push ax			; word space for ?jumpaddress_ip, is ax
	mov ax, word [cs:code_seg]
%if _PM
	jmp entry_to_code_common

entry_to_code_sel:
	push ax
	mov ax, word [cs:code_sel]
%endif

entry_to_code_common:
	lframe 0
	lpar word, jumpaddress_cs_and_orig_ip
	lpar word, jumpaddress_ip
	lenter

	push si
	pushf
	cld

	xchg word [bp + ?jumpaddress_cs_and_orig_ip], ax	; fill function segment
	mov si, ax
	cs lodsw
%if _DEBUG
	cmp al, 0CCh		; debugger breakpoint ?
	jne @F			; no -->
	int3			; break to make it remove the breakpoint
	dec si
	dec si
	cs lodsw		; reload the word
	cmp al, 0CCh
	jne @F

.l:
	int3
	jmp .l

@@:
%endif
	xchg word [bp + ?jumpaddress_ip], ax		; fill function offset
		; (and restore ax)

	popf
	pop si

	lleave
	retf			; jump to dword [bp + ?jumpaddress]



		; debug22 - Interrupt 22h handler
		;
		; This is for DEBUG itself: it's a catch-all for the various Int23
		; and Int24 calls that may occur unpredictably at any time. What we
		; do is pretend to be a command interpreter (which we are, in a sense,
		; just with different sort of commands) by setting our parent PSP
		; value equal to our own PSP so that DOS does not free our memory when
		; we quit. Therefore control ends up here when DOS detects Control-C
		; or an Abort in the critical error prompt is selected.
debug22:
	cli
.cleartraceflag:
	cld			; reestablish things
	mov ax, cs
	mov ds, ax
	mov ss, ax
	mov sp, word [ savesp ]	; restore stack
	times 1 - (($ - $$) & 1) nop	; align in-code parameter
	call entry_to_code_seg, cmd3


	usesection lDEBUG_CODE

	code_insure_low_byte_not_0CCh
		; Begin main command loop.
cmd3:
	mov sp, word [ savesp ]	; restore stack (this must be first)
	_386_o32
	xor ax, ax
	_386_o32
	push ax
	_386_o32
	popf
	cld
	sti
	mov word [ errret ], cmd3
	mov word [ throwret ], errhandler
	mov word [ throwsp ], sp
	push ds
	pop es

%if _PM
	call getsegmented_resetmode
%endif
	call silence_dump

cmd3_serial_init:
	testopt [options], enable_serial
	jz .check_disable_serial
.check_enable_serial:
	testopt [serial_flags], sf_init_done
	jnz .done_serial
.enable_serial:

  call  serial_clear_fifos
  mov word [baseport], _UART_BASE
  call  serial_install_interrupt_handler
  call  serial_init_UART

	setopt [serial_flags], sf_init_done

	mov dx, msg.serial_request_keep
	call putsz

	mov di, line_out
%if _DEBUG
	mov al, '~'		; indicate instance is to be debugged
	stosb
%endif
	mov al, '='
	stosb
	mov al, 32
	stosb

	xor ax, ax
	mov word [getline_timer_count], ax
	mov word [getline_timer_last], ax
	mov word [getline_timer_func], .timer

	call getline00

	call skipcomm0
	dec si
	mov dx, msg.keep
	call isstring?
	je .done_serial

	mov dx, msg.serial_no_keep_enter
.no_keep:
	clropt [options], enable_serial
	call putsz
	jmp cmd3


.timer:
	push ax
	push dx
	push cx
	push es

	mov dx, 40h
	mov es, dx

	mov cx, word [getline_timer_count]
	mov dx, word [getline_timer_last]

	cmp dx, word [es:6Ch]
	je .timer_next
	mov dx, word [es:6Ch]
	inc cx
	mov al, 18
	mul byte [serial_keep_timeout]
	test ax, ax
	jz .timer_next
	cmp cx, ax
	jb .timer_next

	pop es
	mov dx, msg.serial_no_keep_timer
	jmp .no_keep

.timer_next:
	mov word [getline_timer_count], cx
	mov word [getline_timer_last], dx
	pop es
	pop cx
	pop dx
	pop ax
	retn


.check_disable_serial:
	testopt [serial_flags], sf_init_done
	jz .done_serial
.disable_serial:

  call serial_clean_up

	clropt [serial_flags], sf_init_done
.done_serial:

	call ensuredebuggeeloaded	; if no task is active, create a dummy one
	mov di, line_out	; build prompt
%if _DEBUG
	mov al, '~'		; indicate instance is to be debugged
	stosb
%endif
	mov al, '-'		; main prompt
%if _PM
	call ispm
	jnz .realmode
	mov al, '#'		; PM main prompt
.realmode:
%endif
	stosb

	mov byte [hhflag], 0
	and word [hh_depth], 0
	and word [hh_depth_of_single_term], 0
	mov word [getline_timer_func], dmycmd
	setopt [internalflags], pagedcommand	; 2009-02-21: default to page all commands
	clropt [internalflags], usecharcounter	; reset this automatically
	clropt [internalflags], tt_silence | tt_silent_mode
				; reset, in case it's still set
	clropt [internalflags2], dif2_tpg_proceed_bp_set | \
			dif2_bp_failure | dif2_tpg_keep_proceed_bp, 1
	clropt [internalflags2], dif2_closed_input_file

	call getline00		; prompted input, also resets linecounter

	cmp al, 13
	jne .notblank
	mov dx, word [lastcmd]
	mov byte [si], al
	jmp short cmd4
.notblank:
	mov word [lastcmd], dmycmd
	cmp al, ';'
	je cmd3			; if comment -->
	cmp al, '?'
	je help			; if request for help -->
	call uppercase
	sub al, 'A'
	cmp al, 'Y'-'A'
	ja error		; if not recognized
	cbw
	xchg bx, ax
	call skipcomma
	shl bx, 1
	mov dx, word [ cmdlist+bx ]
cmd4:
	mov di, line_out
	call dx
cmd3_j1:
	jmp cmd3		; back to the top


dmycmd:
	retn

help:
	call skipcomma
	call uppercase
%if _EXTHELP
 %if _COND
	mov dx, msg.condhelp
	cmp al, 'C'
	je .spec
 %endif
 %if _OPTIONS
	mov dx, msg.ophelp
	cmp al, 'O'
	je .spec		; option help -->
 %endif
 %if _EXPRESSIONS
	mov dx, msg.expressionhelp
	cmp al, 'E'
	je .spec
 %endif
%endif
%if _EMS
	mov dx, msg.xhelp
	cmp al, 'X'
	je .spec
%endif
	dec si
 %if _BOOTLDR && _EXTHELP
	mov dx, msg.boot
	call isstring?
	mov dx, msg.boothelp
	je .spec
 %endif
	mov dx, msg.run
	call isstring?
	mov dx, msg.help_run
	je .spec
	mov dx, msg.string_build
	call isstring?
	mov bx, msg.build_array
	mov cx, msg.build_short_amount
	je .spec_multi
	lodsb
	call uppercase
	mov cx, msg.build_long_amount
	cmp al, 'B'
	je .spec_multi		; build info -->
%if _EXTHELP
	mov dx, msg.license
	cmp al, 'L'
	je .spec		; licence -->
	mov dx, msg.flaghelp
	cmp al, 'F'
	je .spec		; flag help -->
	mov dx, msg.reghelp
	cmp al, 'R'
	je .spec		; register help -->
 %if _VARIABLES || _OPTIONS || _PSPVARIABLES
	mov dx, msg.varhelp
	cmp al, 'V'
	je .spec		; variable help -->
 %endif
%endif
	mov dx, msg.help	; default help
	db __TEST_IMM8		; (skip lodsb)
.spec:
	lodsb
	call chkeol
prnquit:
	call putsz		; print string
cmd3_j1a:
	jmp short cmd3_j1	; done

errorj1:jmp error

help.spec_multi:
	lodsb
	call chkeol
.loop:
	mov dx, word [bx]
	call putsz
	inc bx
	inc bx
	loop .loop
	jmp short cmd3_j1a


%include "aa.asm"
%include "dd.asm"
%include "rr.asm"
%if _RN
%include "fptostr.asm"
%endif
%include "run.asm"
%include "uu.asm"


	usesection lDEBUG_CODE

		; C command - compare bytes.
cc:
	call parsecm		; parse arguments

		; To make the 16-bit 64 KiB compare hack below work, the
		; full ecx mustn't be increased here for 16-bit segments.
		; The passed ecx isn't higher than FFFFh for 16-bit segments,
		; and a value of 0001_0000h needs to be passed as zero to
		; the hack anyway.
%if _PM
	cmp byte [ss:bAddr32], 0
	je .16
cpu 386
	inc ecx
	jnz cc1
	jmp error
cpu 8086
.16:
%endif
	inc cx
cc1:
	push ds
	push es
	push ss
	pop ds			; ds := cs
	call dohack		; do the interrupt pointer hack
	pop es
	pop ds
%if _PM
	cmp byte [ss:bAddr32], 0
	jz .cmp16
	a32 repe cmpsb
	mov dl, byte [esi-1]
	mov dh, byte [es:edi-1]
	jmp short .cmpdone
.cmp16:
%endif
		; The following 3 instructions make a hack to support 64 KiB
		; compare. The only time we get here with cx = 0 is the first
		; iteration for a 64 KiB compare. In that case, dec cx results
		; in FFFFh making repe cmpsb work. The single cmpsb will either
		; jump the repe cmpsb (if it found a mismatch) or not jump it.
		; The repe cmpsb might be executed with cx = 0, but will then
		; not change anything including the flags so it works.
	dec cx
	cmpsb
	jne .skip
	repe cmpsb		; start comparing
.skip:
	mov dl, byte [si-1]	; save the possibly errant characters
	mov dh, byte [es:di-1]
.cmpdone:
	lahf
	push ds
	push es
	push ss
	pop ds
	call unhack		; undo the interrupt pointer hack
	pop es
	pop ds
	sahf
	je cc2			; if we're done
	push es
	push ss
	pop es
	_386_PM_o32	; mov ebx, edi
	mov bx, di
	mov	di, line_out
	mov	ax, ds
	call hexword
	mov	al, ':'
	stosb
	_386_PM_o32	; mov eax, esi
	mov ax, si
	_386_PM_o32	; dec eax
	dec ax
%if _PM
	cmp byte [ss:bAddr32], 0
	jz .16si
	call hexword_high
.16si:
%endif
	call hexword
	mov ax, 32<<8|32
	stosw
	mov al, dl
	call hexbyte
	mov ax, 32<<8|32
	stosw
	mov al, dh
	call hexbyte
	mov ax, 32<<8|32
	stosw
	pop	ax
	push	ax
	call hexword
	mov al, ':'
	stosb
	_386_PM_o32	; mov eax, ebx
	mov ax, bx
	_386_PM_o32	; dec eax
	dec ax
%if _PM
	cmp byte [ss:bAddr32], 0
	jz .16bx
	call hexword_high
.16bx:
%endif
	call hexword
	push ds
	push ss
	pop ds
	push bx
	push cx
	call putsline_crlf
	pop cx
	pop	di
	pop	ds
	pop	es
%if _PM
	cmp byte [ss:bAddr32],0
	jz cc1_6
cpu 386
	jecxz cc2
cpu 8086
 cc1_j1:
	jmp cc1
cc1_6:
%else
 cc1_j1 equ cc1
%endif
%if 0
	_386_PM_o32	; inc ecx
	inc cx
	_386_PM_a32	; loopd cc1
	loop cc1		; if not done yet
%else
	_386_PM_a32	; jecxz cc2
	jcxz cc2
	jmp cc1			; if not done yet
%endif
cc2:
	push ss			; restore segment registers
	pop ds
	push ss
	pop es
	retn


%if _BOOTLDR
 %include "boot.asm"
%endif


	usesection lDEBUG_CODE

bb:
%if 0
	dec si
	mov bp, si
.loop:
	lodsb
	call uppercase
	cmp al, '_'
	je .loop
	cmp al, "'"
	je .loop
	cmp al, '0'
	jb .end
	cmp al, 'Z'
	ja .end
	cmp al, '9'
	jbe .loop
	cmp al, 'A'
	jae .loop
.end:
	call skipcomm0
	mov dx, 16
	cmp al, 13
	je .gotbase
	cmp al, ';'
	je .gotbase
	call getbyte
	cmp dl, 36
	ja error
	cmp al, 13
	je .gotbase
	cmp al, ';'
	je .gotbase
	call .read

.gotbase:
	call .read
%endif

%if _BOOTLDR
	testopt [internalflags], nodosloaded
	jz @F
	dec si
	dec si			; -> at 'B'
	mov dx, msg.boot
	call isstring?		; check for "BOOT"
	je bootcmd
	inc si			; skip 'B'
	lodsb			; load next
@@:
%endif
	call uppercase
%if _BREAKPOINTS
	cmp al, 'P'
	je point_set
	cmp al, 'N'
	je point_number
	cmp al, 'C'
	je point_clear
	cmp al, 'D'
	je point_disable
	cmp al, 'E'
	je point_enable
	cmp al, 'T'
	je point_toggle
	cmp al, 'L'
	je point_list
%endif
	cmp al, 'U'		; BU command ?
	je bu_breakpoint

%if 0
	cmp al, 'S'
	je bs_setoption
	cmp al, 'U'
	je bc_unsetoption
	cmp al, 'R'
	je br_resetoption
%endif

%if 0
	cmp al, 'D'		; BD command ?
	jne error		; no -->
bd_fakeindos:			; BD command - fake InDOS on/off
	call skipcomma
	cmp al, 13
	je .get
	call getword
	call chkeol		; expect end of line here
	cmp dx, 1
	ja error
	je .set			; selected 1 -->
.clear:				; selected 0
	clropt [options], fakeindos
	jmp short .get
.set:
	setopt [options], fakeindos
.get:
	mov al, '0'
	testopt [options], fakeindos
	jz .zero
	inc ax
.zero:
	mov di, line_out
	stosb
	jmp putsline_crlf
%endif

%ifn _BREAKPOINTS
	jmp short errorj4
%else
	dec si
point_set:
	call skipwhite
	call getpointindex
	jnc @F
	jz error		; "ALL" is invalid
	xor cx, cx
	push ax
.new_loop:
	mov ax, cx		; try this index
	call calcpointbit
%if ((_NUM_B_BP+7)>>3) != 1
	test byte [b_bplist.used_mask+bx], ah
%else
	test byte [b_bplist.used_mask], ah
%endif
	jz .new_found		; found unused one -->

	inc cx			; = next index
	cmp cx, _NUM_B_BP	; valid ?
	jb .new_loop		; yes, try next -->

	mov dx, msg.bb_no_new
	jmp prnquit

.new_found:
	pop ax
	push cx
	jmp @FF

@@:
	push dx
	push ax
	mov ax, dx
	call calcpointbit
%if ((_NUM_B_BP+7)>>3) != 1
	test byte [b_bplist.used_mask+bx], ah
%else
	test byte [b_bplist.used_mask], ah
%endif
	pop ax
	jnz error
@@:
	mov bx, word [reg_cs]
	call getlinearaddr
	jc error
	call getcounter
	xchg bx, dx		; dx:bx = linear
	xchg bx, ax		; dx:ax = linear
	pop bx
	push dx
	push ax
	mov ax, bx
	add bx, bx
	add bx, bx
%if _PM
	add bx, ax
%endif
	pop word [ b_bplist.bp + bx ]
		; These two instructions need to stay in that order.
		; For the non-PM version, the pop overwrites the byte
		; that is then initialized to 0CCh (the breakpoint
		; content byte).
	pop word [ b_bplist.bp + bx + 2 ]
	mov byte [ b_bplist.bp + bx + BPSIZE - 1 ], 0CCh
	mov bx, ax
	add bx, bx
	mov word [ b_bplist.counter + bx ], di
	call calcpointbit
%if ((_NUM_B_BP+7)>>3) != 1
	or byte [b_bplist.used_mask+bx], ah
	not ah
	and byte [b_bplist.disabled_mask+bx], ah
	and byte [b_bplist.sticky_mask+bx], ah
%else
	or byte [b_bplist.used_mask], ah
	not ah
	and byte [b_bplist.disabled_mask], ah
	and byte [b_bplist.sticky_mask], ah
%endif
	retn


		; INP:	al = first character, si -> next character
		; OUT:	di = counter value (defaults to 8000h)
		; CHG:	ax, si (flags not changed)
		;	does not return if error encountered
getcounter:
	pushf
	mov di, 8000h
	call skipwh0
	call iseol?
	je .got_counter
	push dx
	call getword
	mov di, dx
	pop dx
	call chkeol
.got_counter:
	popf
	retn


point_number:
	call skipwhite
	call getpointindex
	call getcounter
	jnc @F
	jnz error		; "NEW" is invalid -->

	xor cx, cx
.all_loop:
	mov ax, cx
	mov dx, cx
	call calcpointbit
%if ((_NUM_B_BP+7)>>3) != 1
	test byte [b_bplist.used_mask+bx], ah
%else
	test byte [b_bplist.used_mask], ah
%endif
	jz .all_next
	call .setnumber
.all_next:
	inc cx
	cmp cx, _NUM_B_BP
	jb .all_loop
	retn

@@:
	mov ax, dx
	call calcpointbit
%if ((_NUM_B_BP+7)>>3) != 1
	test byte [b_bplist.used_mask+bx], ah
%else
	test byte [b_bplist.used_mask], ah
%endif
	jz error

.setnumber:
	mov bx, dx
	add bx, bx
	mov word [b_bplist.counter + bx], di
	retn


point_clear:
	mov di, .clear
	jmp point_clear_enable_disable_toggle_common

.clear:
	not ax
%if ((_NUM_B_BP+7)>>3) != 1
	and byte [b_bplist.used_mask+bx], ah
%else
	and byte [b_bplist.used_mask], ah
%endif
	retn

point_clear_enable_disable_toggle_common:
	call skipwhite
	call getpointindex
	jnc @F
	jnz error		; "NEW" is invalid -->

	call chkeol
	xor cx, cx
.all_loop:
	mov ax, cx
	call calcpointbit
%if ((_NUM_B_BP+7)>>3) != 1
	test byte [b_bplist.used_mask+bx], ah
%else
	test byte [b_bplist.used_mask], ah
%endif
	jz .all_next
	call di
.all_next:
	inc cx
	cmp cx, _NUM_B_BP
	jb .all_loop
	retn

@@:
	call chkeol
	mov ax, dx
	call calcpointbit
%if ((_NUM_B_BP+7)>>3) != 1
	test byte [b_bplist.used_mask+bx], ah
%else
	test byte [b_bplist.used_mask], ah
%endif
	jz error
	call di
	retn


point_enable:
	mov di, .enable
	jmp point_clear_enable_disable_toggle_common

.enable:
	not ax
%if ((_NUM_B_BP+7)>>3) != 1
	and byte [b_bplist.disabled_mask+bx], ah
%else
	and byte [b_bplist.disabled_mask], ah
%endif
	retn


point_disable:
	mov di, .disable
	jmp point_clear_enable_disable_toggle_common

.disable:
%if ((_NUM_B_BP+7)>>3) != 1
	or byte [b_bplist.disabled_mask+bx], ah
%else
	or byte [b_bplist.disabled_mask], ah
%endif
	retn


point_toggle:
	mov di, .toggle
	jmp point_clear_enable_disable_toggle_common

.toggle:
%if ((_NUM_B_BP+7)>>3) != 1
	xor byte [b_bplist.disabled_mask+bx], ah
%else
	xor byte [b_bplist.disabled_mask], ah
%endif
	retn


point_list:
	call skipwhite
	call iseol?
	je .all
	call getpointindex
	jnc @F
	jnz error		; "NEW" is invalid -->
	call chkeol
	jmp .all
@@:
	call chkeol
	mov bx, dx
	mov di, line_out
	call .single
	jmp putsline_crlf

.all:
	xor bp, bp		; high byte: any set points encountered yet,
				; low byte: current line has any set points
	xor bx, bx
	mov di, line_out
.loop:
	push di
	mov al, 32
	mov cx, 40
	rep stosb		; initialize field with blanks
	xor al, al
	stosb			; terminate it
	pop di

	call .single		; fill buffer

	push bx
	test bl, 1		; an odd point ?
	jnz .odd		; yes -->
	mov di, line_out + 40	; write next point after the field
	jmp .was_even
.odd:
	test bp, 00FFh		; any point set in this line ?
	jz .skip_putsline	; no -->
	call putsline_crlf	; put line with linebreak (and no excess blanks)
	and bp, ~00FFh		; clear flag for next line processing
.skip_putsline:
	mov di, line_out	; write next point at start of field
.was_even:
	pop bx
	inc bx
	cmp bx, _NUM_B_BP
	jne .loop
	cmp di, line_out
	je @F
	call putsline_crlf
@@:
	test bp, 0FF00h
	jnz @F
	mov dx, msg.bpnone
	call putsz
@@:
	retn

.single:
	mov si, msg.bp
	call showstring
	push bx
	mov ax, bx
	call hexbyte		; store index of this point
	call calcpointbit
	mov si, msg.bpunused
%if ((_NUM_B_BP+7)>>3) != 1
	test byte [b_bplist.used_mask+bx], ah
%else
	test byte [b_bplist.used_mask], ah
%endif
	jnz @F			; if set -->
	call showstring
	jmp .unused

@@:
	or bp, 0101h		; flag that there was a point set in this line
	mov si, msg.bpdisabled
%if ((_NUM_B_BP+7)>>3) != 1
	test byte [b_bplist.disabled_mask+bx], ah
%else
	test byte [b_bplist.disabled_mask], ah
%endif
	jnz .disabled		; disabled --> (D)
	mov si, msg.bpenabled
.disabled:
	call showstring
	mov si, msg.bpaddress
	call showstring
	 pop ax
	 push ax
	mov si, ax
	add si, si
	add si, si
%if _PM
	add si, ax
%endif
	add si, b_bplist.bp	; -> point
	lodsw
	push ax
%if _PM
	lodsw
	call hexword
%else
	lodsb
	call hexbyte
%endif
	pop ax
	call hexword		; display (linear) address
	lodsb
	push ax
	mov si, msg.bpcontent
	call showstring
	pop ax
	call hexbyte		; display content
	mov si, msg.bpcounter
	call showstring
	 pop ax
	 push ax
	mov bx, ax
	add bx, bx
	mov ax, word [b_bplist.counter + bx]
	call hexword
.unused:
	pop bx			; restore counter (if displaying all)
	retn


		; INP:	ax = 0-based index of point
		; OUT:	(bx-> byte to access. only if at least 9 points)
		;	ah = value to access
		; CHG:	al
calcpointbit:
%if ((_NUM_B_BP+7)>>3) != 1
	mov bx, ax
%endif
	and al, 7
	mov ah, 1
	xchg ax, cx
	shl ch, cl
%if ((_NUM_B_BP+7)>>3) != 1
	mov cl, 3
	shr bx, cl
%else
	xor bx, bx
%endif
	xchg ax, cx
	retn


		; INP:	si->, al=
		; OUT:	NC if a point is specified,
		;	 dx = point index (0-based, below _NUM_B_BP)
		;	CY if a keyword is specified,
		;	 ZR if "ALL" keyword specified
		;	 NZ if "NEW" keyword specified
getpointindex:
	dec si
	mov dx, msg.all
	call isstring?
	je .is_all		; (ZR)
	mov dx, msg.new
	call isstring?
	je .is_new
	lodsb
	call getword
	cmp dx, _NUM_B_BP
	jae error
	clc			; (NC)
	retn

.is_new:
	test si, si		; (NZ)
.is_all:
	stc			; (CY)
	lodsb			; al = separator, si-> after
	retn
%endif


bu_breakpoint:
	lodsb
	call chkeol
%if _DEBUG
	mov dx, msg.bu
	call putsz
	int3
	retn
%else
	mov dx, msg.notbu
	jmp putsz
%endif

%if 0
bs_setoption:
	call skipcomma
	call getdword
	call chkeol		; expect end of line here
	or word [ options ], dx
	or word [ options+2 ], bx
	retn

bc_unsetoption:
	call skipcomma
	call getdword
	call chkeol		; expect end of line here
	not dx
	not bx
	and word [ options ], dx
	and word [ options+2 ], bx
	retn

br_resetoption:
	call skipcomma
	call getdword
	call chkeol		; expect end of line here
	mov ax, dx
	and ax, word [ startoptions ]
	not dx
	and word [ options ], dx
	or word [ options ], ax
	mov ax, bx
	and ax, word [ startoptions+2 ]
	not bx
	and word [ options+2 ], bx
	or word [ options+2 ], ax
	retn
%endif

uppercase:
	cmp al, 'a'
	jb .ret
	cmp al, 'z'
	ja .ret
	and al, TOUPPER
.ret:
	retn


errorj4:
	jmp error


%if 0
		; E command - edit memory.
ee:
	call prephack
	mov bx, word [reg_ds]
	call getaddr		; get address into bx:(e)dx
	call skipcomm0
	cmp al, ';'
	je ee1
	cmp al, 13
	je ee1			; if prompt mode

eeparsestr:
	push dx			; save destination offset
	call getstr		; get data bytes
	mov cx, di
	mov dx, line_out
	sub cx, dx		; length of byte string
	pop di
	mov ax, cx
	dec ax
	add ax, di
	jc short errorj4	; if it wraps around
	call dohack
	mov si, dx
	mov es, bx
%if _PM
	cmp byte [bAddr32], 0
	jz ee_2
cpu 386
	mov dx, di		; dx was destroyed
	mov edi, edx
	movzx esi, si
	movzx ecx, cx
	a32
cpu 8086
ee_2:
%endif
	rep movsb

		; Restore ds + es and undo the interrupt vector hack.
		; This code is also used by the `m' command.
ee0a:
	push ss			; restore ds
	pop ds
	push ss			; restore es
	pop es
	mov di, run2324		; debuggee's int 23/24 values
	call prehak1		; copy things back
	jmp unhack


ee0:
	push ss
	pop ds
	push ss
	pop es
	mov ax, word [ savesp ]
	inc ax
	inc ax
	mov sp, ax		; restore stack
	mov bx, word [ e_addr +4 ]
	_386_PM_o32
	mov dx, word [ e_addr ]	; get back address


		; Prompt mode.
ee1:
	mov word [ errret ], ee0

		; Begin loop over lines.
ee2:				; <--- next line
	mov word [ e_addr +4 ], bx
	_386_PM_o32
	mov word [ e_addr ], dx	; save address
	mov di, line_out
	mov ax, bx		; print out segment and offset
	call hexword
	mov al, ':'
	stosb
	_386_PM_o32		; mov eax, edx
	mov ax, dx
%if _PM
	call testattrhigh	; 32-bit segment ?
	jz .16			; no -->
	call hexword_high
.16:
%endif
	call hexword

		; Begin loop over bytes.
ee3:				; <--- next byte
	mov ax, 32<<8|32	; print old value of byte
	stosw
	call dohack		; do the INT pointer hack
	call readmem		; read mem at BX:(E)DX
	call unhack		; undo the INT pointer hack
	call hexbyte
	mov al, '.'
	stosb
	call getline00		; read input line
	cmp al, 13
	je .end
	cmp al, ';'
	je .end
%if _PM
	xor bx, bx
%endif
	mov dx, 1
	call ee_checkplusminus
	jne .notplusminus
	cmp al, '+'
	je ee3
	jmp short ee2

.notplusminus:



		; INP:	al = character, si-> line
		;	bx:dx = increment to add/subtract if this is an add/sub request
		; OUT:	al, si unchanged
		;	NZ if no add/sub request
		;	ZR if add/sub request,
		;	 [ e_addr ] offset adjusted
ee_checkplusminus:
	cmp al, '-'
	jne .not
	cmp al, '+'
	jne .not
	push si
	push ax
	call skipwhite
	call iseol?
	pop ax
	pop si
	jne .not
	cmp al, '-'
	je .minus
	add word [ e_addr ], dx
_386_PM	adc word [ e_addr+2 ], bx
	jmp short .done

.minus:
	sub word [ e_addr ], dx
_386_PM	sbb word [ e_addr+2 ], bx
.done:
	cmp al, al
.not:
	retn



	push bx
	push dx
	call putsline
	pop dx
	pop bx
	mov si, line_out+16	; address of buffer for characters
	xor cx, cx		; number of characters so far

ee4:
	cmp byte [notatty], 0
	je ee9			; if it's a TTY
	push si
	mov di, line_in+2
	mov si, word [bufnext]
ee5:
	cmp si, word [bufend]
	jb ee6			; if there's a character already
	call fillbuf
	mov al, 13
	jc ee8			; if eof
ee6:
	cmp byte [notatty], 13
	jne ee7			; if no need to compress CR/LF
	cmp byte [si], 10
	jne ee7			; if not a line feed
	inc si			; skip it
	inc byte [notatty]	; avoid repeating this
	jmp short ee5		; next character

ee7:
	lodsb			; get the character
	mov byte [notatty], al
ee8:
	mov word [bufnext], si
	pop si
	jmp short ee10

ee9:
	call getline00


	call getc		; character input without echo
ee10:
%if 1
	cmp al, 13
	je ee13
	cmp al, ';'
	je ee13			; exit edit mode -->
	jmp short
%else
	cmp al, 32
	je ee13			; if done with this byte
	cmp al, 13
	je ee13			; ditto
	cmp al, 8
	je ee11			; if backspace
	cmp al, '-'
	je ee112		; if '-'
	cmp cx,2		; otherwise, it should be a hex character
	jae ee4			; if we have a full byte already
	mov byte [si], al
	call getnyb
	jc ee4			; if it's not a hex character
	inc cx
	lodsb			; get the character back
	jmp short ee12
%endif
ee112:
	call putc
	dec dx			; decrement offset part
	mov di, line_out
	jmp short ee15
ee11:
	jcxz ee4		; if nothing to backspace over
	dec cx
	dec si
	call fullbsout
	jmp short ee4
ee12:
	call putc
	jmp short ee4		; back for more

		; We have a byte (if CX != 0).
ee13:
	jcxz ee14		; if no change for this byte
	mov byte [si], al	; terminate the string
	sub si, cx		; point to beginning
	push cx
	push dx
	lodsb
	call getbyte		; convert byte to binary (DL)
	mov al,dl
	pop dx
	pop	cx
	call dohack		; do the INT pointer hack
	call writemem		; write AL at BX:(E)DX
	mov di, run2324		; debuggee's int 23/24
	call prehak1		; copy things back
	call unhack		; undo the INT pointer hack

		; End the loop over bytes.
ee14:
	inc dx			; increment offset
	mov di,line_out
	cmp al, 13
	je ee16			; if done
	test dl, 7
	jz ee15			; if new line
	not cx
	add cx, byte 4		; compute 3 - cx
	mov al, 32
	rep stosb		; store that many spaces
	jmp ee3			; back for more

ee15:
	mov ax, 10<<8| 13	; terminate this line
	stosw
	jmp ee2			; back for a new line

ee16:
	jmp putsline_crlf	; call putsline and return
%else

		; E command - edit memory.
ee:
	call prephack
	mov bx, word [reg_ds]
	call getaddr		; get address into bx:(e)dx
	call skipcomm0
	cmp al, ';'
	je ee1
	cmp al, 13
	je ee1			; if prompt mode

	push dx			; save destination offset
	call getstr		; get data bytes
	mov cx, di
	mov dx, line_out
	sub cx, dx		; length of byte string
	pop di
	mov ax, cx
	dec ax
	add ax, di
	jc short errorj4	; if it wraps around
	call dohack
	mov si, dx
	mov es, bx
%if _PM
	cmp byte [bAddr32], 0
	jz ee_2
cpu 386
	mov dx, di		; dx was destroyed
	mov edi, edx
	movzx esi, si
	movzx ecx, cx
	a32
cpu 8086
ee_2:
%endif
	rep movsb

		; Restore ds + es and undo the interrupt vector hack.
		; This code is also used by the `m' command.
ee0a:
	push ss			; restore ds
	pop ds
	push ss			; restore es
	pop es
	mov di, run2324		; debuggee's int 23/24 values
	call prehak1		; copy things back
	jmp unhack



		; Prompt mode.
ee1:
		; Begin loop over lines.
ee2:				; <--- next line
	mov di, line_out
	mov ax, bx		; print out segment and offset
	call hexword
	mov al, ':'
	stosb
	_386_PM_o32		; mov eax, edx
	mov ax, dx
%if _PM
	call testattrhigh	; 32-bit segment ?
	jz .16			; no -->
	call hexword_high
.16:
%endif
	call hexword

		; Begin loop over bytes.
ee3:				; <--- next byte
	mov ax, 32<<8|32	; print old value of byte
	stosw
	call dohack		; do the INT pointer hack
	call readmem		; read mem at BX:(E)DX
	call unhack		; undo the INT pointer hack
	call hexbyte
	mov al, '.'
	stosb
	mov byte [ linecounter ], 0	; reset counter
	clropt [internalflags], promptwaiting
	push bx
	push dx
	call putsline
	pop dx
	pop bx
	mov si, line_out+16	; address of buffer for characters
	xor cx, cx		; number of characters so far

ee4:
	cmp byte [notatty], 0
	je ee9			; if it's a TTY
	push si
	mov di, line_in+2
	mov si, word [bufnext]
ee5:
	cmp si, word [bufend]
	jb ee6			; if there's a character already
	call fillbuf
	mov al, 13
	jc ee8			; if eof
ee6:
	cmp byte [notatty], 13
	jne ee7			; if no need to compress CR/LF
	cmp byte [si], 10
	jne ee7			; if not a line feed
	inc si			; skip it
	inc byte [notatty]	; avoid repeating this
	jmp short ee5		; next character

ee7:
	lodsb			; get the character
	mov byte [notatty], al
ee8:
	mov word [bufnext], si
	pop si
	jmp short ee10

ee9:
	call getc		; character input without echo
ee10:
	cmp al, 32
	je ee13
	cmp al, '.'
	je ee13
	cmp al, 13
	je ee13			; all: if done with this byte -->
	cmp al, 8
	je ee11			; if backspace
	cmp al, '-'
	je ee112		; if '-'
	cmp cx, byte 2		; otherwise, it should be a hex character
	jae ee4			; if we have a full byte already
	mov byte [si], al
	call getnyb
	jc ee4			; if it's not a hex character
	inc cx
	lodsb			; get the character back
	jmp short ee12
ee112:
	call putc
	dec dx			; decrement offset part
	mov di, line_out
	jmp short ee15
ee11:
	jcxz ee4		; if nothing to backspace over
	dec cx
	dec si
	call fullbsout
	jmp short ee4
ee12:
	call putc
	jmp short ee4		; back for more

		; We have a byte (if CX != 0).
ee13:
	jcxz ee14		; if no change for this byte
	mov byte [si], al	; terminate the string
	sub si, cx		; point to beginning
	push cx
	push dx
	lodsb
	call getbyte		; convert byte to binary (DL)
	mov al,dl
	pop dx
	pop	cx
	call dohack		; do the INT pointer hack
	call writemem		; write AL at BX:(E)DX
	mov di, run2324		; debuggee's int 23/24
	call prehak1		; copy things back
	call unhack		; undo the INT pointer hack

		; End the loop over bytes.
ee14:
	inc dx			; increment offset
	mov di,line_out
	cmp al, '.'
	je ee16
	cmp al, 13
	je ee16			; both: if done -->
	test dl, 7
	jz ee15			; if new line
	not cx
	add cx, byte 4		; compute 3 - cx
	mov al, 32
	rep stosb		; store that many spaces
	jmp ee3			; back for more

ee15:
	mov ax, 10<<8| 13	; terminate this line
	stosw
	jmp ee2			; back for a new line

ee16:
	jmp putsline_crlf	; call putsline and return
%endif

		; F command - fill memory
ff:
	xor cx, cx		; get address range (no default length)
	mov bx, word [reg_ds]
	call getrange		; get address range into bx:(e)dx
	_386_PM_o32	; sub ecx, edx
	sub cx, dx
	_386_PM_o32	; inc ecx
	inc cx			; (e)cx = number of bytes
	_386_PM_o32	; push ecx
	push cx			; save it
	_386_PM_o32	; push edx
	push dx			; save start address
	call skipcomm0
	call getstr		; get string of bytes
	mov cx, di
	sub cx, line_out
_386_PM	movzx ecx, cx
	_386_PM_o32	; pop edi
	pop di
	_386_PM_o32	; pop eax
	pop ax
%if _PM
	cmp byte [bAddr32], 0
	jz ff16
ff32:
cpu 386
	mov es, bx
	cmp ecx, byte 1
	je .onebytesource
	xor edx, edx		; edx:eax = size
	div ecx
	test eax, eax
	jz .partial
.loop:
	mov esi, line_out
	push ecx
	a32 rep movsb
	pop ecx
	dec eax
	jnz .loop
.partial:
	mov ecx, edx		; get remainder (number of bytes in partial copy)
	jecxz ffret		; if no partial copy -->
	mov si, line_out
	a32 rep movsb
	jmp short ffret		; done -->
.onebytesource:
	mov ecx, eax		; size
	mov al, byte [line_out]
	a32 rep stosb
	jmp short ffret
cpu 8086
ff16:
%endif
	mov es, bx
	cmp cx, byte 1
	je .onebytesource	; a common optimization
	xor dx, dx		; dx:ax = size
	cmp ax, byte 1
	adc dx, byte 0		; convert 0000:0000 to 0001:0000 (0 = 64 KiB)
	div cx			; compute number of whole repetitions
	test ax, ax
	jz .partial		; if less than one whole rep
.loop:
	mov si, line_out
	push cx
	rep movsb
	pop cx
	dec ax
	jnz .loop		; if more to go
.partial:
	mov cx, dx		; get remainder (number of bytes in partial copy)
	jcxz ffret		; if no partial copy -->
	mov si, line_out
	rep movsb
	jmp short ffret		; done -->
.onebytesource:
	mov cx, ax		; size
	mov al, byte [line_out]
	stosb			; cx=0 -> 64 kB
	dec cx
	rep stosb
ffret:
	push ss			; restore es
	pop es
	retn


%if _EXPRESSIONS

		; H command - hex computation
hh:
	call skipcomm0
	push si
	push ax
	or byte [hhflag], 1	; set flag so no operator means add
	call .compute
	pop ax
	pop si
	test byte [hhflag], 4	; any two-fold operation ?
	jz .done		; no -->
	push ax
	mov ax, 32<<8|32
	stosw
	pop ax
	or byte [hhflag], 2	; set flag so no operator means sub
	call .compute
.done:
	jmp putsline_crlf

.compute:
	call getdword
	call chkeol		; expect end of line here
	call .store
	test bx, bx		; result negative ?
	jns .comp_ret		; no -->
	mov ax, " ("
	stosw
	mov al, "-"
	stosb
	neg bx
	neg dx
	sbb bx, byte 0		; neg bx:dx
	call .store		; display "FFFFFFFF (-0001)"
	mov al, ")"
	stosb
.comp_ret:
	retn

.store:
	test bx, bx
	jz .store_nothigh	; no need to display 32-bit value
	mov ax, bx
	call hexword
.store_nothigh:
	mov ax, dx
	jmp hexword

%else
		; H command - hex addition and subtraction.
hh:
	call getdword
	push bx
	push dx
	call skipcomm0
	call getdword
	call chkeol		; expect end of line here
	pop cx
	pop ax			; first value in AX:CX, second in BX:DX
	mov si, ax
	mov bp, cx		; first value in SI:BP now
	mov ax, cx
	add ax, dx
	push ax
	mov ax, si
	adc ax, bx
	jz .nothigh1		; no need to display 32-bit value
	call hexword
.nothigh1:
	pop ax
	call hexword
	mov ax, 2020h
	stosw
	mov ax, bp
	sub ax, dx
	push ax
	mov ax, si
	sbb ax, bx
	jz .nothigh2		; no need to display 32-bit value
	or si, bx
	jz .nothigh2		; both were zero, non-zero result only by carry -->
	call hexword
.nothigh2:
	pop ax
	call hexword
	call putsline_crlf
	retn
%endif


		; O command - output to I/O port.
oo:
	mov ah, 'O'
	mov bx, .tab
	jmp short ii.common


	align 2
.tab:
	dw .byte, .word, .dword


.byte:
	call getbyte		; read value from command line
	call chkeol		; expect end of line here
	xchg ax, dx		; al = value
	pop dx			; recover port number
	out dx, al		; send
	retn

.word:
	call getword
	call chkeol
	xchg ax, dx		; ax = value
	pop dx
	out dx, ax
	retn

.dword:
cpu 386
	call getdword
	call chkeol		; expect end of line here
	push bx
	push dx
	pop eax			; eax = value
	pop dx
	out dx, eax
	retn
cpu 8086


		; I command - input from I/O port.
ii:
	mov ah, 'I'
	mov bx, .tab

		; bx = jump table for byte, word, dword handler
		; ah = letter of the command
		; si, al etc.
.common:
	push ax
	call uppercase
	cmp al, 'W'
	jne .notw
.incbx2:
	inc bx
	inc bx			; use word handler
	call skipwhite		; skip the 'W' til next character
	jmp short .sizeset

.notw:
	cmp al, 'D'
	jne .sizeset
%if 1
	xor ah, byte [si-2]
	jz .d			; "Id" or "Od" --> (uppercase command)
	xor ah, 32
	jnz .sizeset		; no space is allowed between the command and 'D' -->
			; "id" or "od" here (lowercase command)
.d:
%endif
_386	inc bx
_386	inc bx			; use dword handler
	_386_jmps .incbx2	; bx += 2 and skip the 'D'
			; no 386 here. try with D as part of port number
.sizeset:
	call getword		; get port
	pop cx			; restore letter if necessary
	cmp ch, 'I'		; check whether I or O
	jne .o			; O -->
	call chkeol		; expect end of line here for I commands
	db __TEST_IMM8		; (skip push)
.o:
	push dx			; save port number for O commands
	jmp near [cs:bx]


	align 2
.tab:
	dw .byte, .word, .dword

.byte:
	in al, dx
	call hexbyte
	jmp short .done

.word:
	in ax, dx
.doneword:
	call hexword
.done:
	jmp putsline_crlf

.dword:
cpu 386
	in eax, dx
	call hexword_high
cpu 8086
	jmp short .doneword


errorj5:jmp	error


%if _PM
		; OUT:	NC
		;	ZR if in protected mode
		;	NZ otherwise
		; STT:	-
		;	([internalflags] & nodosloaded, [internalflags] & protectedmode set up)
ispm:
	push ax
%if protectedmode & ~0FF00h
 %error Internal flags re-ordered, adjust code here
%endif
	mov al, byte [ss:internalflags+1]	; get flag byte
	and al, protectedmode>>8		; separate PM flag
	xor al, protectedmode>>8		; ZR if in PM (NC)
	pop ax
	retn
%endif


setpspdbg:
	mov bx, ss
setpsp:
%if _BOOTLDR
	testopt [internalflags], nodosloaded
	jnz .ret		; no PSPs -->
%endif
	mov ah, 50h
%if _PM && (_NOEXTENDER || _USESDA)
	call ispm
 %if _NOEXTENDER
	jnz .rm
cpu 286
	push cx
	push dx
	push bx
	push ax
	mov ax, 0006h
	int 31h
	pop ax
	shl cx, 12
	shr dx, 4
	or dx, cx
	mov bx, dx
	call _doscall.pm	; insure non-extended
	pop bx
	pop dx
	pop cx
	retn
cpu 8086
 %else
	jz _int21		; extended
 %endif
%endif
.rm:
%if _USESDA
	cmp word [pSDA+0], byte -1
	je _int21
	push ds
	push si
	lds si, [pSDA]
	mov word [si+10h], bx
	pop si
	pop ds
	retn
%else
	jmp short _int21
%endif
%if _BOOTLDR
.ret:
getpsp.ret:
	retn
%endif

getpsp:
%if _BOOTLDR
	xor bx, bx
	testopt [internalflags], nodosloaded
	jnz .ret		; no PSPs -->
%endif
	mov ah, 51h
%if _PM && (_NOEXTENDER || _USESDA)
	call ispm
 %if _NOEXTENDER
	jnz .rm
	call _doscall.pm	; insure non-extended
	mov ax, 2
	int 31h
	mov bx, ax
	retn
 %else
	jz _int21		; extended
 %endif
%endif
.rm:
%if _USESDA
	cmp word [pSDA+0], byte -1
	je _int21
	push ds
	lds bx, [pSDA]
	mov bx, word [bx+10h]
	pop ds
	retn
%else
	jmp short _int21
%endif

%if _PM && _NOEXTENDER
		; When we support non-extended DPMI, some calls to Int21
		; are (extended) Int21 calls and some are (not extended)
		; calls down to the real mode Int21. doscall is a macro
		; that will always call the non-extended Int21.
%macro doscall 0
	call _doscall
%endmacro

		; Execute a non-extended DOS call
_doscall:
	pushf
	call ispm
	jnz .rm
cpu 286
		; Execute a non-extended DOS call from PM
	popf
.pm:
	push word [ss:pspdbg]
	push 21h
	call intcall
	retn
cpu 8086
.rm:
	popf
	jmp _int21
%else
		; When we don't support non-extended DPMI all Int21 calls
		; are either in Real Mode or extended (all are real Int21
		; instructions).
%macro doscall 0
	int 21h
%endmacro
%endif

		; Execute real Int21 instruction. If this is in PM it might get extended.
_int21:
%if _BOOTLDR
	pushf
	testopt [internalflags], nodosloaded
	jnz .reterr		; no Int21 --> (throw?)
	popf
%endif
	int 21h
	retn
%if _BOOTLDR
.reterr:
	popf
	mov ax, 1
	stc
	retn
%endif


%if _PM
intcall_return_es:
	lframe near
	lpar word, es_ds_value
	lpar_return
	lpar word, int_number
	lpar word, bp_value
	lvar 32h, 86m_call_struc
	lenter
	push es
	mov word [bp + ?86m_call_struc +00h], di	; edi
	mov word [bp + ?86m_call_struc +04h], si	; esi
	mov word [bp + ?86m_call_struc +10h], bx	; ebx
	mov word [bp + ?86m_call_struc +14h], dx	; edx
	mov word [bp + ?86m_call_struc +18h], cx	; ecx
	mov word [bp + ?86m_call_struc +1Ch], ax	; eax
	mov ax, word [bp + ?bp_value]
	mov word [bp + ?86m_call_struc +08h], ax	; bp
	mov al, 0					; (preserve flags!)
	lahf
	xchg al, ah
	mov word [bp + ?86m_call_struc +20h], ax	; flags
	xor ax, ax
	mov word [bp + ?86m_call_struc +2Eh], ax	; sp
	mov word [bp + ?86m_call_struc +30h], ax	; ss
	mov ax, word [bp + ?es_ds_value]		; usually [pspdbg]
	mov word [bp + ?86m_call_struc +22h], ax	; es
	mov word [bp + ?86m_call_struc +24h], ax	; ds
	push ss
	pop es				; => stack
	lea di, [bp + ?86m_call_struc]	; -> 86-Mode call structure
_386	movzx edi, di			; (previously checked b[dpmi32] here)
	mov bx, word [bp + ?int_number]			; int#
	xor cx, cx
	mov ax, 0300h
	int 31h
	mov ah, byte [bp + ?86m_call_struc +20h]	; flags
	sahf
	mov di, word [bp + ?86m_call_struc +00h]	; edi
	mov si, word [bp + ?86m_call_struc +04h]	; esi
	mov bx, word [bp + ?86m_call_struc +10h]	; ebx
	mov dx, word [bp + ?86m_call_struc +14h]	; edx
	mov cx, word [bp + ?86m_call_struc +18h]	; ecx
	mov ax, word [bp + ?86m_call_struc +1Ch]	; eax
	push word [bp + ?86m_call_struc +22h]		; return es value
	pop word [bp + ?es_ds_value]			;  in the parameter
	pop es
	lleave
	lret

intcall:
	lframe near
	lpar word, es_ds_value
	lpar word, int_number
	lenter
	push word [bp + ?es_ds_value]
	push word [bp + ?int_number]
	push word [bp + ?frame_bp]
	call intcall_return_es
		; (discard returned parameter ?es_ds_value, done by lleave)
	lleave , forcerestoresp
	lret


		; Called in PM only, ds unknown.
		;
		; INP:	-
		; OUT:	CY if no DOS extender available ("MS-DOS" on Int2F.168A)
		;	NC if DOS extender available
		; CHG:	-
isextenderavailable:
cpu 286
	push ds
	push es
	pusha
	push ss
	pop ds
	mov si, msg.msdos
_386	movzx esi, si
	mov ax, 168Ah
	int 2Fh
	cmp al, 1			; CY if al is zero
	cmc				; NC if al is zero, CY else
	popa
	pop es
	pop ds
	retn
cpu 8086

nodosextinst:
	push ss
	pop ds
	mov dx, nodosext
	jmp putsz
%endif


		; L command - read a program, or disk sectors, from disk.
ll:
%if _BOOTLDR
	testopt [internalflags], nodosloaded
	jz @F
	mov dx, msg.nobootsupp
	jmp putsz
@@:
%endif

	call parselw		; parse L and W argument format
	jz ll1			; if request to read program
%if _PM && _NOEXTENDER
	call ispm
	jnz .rm
	call isextenderavailable
	jc nodosextinst
.rm:
%endif
	testopt [ss:internalflags], newpacket| ntpacket
	jz .oldint
	mov dl, al		; zero-based drive
	mov si, 6000h		; read, assume "file data"
%if _VDD
	testopt [internalflags], ntpacket
	jnz .vdd
%endif
	inc dl			; one-based drive
	mov ax, 7305h		; ds:(e)bx-> packet
	stc
	int 21h			; use int 21h here, not doscall
	jmp short .done
%if _VDD
.vdd:
	mov ax, word [hVdd]
	mov cx, 5
%if _PM
	add cl, byte [dpmi32]
%endif
	DispatchCall
	jmp short .done
%endif
.oldint:
	int 25h
.done:
	mov dx, reading
	jmp ww1

		; For .COM or .EXE files, we can only load at cs:100.  Check that first.
ll1:
	test byte [fileext], EXT_COM| EXT_EXE
	jz ll4			; if not .COM or .EXE file
	cmp bx, word [reg_cs]
	jne ll2			; if segment is wrong
	cmp dx, 100h
	je ll4			; if address is OK (or not given)
ll2:
	jmp error		; can only load .COM or .EXE at cs:100

		; load (any) file (if not .EXE or .COM, load at BX:DX)
ll3:
	cmp byte [fileext], 0
	jne ll4
	retn

		; open file and get length
ll4:
	mov si, bx		; save destination address, segment
	mov di, dx		; and offset
	mov ax, 3D00h		; open file for reading
	mov dx, DTA
	doscall
	jc ll16			; error
	xchg ax, bx		; mov bx, ax
	mov ax, 4202h		; lseek
	xor cx, cx
	xor dx, dx
	int 21h

;	Split off file types
;	At this point:
;		bx	file handle
;		dx:ax	file length
;		si:di	load address (CS:100h for .EXE or .COM)

	test byte [fileext], EXT_COM | EXT_EXE
	jnz ll13		; if .COM or .EXE file

%if _PM
;--- dont load a file in protected mode,
;--- the read loop makes some segment register arithmetic
	call ispm
	jnz .rm
	mov dx, nopmsupp
	call putsz
	jmp ll12
.rm:
%endif

		; Load it ourselves.
		; For non-.com/.exe files, we just do a read, and set BX:CX to the
		; number of bytes read.

	call ensuredebuggeeloaded	; make sure a debuggee is loaded
	mov es, word [pspdbe]

		; Check the size against available space.
	push	si
	push	bx

	cmp si, word [es:ALASAP]
	pushf
	neg si
	popf
	jae ll6				; if loading past end of mem, allow through ffff
	add si, word [es:ALASAP]	; si = number of paragraphs available
ll6:
	mov cx, 4
	xor bx, bx
ll7:
	shl si, 1
	rcl bx, 1
	loop ll7
	sub si, di
	sbb bx, cx			; bx:si = number of words left
	jb ll9				; if already we're out of space
	cmp bx, dx
	jne ll8
	cmp si, ax
ll8:
	jae ll10			; if not out of space
ll9:
	pop bx				; out of space
	pop si
	mov dx, doserr8			; not enough memory
	call putsz			; print string
	jmp short ll12

ll10:
	pop	bx
	pop	si

;	Store length in registers

; seems a bit unwise to modify registers if a debuggee is running
; but MS DEBUG does it as well

%if 0
	mov cx,[reg_cs]
	cmp cx,[pspdbe]
	jnz .noregmodify
	cmp word [reg_eip], 100h
	jnz .noregmodify
%endif
	mov word [reg_ebx], dx
	mov word [reg_ecx], ax
.noregmodify:

		; Rewind the file
	mov ax, 4200h		; lseek
	xor cx, cx
	xor dx, dx
	int 21h

	mov dx, 0Fh
	and dx, di
	mov cl, 4
	shr di, cl
	add si, di		; si:dx is the address to read to

		; Begin loop over chunks to read
ll11:
	mov ah, 3Fh		; read from file into DS:(E)DX
	mov cx, 0FE00h		; read up to this many bytes
	mov ds, si
	int 21h

	add	si,0fe0h	;wont work in protected-mode!
	cmp	ax,cx
	je	ll11		;if end of file reached

;	Close the file and finish up.

ll12:
	mov ah, 3Eh		; close file
	int 21h
	push ss			; restore ds
	pop ds
	retn			; done

ll13:
		; file is .EXE or .COM
		; Close the file
%if 0
	push ax
	mov ah, 3Eh		; close file
	int 21h
	pop bx			; dx:bx is the file length

		; adjust .exe size by 200h (who knows why)
		; cm: this is wrong. It needs to be adjusted by the header size,
		; which is stored (as number of paragraphs) in the .EXE header.
		; The header size is often 200h, but not always.
	test byte [fileext], EXT_EXE
	jz ll14			; if not .EXE
	sub bx, 200h
	sbb dx, 0
%else
	push dx
	push ax

	mov ax, 4200h		; lseek set
	xor cx, cx
	xor dx, dx
	int 21h
	 push ss
	 pop ds

	mov bp, sp
	mov cx, EXEHEADER_size
	sub sp, cx
	mov dx, sp
	mov si, sp
	mov ah, 3Fh
	int 21h

	push ax
	mov ah, 3Eh		; close file
	int 21h
	pop ax

	cmp ax, cx
	jne .no_exe
	cmp word [si + exeSignature], "MZ"
	je @F
	cmp word [si + exeSignature], "ZM"
	jne .no_exe
@@:

		; This possibly should honour the size of the image in pages
		; as indicated by the header, instead of the file size.
		; Oh well, for now we use the file size (on stack).
	mov ax, [si + exeHeaderSize]
	xor si, si
	mov cx, 4
@@:
	shl ax, 1
	rcl si, 1
	loop @B			; si:ax <<= 4

	mov sp, bp
	pop bx
	pop dx

	sub bx, ax
	sbb dx, si		; file size minus header size

	jmp @F

.no_exe:
	mov sp, bp
	pop bx
	pop dx			; full file size
@@:
%endif

		; Clear registers

ll14:
	push bx
	push dx
;	mov word [reg_ebx], dx
;	mov word [reg_ecx], bx

;--- cancel current process (unless there is none)
;--- this will also put cpu back in real-mode!!!

	call terminate_attached_process
	jz ll_attached_unterminated

	mov di, regs
	mov cx, 16*2	;(8 std, 6 seg, ip, fl) * 2
	xor ax, ax
	rep stosw

	pop word [reg_ebx]
	pop word [reg_ecx]

		; Fix up interrupt vectors in PSP
	mov si, CCIV		; address of original INT 23 and 24 (in PSP)
	mov di, run2324
	movsw
	movsw
	movsw
	movsw

		; Actual program loading.  Use the DOS interrupt.
	mov ax, 4B01h		; load program
	mov dx, DTA		; offset of file to load
	mov bx, execblk		; parameter block
	int 21h			; load it
	jc ll16			; if error
	mov ax, sp
	sub ax, [SPSAV]
	cmp ax, 80h
	jb ll15			; if in range
	mov ax, 80h
ll15:
	mov word [spadjust], ax
	les si, [execblk+14]
	es lodsw		; recover ax
	mov word [reg_eax], ax
	mov word [reg_esp], si
	mov word [reg_ss], es
	les si, [execblk+18]
	mov word [reg_eip], si
	mov word [reg_cs], es
	mov byte [bInit],0
	push ss
	pop es
	call set_efl_to_fl
	call getpsp
	mov ax, bx
	mov word [pspdbe], ax
	clropt [internalflags], attachedterm
	mov di, reg_ds
	stosw
	scasw
	stosw			; reg_es
	call setpspdbg

		; Finish up. Set termination address.
	mov ax, 2522h		; set interrupt vector 22h
	mov dx, int22		; ds => lDEBUG_DATA_ENTRY
	int 21h
	mov ds, word [pspdbe]
	mov word [TPIV], dx
	mov word [TPIV+2], ss	; => lDEBUG_DATA_ENTRY
	push ss
	pop ds

		; Set up initial addresses for 'a', 'd', and 'u' commands.
adusetup:
	mov ax, word [reg_eip]
	mov cx, word [reg_eip+2]
	mov bx, word [reg_cs]
	mov di, a_addr
	stosw			; IP
	mov word [di], cx
	scasw			; skip this word
	xchg ax, bx
	stosw			; CS
	xchg ax, bx		; d_addr
	stosw			; IP
	mov word [di], cx
	scasw
	xchg ax, bx
	stosw			; CS
	xchg ax, bx		; u_addr
	stosw			; IP
	mov word [di], cx
	scasw
	xchg ax, bx
	stosw			; CS
	retn

		; Error messages.  Print and quit.
ll16:
	jmp ww15		; print error message

ll_attached_unterminated:
	mov dx, msg.ll_unterm
	jmp putsz

		; M command - move from place to place.
		;
		; first check if there is more than 1 argument
		; 0 or 1 arguments are handled by the 'M [cpu]' code
mm:
%if 0
	push si
	cmp al, ';'
	je mc
	cmp al, 13
	je mc			; no argument, CPU-related M command
	mov ah, byte [ si ]
	push ax
	and ax, ~(2020h)
	cmp ax, "NC"
	pop ax
	je mc
	call getdword
	cmp al, ';'
	je mc
	cmp al, 13
	je mc			; one argument, CPU-related
	cmp al, 32
	je .blank		; end of first argument
	cmp al, 9
	jne .nonblank		; not end of first argument
.blank:
%else
	push si
	cmp al, ';'
	je mc
	cmp al, 13
	je mc			; no argument, CPU-related M command
.nonblank:
	lodsb			; is a non-space and non-CR ?
	cmp al, ';'
	je mc
	cmp al, 13
	je mc			; one argument, CPU-related
	cmp al, 32
	je .blank		; end of first argument
	cmp al, 9
	jne .nonblank		; not end of first argument
.blank:
%endif
	call skipwh0		; skip blanks behind argument
	cmp al, ';'
	je mc
	cmp al, 13
	je mc			; one argument, CPU-related
	pop si
	dec si
	lodsb
			; It is a normal M command (Move)
	call parsecm		; parse arguments (DS:ESI, ES:EDI, ECX)
	push cx
%if _PM
	call ispm
	jnz .rm
	mov ax, ds
	mov cx, es
	cmp ax, cx
	je .pmsimple		; same selector, simple -->

	mov ax, 0006h
	mov bx, ds
	int 31h			; get selector's base
	jc error
	push cx
	push dx
	mov ax, 0006h
	mov bx, es
	int 31h			; get selector's base
	jc error		; throw
	cmp byte [ss:bAddr32], 0
	je .pm16
[cpu 386]
	pop eax
	push cx
	push dx
	pop edx			; mov edx, cxdx
	add eax, esi		; add offset to source selector's base
	jc error
	add edx, edi		; add offset to destination selector's base
	jc error		; if overflow (> 4 GiB) -->
	cmp eax, edx		; compare linear source to linear destination
	jmp short m3		; and decide whether to move up or down -->
__CPU__

.rm:
	mov ax, ds
	mov bx, ds
	mov dx, es
	mov cl, 12
	shr bx, cl
	shr dx, cl
	push dx
	mov dx, es
	mov cl, 4
	shl ax, cl
	shl dx, cl
	pop cx
	db __TEST_IMM16		; (skip 2 pop instructions)

.pm16:
	pop ax
	pop bx
	add ax, si
	adc bx, byte 0		; add offset to source selector's base
	jc error
	add dx, di
	adc cx, byte 0		; add offset to destination selector's base
	jc error		; if overflow (> 4 GiB) -->
	cmp bx, cx		; compare linear source to linear destination
	jne m3
	cmp ax, dx
	jmp short m3		; and decide whether to move up or down -->

.pmsimple:
	_386_o32	; cmp esi, edi
	cmp si, di
%else
	mov dx, di
	mov bx, es
	mov cl, 4
	shr dx, cl
	add dx, bx		; upper 16 bits of destination
	mov ax, si
	shr ax, cl
	mov bx, ds
	add ax, bx
	cmp ax, dx
	jne m3			; if we know which is larger
	mov ax, si
	and al, 0Fh
	mov bx, di
	and bl, 0Fh
	cmp al, bl
%endif
m3:	pop cx
	lahf
	push ds
	push es
	push ss			; ds := cs
	pop ds
	call dohack		; do the interrupt pointer hack
	pop es
	pop ds
	sahf
	jae .forward		; if forward copy is OK
	_386_PM_o32
	add si, cx
	_386_PM_o32
	add di, cx		; point both behind data
	std			; _AMD_ERRATUM_109_WORKAROUND as below


	numdef AMD_ERRATUM_109_WORKAROUND, 1
		; Refer to comment in init.asm init_movp.

%if _AMD_ERRATUM_109_WORKAROUND
	_386_PM_a32
	jcxz @FF
	_386_PM_o32
	cmp cx, strict byte 20
	ja @FF
@@:
	_386_PM_a32
	movsb
	_386_PM_a32
	loop @B
@@:
%endif
.forward:
	_386_PM_a32
	rep movsb		; do the move
	_386_PM_a32
	movsb			; one more byte (length of zero means 64 KiB. or 4 GiB..)
.was32:
	cld			; restore flag
	jmp ee0a		; restore segments and undo the interrupt pointer hack


		; Other M command: set machine type.
mc:
	pop si
	dec si
	lodsb
	cmp al, 13
	je m10			; if just an 'm' (query machine type)
	cmp al, '?'
	je m10			; if '?' (also query)
	cmp al, '0'
	jb mc1			; if not a digit
	cmp al, '6'
	ja mc1			; ditto
	sub al, '0'
	mov byte [machine], al	; set machine type
	mov byte [mach_87], al	; coprocessor type, too
	retn

mc1:	or al, TOLOWER
	cmp al, 'c'
	je mcc			; if coprocessor declaration
	cmp al, 'n'
	jne short errorj3	; if something else
	lodsb
	or al, TOLOWER
	cmp al, 'c'
	jne short errorj3	; if not 'c' after that
	lodsb
	call chkeol
	mov byte [has_87], 0	; clear coprocessor flag
	retn			; done

mcc:
	call skipwhite		; get next nonblank character
	mov ah, byte [machine]
	cmp ah, 3
	jne mcc2		; if not a 386
	cmp al, '3'
	je mcc1			; if declaring a 387
	cmp al, '2'
	jne mcc2		; if not '2'
	mov ah, 2
mcc1:
	call skipwhite
mcc2:
	call chkeol
	mov byte [has_87], 1	; set coprocessor flag
	mov byte [mach_87], ah	; set copr. type
	retn

		; Display machine type.
m10:
	mov si, msg8088
	mov al, byte [machine]
	cmp al, 0
	je .88or86		; if 8088
	mov si, msgx86
	add al, '0'
	mov byte [si], al
.88or86:
	call showstring
	mov si, no_copr
	cmp byte [has_87], 0
	je m12			; if no coprocessor
	mov si, has_copr
	mov al, byte [mach_87]
	cmp al, byte [machine]
	je m12			; if has coprocessor same as processor
	mov si, has_287
m12:
	call showstring		; show string
	jmp putsline_crlf	; call puts and quit

errorj3:
	jmp error


		; N command - change the name of the program being debugged.
nn:
	push ss
	pop es
%if _BOOTLDR
	testopt [ss:internalflags], nodosloaded
	jz @F
	mov dx, msg.nobootsupp
	jmp putsz
@@:
%endif
	mov	di, DTA		; destination address

		; Copy and canonicalize file name.
nn1:
	call ifsep		; check for separators blank, tab, comma, ;, =
	je nn3			; if end of file name
	cmp al, byte [ss:swch1]
	je nn3			; if '/' (and '/' is the switch character)
	call uppercase
	stosb
	lodsb
	jmp short nn1		; back for more

nn3:
	push ss
	pop ds
	mov al, 0		; null terminate the file name string
	stosb
	mov word [execblk+2], di; save start of command tail

%if _DEBUG4
	push dx
	mov dx, DTA
	call disp_msg
	mov dx, crlf
	call disp_msg
	pop dx
%endif
		; Determine file extension
	cmp di, DTA+1
	je nn3d			; if no file name at all
	cmp di, DTA+5
	jb nn3c			; if no extension (name too short)
	mov al, EXT_HEX
	cmp word [di-5], ".H"
	jne nn3a		; if not .HEX
	cmp word [di-3], "EX"
	je nn3d			; if .HEX
nn3a:
	mov al, EXT_EXE
	cmp word [di-5], ".E"
	jne nn3b		; if not .EXE
	cmp word [di-3], "XE"
	je nn3d			; if .EXE
nn3b:
	mov al, EXT_COM
	cmp word [di-5], ".C"
	jne nn3c		; if not .COM
	cmp word [di-3], "OM"
	je nn3d			; if .COM
nn3c:
	mov al, EXT_OTHER
nn3d:
	mov byte [fileext], al

		; Finish the N command
	push di
	mov di, line_out
	dec si
nn4:
	lodsb			; copy the remainder to line_out
	stosb
	cmp al, 13
	jne nn4

		; Set up FCBs.
	mov si, line_out
	mov di, 5Ch
	call nn6		; do first FCB
	mov byte [reg_eax], al
	mov di, 6Ch
	call nn6		; second FCB
	mov byte [reg_eax+1], al

		; Copy command tail.
	mov si, line_out
	pop di
	push di
	inc di
nn5:
	lodsb
	stosb
	cmp al, 13
	jne nn5			; if not end of string
	push di
	mov cx, di
	sub cx, DTA+128
	neg cx
	xor ax, ax
	rep stosb
	pop di
	pop ax			; recover old DI
	xchg ax, di
	sub ax, di		; compute length of tail
	dec ax
	dec ax
	stosb
%if _DEBUG4
	mov dx, DTA
	call disp_msg
	mov dx, crlf
	call disp_msg
%endif
	retn			; done

		; Subroutine to process an FCB.
nn6:
	lodsb
	call iseol?
	je nn7			; if end
	call ifsep
	je nn6			; if separator
	cmp al, byte [switchar]
	je nn10			; if switch character
nn7:
	dec si
	mov ax, 2901h		; parse filename
	doscall
	push ax			; save AL
nn8:
	lodsb			; skip till separator
	call ifsep
	je nn9			; if separator character
	cmp al, byte [swch1]
	jne nn8			; if not switchar (sort of)
nn9:
	dec si
	pop ax			; recover AL
	cmp al, 1
	jne nn9a		; if not 1
	dec ax
nn9a:
	retn

		; Handle a switch (differently).
nn10:	lodsb
	call iseol?
	je nn7			; if end of string
	call ifsep
	je nn10			; if another separator
	mov al, 0
	stosb
	dec si
	lodsb
	cmp al, 'a'
	jb nn11			; if not a lower case letter
	cmp al, 'z'
	ja nn11
	and al, TOUPPER		; convert to upper case
nn11:	stosb
	mov ax, 32<<8|32
	stosw
	stosw
	stosw
	stosw
	stosw
	xor ax, ax
	stosw
	stosw
	stosw
	stosw
	retn			; return with al = 0


		; Ensure segment in bx is writeable
		;
		; INP:	bx = selector/segment
		; OUT:	NC if in 86M, bx unchanged
		;	NC if in PM and bx not a code segment, bx unchanged
		;	NC if in PM and was a code segment,
		;	 bx = word [scratchsel], set up to mirror INP:bx selector
		;	CY if in PM and a failure occurred, segment not writeable
		; CHG:	bx
		; STT:	(if in PM) es = ss = debugger data selector
%if _PM
verifysegm:
	call ispm
	jnz .rm			; (NC)
	push ax
	_386_o32	; push edi
	push di
	push bp
	mov bp, sp
	sub sp, 8
	mov di, sp
_386	movzx edi, di
	mov ax, 000Bh		; get descriptor
	int 31h
	jc @F
	test byte [di+5], 8	; code segment ?
	jz @F			; (NC) no -->
	and byte [di+5], 0F3h	; reset CODE+conforming attr
	or byte [di+5], 2	; set writable
	mov bx, word [scratchsel]
	mov ax, 000Ch
	int 31h
@@:
	mov sp, bp
	pop bp
	_386_o32	; pop edi
	pop di
	pop ax
.rm:
	retn

cpu 286
setrmsegm:
	mov bx, word [ss:scratchsel]
setrmaddr:		;<--- set selector in BX to segment address in DX
	mov cx, dx
	shl dx, 4
	shr cx, 12
	mov ax, 7
	int 31h
	retn
__CPU__
%endif

		; Read a byte relative to cs:eip
		;
		; INP:	reg_cs, reg_eip
		;	cx = (signed) eip adjustment
		; OUT:	al = byte at that address
		;	(e)bx = new offset (eip+adjustment)
		; CHG:	-
getcseipbyte:
	push es
%if _PM
	mov bx, word [reg_cs]
	mov es, bx
	call testattrhigh
	jz .16
cpu 386
	mov ebx, dword [reg_eip]
	push edx
	movsx edx, cx
	add ebx, edx
	mov al, byte [es:ebx]
	pop edx
	pop es
	retn
cpu 8086
.16:
%else
	mov es, word [reg_cs]
%endif
	mov bx, word [reg_eip]
	add bx, cx
	mov al, byte [es:bx]
	pop es
	retn

		; Write to a byte relative to cs:eip
		;
		; INP:	reg_cs, reg_eip
		;	cx = (signed) eip adjustment
		; OUT:	al = byte at that address
		; CHG:	(e)bx
setcseipbyte:
	push es
%if _PM
	mov bx, word [reg_cs]
	call verifysegm
	jc .ret
	mov es, bx
	call testattrhigh
	jz .16
cpu 386
	mov ebx, dword [reg_eip]
	push edx
	movsx edx, cx
	mov byte [es:ebx+edx],al
	pop edx
	pop es
	retn
cpu 8086
.16:
%else
	mov es, word [reg_cs]
%endif
	mov bx, word [reg_eip]
	add bx, cx
	mov byte [es:bx], al
.ret:
	pop es
	retn

		; Exchange byte with memory
		;
		; INP:	bx:(e)dx-> destination byte
		;	al = source byte
		; OUT:	CY if failed due to segment not writable
		;	NC if successful,
		;	 al = previous value of destination byte
		; CHG:	ah
writemem:
%if _DEBUG1
	push dx
	push ax

	call getlinear.do_not_use_test
	jc @F			; already an error ?  then return --> (CY)
	push bx
	push cx
	mov bx, test_records_Writemem
	call handle_test_case_multiple_16
				; check whether this should testcase the error
				; CY to indicate error from this call
	pop cx
	pop bx
@@:
	pop ax
	pop dx
	jnc .do_not_use_test
	retn			; return CY here

%endif
.do_not_use_test:

	mov ah, al
%if _PM
	call ispm
	jnz .16			; (NC from ispm) -->
	call verifysegm		; make bx a writeable segment
	jc .ret
	call testattrhigh
	jz .16			; (NC from testattrhigh) -->
cpu 386
	push ds
	mov ds, bx
	xchg al, byte [edx]
	cmp ah, byte [edx]
	pop ds
cpu 8086
	jmp short .cmp
.16:
%endif
	push ds
	mov ds, bx
	push bx
	mov bx, dx
	xchg al, byte [bx]
	cmp ah, byte [bx]
	pop bx
	pop ds
.cmp:
	je .ret			; (NC)
	stc			; Failed to compare (i.e. memory wasn't our byte after writing).
				; This check catches ROM that will silently fail to write.
.ret:
	retn


;--- read byte at BX:EDX into AL

readmem:
%if _DEBUG1
	push dx
	push ax

	call getlinear.do_not_use_test
	jc @F			; already an error ?  then return --> (CY)
	push bx
	push cx
	mov bx, test_records_Readmem
	call handle_test_case_multiple_16
				; check whether this should testcase the error
				; CY to indicate error from this call
	pop cx
	pop bx
@@:
	pop ax
	pop dx
	jnc .do_not_use_test
	mov al, byte [test_readmem_value]
				; return a most likely wrong value
	retn

%endif
.do_not_use_test:

%if _PM
	call testattrhigh
	jz .16
	push ds
	mov ds, bx
	mov al, byte [edx]
	pop ds
	retn
.16:
%endif
	push ds
	push bx
	mov ds, bx
	mov bx, dx
	mov al, byte [bx]
	pop bx
	pop ds
	retn


		; Q command - quit.
qq:
 %if _BOOTLDR
		; Test whether we are in non-DOS mode, and were
		; currently entered in protected mode. Since
		; this will make the entire operation fail,
		; it has to be checked for before modifying
		; or releasing any of the resources.
		; (Does this ever occur? No?)
	testopt [internalflags], nodosloaded
	jz .notpmnodos
%if _PM
	call ispm
  %if _TSR	; same message, reuse code
	jz .cannotpmquit
  %else
	jnz .notpmnodos_nodos
	mov dx, msg.cannotpmquit
	jmp putsz
  %endif
%endif
.notpmnodos_nodos:
	call bootgetmemorysize		; dx => behind usable memory
	mov ax, word [ boot_new_memsizekib ]
	mov cl, 6
	shl ax, cl
	cmp ax, dx			; same?
	je @F
	mov dx, msg.cannotbootquit_memsizes
	jmp .putsz
%if !_TSR || !_PM
	.putsz equ putsz
%endif

@@:
.notpmnodos:
 %endif
%if _PM
 %if _TSR
		; Test whether we are in TSR mode, and were
		; currently entered in protected mode. Since
		; this will make the entire operation fail,
		; it has to be checked for before modifying
		; or releasing any of the resources.
	testopt [internalflags], tsrmode
	jz .notpmtsr
	call ispm
	jnz .notpmtsr

; This isn't yet implemented. Broken down:
; * Uses terminate_attached_process which returns in real mode.
;  * Exception vectors are implicitly restored/discarded by that.
; * (RM) Interrupt vectors are currently restored in real mode. Unnecessary.
; * The VDD is un-registered in real mode. Necessary?
; * Normal 21.4C is used to return to the real parent.
;  * We have to discard our DOS process resources. Any DPMI TSR resources?
;  * We must again gain control in debuggee's mode after discarding them.
;  * We must return to the debuggee and seemlessly discard our memory. The
;    stack trick possibly/probably does not work in protected mode.

.cannotpmquit:
	mov dx, msg.cannotpmquit
.putsz:
	jmp putsz

.notpmtsr:
 %endif

 %if (nohook2F)&~0FF00h
  %fatal Internal flags re-ordered, adjust code here
 %endif
	mov ax, [internalflags]
 	mov al, __TEST_IMM8
	xchg al, [dpmidisable]		 	; disable DPMI hook
						; (SMC in section lDEBUG_DATA_ENTRY)
	push ax
	setopt [internalflags], nohook2F	; avoid a new hook while terminating
%endif
%if _BOOTLDR
	testopt [internalflags], nodosloaded
	jnz .restoreints
%endif

		; Cancel child's process if any.
		; This will drop to real mode if debuggee is in protected mode.
%if _TSR
	testopt [internalflags], tsrmode
	jnz .restoreints
%endif

	call terminate_attached_process
	jz qq_attached_unterminated

.restoreints:
	pop ax					; (discard)
		; Restore interrupt vectors.
	call serial_clean_up			; unhook interrupt
	clropt [serial_flags], sf_init_done	; clear (in case return to cmd3)

	mov di, intsave
	mov si, inttab
	mov cx, inttab_number
.nextint:
	lodsb
%if _BOOTLDR
	xor bx, bx
%endif
	mov bl, al
	inc si
	inc si
	xchg si, di
	lodsw
	mov dx, ax
	lodsw
	xchg si, di
	; cmp dx, byte -1
	; je .norestore
	push ds
%if _BOOTLDR
	testopt [internalflags], nodosloaded
	jz .int21_25
	push ax
	push dx
	xor ax, ax
	mov ds, ax
	add bx, bx
	add bx, bx
	pop word [ bx ]
	pop word [ bx+2 ]
	jmp short .popds
.int21_25:
%endif
	mov ds, ax
	mov al, bl
	mov ah, 25h
	int 21h
.popds:
	pop ds
.norestore:
	loop .nextint

%if _PM
	testopt [internalflags], hooked2F
	jz .noint2F
	push ds
%if _BOOTLDR
	testopt [internalflags], nodosloaded
	jz .int21_25_2f
	push word [oldi2F+2]
	push word [oldi2F]
	xor ax, ax
	mov ds, ax
	pop word [2Fh*4]
	pop word [2Fh*4+2]
	jmp short .popds2f
.int21_25_2f:
%endif
	mov ax, 252Fh
	lds dx, [oldi2F]
	int 21h
.popds2f:
	pop ds
.noint2F:
%endif

		; Release the registered VDD.
%if _VDD
	testopt [internalflags], ntpacket
	jz .novdd
	mov ax, word [hVdd]
	UnRegisterModule
.novdd:
%endif

		; Restore termination address.
%if _BOOTLDR
	testopt [internalflags], nodosloaded
	jnz .bootterminate	; terminate -->
%endif
%if _TSR
	testopt [internalflags], tsrmode
	jz .nontsrterminate
	xor si, si
	mov es, word [auxbuff_segorsel]
	xor di, di
	xor ax, ax
	mov cx, 8
	rep stosw		; 10h MCB bytes
	mov cx, 40h
	rep movsw		; 80h PSP bytes
	mov ax, es
	inc ax
	mov word [es:1], ax	; fake MCB
	push ds
	mov ds, ax
	mov word [34h], 18h
	mov word [36h], ax	; insure default PHT and fix segment
	mov word [32h], 1	; only one PHT entry (zero might crash)
	mov byte [18h], -1	; PHT entry is closed
	mov word [2Ch], 0	; PSP clear
	call .setparent		; make it self-owned, just in case
	mov bx, ss
	dec bx
	mov ds, bx		; => our (real) MCB
	mov word [1], ax	; parent = fake PSP
	pop ds
	call .setparent		; make the fake PSP our parent
	jmp short terminate	; see ya

.nontsrterminate:
%endif
	mov si, psp22		; restore termination address
	mov di, TPIV
	movsw
	movsw
	mov di, 16h		; restore PSP of parent
	movsw
		; Really done.
terminate:			; re-used by terminate_attached_process
	mov ax, 4C00h		; quit
	int 21h


qq_attached_unterminated:
		; Restore state:
%if _PM
 %if (nohook2F)&~0FF00h
  %fatal Internal flags re-ordered, adjust code here
 %endif
 	pop ax
	mov [dpmidisable], al	; (SMC in section lDEBUG_DATA_ENTRY)
	and ah, nohook2F>>8
	clropt [internalflags], nohook2F
	or [internalflags+1], ah
%endif
	mov dx, msg.qq_unterm
	jmp putsz


	usesection lDEBUG_DATA_ENTRY

%if _TSR
qq.proceedtsrtermination:
	cli
	cld
	mov ax, cs
	mov ds, ax
	mov ss, ax
	mov sp, stack_end
	sti
	sub word [reg_esp], 2+4+((qq.tsrfreecode_size+1)&~1)
	mov di, word [reg_esp]	; -> stack frame
	mov es, word [reg_ss]
	mov ax, word [reg_ds]
	stosw			; debuggee's ds
	mov ax, word [reg_eip]
	stosw
	mov ax, word [reg_cs]
	stosw			; debuggee's cs:ip
	push es
	push di
	mov si, qq.tsrfreecode
	mov cx, ((qq.tsrfreecode_size+1)>>1)
	rep movsw		; code on stack
	mov ax, cs
	dec ax
	mov word [reg_ds], ax	; = our MCB
	pop word [reg_eip]
	pop word [reg_cs]	; -> code on stack

	call entry_to_code_seg, .proceedtsrcode


	usesection lDEBUG_CODE

	code_insure_low_byte_not_0CCh
.proceedtsrcode:
%if _DEBUG
		; avoid hooking interrupts again:
	mov byte [cs:..@patch_tsr_quit_run], __JMP_REL8
				; (SMC in section lDEBUG_CODE)
%endif
	jmp run			; run this


	usesection lDEBUG_DATA_ENTRY

	align 2
	; Note that since we are in control of debuggee's TF and
	; reset it every time the debugger is entered, this code
	; will not be entered with TF set. It might be entered
	; with IF set and an interrupt might occur; the only harm
	; done then is that the interrupt handler has less stack
	; available. All flags must be preserved by this code.
qq.tsrfreecode:
	mov word [1], 0		; free the MCB
	pop ds			; restore debuggee's ds
	retf ((qq.tsrfreecode_size+1)&~1)	; jump
qq.tsrfreecode_size: equ $-qq.tsrfreecode


	usesection lDEBUG_CODE

qq.setparent:
	mov word [16h], ax
	mov word [0Ah], qq.proceedtsrtermination
	mov word [0Ah+2], ss
	retn
%endif

%if _BOOTLDR
qq.bootterminate:
	sub word [reg_esp], 2*8+4+((qq.bootfreecode_size+1)&~1)
	mov di, word [reg_esp]	; -> stack frame
	mov es, word [reg_ss]
	mov ax, word [reg_ds]
	stosw
	mov ax, word [reg_es]
	stosw
	mov ax, word [reg_esi]
	stosw
	mov ax, word [reg_edi]
	stosw
	mov ax, word [reg_eax]
	stosw
	mov ax, word [reg_ecx]
	stosw
	mov ax, word [reg_ebx]
	stosw
	mov ax, word [reg_edx]
	stosw
	mov ax, word [reg_eip]
	stosw
	mov ax, word [reg_cs]
	stosw			; debuggee's cs:ip
	push es
	push di
	 push ds
	  push cs
	  pop ds		; => lDEBUG_CODE
	mov si, qq.bootfreecode
	mov cx, ((qq.bootfreecode_size+1)>>1)
	rep movsw		; code on stack
	 pop ds

	 push ss
	 pop es

	mov ax, word [ boot_new_memsizekib ]
	mov cl, 6
	shl ax, cl		; ax => source of EBDA (new position)
	mov dx, word [ boot_old_memsizekib ]
	shl dx, cl		; dx => destination of EBDA (old position)
	xor cx, cx		; size of EBDA to move (if none)
	push ds
	mov ds, cx
	mov bx, word [40Eh]	; new ref in word [0:40Eh] (if none)
	pop ds
	cmp byte [ boot_ebdaflag ], 0	; any EBDA ?
	jz .noebda

	push ds
	mov ds, ax		; => EBDA
	xor bx, bx
	mov bl, byte [ 0 ]	; EBDA size in KiB
	mov cl, 6
	shl bx, cl		; *64, to paragraphs
	mov cx, bx		; = size of EBDA to move (in paragraphs)
	mov bx, dx		; = new EBDA reference to put in word [0:40Eh]
	pop ds

.noebda:
	mov word [reg_eax], ax	; => relocated (new) EBDA position
				;  (in front of debugger image)
	mov word [reg_ebx], bx	; = what to put in word [0:40Eh],
				;  unchanged content of that word if no EBDA
	mov word [reg_ecx], cx	; = EBDA size, 0 if no EBDA
	mov word [reg_edx], dx	; = original (old) EBDA position
				; = original mem size (in paras)
				;  (behind/in debugger image)
	mov word [reg_ds], 0

	pop word [reg_eip]
	pop word [reg_cs]	; -> code on stack
	; call dumpregs
%if _DEBUG
		; avoid hooking interrupts again:
	mov byte [cs:..@patch_tsr_quit_run], __JMP_REL8
				; (SMC in section lDEBUG_CODE)
%endif
	; jmp cmd3
	jmp run			; run this


	align 2
qq.bootfreecode:
	pushf
	call movp		; move EBDA back (if any)
	mov word [40Eh], bx	; back relocate EBDA (if any)
	mov cl, 6
	shr dx, cl		; = to KiB
	mov word [413h], dx	; back relocate mem size
	popf
	pop ds
	pop es
	pop si
	pop di
	pop ax
	pop cx
	pop bx
	pop dx
	retf ((qq.bootfreecode_size+1)&~1)


		; Move paragraphs
		;
		; INP:	ax:0-> source
		;	dx:0-> destination
		;	cx = number of paragraphs
		; CHG:	-
		; Note:	Doesn't work correctly on HMA; doesn't always wrap to LMA either.
		;	Do not provide a wrapped/HMA source or destination!
movp:
	push cx
	push ds
	push si
	push es
	push di

	cmp ax, dx		; source below destination ?
	jb .down		; yes, move from top down -->
	je .return		; same, no need to move -->

	push ax
	push dx
.uploop:
	mov ds, ax
	mov es, dx
	xor di, di
	xor si, si		; -> start of segment
	sub cx, 1000h		; 64 KiB left ?
	jbe .uplast		; no -->
	push cx
	mov cx, 10000h /2
	rep movsw		; move 64 KiB
	pop cx
	add ax, 1000h
	add dx, 1000h		; -> next segment
	jmp short .uploop	; proceed for more -->
.uplast:
	add cx, 1000h		; restore counter
	shl cx, 1
	shl cx, 1
	shl cx, 1		; *8, paragraphs to words
	rep movsw		; move last part
	pop dx
	pop ax
	jmp short .return

.down:
	std			; _AMD_ERRATUM_109_WORKAROUND as below
.dnloop:
	sub cx, 1000h		; 64 KiB left ?
	jbe .dnlast		; no -->
	push ax
	push dx
	add ax, cx
	add dx, cx
	mov ds, ax		; -> 64 KiB not yet moved
	mov es, dx
	pop dx
	pop ax
	mov di, -2
	mov si, di		; moved from last word down
	push cx
	mov cx, 10000h /2
	rep movsw		; move 64 KiB
	pop cx
	jmp short .dnloop	; proceed for more -->
.dnlast:
	add cx, 1000h		; restore counter
	shl cx, 1
	shl cx, 1
	shl cx, 1		; *8, paragraphs to words
	mov di, cx
	dec di
	shl di, 1		; words to offset, -> last word
	mov si, di
	mov ds, ax
	mov es, dx		; first segment correct


	numdef AMD_ERRATUM_109_WORKAROUND, 1
		; Refer to comment in init.asm init_movp.

%if _AMD_ERRATUM_109_WORKAROUND
	jcxz @FF
	cmp cx, 20
	ja @FF
@@:
	movsw
	loop @B
@@:
%endif
	rep movsw		; move first part
	cld
.return:
	pop di
	pop es
	pop si
	pop ds
	pop cx
	retn
qq.bootfreecode_size: equ $-qq.bootfreecode
%endif


		; S command - search for a string of bytes.
sss:
	mov bx, word [reg_ds]	; get search range
	_386_PM_o32		; xor ecx, ecx
	xor cx, cx
	_386_PM_o32
	mov word [sscounter], cx; mov dword [sscounter], ecx
	call getrangeX		; get address range into BX:(E)DX..BX:(E)CX
	call skipcomm0
	_386_PM_o32		; push edx
	push dx
	_386_PM_o32		; push ecx
	push cx
	push bx
	push ax
	push si
	push word [throwret]
	push word [throwsp]
	mov word [throwret], .catch
	mov word [throwsp], sp
	mov bx, word [reg_ds]	; get search range
	xor cx, cx
	call getrangeX		; try to get second range
	call chkeol		; and insure end-of-line
				; successful if it returned
	pop word [throwsp]
	pop word [throwret]	; restore throw destination
	add sp, byte 4		; discard ax/si
	_386_PM_o32		; mov esi, edx
	mov si, dx		; bx:esi-> source string
	_386_PM_o32		; sub ecx, edx
	sub cx, dx		; ecx = count - 1
	jmp short .setesedi

.catch:				; second getrangeX failed
	pop word [throwsp]
	pop word [throwret]	; restore throw destination
	pop si
	pop ax			; restore line
	call getstr		; get string of bytes
	sub di, line_out	; di = number of bytes to look for
	mov cx, di
	dec di			;     minus one
	mov si, line_out
	push di
	mov es, word [auxbuff_segorsel]
	xor di, di
	rep movsb		; move to auxbuff
	_386_PM_o32	; xor esi, esi
	xor si, si
	mov bx, es		; bx:esi -> auxbuff
	pop cx
_386_PM	movzx ecx, cx		; ecx = count - 1
.setesedi:
	call prephack		; set up for the interrupt vector hack
	call dohack
	mov ds, bx
	pop es
	_386_PM_jmpn .386init	; 386 -->
.init:
	pop bx
	pop dx
.init_popped:
	sub bx, dx		; bx = number of bytes in search range minus one
	sub bx, cx		; = number of possible positions of string minus 1
	jb error
	mov di, dx
	mov dx, cx
	mov cx, bx

		; ds:si-> search string, length (dx+1)
		; es:di-> data to search in, (cx+1) bytes
.loop:
	or al, 1		; NZ (iff cx==0, repne scasb doesn't change ZF)
	push si
	lodsb			; first character in al
	repne scasb		; look for first byte
	je .foundbyte
	scasb			; count in cx was cnt-1
	jne .done
.foundbyte:
	push cx
	push di
	mov cx, dx
	repe cmpsb		; compare string behind first byte
	pop di
	je .display		; if equal
.next:
	pop cx
	pop si
	inc cx			; increase first because loop decreases it
	loop .loop		; go back for more
	db __TEST_IMM8		; (skip pop)
.done:
	pop si			; discard
.commondone:
	push ss
	pop ds
	call unhack
	mov di, line_out
%if _PM
	mov ax, word [sscounter+2]
	test ax, ax
	jnz .nohighcounter
	call hexword
.nohighcounter:
%endif
	mov ax, word [sscounter]
	call hexword
	call putsline
	mov dx, msg.matches
	jmp putsz

.display:
	mov bx, es
	push di
	push ds
	push es
	 push ss
	 pop ds
	call unhack		; undo the interrupt vector hack and restore es
	inc word [sscounter]
	push di
	mov ax, bx
	mov di, line_out
	call hexword		; 4 (segment)
	mov al, ':'
	stosb			; +1=5
	pop ax
	dec ax
	call hexword
%if _SDUMP
	stc
	adc ax, dx		; -> behind result
	jbe .noresult		; end of segment
	mov si, ax
	mov ax, 32<<8|32
	stosw
	lea bx, [di+3*16]
	mov cx, si
	neg cx
	cmp cx, byte 16
	jbe .cxdone
	mov cx, 16
.cxdone:
	 pop ds
	 push ds		; restore search's segment
	push cx
.disploop:
	lodsb
	call dd_store
	loop .disploop
	pop cx
	 push ss
	 pop ds
	neg cx
	add cx, byte 16
	jz .noblanks
.loopblanks:
	mov ax, 32<<8|32
	stosw
	stosb
	loop .loopblanks
.noblanks:
	mov byte [di-(1+(8*3))], '-'
	mov di, bx
.noresult:
%endif	; _SDUMP
	push dx
	call putsline_crlf
	call dohack
	pop dx
	pop es
	pop ds
	pop di
	jmp .next

%if _PM
.386init:
cpu 386
	pop ebx
	pop edx
	call ispm
	jnz .init_popped	; not PM -->
	sub ebx, edx		; ebx = number of bytes in search range minus one
	sub ebx, ecx		; = number of possible positions of string minus 1
	jb error
	mov edi, edx
	mov edx, ecx
	mov ecx, ebx

		; ds:esi-> search string, length (edx+1)
		; es:edi-> data to search in, (ecx+1) bytes
		; Although 386+ RM still uses 64 KiB segments, it allows
		; us to use the 32-bit addressing variant of the string
		; instructions as long as we never access any byte above
		; the 64 KiB limit. (Even if the index register contains
		; 00010000h after an instruction executed.)
.386loop:
	or al, 1		; NZ (iff cx==0, repne scasb doesn't change ZF)
	push esi
	a32 lodsb		; first character in al
	a32 repne scasb		; look for first byte
	je .386foundbyte
	a32 scasb		; count in ecx was cnt-1
	jne .386done
.386foundbyte:
	push ecx
	push edi
	mov ecx, edx
	a32 repe cmpsb		; compare string behind first byte
	pop edi
	je .386display		; if equal
.386next:
	pop ecx
	pop esi
	inc ecx			; increase first because loop decreases it
	loop .386loop, ecx	; go back for more
	db __TEST_IMM16		; (skip pop)
.386done:
	pop esi			; discard
	jmp .commondone

.386display:
	mov bx, es
	push edi
	push ds
	push es
	call unhack		; undo the interrupt vector hack and restore es
	inc dword [sscounter]
	push edi
	mov ax, bx
	mov di, line_out
	call hexword		; 4 (segment)
	mov al, ':'
	stosb			; +1=5
	pop eax
	dec eax
	call testattrhigh
	jz .noa32
	call hexword_high
.noa32:
	call hexword
%if _SDUMP
	stc
	adc eax, edx		; -> behind result
	jbe .386noresult	; end of segment
	mov esi, eax
	mov ax, 32<<8|32
	stosw
	lea bx, [di+3*16]
	mov ecx, esi
	neg ecx
	cmp ecx, byte 16
	jbe .386cxdone
	mov cx, 16
.386cxdone:
	 pop ds
	 push ds		; restore search's segment
	push cx
.386disploop:
	a32 lodsb
	call dd_store
	loop .386disploop
	pop cx
	 push ss
	 pop ds
	neg cx
	add cx, byte 16
	jz .386noblanks
.386loopblanks:
	mov ax, 32<<8|32
	stosw
	stosb
	loop .386loopblanks
.386noblanks:
	mov byte [di-(1+(8*3))], '-'
	mov di, bx
.386noresult:
%endif	; _SDUMP
	push dx
	call putsline_crlf
	call dohack
	pop dx
	pop es
	pop ds
	pop edi
	jmp .386next
cpu 8086
%endif	; _PM


%if 0
getdebuggeebyte:
	push bp
	mov bp, sp
	sub sp, byte 4
	push bx
	push cx
%define _dedata -4
%define _bp 0
%define _ip 2
%define _adroffset 4
%define _adrsegment 8
	test byte [], memorydump
	jz .realmemory

	jmp short .return
.realmemory32:
.realmemory:
	mov ax, word [ bp + _adrsegment ]
	mov bx, word [ bp + _adroffset ]
	push ds
	mov ds, ax
	push word [ bx ]
	pop word [ bp + _dedata ]
	push word [ bx +2 ]
	pop word [ bp + _dedata +2 ]
	pop ds
;	test ax, ax
;	jnz .return
	mov dx, ax
	mov cl, 4
	shl ax, cl
	mov cl, 12
	shr dx, cl
	add ax, bx
	adc dx, byte 0
	jnz .return
	sub ax, 23h*4
	jb .return
	cmp ax, 2*4
	jae .return

	push ds
	xor bx, bx
	mov ds, bx
	push si
	push di
	mov si, 22h*4
	mov di, hackints.dummy22
	movsw
	movsw
	mov bl, 8
	add si, bx
	add di, bx
	movsw
	movsw

	mov cl, byte [ bx - 4 + hackints2324 ]
	mov byte [ bp + _dedata ], cl
.return:
	pop cx
	pop bx
	pop ax
	pop dx
	pop bp
	retn 6


		; Interrupt hack table
		;
		; This contains the Int23 and Int24 handler we want to show
		; the user. As we'll retrieve a dword per access,
hackints:
.dummy22:	dd 0
.23:		dd 0
.24:		dd 0
.dummy25:	dd 0
%endif


lockdrive:
	push ax
	push bx
	push cx
	push dx
	mov bl, al
	inc bl
	mov bh, 0
	mov cx, 084Ah
	mov dx, 0001h
	mov ax, 440Dh
	int 21h
	pop dx
	pop cx
	pop bx
	pop ax
	retn

unlockdrive:
	push ax
	push bx
	push cx
	push dx
	mov bl, al
	inc bl
	mov bh, 0
	mov cx, 086Ah
	mov dx, 0001h
	mov ax, 440Dh
	int 21h
	pop dx
	pop cx
	pop bx
	pop ax
	retn


		; W command - write a program, or disk sectors, to disk.
ww:
%if _BOOTLDR
	testopt [internalflags], nodosloaded
	jz @F
	mov dx, msg.nobootsupp
	jmp putsz
@@:
%endif

	call parselw		; parse L and W argument format
	jz ww4			; if request to write program
%if _PM && _NOEXTENDER
	call ispm
	jnz .rm
	call isextenderavailable
	jc nodosextinst
.rm:
%endif
	testopt [ss:internalflags], newpacket| ntpacket
	jz .oldint
	mov dl, al		; A: = 0, ...
	mov si, 6001h		; write, assume "file data"
%if _VDD
	testopt [internalflags], ntpacket
	jnz .vdd
%endif
	inc dl			; A: = 1, ...
	call lockdrive
	mov ax, 7305h		; ds:(e)bx-> packet
	stc
	int 21h			; use int 21h here, not doscall
	pushf
	call unlockdrive
	popf
	jmp short .done
%if _VDD
.vdd:
	mov ax, word [hVdd]
	mov cx, 5
%if _PM
	add cl, byte [dpmi32]
%endif
	DispatchCall
	jmp short .done
%endif
.oldint:
	int 26h
.done:
	mov dx, writing
ww1:
	mov bx, ss		; restore segment registers
	mov ds, bx
	mov sp, word [savesp]
	mov es, bx
	jnc ww3			; if no error
	cmp al, 0Ch
	jbe ww2			; if in range
	mov al, 0Ch
ww2:
	cbw			; ah = 0
	mov bx, dskerrs		; -> byte table
	xlatb			; get offset from dskerrs
	add ax, bx		; -> message
	mov di, line_out
	mov si, ax
	call showstring
	mov si, dx
	call showstring
	mov si, drive
	call showstring
	call putsline_crlf
ww3:
	jmp cmd3		; can't return because Int26 leaves stack wrong

;	Write to file.  First check the file extension.
;   size of file is in client's BX:CX,
;   default start address is DS:100h

ww4:
	mov al, byte [fileext]	; get flags of file extension
	test al, EXT_EXE + EXT_HEX
	jz ww5			; if not EXE or HEX
	mov dx, nowhexe
	jmp short ww6

ww5:
	cmp al, 0
	jnz ww7			; if extension exists
	mov dx, nownull
ww6:
	jmp ww16

		; File extension is OK; write it.  First, create the file.
ww7:
%if _PM
	call ispm
	jnz ww7_1
	mov dx, nopmsupp
	jmp putsz
ww7_1:
%endif
	mov bp, line_out
	cmp dh, 0FEh
	jb ww8			; if (dx < 0xFE00)
	sub dh, 0FEh		;  dx -= 0xFE00;
	add bx, 0FE0h
ww8:
	mov word [bp+10], dx	; save lower part of address in line_out+10
	mov si, bx		; upper part goes into si
	mov ah, 3Ch		; create file
	xor cx, cx		; no attributes
	mov dx, DTA
	doscall
	jc ww15			; if error
	push ax			; save file handle

		; Print message about writing.
	mov dx, wwmsg1
	call putsz		; print string
	mov ax, word [reg_ebx]
	cmp ax, 10h
	jb ww9			; if not too large
	xor ax, ax		; too large:  zero it out
ww9:
	mov word [bp+8], ax
	test ax, ax
	jz ww10
	call hexnyb
ww10:
	mov ax, word [reg_ecx]
	mov word [bp+6], ax
	call hexword
	call putsline		; print size
	mov dx, wwmsg2
	call putsz		; print string

		; Now write the file.  Size remaining is in line_out+6.
	pop bx			; recover file handle
	mov dx, word [bp+10]	; address to write from is si:dx
ww11:
	mov ax, 0FE00h
	sub ax, dx
	cmp byte [bp+8], 0
	jnz ww12		; if more than 0FE00h bytes remaining
	cmp ax, word [bp+6]
	jb ww12			; ditto
	mov ax, word [bp+6]
ww12:
	xchg ax, cx		; mov cx, ax
	mov ds, si
	mov ah, 40h		; write to file
	int 21h			; use INT, not doscall
	push ss			; restore DS
	pop ds
	cmp ax, cx
	jne ww13		; if disk full
	xor dx, dx		; next time write from xxxx:0
	add si, 0FE0h		; update segment pointer
	sub word [bp+6], cx
	lahf
	sbb byte [bp+8], 0
	jnz ww11		; if more to go
	sahf
	jnz ww11		; ditto
	jmp short ww14		; done

ww13:
	mov dx, diskful
	call putsz		; print string
	call ww14		; close file

	mov ah, 41h		; unlink file
	mov dx, DTA
	doscall
	retn

		; Close the file.
ww14:
	mov ah, 3Eh		; close file
	int 21h
	retn

		; Error opening file.  This is also called by the load command.
ww15:
	cmp ax, byte 2
	mov dx, doserr2		; File not found
	je ww16
	cmp ax, byte 3
	mov dx, doserr3		; Path not found
	je ww16
	cmp ax, byte 5
	mov dx, doserr5		; Access denied
	je ww16
	cmp ax, byte 8
	mov dx, doserr8		; Insufficient memory
	je ww16
	mov di, openerr1
	call hexword
	mov dx, openerr		; Error ____ opening file
ww16:
	jmp putsz


%ifn _EMS
xx: equ error
%else
		; X commands - manipulate EMS memory.
		;
		; Reference:
		;  http://www.nondot.org/sabre/os/files/MemManagement/LIMEMS41.txt

xx:	cmp al, '?'
	je xhelp		; if a call for help
	or al, TOLOWER
	cmp al, 'a'
	je xa			; if XA command
	cmp al, 'd'
	je xd			; if XD command
	cmp al, 'm'
	je xm			; if XM command
	cmp al, 'r'
	je xr			; if XR command
	cmp al, 's'
	je xs			; if XS command
	jmp error

xhelp:	lodsb
	call chkeol
	mov dx, msg.xhelp
	jmp putsz		; print string and return

		; XA - Allocate EMS.
xa:	call emschk
	call skipcomma
	call getword		; get argument into DX
	call chkeol		; expect end of line here
	mov bx, dx

	mov ax, 5A00h		; use the EMS 4.0 version to alloc 0 pages
	test bx, bx
	jz short .nullcnt
	mov ah, 43h		; allocate handle
.nullcnt:
	call emscall
	xchg ax, dx		; mov ax, dx
	mov di, xaans1
	call hexword
	mov dx, xaans
	jmp putsz		; print string and return

		; XD - Deallocate EMS handle.
xd:	call emschk
	call skipcomma
	call getword		; get argument into DX
	call chkeol		; expect end of line here

	mov ah, 45h		; deallocate handle
	call emscall
	xchg ax, dx		; mov ax,dx
	mov di, xdans1
	call hexword
	mov dx, xdans
	jmp putsz		; print string and return

		; XR - Reallocate EMS handle.
xr:	call emschk
	call skipcomma
	call getword		; get handle argument into DX
	mov bx, dx
	call skipcomma
	call getword		; get count argument into DX
	call chkeol		; expect end of line here
	xchg bx, dx

	mov ah, 51h		; reallocate handle
	call emscall
	mov dx, xrans
	jmp putsz		; print string and return

		; XM - Map EMS memory to physical page.
xm:	call emschk
	call skipcomma
	call getword		; get logical page
	mov bx, dx		; save it in BX
	call skipcomm0
	call getbyte		; get physical page (DL)
	push dx
	call skipcomm0
	call getword		; get handle into DX
	call chkeol		; expect end of line
	 pop ax			; recover physical page into AL
	 push ax
	mov ah, 44h		; function 5 - map memory
	call emscall
	mov di, xmans1
	xchg ax, bx		; mov al,bl
	call hexbyte
	add di, xmans2-xmans1-2
	pop ax
	call hexbyte
	mov dx, xmans
	jmp putsz		; print string and return

		; XS - Print EMS status.
xs:
	call emschk
	lodsb
	call chkeol		; no arguments allowed

		; First print out the handles and handle sizes.  This can be done either
		; by trying all possible handles or getting a handle table.
		; The latter is preferable, if it fits in memory.
	mov ah, 4Bh		; function 12 - get handle count
	call emscall
	cmp bx, (line_out_end-line_out)/4
	jbe short xs3			; if we can do it by getting the table
	xor dx, dx		; handle

xs1:
	mov ah, 4Ch		; function 13 - get handle pages
	call emscall.witherrors
	jnz short .err
	xchg ax, bx		; mov ax,bx
	call hndlshow
.cont:
	inc dl			; end of loop
	jnz short xs1		; if more to be done
	jmp short xs5		; done with this part

.err:
	cmp ah, 83h		; no such handle?
	je short .cont		; just skip -->
	jmp short emscall.errorhandle	; if other error -->

		; Get the information in tabular form.
xs3:
	mov ah, 4Dh		; function 14 - get all handle pages
	mov di, line_out
	call emscall
	test bx, bx
	jz short xs5
	mov si, di
xs4:
	lodsw
	xchg ax, dx
	lodsw
	call hndlshow
	dec bx
	jnz short xs4		; if more to go

xs5:
	mov dx, crlf
	call putsz		; print string

		; Next print the mappable physical address array.
		; The size of the array shouldn't be a problem.
	mov ax, 5800h		; function 25 - get mappable phys. address array
	mov di, line_out	; address to put array
	call emscall
	mov dx, xsnopgs
	jcxz xs7		; NO mappable pages!

	mov si, di
xs6:
	push cx
	lodsw
	mov di, xsstr2b
	call hexword
	lodsw
	mov di, xsstr2a
	call hexbyte
	mov dx, xsstr2
	call putsz		; print string
	pop cx			; end of loop
	test cl, 1
	jz short xs_nonl
	mov dx, crlf		; blank line
	call putsz		; print string
xs_nonl:
	loop xs6
	mov dx, crlf		; blank line
xs7:
	call putsz		; print string

		; Finally, print the cumulative totals.
	mov ah, 42h		; function 3 - get unallocated page count
	call emscall
	mov ax, dx		; total pages available
	sub ax, bx		; number of pages allocated
	mov bx, xsstrpg
	call sumshow		; print the line
	mov ah, 4Bh		; function 12 - get handle count
	call emscall
	xchg ax, bx		; ax = number of handles allocated
	mov dx, 0FFh		; total number of handles
	mov bx, xsstrhd
	jmp sumshow		; print the line

		; Call EMS
emscall:
	call .witherrors
	jz short .ret		; return if OK
.errorhandle:
	mov al, ah
	cmp al, 8Bh
	jg short .ce2		; if out of range (signed comparison intended)
	cbw
	mov bx, ax
	shl bx, 1
	mov dx, word [emserrs+100h+bx]
	test dx, dx
	jnz short .ce4		; if there's a word there
.ce2:
	mov dx, emserrx
	call putsz
	mov di, line_out
	call hexbyte
	call putsline_crlf
	jmp cmd3		; quit

.witherrors:
%if _PM
	call ispm
	jnz short .rm
[cpu 286]
	push word [ss:pspdbg]
	push 67h
	call intcall
	db __TEST_IMM16		; (skip int opcode)
__CPU__
.rm:
%endif
	int 67h
	test ah, ah
.ret:
emschk.ret:
	retn

		; Check for EMS

emschk:
%if _PM
	call ispm
	jnz short .rm
	mov bl, 67h
	mov ax, 0200h
	int 31h
	mov ax, cx
	mov bx, dx
	jmp short .check
.rm:
%endif
	push es
%if _BOOTLDR
	testopt [internalflags], nodosloaded
	jz short .int21_35
	xor ax, ax
	mov es, ax
		; maybe should disable this?
	les bx, [es:67h*4]
	jmp short .gotint
.int21_35:
%endif
	mov ax, 3567h		; get interrupt vector 67h
	int 21h
.gotint:
	mov ax, es
	pop es
.check:
	inc bx
	jz short .failed	; was 0FFFFh -->
	test ax, ax
	jz short .failed	; was 0000h -->
	mov ah, 46h
	call emscall.witherrors	; get version
	jz short .ret		; success -->
.failed:
	mov dx, emsnot
emscall.ce4:
	jmp prnquit		; otherwise abort with message -->

		; HNDLSHOW - Print XS line giving the handle and pages allocated.
		;
		; Entry	DX	Handle
		;	AX	Number of pages
		;
		; Exit	Line printed
		;
		; Uses	ax,cl,di.
hndlshow:
	mov di, xsstr1b
	call hexword
	mov ax, dx
	mov di, xsstr1a
	call hexword
	push dx
	mov dx, xsstr1
	call putsz		; print string
	pop dx
	retn

		; SUMSHOW - Print summary line for XS command.
		;
		; Entry	AX	Number of xxxx's that have been used
		;	DX	Total number of xxxx's
		;	BX	Name of xxxx
		;
		; Exit	String printed
		;
		; Uses	AX, CX, DX, DI
sumshow:
	mov di, xsstr3
	push di
	call trimhex
	xchg ax, dx		; mov ax,dx
	mov di, xsstr3a
	call trimhex
	pop dx			; mov dx,xsstr3
	call putsz		; print string
	mov dx, bx
	call putsz		; print string
	mov dx, xsstr4
	jmp putsz		; print string and return

		; TRIMHEX - Print word without leading zeroes.
		;
		; Entry	AX	Number to print
		;	DI	Where to print it
		;
		; Uses	AX, CX, DI.
trimhex:
	call hexword
	sub di, 4		; back up DI to start of word
	mov cx, 3
	mov al, '0'
.loop:
	scasb
	jne .done		; return if not a '0'
	mov byte [di-1], ' '
	loop .loop
.done:
	retn
%endif	; _EMS

		; Error handlers.
error:
	push ss
	pop es
	push ss
	pop ds
	mov cx, si
	sub cx, line_in+3
	add cx, word [promptlen]; number of spaces to skip
	cmp cx, 128
	jbe .valid
	xor cx, cx		; if we're really messed up
.valid:
	mov sp, [throwsp]
	jmp near [throwret]
		; INP:	cx = number of spaces to indent

		; This is the default address in throwret.
		; Display the error, then jump to errret.
errhandler:
.:
	sub cx, byte 80
	jnc .
	add cx, byte 80
	jz err2
	mov al, 32
.loop:
	call putc
	loop .loop
err2:
	mov dx, errcarat
	call putsz		; print string
	mov word [lastcmd], dmycmd
				; cancel command repetition
	jmp near [errret]	; return to the prompt (cmd3, aa01)


		; Terminate the attached process, if any
		;
		; OUT:	NZ if now no process attached
		;	ZR if still a process attached,
		;	 ie we failed to terminate this one
terminate_attached_process:
	testopt [internalflags], attachedterm
	jnz @F

	mov word [reg_cs], cs
	mov word [reg_eip], terminate
	 push ax		; (dummy to take space for return address)
	mov word [reg_ss], ss
	mov word [reg_esp], sp	; save current ss:sp
	 pop ax			; (discard)
%if _PM
	xor ax, ax
	mov word [reg_eip+2], ax
	mov word [reg_esp+2], ax
%endif
	call run
		; The dummy stack space above is to hold the return address
		; of this call. The debugger stack is used by this run.

	testopt [internalflags], attachedterm
@@:
	retn


;--- this is called by "run"
;--- better don't use INTs inside
;--- set debuggee's INT 23/24

		; Low-level functions to reset to debuggee's interrupt vectors 23h/24h
		; INP:	-
		; OUT:	-
		; CHG:	bx, (e)dx, cx, ax
		; STT:	ds = our segment
		;	Do not use Int21, even if not in InDOS mode
setint2324:
%if _BOOTLDR
	testopt [internalflags], nodosloaded
	jnz .ret		; don't touch int23/24 -->
%endif
%if _PM
	call ispm
	jz .pm
%endif
	push es
	push di
	push si

	xor di, di
	mov es, ax
	mov di, 23h *4
	mov si, run2324
	movsw
	movsw
	movsw
	movsw

%if _PM
	call hook2F
%endif
	pop si
	pop di
	pop es
.ret:
	retn
%if _PM
.pm:
	push si
	mov si, run2324
	mov bx, 0223h
.loop:
	_386_o32		; mov edx, dword [si+0]
	mov dx, word [si+0]
	mov cx, word [si+4]
	mov ax, 0205h
	int 31h
	add si, 6
	inc bl
	dec bh
	jnz .loop
	pop si
	retn
%endif

		; Low-level functions to save debuggee's interrupt vectors 23h/24h
		;  and set our interrupt vectors instead
		; INP:	-
		; OUT:	-
		; CHG:	-
		; STT:	ds = our segment
		;	Do not use Int21, even if not in InDOS mode
getint2324:
%if _BOOTLDR
	testopt [internalflags], nodosloaded
	jnz .ret		; don't touch int23/24 -->
%endif
%if _PM
	call ispm
	jz .pm
%endif
	push si
	push di
	push es

	push ds
	pop es
	xor di, di
	mov ds, di
	mov di, run2324
	mov si, 23h *4
	push si
	movsw			; save interrupt vector 23h
	movsw
	movsw			; save interrupt vector 24h
	movsw
	pop di
	push es
	pop ds
	xor si, si
	mov es, si
	mov si, CCIV
	movsw
	movsw
	movsw
	movsw

	pop es
	pop di
	pop si
.ret:
	retn
%if _PM
cpu 286
.pm:
	_386_o32
	pusha
	mov di, run2324
	mov bx, 0223h
.loop:
	mov ax, 0204h
	int 31h
	_386_o32		; mov dword [di+0], edx
	mov word [di+0], dx
	mov word [di+4], cx
	add di, byte 6
	inc bl
	dec bh
	jnz .loop
%if _ONLYNON386
	db __TEST_IMM8		; (skip pusha)
%else
	db __TEST_IMM16		; (skip pushad)
%endif

restoredbgi2324:
setdbgi2324:
	_386_o32
	pusha
	mov si, dbg2324
	mov bx, 0223h
_386	xor edx, edx
.loop:
	lodsw
	mov dx, ax
	mov cx, word [cssel]
	mov ax, 0205h
	int 31h
	inc bl
	dec bh
	jnz .loop
	_386_o32
	popa
	retn
cpu 8086
%endif

%if 0
The next three subroutines concern the handling of Int23 and 24.
These interrupt vectors are saved and restored when running the
child process, but are not active when DEBUG itself is running.
It is still useful for the programmer to be able to check where Int23
and 24 point, so these values are copied into the interrupt table
during parts of the C, D, (DX, DI,) E, M, and S commands, so that
they appear to be in effect. The E command also copies these values
back.

Between calls to dohack and unhack, there should be no calls to DOS,
so that there is no possibility of these vectors being used when
DEBUG itself is running.

; As long as no DOS is loaded anyway, Int23 and Int24 won't be touched
by us, so the whole hack is unnecessary and will be skipped.
%endif

		; PREPHACK - Set up for interrupt vector substitution.
		; Entry	es = cs
prephack:
	cmp byte [hakstat], 0
	jne .err		; if hack status error -->
	push di
	mov di, sav2324		; debugger's Int2324
	call prehak1
	pop di
	retn

.err:
	push dx
	mov dx, ph_msg
	call putsz		; display error
	pop dx
	retn

		; INP:	di-> saved interrupt vectors
		; OUT:	-
		; CHG:	-
prehak1:
%if _PM
	call ispm
	jz .pm			; nothing to do
%endif
	push ds
	push si
	xor si, si
	mov ds, si
	mov si, 23h *4
	movsw
	movsw
	movsw
	movsw
	pop si
	pop ds
.pm:
	retn


		; DOHACK - Fake the interrupt vectors 23h and 24h to debuggee's
		; UNHACK - Restore interrupt vectors 23h and 24h to our values
		;	It's OK to do either of these twice in a row.
		;	In particular, the S command may do unhack twice in a row.
		; INP:	ds = our segment
		; OUT:	es = our segment
		; CHG:	-
		; STT:	Do not use Int21
dohack:
	push ss
	pop es
%if _BOOTLDR
	testopt [internalflags], nodosloaded
	jnz unhack.ret		; nothing to hack -->
%endif
	push si
	mov byte [hakstat], 1
	mov si, run2324		; debuggee's interrupt vectors
%if _PM
	call ispm
	jnz unhack.common
cpu 286
	_386_o32
	pusha
	mov bx, 0223h
.pm_loop:
	_386_o32
	mov dx, word [si+0+0]
	mov cx, word [si+0+4]
	mov ax, 205h
	int 31h
	add si, byte 6
	inc bl
	dec bh
	jnz .pm_loop
	_386_o32
	popa
	pop si
	retn
cpu 8086
%else
	jmp short unhack.common
%endif

unhack:
	push ss
	pop es
%if _BOOTLDR
	testopt [internalflags], nodosloaded
	jnz .ret		; nothing to hack -->
%endif
	mov byte [hakstat], 0
%if _PM
	call ispm
	jz restoredbgi2324
%endif
	push si
	mov si, sav2324		; debugger's interrupt vectors
.common:
	push di
	push es
	xor di, di
	mov es, di
	mov di, 23h *4
	movsw
	movsw
	movsw
	movsw
	pop es
	pop di
	pop si
.ret:
	retn


		; OUT:	NZ if InDOS mode
		;	ZR if not
		; CHG:	-
		; STT:	ss = ds
InDos:
%if _BOOTLDR
	testopt [internalflags], nodosloaded
	jnz .return		; always "in DOS" -->
%endif
	testopt [options], fakeindos
	jnz .return		; faking InDOS on anyway -->
	push ds
	push si
	lds si, [pInDOS]
	cmp byte [si], 0
	pop si
	pop ds
.return:
	retn


;	PARSECM - Parse command line for C and M commands.
;	Entry	AL		First nonwhite character of parameters
;		SI		Address of the character after that
;	Exit	DS:ESI		Address from first parameter
;		ES:EDI		Address from second parameter
;		ECX		Length of address range minus one
;		[bAddr32]	Set if any high word non-zero

parsecm:
	call prephack
	mov bx, word [reg_ds]	; get source range
	xor cx, cx
	call getrange		; get address range into bx:(e)dx bx:(e)cx
	push bx			; save segment first address
	call skipcomm0
	mov bx, word [reg_ds]
	_386_PM_o32	; sub ecx, edx
	sub cx, dx		; number of bytes minus one
	_386_PM_o32	; push edx
	push dx
	_386_PM_o32	; push ecx
	push cx
%if _PM
	mov cl, byte [bAddr32]
	push cx
	call getaddr		; get destination address into bx:edx
	pop cx
	or byte [bAddr32], cl	; if either is 32-bit, handle both as 32-bit
%else
	call getaddr		; get destination address into bx:dx
%endif
	_386_PM_o32
	pop cx		; pop ecx
	_386_PM_o32	; mov edi, edx
	mov di, dx
	_386_PM_o32
	add dx, cx	; add edx, ecx
	jc short errorj7	; if it wrapped around
	call chkeol		; expect end of line
	mov es, bx
	_386_PM_o32	; pop esi
	pop si
	pop ds
	retn

errorj7:
	jmp error

;	PARSELW - Parse command line for L and W commands.
;
;	Entry	AL	First nonwhite character of parameters
;		SI	Address of the character after that
;
;	Exit	If there is at most one argument (program load/write), then the
;		zero flag is set, and registers are set as follows:
;		bx:(e)dx	Transfer address
;
;		If there are more arguments (absolute disk read/write), then the
;		zero flag is clear, and registers are set as follows:
;
;		DOS versions prior to 3.31:
;		AL	Drive number
;		CX	Number of sectors to read
;		DX	Beginning logical sector number
;		DS:BX	Transfer address
;
;		Later DOS versions:
;		AL	Drive number
;		BX	Offset of packet
;		CX	0FFFFh

	usesection lDEBUG_DATA_ENTRY
	align 4
packet:	dd 0		; sector number
	dw 0		; number of sectors to read
	dd 0		; transfer address Segm:OOOO
%if _PM
	dw 0		; transfer address might be Segm:OOOOOOOO!
%endif

	usesection lDEBUG_CODE
parselw:
	mov bx, word [reg_cs]	; default segment
	_386 xor edx, edx
	mov dx, 100h		; default offset
	call iseol?
	je plw2			; if no arguments
	call getaddr		; get buffer address into bx:(e)dx
	call skipcomm0
	call iseol?
	je plw2			; if only one argument
	push bx			; save segment
	push dx			; save offset
	mov bx, 80h		; max number of sectors to read
	neg dx
	jz plw1			; if address is zero
	mov cl, 9
	shr dx, cl		; max number of sectors which can be read
	mov di, dx
plw1:
	cmp byte [si], ':'	; drive letter specification ?
	jne @F			; no -->

	push ax
	call uppercase
	sub al, 'A'
	cmp al, 32		; valid drive ?
	mov dl, al		; put drive number
	inc si			; -> past the colon
	pop ax
	jb @FF			; got it -->
	dec si			; -> at colon

@@:
	call getbyte		; get drive number (DL)
	db __TEST_IMM8		; (skip lodsb)
@@:
	lodsb
	call skipcomm0
	push dx
	add dl, 'A'
	mov byte [driveno], dl
	call getdword		; get relative sector number
	call skipcomm0
	push bx			; save sector number high
	push dx			; save sector number low
	push si			; in case we find an error
	call getword		; get sector count
	dec dx
	cmp dx, di
	jae errorj7		; if too many sectors
	inc dx
	mov cx, dx
	call chkeol		; expect end of line
	testopt [internalflags], oldpacket| newpacket| ntpacket
	jnz plw3		; if using a packet -->
	pop si			; in case of error
	pop dx			; get LoWord starting logical sector number
	pop bx			; get HiWord
	test bx, bx		; just a 16-bit sector number possible
	jnz errorj7		; if too big
	pop ax			; drive number
	pop bx			; transfer buffer ofs
	pop ds			; transfer buffer seg
	test cx, cx		; NZ
plw2:
	retn

		; disk I/O packet for Int25/Int26, Int21.7305, VDD
plw3:
	pop bx			; discard si
	mov bx, packet
	pop word [bx+0]		; LoWord sector number
	pop word [bx+2]		; HiWord sector number
	mov word [bx+4], cx	; number of sectors
	pop ax			; drive number
	pop word [bx+6]		; transfer address ofs
	pop dx
	xor cx, cx
%if _PM
	call ispm
	jnz plw3_1
	cmp byte [dpmi32], 0
	jz plw3_1
cpu 386
	mov word [bx+10], dx	; save segment of transfer buffer
	movzx ebx, bx
	shr edx, 16		; get HiWord(offset)
	cmp byte [bAddr32], 1
	jz plw3_1
	xor dx, dx
cpu 8086
plw3_1:
%endif
	mov word [bx+8], dx	; transfer address seg
	dec cx			; NZ and make cx = -1
	retn


%include "expr.asm"


%include "lineio.asm"


	usesection lDEBUG_CODE

%if _BOOTLDR
		; Determine the amount of actual memory
		;
		; This is important to call at the time we need the size,
		; not just save the size initially. Loading other pre-boot
		; installers or RPLs will change the size.
		;
		; INP:	-
		; OUT:	dx = segment behind usable memory (taking EBDAs & RPLs into account)
		;	ds = ss
		; CHG:	ax, cx, di, si, ds
bootgetmemorysize:
	push es
	xor ax, ax
	mov ds, ax
	int 12h					; get memory size in KiB
	mov cl, 6
	shl ax, cl				; *64, convert to paragraphs
	push ax
	lds si, [ 2Fh *4 ]			; get current Int2F
	inc si					; pointer valid (not 0FFFFh) ? (left increased!)
	jz .norpl				; no -->
	mov ax, ds
	test ax, ax				; segment valid (not zero) ?
	jz .norpl				; no -->
	times 2 inc si				; +3 with above inc
	push cs
	pop es
	mov di, .rpl
	mov cx, .rpl_size
	repe cmpsb				; "RPL" signature ?
	jne .norpl				; no -->
	pop dx
	mov ax, 4A06h
	int 2Fh					; adjust usable memory size for RPL
	db __TEST_IMM8				; (skip pop)
.norpl:
	pop dx
		; dx = segment behind last available memory
	 push ss
	 pop ds
	pop es
	retn

.rpl:	db "RPL"
	endarea .rpl
%endif


;--- ensure a debuggee is loaded
;--- set SI:DI to CS:IP, preserve AX, BX, DX

ensuredebuggeeloaded:
	push ax
	testopt [internalflags], attachedterm
	jnz @F			; not loaded, create -->
	pop ax
	retn			; done

@@:
	push bx
	push dx

	call set_efl_to_fl	; initialize EFL, and ax = 0
	mov di, regs
	mov cx, 15*2		; (8 standard + 6 seg + eip) * 2
	rep stosw		; initialize all regs
%if _BOOTLDR
	testopt [internalflags], nodosloaded
	jz .dos
	mov ax, 60h
	push ax
	mov di, reg_ds
	stosw
	scasw			; (skip dummy high word)
	stosw
	scasw
	stosw
	scasw
	stosw
	call adusetup
	call bootgetmemorysize
	sub dx, 60h
	cmp dx, 1000h
	jbe .bootbelow64kib	; if memory left <= 64 KiB
	xor dx, dx		; dx = 1000h (same thing, after shifting)
.bootbelow64kib:
	mov cl, 4
	shl dx, cl
	dec dx
	dec dx
	mov word [reg_esp], dx
	pop es
	xchg dx, di		; es:di = child stack pointer
	xor ax, ax
	stosw			; push 0 on client's stack

	cmp byte [bInit], 0
	jnz .bootnomemtouch
	inc byte [bInit]
	mov word [es:0], 019CDh	; place opcode for int 19h at cs:ip
.bootnomemtouch:
	mov word [pspdbe], es
	jmp .return

.dos:
%endif
	mov byte [reg_eip+1], 100h>>8
	mov ah, 48h		; get size of largest free block
	mov bx, -1
	int 21h
	cmp bx, 11h		; enough for PSP + one paragraph for code/stack ?
	jb .return		; no -->
	mov ah, 48h		; allocate it
	int 21h
	jc .return		; (memory taken between the calls)

	push bx
	mov di, reg_ds		; fill segment registers ds,es,ss,cs
	stosw
	scasw			; (skip dummy high word)
	stosw
	scasw
	stosw
	scasw
	stosw
	call adusetup
	mov bx, word [reg_cs]	; bx:dx = where to load program
	mov es, bx
	pop ax			; get size of memory block
	mov dx, ax
	add dx, bx
	mov word [es:ALASAP], dx
	cmp ax, 1000h
	jbe .below64kib		; if memory left <= 64 KiB
	xor ax, ax		; ax = 1000h (same thing, after shifting)
.below64kib:
	mov cl, 4
	shl ax, cl
	dec ax
	dec ax
	mov word [reg_esp], ax
	xchg ax, di		; es:di = child stack pointer
	xor ax, ax
	stosw			; push 0 on client's stack

		; Create a PSP
	mov ah, 55h		; create child PSP
	mov dx, es
	mov si, word [es:ALASAP]
	clc			; works around OS/2 bug
	int 21h
	call setpspdbg		; reset PSP to ours

		; Finish up. Set termination address.
	mov ax, 2522h		; set interrupt vector 22h
	mov dx, int22
	int 21h
	mov word [es:TPIV], dx
	mov word [es:TPIV+2], ds

	cmp byte [bInit], 0
	jnz .nomemtouch
	inc byte [bInit]
	mov byte [es:100h], 0C3h	; place opcode for retn at cs:ip
.nomemtouch:

	mov word [pspdbe], es
	mov ax, es
	dec ax
	mov es, ax
	inc ax
	mov word [es:8+0], "DE"
	mov word [es:8+2], "BU"
	mov word [es:8+4], "GG"
	mov word [es:8+6], "EE"	; set MCB name
	mov word [es:1], ax	; set MCB owner
.return:
	clropt [internalflags], attachedterm
	push ss
	pop es

	mov si, word [reg_cs]
	mov di, word [reg_eip]
	pop dx
	pop bx
	pop ax
	retn


set_efl_to_fl:
	xor ax, ax		; initialize ax = 0 and FL = ZR NC etc
_no386	push ax			; dummy high word
	_386_o32	; pushfd
	pushf
	pop word [reg_efl]	; set to FL
	pop word [reg_efl+2]	; set to high word of EFL, or zero
	retn


%if _PM
		; Hook Int2F if a DPMI host is found. However for Win9x and DosEmu
		; Int2F.1687 is not hooked because it doesn't work. Debugging in
		; protected mode may still work, but the initial switch must be
		; single-stepped.
		;
		; CHG:	ax, bx, cx, dx, di
		; STT:	V86/RM
hook2F:
	testopt [internalflags], hooked2F|(!_BOOTLDR-1&nodosloaded)
	jnz .return		; don't hook now -->
@@:
	mov ax, 1687h		; DPMI host installed?
	int 2Fh
	test ax, ax
	jnz .return
	mov word [dpmientry+0], di	; true host DPMI entry
	mov word [dpmientry+2], es
	mov word [dpmiwatch+0], di
	mov word [dpmiwatch+2], es
	testopt [internalflags], nohook2F
	jnz .return		; can't hook Int2F -->
	mov word [dpmiwatch+0], mydpmientry
	mov word [dpmiwatch+2], ds	; => lDEBUG_DATA_ENTRY
	mov ax, 352Fh
	int 21h
	mov word [oldi2F+0], bx
	mov word [oldi2F+2], es
	mov dx, debug2F		; ds => lDEBUG_DATA_ENTRY
	mov ax, 252Fh
	int 21h

		; Test whether we can hook the DPMI entrypoint call.
	mov ax, 1687h
	int 2Fh
	test ax, ax
	jnz .nohost
	cmp di, mydpmientry	; our entrypoint returned ?
	jne .nohook
	mov ax, es
	mov bx, ds		; bx => lDEBUG_DATA_ENTRY
	cmp ax, bx
	jne .nohook		; no -->

	setopt [internalflags], hooked2F
%if _DISPHOOK
	mov ax, ds		; ax => lDEBUG_DATA_ENTRY
	push ds
	pop es
	mov di, dpmihookcs
	call hexword
	mov dx, dpmihook
	call putsz
%endif
.return:
	push ds
	pop es
	retn

.nohost:
.nohook:
	lds dx, [oldi2F]
	mov ax, 252Fh
	int 21h			; unhook
	push ss
	pop ds
	push ss
	pop es			; restore segregs
	setopt [internalflags], nohook2F
				; note that we cannot hook
	mov dx, msg.dpmi_no_hook
	call putsz		; display message about it
	jmp @B
%endif


	usesection lDEBUG_DATA_ENTRY
	align 16
ldebug_data_entry_size	equ $-section.lDEBUG_DATA_ENTRY.vstart
	endarea ldebug_data_entry, 1

	usesection ASMTABLE1
	align 16
asmtable1_size		equ $-section.ASMTABLE1.vstart
	endarea asmtable1, 1

	usesection ASMTABLE2
	align 16
asmtable2_size		equ $-section.ASMTABLE2.vstart
	endarea asmtable2, 1


	numdef SHOWASMTABLESIZE, 0
%if _SHOWASMTABLESIZE
%assign ASMTABLESIZE asmtable1_size + asmtable2_size
%warning asmtables hold ASMTABLESIZE bytes
%endif


	usesection DATASTACK
%define SECTIONFIXUP -$$+100h+ldebug_data_entry_size \
			+asmtable1_size+asmtable2_size

		; I/O buffers
	alignb 2
line_in:	resb 1			; maximal length of input line
		resb 1			; actual length (must be one less than previous byte)
		resb 255		; buffer for 13-terminated input line
.end:
				; b_bplist and g_bplist are expected in that order by initcont
%if _BREAKPOINTS
	alignb 2
b_bplist:
.used_mask:	resb (_NUM_B_BP+7)>>3	; bitmask of used points
.disabled_mask:	resb (_NUM_B_BP+7)>>3	; bitmask of disabled points
.sticky_mask:	resb (_NUM_B_BP+7)>>3	; bitmask of sticky points
					; desc: stay around during DEBUG's operation unless
					; explicitly removed/un-stickified. This allows
					; to keep breakpoints around while changing from PM.
					; Hits while in DEBUG are ignored though, use DDEBUG.
					; Disabling won't remove them, just ignores hits.
	alignb 2
.bp:		resb _NUM_B_BP*BPSIZE
	alignb 2
.counter:	resw _NUM_B_BP
%endif
%if _NUM_G_BP
g_bplist:
.used_count:	resb 1			; for the byte counter of saved breakpoints
.bp:		resb _NUM_G_BP*BPSIZE
.end:
%endif

		; $ - $$	= offset into section
		; % 2		= 1 if odd offset, 0 if even
		; 2 -		= 1 if odd, 2 if even
		; % 2		= 1 if odd, 0 if even
	; resb (2 - (($-$$) % 2)) % 2
		; $ - $$	= offset into section
		; % 2		= 1 if odd offset, 0 if even
		; 1 -		= 0 if odd, 1 if even
	resb 1 - (($-$$) % 2)		; make line_out aligned
trim_overflow:	resb 1			; actually part of line_out to avoid overflow of trimputs loop
line_out:	resb 263
		resb 1			; reserved for terminating zero
line_out_end:

	alignb 2
rxhead:		resw 1
rxtail:		resw 1
txhead:		resw 1
txtail:		resw 1
baseport:	resw 1
	alignb 16
rxfifo:		resb _RXFIFOSIZE
	alignb 16
txfifo:		resb _TXFIFOSIZE

		alignb 16		; stack might be re-used as GDT, so align it on a paragraph
stack:		resb _STACKSIZE
		alignb 2		; ensure stack aligned
stack_end:

datastack_size	equ $-section.DATASTACK.vstart
	endarea datastack, 1


	usesection INIT
initstart:

%include "init.asm"

	usesection INIT
		align 16
init_size	equ $-section.INIT.vstart
	endarea init, 1


	numdef SHOWINITSIZE, 0
%if _SHOWINITSIZE
%assign INITSIZE init_size
%warning init holds INITSIZE bytes
%endif


	usesection lDEBUG_CODE
	align 16
ldebug_code_size	equ $-section.lDEBUG_CODE.vstart
	endarea ldebug_code, 1


auxbuff_size: equ (_AUXBUFFSIZE+15) & ~15
	endarea auxbuff, 1

transimagepsp_size:	equ 100h+ldebug_data_entry_size \
			+asmtable1_size+asmtable2_size \
			+datastack_size+ldebug_code_size+init_size
	endarea transimagepsp, 1		; size of PSP and image during installation

pspsegment_size:	equ 100h+ldebug_data_entry_size \
			+asmtable1_size+asmtable2_size \
			+datastack_size
	endarea pspsegment, 1			; size of PSP and image when installed

resimagepsp_size:	equ pspsegment_size+ldebug_code_size+auxbuff_size
	endarea resimagepsp, 1			; size of PSP and image when installed


%if transimagepsp_size > resimagepsp_size
 %fatal "resimagepsp_size assumed larger"
%endif

 %assign __CHECK_RESIDENTSIZE pspsegment_size
 %if __CHECK_RESIDENTSIZE > (64 * 1024)
  %error resident size of PSP segment too large (%[__CHECK_RESIDENTSIZE])
 %endif

%if CODE_INSURE_COUNT
 %warning code_insure_low_byte_not_0CCh needed CODE_INSURE_COUNT times
%endif
